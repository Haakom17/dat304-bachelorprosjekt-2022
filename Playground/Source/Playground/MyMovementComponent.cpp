// Fill out your copyright notice in the Description page of Project Settings.


#include "MyMovementComponent.h"

void UMyMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	FVector DesiredMovementThisFrame = FVector(0.1f, 0.0f, 0.0f);

	FHitResult Hit;
	SafeMoveUpdatedComponent(DesiredMovementThisFrame, UpdatedComponent->GetComponentRotation(), true, Hit);

}
