// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYGROUND_CollidingPawn_generated_h
#error "CollidingPawn.generated.h already included, missing '#pragma once' in CollidingPawn.h"
#endif
#define PLAYGROUND_CollidingPawn_generated_h

#define Playground_Source_Playground_CollidingPawn_h_12_SPARSE_DATA
#define Playground_Source_Playground_CollidingPawn_h_12_RPC_WRAPPERS
#define Playground_Source_Playground_CollidingPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Playground_Source_Playground_CollidingPawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACollidingPawn(); \
	friend struct Z_Construct_UClass_ACollidingPawn_Statics; \
public: \
	DECLARE_CLASS(ACollidingPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(ACollidingPawn)


#define Playground_Source_Playground_CollidingPawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesACollidingPawn(); \
	friend struct Z_Construct_UClass_ACollidingPawn_Statics; \
public: \
	DECLARE_CLASS(ACollidingPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(ACollidingPawn)


#define Playground_Source_Playground_CollidingPawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACollidingPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACollidingPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollidingPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollidingPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollidingPawn(ACollidingPawn&&); \
	NO_API ACollidingPawn(const ACollidingPawn&); \
public:


#define Playground_Source_Playground_CollidingPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACollidingPawn(ACollidingPawn&&); \
	NO_API ACollidingPawn(const ACollidingPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACollidingPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACollidingPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACollidingPawn)


#define Playground_Source_Playground_CollidingPawn_h_12_PRIVATE_PROPERTY_OFFSET
#define Playground_Source_Playground_CollidingPawn_h_9_PROLOG
#define Playground_Source_Playground_CollidingPawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_CollidingPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_CollidingPawn_h_12_SPARSE_DATA \
	Playground_Source_Playground_CollidingPawn_h_12_RPC_WRAPPERS \
	Playground_Source_Playground_CollidingPawn_h_12_INCLASS \
	Playground_Source_Playground_CollidingPawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Playground_Source_Playground_CollidingPawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_CollidingPawn_h_12_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_CollidingPawn_h_12_SPARSE_DATA \
	Playground_Source_Playground_CollidingPawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Playground_Source_Playground_CollidingPawn_h_12_INCLASS_NO_PURE_DECLS \
	Playground_Source_Playground_CollidingPawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYGROUND_API UClass* StaticClass<class ACollidingPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Playground_Source_Playground_CollidingPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
