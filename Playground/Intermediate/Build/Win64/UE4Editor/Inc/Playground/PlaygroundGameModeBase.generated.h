// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYGROUND_PlaygroundGameModeBase_generated_h
#error "PlaygroundGameModeBase.generated.h already included, missing '#pragma once' in PlaygroundGameModeBase.h"
#endif
#define PLAYGROUND_PlaygroundGameModeBase_generated_h

#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_SPARSE_DATA
#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_RPC_WRAPPERS
#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPlaygroundGameModeBase(); \
	friend struct Z_Construct_UClass_APlaygroundGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APlaygroundGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(APlaygroundGameModeBase)


#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPlaygroundGameModeBase(); \
	friend struct Z_Construct_UClass_APlaygroundGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APlaygroundGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(APlaygroundGameModeBase)


#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlaygroundGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlaygroundGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaygroundGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaygroundGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaygroundGameModeBase(APlaygroundGameModeBase&&); \
	NO_API APlaygroundGameModeBase(const APlaygroundGameModeBase&); \
public:


#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APlaygroundGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APlaygroundGameModeBase(APlaygroundGameModeBase&&); \
	NO_API APlaygroundGameModeBase(const APlaygroundGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APlaygroundGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APlaygroundGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APlaygroundGameModeBase)


#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Playground_Source_Playground_PlaygroundGameModeBase_h_12_PROLOG
#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_SPARSE_DATA \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_RPC_WRAPPERS \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_INCLASS \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Playground_Source_Playground_PlaygroundGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_SPARSE_DATA \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Playground_Source_Playground_PlaygroundGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYGROUND_API UClass* StaticClass<class APlaygroundGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Playground_Source_Playground_PlaygroundGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
