// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYGROUND_MyPawnMovementComponent_generated_h
#error "MyPawnMovementComponent.generated.h already included, missing '#pragma once' in MyPawnMovementComponent.h"
#endif
#define PLAYGROUND_MyPawnMovementComponent_generated_h

#define Playground_Source_Playground_MyPawnMovementComponent_h_15_SPARSE_DATA
#define Playground_Source_Playground_MyPawnMovementComponent_h_15_RPC_WRAPPERS
#define Playground_Source_Playground_MyPawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Playground_Source_Playground_MyPawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyPawnMovementComponent(); \
	friend struct Z_Construct_UClass_UMyPawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyPawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(UMyPawnMovementComponent)


#define Playground_Source_Playground_MyPawnMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyPawnMovementComponent(); \
	friend struct Z_Construct_UClass_UMyPawnMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyPawnMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(UMyPawnMovementComponent)


#define Playground_Source_Playground_MyPawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyPawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyPawnMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyPawnMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyPawnMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyPawnMovementComponent(UMyPawnMovementComponent&&); \
	NO_API UMyPawnMovementComponent(const UMyPawnMovementComponent&); \
public:


#define Playground_Source_Playground_MyPawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyPawnMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyPawnMovementComponent(UMyPawnMovementComponent&&); \
	NO_API UMyPawnMovementComponent(const UMyPawnMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyPawnMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyPawnMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyPawnMovementComponent)


#define Playground_Source_Playground_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Playground_Source_Playground_MyPawnMovementComponent_h_12_PROLOG
#define Playground_Source_Playground_MyPawnMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_SPARSE_DATA \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_RPC_WRAPPERS \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_INCLASS \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Playground_Source_Playground_MyPawnMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_SPARSE_DATA \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	Playground_Source_Playground_MyPawnMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYGROUND_API UClass* StaticClass<class UMyPawnMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Playground_Source_Playground_MyPawnMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
