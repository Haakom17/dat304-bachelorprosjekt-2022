// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PLAYGROUND_MyMovementComponent_generated_h
#error "MyMovementComponent.generated.h already included, missing '#pragma once' in MyMovementComponent.h"
#endif
#define PLAYGROUND_MyMovementComponent_generated_h

#define Playground_Source_Playground_MyMovementComponent_h_15_SPARSE_DATA
#define Playground_Source_Playground_MyMovementComponent_h_15_RPC_WRAPPERS
#define Playground_Source_Playground_MyMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Playground_Source_Playground_MyMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMyMovementComponent(); \
	friend struct Z_Construct_UClass_UMyMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(UMyMovementComponent)


#define Playground_Source_Playground_MyMovementComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMyMovementComponent(); \
	friend struct Z_Construct_UClass_UMyMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UMyMovementComponent, UMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Playground"), NO_API) \
	DECLARE_SERIALIZER(UMyMovementComponent)


#define Playground_Source_Playground_MyMovementComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyMovementComponent(UMyMovementComponent&&); \
	NO_API UMyMovementComponent(const UMyMovementComponent&); \
public:


#define Playground_Source_Playground_MyMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMyMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMyMovementComponent(UMyMovementComponent&&); \
	NO_API UMyMovementComponent(const UMyMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMyMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMyMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMyMovementComponent)


#define Playground_Source_Playground_MyMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Playground_Source_Playground_MyMovementComponent_h_12_PROLOG
#define Playground_Source_Playground_MyMovementComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_MyMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_MyMovementComponent_h_15_SPARSE_DATA \
	Playground_Source_Playground_MyMovementComponent_h_15_RPC_WRAPPERS \
	Playground_Source_Playground_MyMovementComponent_h_15_INCLASS \
	Playground_Source_Playground_MyMovementComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Playground_Source_Playground_MyMovementComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Playground_Source_Playground_MyMovementComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Playground_Source_Playground_MyMovementComponent_h_15_SPARSE_DATA \
	Playground_Source_Playground_MyMovementComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Playground_Source_Playground_MyMovementComponent_h_15_INCLASS_NO_PURE_DECLS \
	Playground_Source_Playground_MyMovementComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PLAYGROUND_API UClass* StaticClass<class UMyMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Playground_Source_Playground_MyMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
