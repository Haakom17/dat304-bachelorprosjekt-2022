// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVUtilities/Public/Movement/NVMovementComponentBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVMovementComponentBase() {}
// Cross Module References
	NVUTILITIES_API UClass* Z_Construct_UClass_UNVMovementComponentBase_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_UNVMovementComponentBase();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_NVUtilities();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
// End Cross Module References
	void UNVMovementComponentBase::StaticRegisterNativesUNVMovementComponentBase()
	{
	}
	UClass* Z_Construct_UClass_UNVMovementComponentBase_NoRegister()
	{
		return UNVMovementComponentBase::StaticClass();
	}
	struct Z_Construct_UClass_UNVMovementComponentBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MoveDirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MoveDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFaceMovingDirection_MetaData[];
#endif
		static void NewProp_bFaceMovingDirection_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFaceMovingDirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpeedAcceleration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpeedAcceleration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpeedRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpeedRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVMovementComponentBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "IncludePath", "Movement/NVMovementComponentBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_TargetSpeed_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_TargetSpeed = { "TargetSpeed", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, TargetSpeed), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_TargetSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_TargetSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_CurrentSpeed_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_CurrentSpeed = { "CurrentSpeed", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, CurrentSpeed), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_CurrentSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_CurrentSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_MoveDirection_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_MoveDirection = { "MoveDirection", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, MoveDirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_MoveDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_MoveDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_RotationSpeed_MetaData[] = {
		{ "Category", "NVMovementComponentBase" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
		{ "ToolTip", "How fast (degree per seconds) the actor rotate when it need to change direction" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_RotationSpeed = { "RotationSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, RotationSpeed), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_RotationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_RotationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection_MetaData[] = {
		{ "Category", "NVMovementComponentBase" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
		{ "ToolTip", "If true, rotate the actor to face where it's moving" },
	};
#endif
	void Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection_SetBit(void* Obj)
	{
		((UNVMovementComponentBase*)Obj)->bFaceMovingDirection = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection = { "bFaceMovingDirection", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVMovementComponentBase), &Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedAcceleration_MetaData[] = {
		{ "Category", "NVMovementComponentBase" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
		{ "ToolTip", "How fast can the movement speed change (world units per seconds)\nIf SpeedAcceleration < 0 then this movement doesn't use acceleration and instantly move with the maximum speed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedAcceleration = { "SpeedAcceleration", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, SpeedAcceleration), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedAcceleration_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedAcceleration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedRange_MetaData[] = {
		{ "Category", "NVMovementComponentBase" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponentBase.h" },
		{ "ToolTip", "Editor properties\nThe min and max movement speed (world units per seconds).\nNOTE: If the movement doesn't have acceleration then just use the max speed" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedRange = { "SpeedRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponentBase, SpeedRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVMovementComponentBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_TargetSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_CurrentSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_MoveDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_RotationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_bFaceMovingDirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedAcceleration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponentBase_Statics::NewProp_SpeedRange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVMovementComponentBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVMovementComponentBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVMovementComponentBase_Statics::ClassParams = {
		&UNVMovementComponentBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVMovementComponentBase_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponentBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponentBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVMovementComponentBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVMovementComponentBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVMovementComponentBase, 4055488496);
	template<> NVUTILITIES_API UClass* StaticClass<UNVMovementComponentBase>()
	{
		return UNVMovementComponentBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVMovementComponentBase(Z_Construct_UClass_UNVMovementComponentBase, &UNVMovementComponentBase::StaticClass, TEXT("/Script/NVUtilities"), TEXT("UNVMovementComponentBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVMovementComponentBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
