// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVUtilities/Public/Movement/NVMovementComponent_Waypoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVMovementComponent_Waypoint() {}
// Cross Module References
	NVUTILITIES_API UClass* Z_Construct_UClass_UNVMovementComponent_Waypoint_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_UNVMovementComponent_Waypoint();
	NVUTILITIES_API UClass* Z_Construct_UClass_UNVMovementComponentBase();
	UPackage* Z_Construct_UPackage__Script_NVUtilities();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVWaypoint_NoRegister();
	NVUTILITIES_API UEnum* Z_Construct_UEnum_NVUtilities_ENVPathNavigationType();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVNavigationPath_NoRegister();
// End Cross Module References
	void UNVMovementComponent_Waypoint::StaticRegisterNativesUNVMovementComponent_Waypoint()
	{
	}
	UClass* Z_Construct_UClass_UNVMovementComponent_Waypoint_NoRegister()
	{
		return UNVMovementComponent_Waypoint::StaticClass();
	}
	struct Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaitingCountdown_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaitingCountdown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWaypointIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentWaypointIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentDestinationWaypoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentDestinationWaypoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldTeleport_MetaData[];
#endif
		static void NewProp_bShouldTeleport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldTeleport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaypointStopDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaypointStopDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CloseEnoughDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CloseEnoughDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NavigationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NavigationType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NavigationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathToFollow_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PathToFollow;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVMovementComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "IncludePath", "Movement/NVMovementComponent_Waypoint.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaitingCountdown_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaitingCountdown = { "WaitingCountdown", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, WaitingCountdown), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaitingCountdown_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaitingCountdown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentWaypointIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentWaypointIndex = { "CurrentWaypointIndex", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, CurrentWaypointIndex), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentWaypointIndex_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentWaypointIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentDestinationWaypoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentDestinationWaypoint = { "CurrentDestinationWaypoint", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, CurrentDestinationWaypoint), Z_Construct_UClass_ANVWaypoint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentDestinationWaypoint_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentDestinationWaypoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport_MetaData[] = {
		{ "Category", "NVMovementComponent_Waypoint" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
		{ "ToolTip", "If true, instead of moving between waypoint, just teleport instantly between them" },
	};
#endif
	void Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport_SetBit(void* Obj)
	{
		((UNVMovementComponent_Waypoint*)Obj)->bShouldTeleport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport = { "bShouldTeleport", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVMovementComponent_Waypoint), &Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaypointStopDuration_MetaData[] = {
		{ "Category", "NVMovementComponent_Waypoint" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
		{ "ToolTip", "How long to stay at each waypoint" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaypointStopDuration = { "WaypointStopDuration", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, WaypointStopDuration), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaypointStopDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaypointStopDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CloseEnoughDistance_MetaData[] = {
		{ "Category", "NVMovementComponent_Waypoint" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
		{ "ToolTip", "When the actor get in this distance of a waypoint, it's considered as reach it" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CloseEnoughDistance = { "CloseEnoughDistance", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, CloseEnoughDistance), METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CloseEnoughDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CloseEnoughDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType_MetaData[] = {
		{ "Category", "NVMovementComponent_Waypoint" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType = { "NavigationType", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, NavigationType), Z_Construct_UEnum_NVUtilities_ENVPathNavigationType, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_PathToFollow_MetaData[] = {
		{ "Category", "NVMovementComponent_Waypoint" },
		{ "ModuleRelativePath", "Public/Movement/NVMovementComponent_Waypoint.h" },
		{ "ToolTip", "Editor properties\nReference to the path that the owner actor need to follow" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_PathToFollow = { "PathToFollow", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVMovementComponent_Waypoint, PathToFollow), Z_Construct_UClass_ANVNavigationPath_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_PathToFollow_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_PathToFollow_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaitingCountdown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentWaypointIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CurrentDestinationWaypoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_bShouldTeleport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_WaypointStopDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_CloseEnoughDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_NavigationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::NewProp_PathToFollow,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVMovementComponent_Waypoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::ClassParams = {
		&UNVMovementComponent_Waypoint::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVMovementComponent_Waypoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVMovementComponent_Waypoint, 3106187246);
	template<> NVUTILITIES_API UClass* StaticClass<UNVMovementComponent_Waypoint>()
	{
		return UNVMovementComponent_Waypoint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVMovementComponent_Waypoint(Z_Construct_UClass_UNVMovementComponent_Waypoint, &UNVMovementComponent_Waypoint::StaticClass, TEXT("/Script/NVUtilities"), TEXT("UNVMovementComponent_Waypoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVMovementComponent_Waypoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
