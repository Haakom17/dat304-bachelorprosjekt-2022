// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVUTILITIES_NVNavigationPath_generated_h
#error "NVNavigationPath.generated.h already included, missing '#pragma once' in NVNavigationPath.h"
#endif
#define NVUTILITIES_NVNavigationPath_generated_h

#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_RPC_WRAPPERS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVNavigationPath(); \
	friend struct Z_Construct_UClass_ANVNavigationPath_Statics; \
public: \
	DECLARE_CLASS(ANVNavigationPath, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVNavigationPath)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_INCLASS \
private: \
	static void StaticRegisterNativesANVNavigationPath(); \
	friend struct Z_Construct_UClass_ANVNavigationPath_Statics; \
public: \
	DECLARE_CLASS(ANVNavigationPath, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVNavigationPath)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVNavigationPath(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVNavigationPath) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVNavigationPath); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVNavigationPath); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVNavigationPath(ANVNavigationPath&&); \
	NO_API ANVNavigationPath(const ANVNavigationPath&); \
public:


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVNavigationPath(ANVNavigationPath&&); \
	NO_API ANVNavigationPath(const ANVNavigationPath&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVNavigationPath); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVNavigationPath); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVNavigationPath)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_28_PROLOG
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_RPC_WRAPPERS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_INCLASS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class ANVNavigationPath>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVUtilities_Source_NVUtilities_Public_NVNavigationPath_h


#define FOREACH_ENUM_ENVPATHNAVIGATIONTYPE(op) \
	op(ENVPathNavigationType::Sequential) \
	op(ENVPathNavigationType::Random) \
	op(ENVPathNavigationType::NVPathNavigationType_MAX) 

enum class ENVPathNavigationType : uint8;
template<> NVUTILITIES_API UEnum* StaticEnum<ENVPathNavigationType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
