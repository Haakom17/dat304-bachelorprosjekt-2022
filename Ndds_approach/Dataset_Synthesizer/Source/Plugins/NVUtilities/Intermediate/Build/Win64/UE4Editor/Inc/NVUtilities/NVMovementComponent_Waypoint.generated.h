// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVUTILITIES_NVMovementComponent_Waypoint_generated_h
#error "NVMovementComponent_Waypoint.generated.h already included, missing '#pragma once' in NVMovementComponent_Waypoint.h"
#endif
#define NVUTILITIES_NVMovementComponent_Waypoint_generated_h

#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_RPC_WRAPPERS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVMovementComponent_Waypoint(); \
	friend struct Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics; \
public: \
	DECLARE_CLASS(UNVMovementComponent_Waypoint, UNVMovementComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(UNVMovementComponent_Waypoint)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUNVMovementComponent_Waypoint(); \
	friend struct Z_Construct_UClass_UNVMovementComponent_Waypoint_Statics; \
public: \
	DECLARE_CLASS(UNVMovementComponent_Waypoint, UNVMovementComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(UNVMovementComponent_Waypoint)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVMovementComponent_Waypoint(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVMovementComponent_Waypoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVMovementComponent_Waypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVMovementComponent_Waypoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVMovementComponent_Waypoint(UNVMovementComponent_Waypoint&&); \
	NO_API UNVMovementComponent_Waypoint(const UNVMovementComponent_Waypoint&); \
public:


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVMovementComponent_Waypoint(UNVMovementComponent_Waypoint&&); \
	NO_API UNVMovementComponent_Waypoint(const UNVMovementComponent_Waypoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVMovementComponent_Waypoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVMovementComponent_Waypoint); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVMovementComponent_Waypoint)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PathToFollow() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, PathToFollow); } \
	FORCEINLINE static uint32 __PPO__NavigationType() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, NavigationType); } \
	FORCEINLINE static uint32 __PPO__CloseEnoughDistance() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, CloseEnoughDistance); } \
	FORCEINLINE static uint32 __PPO__WaypointStopDuration() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, WaypointStopDuration); } \
	FORCEINLINE static uint32 __PPO__bShouldTeleport() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, bShouldTeleport); } \
	FORCEINLINE static uint32 __PPO__CurrentDestinationWaypoint() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, CurrentDestinationWaypoint); } \
	FORCEINLINE static uint32 __PPO__CurrentWaypointIndex() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, CurrentWaypointIndex); } \
	FORCEINLINE static uint32 __PPO__WaitingCountdown() { return STRUCT_OFFSET(UNVMovementComponent_Waypoint, WaitingCountdown); }


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_18_PROLOG
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_RPC_WRAPPERS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_INCLASS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class UNVMovementComponent_Waypoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponent_Waypoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
