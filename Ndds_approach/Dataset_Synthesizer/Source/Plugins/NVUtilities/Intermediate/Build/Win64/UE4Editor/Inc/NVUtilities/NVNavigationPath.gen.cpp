// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVUtilities/Public/NVNavigationPath.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVNavigationPath() {}
// Cross Module References
	NVUTILITIES_API UEnum* Z_Construct_UEnum_NVUtilities_ENVPathNavigationType();
	UPackage* Z_Construct_UPackage__Script_NVUtilities();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVNavigationPath_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVNavigationPath();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVWaypoint_NoRegister();
// End Cross Module References
	static UEnum* ENVPathNavigationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVUtilities_ENVPathNavigationType, Z_Construct_UPackage__Script_NVUtilities(), TEXT("ENVPathNavigationType"));
		}
		return Singleton;
	}
	template<> NVUTILITIES_API UEnum* StaticEnum<ENVPathNavigationType>()
	{
		return ENVPathNavigationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVPathNavigationType(ENVPathNavigationType_StaticEnum, TEXT("/Script/NVUtilities"), TEXT("ENVPathNavigationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVUtilities_ENVPathNavigationType_Hash() { return 2660082304U; }
	UEnum* Z_Construct_UEnum_NVUtilities_ENVPathNavigationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVUtilities();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVPathNavigationType"), 0, Get_Z_Construct_UEnum_NVUtilities_ENVPathNavigationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVPathNavigationType::Sequential", (int64)ENVPathNavigationType::Sequential },
				{ "ENVPathNavigationType::Random", (int64)ENVPathNavigationType::Random },
				{ "ENVPathNavigationType::NVPathNavigationType_MAX", (int64)ENVPathNavigationType::NVPathNavigationType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/NVNavigationPath.h" },
				{ "NVPathNavigationType_MAX.Hidden", "" },
				{ "Random.ToolTip", "Randomly pick the next waypoint after the current one" },
				{ "Sequential.ToolTip", "Follow point to point in the WaypointList" },
				{ "ToolTip", "The enum represent how to navigate a path" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVUtilities,
				nullptr,
				"ENVPathNavigationType",
				"ENVPathNavigationType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void ANVNavigationPath::StaticRegisterNativesANVNavigationPath()
	{
	}
	UClass* Z_Construct_UClass_ANVNavigationPath_NoRegister()
	{
		return ANVNavigationPath::StaticClass();
	}
	struct Z_Construct_UClass_ANVNavigationPath_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bClosedPath_MetaData[];
#endif
		static void NewProp_bClosedPath_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bClosedPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaypointList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WaypointList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaypointList_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVNavigationPath_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVNavigationPath_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "NVNavigationPath.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVNavigationPath.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath_MetaData[] = {
		{ "Category", "Zone" },
		{ "ModuleRelativePath", "Public/NVNavigationPath.h" },
		{ "ToolTip", "If true, the last waypoint in the list will be connected to the first one\nOtherwise the movement stop at the last waypoint" },
	};
#endif
	void Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath_SetBit(void* Obj)
	{
		((ANVNavigationPath*)Obj)->bClosedPath = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath = { "bClosedPath", nullptr, (EPropertyFlags)0x0010000000000815, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVNavigationPath), &Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList_MetaData[] = {
		{ "Category", "Zone" },
		{ "ModuleRelativePath", "Public/NVNavigationPath.h" },
		{ "ToolTip", "Editor properties\nList of waypoints in the path" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList = { "WaypointList", nullptr, (EPropertyFlags)0x0010000000000815, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVNavigationPath, WaypointList), METADATA_PARAMS(Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList_Inner = { "WaypointList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ANVWaypoint_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVNavigationPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_bClosedPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVNavigationPath_Statics::NewProp_WaypointList_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVNavigationPath_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVNavigationPath>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVNavigationPath_Statics::ClassParams = {
		&ANVNavigationPath::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVNavigationPath_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ANVNavigationPath_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ANVNavigationPath_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVNavigationPath_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVNavigationPath()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVNavigationPath_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVNavigationPath, 1706896543);
	template<> NVUTILITIES_API UClass* StaticClass<ANVNavigationPath>()
	{
		return ANVNavigationPath::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVNavigationPath(Z_Construct_UClass_ANVNavigationPath, &ANVNavigationPath::StaticClass, TEXT("/Script/NVUtilities"), TEXT("ANVNavigationPath"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVNavigationPath);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
