// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVUTILITIES_NVMovementComponentBase_generated_h
#error "NVMovementComponentBase.generated.h already included, missing '#pragma once' in NVMovementComponentBase.h"
#endif
#define NVUTILITIES_NVMovementComponentBase_generated_h

#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_RPC_WRAPPERS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVMovementComponentBase(); \
	friend struct Z_Construct_UClass_UNVMovementComponentBase_Statics; \
public: \
	DECLARE_CLASS(UNVMovementComponentBase, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(UNVMovementComponentBase)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUNVMovementComponentBase(); \
	friend struct Z_Construct_UClass_UNVMovementComponentBase_Statics; \
public: \
	DECLARE_CLASS(UNVMovementComponentBase, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(UNVMovementComponentBase)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVMovementComponentBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVMovementComponentBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVMovementComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVMovementComponentBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVMovementComponentBase(UNVMovementComponentBase&&); \
	NO_API UNVMovementComponentBase(const UNVMovementComponentBase&); \
public:


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVMovementComponentBase(UNVMovementComponentBase&&); \
	NO_API UNVMovementComponentBase(const UNVMovementComponentBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVMovementComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVMovementComponentBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVMovementComponentBase)


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SpeedRange() { return STRUCT_OFFSET(UNVMovementComponentBase, SpeedRange); } \
	FORCEINLINE static uint32 __PPO__SpeedAcceleration() { return STRUCT_OFFSET(UNVMovementComponentBase, SpeedAcceleration); } \
	FORCEINLINE static uint32 __PPO__bFaceMovingDirection() { return STRUCT_OFFSET(UNVMovementComponentBase, bFaceMovingDirection); } \
	FORCEINLINE static uint32 __PPO__RotationSpeed() { return STRUCT_OFFSET(UNVMovementComponentBase, RotationSpeed); } \
	FORCEINLINE static uint32 __PPO__MoveDirection() { return STRUCT_OFFSET(UNVMovementComponentBase, MoveDirection); } \
	FORCEINLINE static uint32 __PPO__CurrentSpeed() { return STRUCT_OFFSET(UNVMovementComponentBase, CurrentSpeed); } \
	FORCEINLINE static uint32 __PPO__TargetSpeed() { return STRUCT_OFFSET(UNVMovementComponentBase, TargetSpeed); }


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_15_PROLOG
#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_RPC_WRAPPERS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_INCLASS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class UNVMovementComponentBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVMovementComponentBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
