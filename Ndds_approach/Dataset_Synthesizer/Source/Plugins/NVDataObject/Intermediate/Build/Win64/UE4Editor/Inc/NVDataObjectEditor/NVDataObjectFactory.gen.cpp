// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVDataObjectEditor/Public/NVDataObjectFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVDataObjectFactory() {}
// Cross Module References
	NVDATAOBJECTEDITOR_API UClass* Z_Construct_UClass_UNVDataObjectFactory_NoRegister();
	NVDATAOBJECTEDITOR_API UClass* Z_Construct_UClass_UNVDataObjectFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_NVDataObjectEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObjectAsset_NoRegister();
// End Cross Module References
	void UNVDataObjectFactory::StaticRegisterNativesUNVDataObjectFactory()
	{
	}
	UClass* Z_Construct_UClass_UNVDataObjectFactory_NoRegister()
	{
		return UNVDataObjectFactory::StaticClass();
	}
	struct Z_Construct_UClass_UNVDataObjectFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NVDataObjectClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_NVDataObjectClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVDataObjectFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_NVDataObjectEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "NVDataObjectFactory.h" },
		{ "ModuleRelativePath", "Public/NVDataObjectFactory.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectFactory_Statics::NewProp_NVDataObjectClass_MetaData[] = {
		{ "Category", "DataAsset" },
		{ "ModuleRelativePath", "Public/NVDataObjectFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UNVDataObjectFactory_Statics::NewProp_NVDataObjectClass = { "NVDataObjectClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVDataObjectFactory, NVDataObjectClass), Z_Construct_UClass_UNVDataObjectAsset_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectFactory_Statics::NewProp_NVDataObjectClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectFactory_Statics::NewProp_NVDataObjectClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVDataObjectFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVDataObjectFactory_Statics::NewProp_NVDataObjectClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVDataObjectFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVDataObjectFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVDataObjectFactory_Statics::ClassParams = {
		&UNVDataObjectFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVDataObjectFactory_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectFactory_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVDataObjectFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVDataObjectFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVDataObjectFactory, 1634230784);
	template<> NVDATAOBJECTEDITOR_API UClass* StaticClass<UNVDataObjectFactory>()
	{
		return UNVDataObjectFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVDataObjectFactory(Z_Construct_UClass_UNVDataObjectFactory, &UNVDataObjectFactory::StaticClass, TEXT("/Script/NVDataObjectEditor"), TEXT("UNVDataObjectFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVDataObjectFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
