// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomRotationComponent_generated_h
#error "RandomRotationComponent.generated.h already included, missing '#pragma once' in RandomRotationComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomRotationComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomRotationComponent(); \
	friend struct Z_Construct_UClass_URandomRotationComponent_Statics; \
public: \
	DECLARE_CLASS(URandomRotationComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomRotationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURandomRotationComponent(); \
	friend struct Z_Construct_UClass_URandomRotationComponent_Statics; \
public: \
	DECLARE_CLASS(URandomRotationComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomRotationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomRotationComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomRotationComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomRotationComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomRotationComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomRotationComponent(URandomRotationComponent&&); \
	NO_API URandomRotationComponent(const URandomRotationComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomRotationComponent(URandomRotationComponent&&); \
	NO_API URandomRotationComponent(const URandomRotationComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomRotationComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomRotationComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomRotationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RandomRotationData() { return STRUCT_OFFSET(URandomRotationComponent, RandomRotationData); } \
	FORCEINLINE static uint32 __PPO__bRelatedToOriginRotation() { return STRUCT_OFFSET(URandomRotationComponent, bRelatedToOriginRotation); } \
	FORCEINLINE static uint32 __PPO__OriginalRotation() { return STRUCT_OFFSET(URandomRotationComponent, OriginalRotation); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_14_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomRotationComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomRotationComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
