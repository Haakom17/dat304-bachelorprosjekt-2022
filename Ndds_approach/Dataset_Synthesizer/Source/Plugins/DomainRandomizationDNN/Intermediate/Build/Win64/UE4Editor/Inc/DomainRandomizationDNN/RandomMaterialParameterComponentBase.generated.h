// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMaterialParameterComponentBase_generated_h
#error "RandomMaterialParameterComponentBase.generated.h already included, missing '#pragma once' in RandomMaterialParameterComponentBase.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMaterialParameterComponentBase_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMaterialParameterComponentBase(); \
	friend struct Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParameterComponentBase, URandomComponentBase, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParameterComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_INCLASS \
private: \
	static void StaticRegisterNativesURandomMaterialParameterComponentBase(); \
	friend struct Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParameterComponentBase, URandomComponentBase, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParameterComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMaterialParameterComponentBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMaterialParameterComponentBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParameterComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParameterComponentBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParameterComponentBase(URandomMaterialParameterComponentBase&&); \
	NO_API URandomMaterialParameterComponentBase(const URandomMaterialParameterComponentBase&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParameterComponentBase(URandomMaterialParameterComponentBase&&); \
	NO_API URandomMaterialParameterComponentBase(const URandomMaterialParameterComponentBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParameterComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParameterComponentBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMaterialParameterComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaterialParameterNames() { return STRUCT_OFFSET(URandomMaterialParameterComponentBase, MaterialParameterNames); } \
	FORCEINLINE static uint32 __PPO__MaterialSelectionConfigData() { return STRUCT_OFFSET(URandomMaterialParameterComponentBase, MaterialSelectionConfigData); } \
	FORCEINLINE static uint32 __PPO__AffectedComponentType() { return STRUCT_OFFSET(URandomMaterialParameterComponentBase, AffectedComponentType); } \
	FORCEINLINE static uint32 __PPO__OwnerMeshComponents() { return STRUCT_OFFSET(URandomMaterialParameterComponentBase, OwnerMeshComponents); } \
	FORCEINLINE static uint32 __PPO__OwnerDecalComponents() { return STRUCT_OFFSET(URandomMaterialParameterComponentBase, OwnerDecalComponents); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_18_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMaterialParameterComponentBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParameterComponentBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
