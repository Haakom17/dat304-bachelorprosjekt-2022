// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/RandomDataObject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomDataObject() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomDataObject_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomDataObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMovementDataObject_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMovementDataObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomLocationData();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomRotationDataObject_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomRotationDataObject();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomRotationData();
// End Cross Module References
	void URandomDataObject::StaticRegisterNativesURandomDataObject()
	{
	}
	UClass* Z_Construct_UClass_URandomDataObject_NoRegister()
	{
		return URandomDataObject::StaticClass();
	}
	struct Z_Construct_UClass_URandomDataObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldRandomize_MetaData[];
#endif
		static void NewProp_bShouldRandomize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldRandomize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomDataObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomDataObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "RandomDataObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "====================== URandomDataObject ======================" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
	void Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize_SetBit(void* Obj)
	{
		((URandomDataObject*)Obj)->bShouldRandomize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize = { "bShouldRandomize", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomDataObject), &Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomDataObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomDataObject_Statics::NewProp_bShouldRandomize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomDataObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomDataObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomDataObject_Statics::ClassParams = {
		&URandomDataObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomDataObject_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomDataObject_Statics::PropPointers),
		0,
		0x003010A1u,
		METADATA_PARAMS(Z_Construct_UClass_URandomDataObject_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomDataObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomDataObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomDataObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomDataObject, 2997761031);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomDataObject>()
	{
		return URandomDataObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomDataObject(Z_Construct_UClass_URandomDataObject, &URandomDataObject::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomDataObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomDataObject);
	void URandomMovementDataObject::StaticRegisterNativesURandomMovementDataObject()
	{
	}
	UClass* Z_Construct_UClass_URandomMovementDataObject_NoRegister()
	{
		return URandomMovementDataObject::StaticClass();
	}
	struct Z_Construct_UClass_URandomMovementDataObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomSpeedRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomSpeedRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldTeleport_MetaData[];
#endif
		static void NewProp_bShouldTeleport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldTeleport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomLocationVolume;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData[];
#endif
		static void NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseObjectAxesInsteadOfWorldAxes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomLocationData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRelatedToOriginLocation_MetaData[];
#endif
		static void NewProp_bRelatedToOriginLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRelatedToOriginLocation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMovementDataObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomDataObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "RandomDataObject.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomSpeedRange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "!bShouldTeleport" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomSpeedRange = { "RandomSpeedRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementDataObject, RandomSpeedRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomSpeedRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomSpeedRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "If true, the actor will instantly teleport whenever location changed\notherwise the object will move toward the new location with speed in the RandomSpeedRange" },
	};
#endif
	void Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport_SetBit(void* Obj)
	{
		((URandomMovementDataObject*)Obj)->bShouldTeleport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport = { "bShouldTeleport", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementDataObject), &Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationVolume_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "!bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "The volume to choose a random location from" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationVolume = { "RandomLocationVolume", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementDataObject, RandomLocationVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationVolume_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationVolume_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "If true, the location will be picked along the object's axes instead of the world axes" },
	};
#endif
	void Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit(void* Obj)
	{
		((URandomMovementDataObject*)Obj)->bUseObjectAxesInsteadOfWorldAxes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes = { "bUseObjectAxesInsteadOfWorldAxes", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementDataObject), &Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationData = { "RandomLocationData", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementDataObject, RandomLocationData), Z_Construct_UScriptStruct_FRandomLocationData, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationData_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "If true, the owner will be moving around its original location\nOtherwise the random location will be selected in the RandomLocationVolume" },
	};
#endif
	void Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation_SetBit(void* Obj)
	{
		((URandomMovementDataObject*)Obj)->bRelatedToOriginLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation = { "bRelatedToOriginLocation", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementDataObject), &Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMovementDataObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomSpeedRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bShouldTeleport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationVolume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_RandomLocationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementDataObject_Statics::NewProp_bRelatedToOriginLocation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMovementDataObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMovementDataObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMovementDataObject_Statics::ClassParams = {
		&URandomMovementDataObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMovementDataObject_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMovementDataObject_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomMovementDataObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMovementDataObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMovementDataObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMovementDataObject, 141582642);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMovementDataObject>()
	{
		return URandomMovementDataObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMovementDataObject(Z_Construct_UClass_URandomMovementDataObject, &URandomMovementDataObject::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMovementDataObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMovementDataObject);
	void URandomRotationDataObject::StaticRegisterNativesURandomRotationDataObject()
	{
	}
	UClass* Z_Construct_UClass_URandomRotationDataObject_NoRegister()
	{
		return URandomRotationDataObject::StaticClass();
	}
	struct Z_Construct_UClass_URandomRotationDataObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRelatedToOriginRotation_MetaData[];
#endif
		static void NewProp_bRelatedToOriginRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRelatedToOriginRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomRotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomRotationData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomRotationDataObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomDataObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationDataObject_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "RandomDataObject.h" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
		{ "ToolTip", "If true, the owner will be rotated around its original rotation\nOtherwise the random rotation will be around the Zero rotation" },
	};
#endif
	void Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation_SetBit(void* Obj)
	{
		((URandomRotationDataObject*)Obj)->bRelatedToOriginRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation = { "bRelatedToOriginRotation", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomRotationDataObject), &Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_RandomRotationData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomDataObject.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_RandomRotationData = { "RandomRotationData", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomRotationDataObject, RandomRotationData), Z_Construct_UScriptStruct_FRandomRotationData, METADATA_PARAMS(Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_RandomRotationData_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_RandomRotationData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomRotationDataObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_bRelatedToOriginRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomRotationDataObject_Statics::NewProp_RandomRotationData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomRotationDataObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomRotationDataObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomRotationDataObject_Statics::ClassParams = {
		&URandomRotationDataObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomRotationDataObject_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomRotationDataObject_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URandomRotationDataObject_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomRotationDataObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomRotationDataObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomRotationDataObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomRotationDataObject, 331157035);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomRotationDataObject>()
	{
		return URandomRotationDataObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomRotationDataObject(Z_Construct_UClass_URandomRotationDataObject, &URandomRotationDataObject::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomRotationDataObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomRotationDataObject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
