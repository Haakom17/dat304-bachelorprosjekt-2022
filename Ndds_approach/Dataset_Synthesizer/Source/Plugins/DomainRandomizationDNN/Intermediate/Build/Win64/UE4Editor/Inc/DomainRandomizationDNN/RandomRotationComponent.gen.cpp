// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomRotationComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomRotationComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomRotationComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomRotationComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomRotationData();
// End Cross Module References
	void URandomRotationComponent::StaticRegisterNativesURandomRotationComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomRotationComponent_NoRegister()
	{
		return URandomRotationComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomRotationComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRelatedToOriginRotation_MetaData[];
#endif
		static void NewProp_bRelatedToOriginRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRelatedToOriginRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomRotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomRotationData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomRotationComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomRotationComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomRotationComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_OriginalRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomRotationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_OriginalRotation = { "OriginalRotation", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomRotationComponent, OriginalRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_OriginalRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_OriginalRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomRotationComponent.h" },
		{ "ToolTip", "If true, the owner will be rotated around its original rotation\nOtherwise the random rotation will be around the Zero rotation" },
	};
#endif
	void Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation_SetBit(void* Obj)
	{
		((URandomRotationComponent*)Obj)->bRelatedToOriginRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation = { "bRelatedToOriginRotation", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomRotationComponent), &Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_RandomRotationData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomRotationComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_RandomRotationData = { "RandomRotationData", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomRotationComponent, RandomRotationData), Z_Construct_UScriptStruct_FRandomRotationData, METADATA_PARAMS(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_RandomRotationData_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_RandomRotationData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomRotationComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_OriginalRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_bRelatedToOriginRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomRotationComponent_Statics::NewProp_RandomRotationData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomRotationComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomRotationComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomRotationComponent_Statics::ClassParams = {
		&URandomRotationComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomRotationComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomRotationComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomRotationComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomRotationComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomRotationComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomRotationComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomRotationComponent, 1931417360);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomRotationComponent>()
	{
		return URandomRotationComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomRotationComponent(Z_Construct_UClass_URandomRotationComponent, &URandomRotationComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomRotationComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomRotationComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
