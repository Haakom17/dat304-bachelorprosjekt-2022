// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_NVSceneMarker_FocalPoint_generated_h
#error "NVSceneMarker_FocalPoint.generated.h already included, missing '#pragma once' in NVSceneMarker_FocalPoint.h"
#endif
#define DOMAINRANDOMIZATIONDNN_NVSceneMarker_FocalPoint_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneMarker_FocalPoint(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_FocalPoint, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_FocalPoint) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_FocalPoint*>(this); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneMarker_FocalPoint(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_FocalPoint, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_FocalPoint) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_FocalPoint*>(this); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneMarker_FocalPoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_FocalPoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_FocalPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_FocalPoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_FocalPoint(ANVSceneMarker_FocalPoint&&); \
	NO_API ANVSceneMarker_FocalPoint(const ANVSceneMarker_FocalPoint&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_FocalPoint(ANVSceneMarker_FocalPoint&&); \
	NO_API ANVSceneMarker_FocalPoint(const ANVSceneMarker_FocalPoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_FocalPoint); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_FocalPoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_FocalPoint)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BillboardComponent() { return STRUCT_OFFSET(ANVSceneMarker_FocalPoint, BillboardComponent); } \
	FORCEINLINE static uint32 __PPO__SpringArm_ViewPointComponent() { return STRUCT_OFFSET(ANVSceneMarker_FocalPoint, SpringArm_ViewPointComponent); } \
	FORCEINLINE static uint32 __PPO__SceneMarker() { return STRUCT_OFFSET(ANVSceneMarker_FocalPoint, SceneMarker); } \
	FORCEINLINE static uint32 __PPO__bUseOrbitalMovement() { return STRUCT_OFFSET(ANVSceneMarker_FocalPoint, bUseOrbitalMovement); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_17_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ANVSceneMarker_FocalPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_FocalPoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
