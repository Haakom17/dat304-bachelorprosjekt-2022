// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomLookAtComponent_generated_h
#error "RandomLookAtComponent.generated.h already included, missing '#pragma once' in RandomLookAtComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomLookAtComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomLookAtComponent(); \
	friend struct Z_Construct_UClass_URandomLookAtComponent_Statics; \
public: \
	DECLARE_CLASS(URandomLookAtComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLookAtComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURandomLookAtComponent(); \
	friend struct Z_Construct_UClass_URandomLookAtComponent_Statics; \
public: \
	DECLARE_CLASS(URandomLookAtComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLookAtComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomLookAtComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomLookAtComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLookAtComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLookAtComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLookAtComponent(URandomLookAtComponent&&); \
	NO_API URandomLookAtComponent(const URandomLookAtComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLookAtComponent(URandomLookAtComponent&&); \
	NO_API URandomLookAtComponent(const URandomLookAtComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLookAtComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLookAtComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomLookAtComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__FocalTargetActors() { return STRUCT_OFFSET(URandomLookAtComponent, FocalTargetActors); } \
	FORCEINLINE static uint32 __PPO__RotationSpeed() { return STRUCT_OFFSET(URandomLookAtComponent, RotationSpeed); } \
	FORCEINLINE static uint32 __PPO__CurrentFocalTarget() { return STRUCT_OFFSET(URandomLookAtComponent, CurrentFocalTarget); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_14_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomLookAtComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLookAtComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
