// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/DRSceneManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDRSceneManager() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ADRSceneManager_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ADRSceneManager();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneManager();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_AGroupActorManager_NoRegister();
// End Cross Module References
	void ADRSceneManager::StaticRegisterNativesADRSceneManager()
	{
	}
	UClass* Z_Construct_UClass_ADRSceneManager_NoRegister()
	{
		return ADRSceneManager::StaticClass();
	}
	struct Z_Construct_UClass_ADRSceneManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsReady_MetaData[];
#endif
		static void NewProp_bIsReady_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsReady;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NoiseActorManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NoiseActorManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupActorManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GroupActorManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADRSceneManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ANVSceneManager,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADRSceneManager_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering Replication Tick Tags Input Actor Rendering" },
		{ "IncludePath", "DRSceneManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DRSceneManager.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "The new actor which get annotated and have its info captured and exported\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRSceneManager.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	void Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady_SetBit(void* Obj)
	{
		((ADRSceneManager*)Obj)->bIsReady = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady = { "bIsReady", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADRSceneManager), &Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADRSceneManager_Statics::NewProp_NoiseActorManager_MetaData[] = {
		{ "Category", "DRSceneManager" },
		{ "ModuleRelativePath", "Public/DRSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADRSceneManager_Statics::NewProp_NoiseActorManager = { "NoiseActorManager", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADRSceneManager, NoiseActorManager), Z_Construct_UClass_AGroupActorManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_NoiseActorManager_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_NoiseActorManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADRSceneManager_Statics::NewProp_GroupActorManager_MetaData[] = {
		{ "Category", "DRSceneManager" },
		{ "ModuleRelativePath", "Public/DRSceneManager.h" },
		{ "ToolTip", "Editor properties\nReference to the actor that manage the training actors" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADRSceneManager_Statics::NewProp_GroupActorManager = { "GroupActorManager", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADRSceneManager, GroupActorManager), Z_Construct_UClass_AGroupActorManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_GroupActorManager_MetaData, ARRAY_COUNT(Z_Construct_UClass_ADRSceneManager_Statics::NewProp_GroupActorManager_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADRSceneManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADRSceneManager_Statics::NewProp_bIsReady,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADRSceneManager_Statics::NewProp_NoiseActorManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADRSceneManager_Statics::NewProp_GroupActorManager,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADRSceneManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADRSceneManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADRSceneManager_Statics::ClassParams = {
		&ADRSceneManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADRSceneManager_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ADRSceneManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ADRSceneManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ADRSceneManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADRSceneManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADRSceneManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADRSceneManager, 1058328561);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ADRSceneManager>()
	{
		return ADRSceneManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADRSceneManager(Z_Construct_UClass_ADRSceneManager, &ADRSceneManager::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ADRSceneManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADRSceneManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
