// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_GroupActorManager_generated_h
#error "GroupActorManager.generated.h already included, missing '#pragma once' in GroupActorManager.h"
#endif
#define DOMAINRANDOMIZATIONDNN_GroupActorManager_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FNVActorTemplateConfig>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGroupActorManager(); \
	friend struct Z_Construct_UClass_AGroupActorManager_Statics; \
public: \
	DECLARE_CLASS(AGroupActorManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(AGroupActorManager)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_INCLASS \
private: \
	static void StaticRegisterNativesAGroupActorManager(); \
	friend struct Z_Construct_UClass_AGroupActorManager_Statics; \
public: \
	DECLARE_CLASS(AGroupActorManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(AGroupActorManager)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGroupActorManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGroupActorManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGroupActorManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGroupActorManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGroupActorManager(AGroupActorManager&&); \
	NO_API AGroupActorManager(const AGroupActorManager&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGroupActorManager(AGroupActorManager&&); \
	NO_API AGroupActorManager(const AGroupActorManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGroupActorManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGroupActorManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGroupActorManager)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ManagedActors() { return STRUCT_OFFSET(AGroupActorManager, ManagedActors); } \
	FORCEINLINE static uint32 __PPO__TemplateActors() { return STRUCT_OFFSET(AGroupActorManager, TemplateActors); } \
	FORCEINLINE static uint32 __PPO__CountdownUntilNextSpawn() { return STRUCT_OFFSET(AGroupActorManager, CountdownUntilNextSpawn); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_35_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class AGroupActorManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_GroupActorManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
