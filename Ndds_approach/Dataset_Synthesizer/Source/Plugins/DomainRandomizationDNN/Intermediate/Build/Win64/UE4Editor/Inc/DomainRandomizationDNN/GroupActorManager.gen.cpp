// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/GroupActorManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroupActorManager() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FNVActorTemplateConfig();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_AGroupActorManager_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_AGroupActorManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FInt32Interval();
// End Cross Module References
class UScriptStruct* FNVActorTemplateConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FNVActorTemplateConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVActorTemplateConfig, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("NVActorTemplateConfig"), sizeof(FNVActorTemplateConfig), Get_Z_Construct_UScriptStruct_FNVActorTemplateConfig_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FNVActorTemplateConfig>()
{
	return FNVActorTemplateConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVActorTemplateConfig(FNVActorTemplateConfig::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("NVActorTemplateConfig"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVActorTemplateConfig
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVActorTemplateConfig()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVActorTemplateConfig")),new UScriptStruct::TCppStructOps<FNVActorTemplateConfig>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVActorTemplateConfig;
	struct Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorOverrideMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorOverrideMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVActorTemplateConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorOverrideMesh_MetaData[] = {
		{ "Category", "NVActorTemplateConfig" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The mesh to be used by the new actor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorOverrideMesh = { "ActorOverrideMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVActorTemplateConfig, ActorOverrideMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorOverrideMesh_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorOverrideMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorClass_MetaData[] = {
		{ "Category", "NVActorTemplateConfig" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The class to be used to spawn the new actor" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVActorTemplateConfig, ActorClass), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorClass_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorOverrideMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::NewProp_ActorClass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"NVActorTemplateConfig",
		sizeof(FNVActorTemplateConfig),
		alignof(FNVActorTemplateConfig),
		Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVActorTemplateConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVActorTemplateConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVActorTemplateConfig"), sizeof(FNVActorTemplateConfig), Get_Z_Construct_UScriptStruct_FNVActorTemplateConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVActorTemplateConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVActorTemplateConfig_Hash() { return 3970441855U; }
	void AGroupActorManager::StaticRegisterNativesAGroupActorManager()
	{
	}
	UClass* Z_Construct_UClass_AGroupActorManager_NoRegister()
	{
		return AGroupActorManager::StaticClass();
	}
	struct Z_Construct_UClass_AGroupActorManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CountdownUntilNextSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CountdownUntilNextSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TemplateActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TemplateActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TemplateActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManagedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ManagedActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ManagedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpawnDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomLocationVolume;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayoutGenerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LayoutGenerator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalNumberOfActorsToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TotalNumberOfActorsToSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CountPerActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CountPerActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideActorMeshes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OverrideActorMeshes;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverrideActorMeshes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClassesToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorClassesToSpawn;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClassesToSpawn_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoActive_MetaData[];
#endif
		static void NewProp_bAutoActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoActive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGroupActorManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "GroupActorManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Manages array of actors to spawn with mesh and spatial randomization control." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountdownUntilNextSpawn_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountdownUntilNextSpawn = { "CountdownUntilNextSpawn", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, CountdownUntilNextSpawn), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountdownUntilNextSpawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountdownUntilNextSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors = { "TemplateActors", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, TemplateActors), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors_Inner = { "TemplateActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "Transient" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors = { "ManagedActors", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, ManagedActors), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors_Inner = { "ManagedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_SpawnDuration_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "How long to wait until we spawn a new group of actors again\nNOTE: If SpawnDuration <= 0 then we only spawn the actors once" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_SpawnDuration = { "SpawnDuration", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, SpawnDuration), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_SpawnDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_SpawnDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_RandomLocationVolume_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The volume to choose the location where those managed actor can be in" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_RandomLocationVolume = { "RandomLocationVolume", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, RandomLocationVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_RandomLocationVolume_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_RandomLocationVolume_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_LayoutGenerator_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The layout for these managed actors" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_LayoutGenerator = { "LayoutGenerator", nullptr, (EPropertyFlags)0x0012020000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, LayoutGenerator), Z_Construct_UClass_USpatialLayoutGenerator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_LayoutGenerator_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_LayoutGenerator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TotalNumberOfActorsToSpawn_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "Total number of actors to spawn" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TotalNumberOfActorsToSpawn = { "TotalNumberOfActorsToSpawn", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, TotalNumberOfActorsToSpawn), Z_Construct_UScriptStruct_FInt32Interval, METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TotalNumberOfActorsToSpawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TotalNumberOfActorsToSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountPerActor_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "How many instances of each actors do we need to spawn" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountPerActor = { "CountPerActor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, CountPerActor), Z_Construct_UScriptStruct_FInt32Interval, METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountPerActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountPerActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The list of meshes to used for managed actors\nNOTE: If there are meshes in this list, the system will only use the first class in" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes = { "OverrideActorMeshes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, OverrideActorMeshes), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes_Inner = { "OverrideActorMeshes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "The list of actor classes to spawn and manage" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn = { "ActorClassesToSpawn", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGroupActorManager, ActorClassesToSpawn), METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn_Inner = { "ActorClassesToSpawn", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive_MetaData[] = {
		{ "Category", "GroupActorManager" },
		{ "ModuleRelativePath", "Public/GroupActorManager.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive_SetBit(void* Obj)
	{
		((AGroupActorManager*)Obj)->bAutoActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive = { "bAutoActive", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AGroupActorManager), &Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive_SetBit, METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive_MetaData, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGroupActorManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountdownUntilNextSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TemplateActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ManagedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_SpawnDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_RandomLocationVolume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_LayoutGenerator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_TotalNumberOfActorsToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_CountPerActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_OverrideActorMeshes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_ActorClassesToSpawn_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGroupActorManager_Statics::NewProp_bAutoActive,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGroupActorManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGroupActorManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGroupActorManager_Statics::ClassParams = {
		&AGroupActorManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AGroupActorManager_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AGroupActorManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AGroupActorManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGroupActorManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGroupActorManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGroupActorManager, 857591872);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<AGroupActorManager>()
	{
		return AGroupActorManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGroupActorManager(Z_Construct_UClass_AGroupActorManager, &AGroupActorManager::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("AGroupActorManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGroupActorManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
