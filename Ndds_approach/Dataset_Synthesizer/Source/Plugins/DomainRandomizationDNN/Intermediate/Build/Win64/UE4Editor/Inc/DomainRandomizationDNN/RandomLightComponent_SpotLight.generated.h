// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomLightComponent_SpotLight_generated_h
#error "RandomLightComponent_SpotLight.generated.h already included, missing '#pragma once' in RandomLightComponent_SpotLight.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomLightComponent_SpotLight_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomLightComponent_SpotLight(); \
	friend struct Z_Construct_UClass_URandomLightComponent_SpotLight_Statics; \
public: \
	DECLARE_CLASS(URandomLightComponent_SpotLight, URandomLightComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLightComponent_SpotLight)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_INCLASS \
private: \
	static void StaticRegisterNativesURandomLightComponent_SpotLight(); \
	friend struct Z_Construct_UClass_URandomLightComponent_SpotLight_Statics; \
public: \
	DECLARE_CLASS(URandomLightComponent_SpotLight, URandomLightComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLightComponent_SpotLight)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomLightComponent_SpotLight(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomLightComponent_SpotLight) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLightComponent_SpotLight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLightComponent_SpotLight); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLightComponent_SpotLight(URandomLightComponent_SpotLight&&); \
	NO_API URandomLightComponent_SpotLight(const URandomLightComponent_SpotLight&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLightComponent_SpotLight(URandomLightComponent_SpotLight&&); \
	NO_API URandomLightComponent_SpotLight(const URandomLightComponent_SpotLight&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLightComponent_SpotLight); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLightComponent_SpotLight); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomLightComponent_SpotLight)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bShouldModifyInnerConeAngle() { return STRUCT_OFFSET(URandomLightComponent_SpotLight, bShouldModifyInnerConeAngle); } \
	FORCEINLINE static uint32 __PPO__InnerConeAngleRange() { return STRUCT_OFFSET(URandomLightComponent_SpotLight, InnerConeAngleRange); } \
	FORCEINLINE static uint32 __PPO__bShouldModifyOuterConeAngle() { return STRUCT_OFFSET(URandomLightComponent_SpotLight, bShouldModifyOuterConeAngle); } \
	FORCEINLINE static uint32 __PPO__OuterConeAngleRange() { return STRUCT_OFFSET(URandomLightComponent_SpotLight, OuterConeAngleRange); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_20_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomLightComponent_SpotLight>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_SpotLight_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
