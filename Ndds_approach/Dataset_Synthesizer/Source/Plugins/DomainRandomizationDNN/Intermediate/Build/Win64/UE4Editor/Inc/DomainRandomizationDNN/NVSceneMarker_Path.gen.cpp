// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SceneMarker/NVSceneMarker_Path.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneMarker_Path() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_Path_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_Path();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneMarkerComponent();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister();
// End Cross Module References
	void ANVSceneMarker_Path::StaticRegisterNativesANVSceneMarker_Path()
	{
	}
	UClass* Z_Construct_UClass_ANVSceneMarker_Path_NoRegister()
	{
		return ANVSceneMarker_Path::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneMarker_Path_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMarker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneMarker;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneMarker_Path_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_Path_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering" },
		{ "IncludePath", "SceneMarker/NVSceneMarker_Path.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_Path.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "ANVSceneMarker_Path - The scene marker actors are placed in the map and control how the capturer move around\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_Path_Statics::NewProp_SceneMarker_MetaData[] = {
		{ "Category", "NVSceneMarker_Path" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_Path.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneMarker_Path_Statics::NewProp_SceneMarker = { "SceneMarker", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneMarker_Path, SceneMarker), Z_Construct_UScriptStruct_FNVSceneMarkerComponent, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_Path_Statics::NewProp_SceneMarker_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Path_Statics::NewProp_SceneMarker_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneMarker_Path_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_Path_Statics::NewProp_SceneMarker,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ANVSceneMarker_Path_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister, (int32)VTABLE_OFFSET(ANVSceneMarker_Path, INVSceneMarkerInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneMarker_Path_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneMarker_Path>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneMarker_Path_Statics::ClassParams = {
		&ANVSceneMarker_Path::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVSceneMarker_Path_Statics::PropPointers,
		InterfaceParams,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Path_Statics::PropPointers),
		ARRAY_COUNT(InterfaceParams),
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_Path_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Path_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneMarker_Path()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneMarker_Path_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneMarker_Path, 3580373068);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ANVSceneMarker_Path>()
	{
		return ANVSceneMarker_Path::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneMarker_Path(Z_Construct_UClass_ANVSceneMarker_Path, &ANVSceneMarker_Path::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ANVSceneMarker_Path"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneMarker_Path);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
