// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/DRUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDRUtils() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType();
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomAssetStreamer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandUtils();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomMaterialSelection();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomColorData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomScale3DData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomLocationData();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomRotationData();
// End Cross Module References
	static UEnum* EAffectedMaterialOwnerComponentType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("EAffectedMaterialOwnerComponentType"));
		}
		return Singleton;
	}
	template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<EAffectedMaterialOwnerComponentType>()
	{
		return EAffectedMaterialOwnerComponentType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EAffectedMaterialOwnerComponentType(EAffectedMaterialOwnerComponentType_StaticEnum, TEXT("/Script/DomainRandomizationDNN"), TEXT("EAffectedMaterialOwnerComponentType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType_Hash() { return 3886931090U; }
	UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EAffectedMaterialOwnerComponentType"), 0, Get_Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EAffectedMaterialOwnerComponentType::OnlyAffectMeshComponents", (int64)EAffectedMaterialOwnerComponentType::OnlyAffectMeshComponents },
				{ "EAffectedMaterialOwnerComponentType::OnlyAffectDecalComponents", (int64)EAffectedMaterialOwnerComponentType::OnlyAffectDecalComponents },
				{ "EAffectedMaterialOwnerComponentType::AffectBothMeshAndDecalComponents", (int64)EAffectedMaterialOwnerComponentType::AffectBothMeshAndDecalComponents },
				{ "EAffectedMaterialOwnerComponentType::AffectedMaterialOwnerComponentType_MAX", (int64)EAffectedMaterialOwnerComponentType::AffectedMaterialOwnerComponentType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AffectedMaterialOwnerComponentType_MAX.Hidden", "" },
				{ "ModuleRelativePath", "Public/DRUtils.h" },
				{ "ToolTip", "This enum is used by random material components to select which components it should modify material" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
				nullptr,
				"EAffectedMaterialOwnerComponentType",
				"EAffectedMaterialOwnerComponentType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMaterialSelectionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("EMaterialSelectionType"));
		}
		return Singleton;
	}
	template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<EMaterialSelectionType>()
	{
		return EMaterialSelectionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMaterialSelectionType(EMaterialSelectionType_StaticEnum, TEXT("/Script/DomainRandomizationDNN"), TEXT("EMaterialSelectionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType_Hash() { return 1038612256U; }
	UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMaterialSelectionType"), 0, Get_Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMaterialSelectionType::ModifyAllMaterials", (int64)EMaterialSelectionType::ModifyAllMaterials },
				{ "EMaterialSelectionType::ModifyMaterialsInIndexesList", (int64)EMaterialSelectionType::ModifyMaterialsInIndexesList },
				{ "EMaterialSelectionType::ModifyMaterialInSlotNamesList", (int64)EMaterialSelectionType::ModifyMaterialInSlotNamesList },
				{ "EMaterialSelectionType::MaterialSelectionType_MAX", (int64)EMaterialSelectionType::MaterialSelectionType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MaterialSelectionType_MAX.Hidden", "" },
				{ "ModifyAllMaterials.ToolTip", "Modify all the materials in the mesh" },
				{ "ModifyMaterialInSlotNamesList.ToolTip", "Modify all the materials in the MaterialSlotNames list" },
				{ "ModifyMaterialsInIndexesList.ToolTip", "Modify all the materials in the MaterialIndexes list" },
				{ "ModuleRelativePath", "Public/DRUtils.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
				nullptr,
				"EMaterialSelectionType",
				"EMaterialSelectionType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ERandomColorType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("ERandomColorType"));
		}
		return Singleton;
	}
	template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ERandomColorType>()
	{
		return ERandomColorType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ERandomColorType(ERandomColorType_StaticEnum, TEXT("/Script/DomainRandomizationDNN"), TEXT("ERandomColorType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType_Hash() { return 2361357673U; }
	UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ERandomColorType"), 0, Get_Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ERandomColorType::RandomizeAllColor", (int64)ERandomColorType::RandomizeAllColor },
				{ "ERandomColorType::RandomizeBetweenTwoColors", (int64)ERandomColorType::RandomizeBetweenTwoColors },
				{ "ERandomColorType::RandomizeAroundAColor", (int64)ERandomColorType::RandomizeAroundAColor },
				{ "ERandomColorType::RandomColorType_MAX", (int64)ERandomColorType::RandomColorType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DRUtils.h" },
				{ "RandomColorType_MAX.Hidden", "" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
				nullptr,
				"ERandomColorType",
				"ERandomColorType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FRandomAssetStreamer::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomAssetStreamer_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomAssetStreamer, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomAssetStreamer"), sizeof(FRandomAssetStreamer), Get_Z_Construct_UScriptStruct_FRandomAssetStreamer_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomAssetStreamer>()
{
	return FRandomAssetStreamer::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomAssetStreamer(FRandomAssetStreamer::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomAssetStreamer"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomAssetStreamer
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomAssetStreamer()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomAssetStreamer")),new UScriptStruct::TCppStructOps<FRandomAssetStreamer>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomAssetStreamer;
	struct Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnusedAssetCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UnusedAssetCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastUsedAssetIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LastUsedAssetIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastLoadedAssetIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LastLoadedAssetIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LoadingAssetReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LoadingAssetReferences;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LoadingAssetReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LoadedAssetReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LoadedAssetReferences;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LoadedAssetReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllAssetReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllAssetReferences;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AllAssetReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManagedAssetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ManagedAssetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetDirectories;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetDirectories_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "This struct manage a large amount numbers of assets by" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomAssetStreamer>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_UnusedAssetCount_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_UnusedAssetCount = { "UnusedAssetCount", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, UnusedAssetCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_UnusedAssetCount_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_UnusedAssetCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastUsedAssetIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastUsedAssetIndex = { "LastUsedAssetIndex", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, LastUsedAssetIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastUsedAssetIndex_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastUsedAssetIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastLoadedAssetIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastLoadedAssetIndex = { "LastLoadedAssetIndex", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, LastLoadedAssetIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastLoadedAssetIndex_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastLoadedAssetIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "List of assets is streaming in" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences = { "LoadingAssetReferences", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, LoadingAssetReferences), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences_Inner = { "LoadingAssetReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "Circular queue of loaded assets" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences = { "LoadedAssetReferences", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, LoadedAssetReferences), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences_Inner = { "LoadedAssetReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "List of all the assets in the managed directory" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences = { "AllAssetReferences", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, AllAssetReferences), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences_Inner = { "AllAssetReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_ManagedAssetClass_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "Class of assets to get" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_ManagedAssetClass = { "ManagedAssetClass", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, ManagedAssetClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_ManagedAssetClass_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_ManagedAssetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories_MetaData[] = {
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "Transient properties\nPath to the directory where we want to get the assets from" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories = { "AssetDirectories", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomAssetStreamer, AssetDirectories), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories_Inner = { "AssetDirectories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_UnusedAssetCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastUsedAssetIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LastLoadedAssetIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadingAssetReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_LoadedAssetReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AllAssetReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_ManagedAssetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::NewProp_AssetDirectories_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomAssetStreamer",
		sizeof(FRandomAssetStreamer),
		alignof(FRandomAssetStreamer),
		Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomAssetStreamer()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomAssetStreamer_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomAssetStreamer"), sizeof(FRandomAssetStreamer), Get_Z_Construct_UScriptStruct_FRandomAssetStreamer_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomAssetStreamer_Hash() { return 1509326914U; }
class UScriptStruct* FRandUtils::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandUtils_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandUtils, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandUtils"), sizeof(FRandUtils), Get_Z_Construct_UScriptStruct_FRandUtils_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandUtils>()
{
	return FRandUtils::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandUtils(FRandUtils::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandUtils"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandUtils
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandUtils()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandUtils")),new UScriptStruct::TCppStructOps<FRandUtils>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandUtils;
	struct Z_Construct_UScriptStruct_FRandUtils_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandUtils_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandUtils_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandUtils>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandUtils_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandUtils",
		sizeof(FRandUtils),
		alignof(FRandUtils),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandUtils_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandUtils_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandUtils()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandUtils_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandUtils"), sizeof(FRandUtils), Get_Z_Construct_UScriptStruct_FRandUtils_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandUtils_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandUtils_Hash() { return 3249039422U; }
class UScriptStruct* FRandomMaterialSelection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomMaterialSelection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomMaterialSelection, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomMaterialSelection"), sizeof(FRandomMaterialSelection), Get_Z_Construct_UScriptStruct_FRandomMaterialSelection_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomMaterialSelection>()
{
	return FRandomMaterialSelection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomMaterialSelection(FRandomMaterialSelection::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomMaterialSelection"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomMaterialSelection
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomMaterialSelection()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomMaterialSelection")),new UScriptStruct::TCppStructOps<FRandomMaterialSelection>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomMaterialSelection;
	struct Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSlotNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MaterialSlotNames;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialSlotNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialIndexes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MaterialIndexes;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaterialIndexes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectMaterialBySlotNames_MetaData[];
#endif
		static void NewProp_bSelectMaterialBySlotNames_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectMaterialBySlotNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectMaterialByIndexes_MetaData[];
#endif
		static void NewProp_bSelectMaterialByIndexes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectMaterialByIndexes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSelectionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialSelectionType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialSelectionType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "FRandomMaterialSelection represent how to select materials from a mesh to modify" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomMaterialSelection>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bSelectMaterialBySlotNames" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "List of names of materials in the owner mesh to modify" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames = { "MaterialSlotNames", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomMaterialSelection, MaterialSlotNames), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames_Inner = { "MaterialSlotNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bSelectMaterialByIndexes" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "List of indexes of materials in the owner mesh to modify" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes = { "MaterialIndexes", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomMaterialSelection, MaterialIndexes), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes_Inner = { "MaterialIndexes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames_SetBit(void* Obj)
	{
		((FRandomMaterialSelection*)Obj)->bSelectMaterialBySlotNames = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames = { "bSelectMaterialBySlotNames", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomMaterialSelection), &Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes_SetBit(void* Obj)
	{
		((FRandomMaterialSelection*)Obj)->bSelectMaterialByIndexes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes = { "bSelectMaterialByIndexes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomMaterialSelection), &Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "Editor properties\nDecide how to select materials from the owner's mesh to modify" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType = { "MaterialSelectionType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomMaterialSelection, MaterialSelectionType), Z_Construct_UEnum_DomainRandomizationDNN_EMaterialSelectionType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSlotNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialIndexes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialBySlotNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_bSelectMaterialByIndexes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::NewProp_MaterialSelectionType_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomMaterialSelection",
		sizeof(FRandomMaterialSelection),
		alignof(FRandomMaterialSelection),
		Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomMaterialSelection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomMaterialSelection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomMaterialSelection"), sizeof(FRandomMaterialSelection), Get_Z_Construct_UScriptStruct_FRandomMaterialSelection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomMaterialSelection_Hash() { return 588592381U; }
class UScriptStruct* FRandomColorData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomColorData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomColorData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomColorData"), sizeof(FRandomColorData), Get_Z_Construct_UScriptStruct_FRandomColorData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomColorData>()
{
	return FRandomColorData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomColorData(FRandomColorData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomColorData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomColorData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomColorData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomColorData")),new UScriptStruct::TCppStructOps<FRandomColorData>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomColorData;
	struct Z_Construct_UScriptStruct_FRandomColorData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxValueChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxValueChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSaturationChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxSaturationChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxHueChange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxHueChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MainColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MainColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeAroundAColor_MetaData[];
#endif
		static void NewProp_bRandomizeAroundAColor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeAroundAColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeInHSV_MetaData[];
#endif
		static void NewProp_bRandomizeInHSV_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeInHSV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SecondColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FirstColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeBetweenTwoColors_MetaData[];
#endif
		static void NewProp_bRandomizeBetweenTwoColors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeBetweenTwoColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomizationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RandomizationType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RandomizationType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomColorData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomColorData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxValueChange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeAroundAColor" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxValueChange = { "MaxValueChange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, MaxValueChange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxValueChange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxValueChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxSaturationChange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeAroundAColor" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxSaturationChange = { "MaxSaturationChange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, MaxSaturationChange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxSaturationChange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxSaturationChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxHueChange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeAroundAColor" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxHueChange = { "MaxHueChange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, MaxHueChange), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxHueChange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxHueChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MainColor_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeAroundAColor" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MainColor = { "MainColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, MainColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MainColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MainColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor_SetBit(void* Obj)
	{
		((FRandomColorData*)Obj)->bRandomizeAroundAColor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor = { "bRandomizeAroundAColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomColorData), &Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeBetweenTwoColors" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "If true, the color will be chosen randomly in the HSV value instead of the RGB one" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV_SetBit(void* Obj)
	{
		((FRandomColorData*)Obj)->bRandomizeInHSV = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV = { "bRandomizeInHSV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomColorData), &Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_SecondColor_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeBetweenTwoColors" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_SecondColor = { "SecondColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, SecondColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_SecondColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_SecondColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_FirstColor_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bRandomizeBetweenTwoColors" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_FirstColor = { "FirstColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, FirstColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_FirstColor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_FirstColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors_SetBit(void* Obj)
	{
		((FRandomColorData*)Obj)->bRandomizeBetweenTwoColors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors = { "bRandomizeBetweenTwoColors", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomColorData), &Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType_MetaData[] = {
		{ "Category", "RandomColorData" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType = { "RandomizationType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomColorData, RandomizationType), Z_Construct_UEnum_DomainRandomizationDNN_ERandomColorType, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomColorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxValueChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxSaturationChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MaxHueChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_MainColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeAroundAColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeInHSV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_SecondColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_FirstColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_bRandomizeBetweenTwoColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomColorData_Statics::NewProp_RandomizationType_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomColorData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomColorData",
		sizeof(FRandomColorData),
		alignof(FRandomColorData),
		Z_Construct_UScriptStruct_FRandomColorData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomColorData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomColorData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomColorData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomColorData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomColorData"), sizeof(FRandomColorData), Get_Z_Construct_UScriptStruct_FRandomColorData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomColorData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomColorData_Hash() { return 611694714U; }
class UScriptStruct* FRandomScale3DData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomScale3DData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomScale3DData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomScale3DData"), sizeof(FRandomScale3DData), Get_Z_Construct_UScriptStruct_FRandomScale3DData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomScale3DData>()
{
	return FRandomScale3DData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomScale3DData(FRandomScale3DData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomScale3DData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomScale3DData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomScale3DData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomScale3DData")),new UScriptStruct::TCppStructOps<FRandomScale3DData>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomScale3DData;
	struct Z_Construct_UScriptStruct_FRandomScale3DData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeZAxis_MetaData[];
#endif
		static void NewProp_bRandomizeZAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeZAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_YAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeYAxis_MetaData[];
#endif
		static void NewProp_bRandomizeYAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeYAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_XAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeXAxis_MetaData[];
#endif
		static void NewProp_bRandomizeXAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeXAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniformScaleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UniformScaleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUniformScale_MetaData[];
#endif
		static void NewProp_bUniformScale_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUniformScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomScale3DData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_ZAxisRange_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "EditCondition", "bRandomizeZAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of scale in the Z axis (in world space)\nNOTE: Minimum value is set at 0.001" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_ZAxisRange = { "ZAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomScale3DData, ZAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_ZAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_ZAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the actor can be scaled along the Z axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis_SetBit(void* Obj)
	{
		((FRandomScale3DData*)Obj)->bRandomizeZAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis = { "bRandomizeZAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomScale3DData), &Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_YAxisRange_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "EditCondition", "bRandomizeYAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of scale in the Y axis (in world space)\nNOTE: Minimum value is set at 0.001" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_YAxisRange = { "YAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomScale3DData, YAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_YAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_YAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the actor can be scaled along the Y axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis_SetBit(void* Obj)
	{
		((FRandomScale3DData*)Obj)->bRandomizeYAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis = { "bRandomizeYAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomScale3DData), &Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_XAxisRange_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "EditCondition", "bRandomizeXAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of scale in the X axis (in world space)\nNOTE: Minimum value is set at 0.001" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_XAxisRange = { "XAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomScale3DData, XAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_XAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_XAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the actor can be scaled along the X axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis_SetBit(void* Obj)
	{
		((FRandomScale3DData*)Obj)->bRandomizeXAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis = { "bRandomizeXAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomScale3DData), &Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_UniformScaleRange_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "EditCondition", "bUniformScale" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_UniformScaleRange = { "UniformScaleRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomScale3DData, UniformScaleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_UniformScaleRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_UniformScaleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale_MetaData[] = {
		{ "Category", "RandomScale3DData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, all the axes will use the same scale value\nOtherwise, the actor can have different scales in different axis\nNOTE: If this is true, the scale will be chosen in UniformScaleRange and the separated axis scale range will be ignored" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale_SetBit(void* Obj)
	{
		((FRandomScale3DData*)Obj)->bUniformScale = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale = { "bUniformScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomScale3DData), &Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomScale3DData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_ZAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeZAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_YAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeYAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_XAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bRandomizeXAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_UniformScaleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomScale3DData_Statics::NewProp_bUniformScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomScale3DData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomScale3DData",
		sizeof(FRandomScale3DData),
		alignof(FRandomScale3DData),
		Z_Construct_UScriptStruct_FRandomScale3DData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomScale3DData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomScale3DData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomScale3DData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomScale3DData"), sizeof(FRandomScale3DData), Get_Z_Construct_UScriptStruct_FRandomScale3DData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomScale3DData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomScale3DData_Hash() { return 1585711652U; }
class UScriptStruct* FRandomLocationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomLocationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomLocationData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomLocationData"), sizeof(FRandomLocationData), Get_Z_Construct_UScriptStruct_FRandomLocationData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomLocationData>()
{
	return FRandomLocationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomLocationData(FRandomLocationData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomLocationData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomLocationData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomLocationData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomLocationData")),new UScriptStruct::TCppStructOps<FRandomLocationData>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomLocationData;
	struct Z_Construct_UScriptStruct_FRandomLocationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ZAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeZAxis_MetaData[];
#endif
		static void NewProp_bRandomizeZAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeZAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_YAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeYAxis_MetaData[];
#endif
		static void NewProp_bRandomizeYAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeYAxis;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_XAxisRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_XAxisRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeXAxis_MetaData[];
#endif
		static void NewProp_bRandomizeXAxis_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeXAxis;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomLocationData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_ZAxisRange_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "EditCondition", "bRandomizeZAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of location in the Y axis (in world space)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_ZAxisRange = { "ZAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomLocationData, ZAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_ZAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_ZAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the location can be along Z axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis_SetBit(void* Obj)
	{
		((FRandomLocationData*)Obj)->bRandomizeZAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis = { "bRandomizeZAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomLocationData), &Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_YAxisRange_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "EditCondition", "bRandomizeYAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of location in the Y axis (in world space)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_YAxisRange = { "YAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomLocationData, YAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_YAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_YAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the location can be along Y axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis_SetBit(void* Obj)
	{
		((FRandomLocationData*)Obj)->bRandomizeYAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis = { "bRandomizeYAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomLocationData), &Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_XAxisRange_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "EditCondition", "bRandomizeXAxis" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of location in the X axis (in world space)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_XAxisRange = { "XAxisRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomLocationData, XAxisRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_XAxisRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_XAxisRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis_MetaData[] = {
		{ "Category", "RandomLocationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the location can be along X axis in world space" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis_SetBit(void* Obj)
	{
		((FRandomLocationData*)Obj)->bRandomizeXAxis = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis = { "bRandomizeXAxis", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomLocationData), &Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomLocationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_ZAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeZAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_YAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeYAxis,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_XAxisRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomLocationData_Statics::NewProp_bRandomizeXAxis,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomLocationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomLocationData",
		sizeof(FRandomLocationData),
		alignof(FRandomLocationData),
		Z_Construct_UScriptStruct_FRandomLocationData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomLocationData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomLocationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomLocationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomLocationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomLocationData"), sizeof(FRandomLocationData), Get_Z_Construct_UScriptStruct_FRandomLocationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomLocationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomLocationData_Hash() { return 3218293365U; }
class UScriptStruct* FRandomRotationData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FRandomRotationData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRandomRotationData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("RandomRotationData"), sizeof(FRandomRotationData), Get_Z_Construct_UScriptStruct_FRandomRotationData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FRandomRotationData>()
{
	return FRandomRotationData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRandomRotationData(FRandomRotationData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("RandomRotationData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomRotationData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomRotationData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("RandomRotationData")),new UScriptStruct::TCppStructOps<FRandomRotationData>);
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFRandomRotationData;
	struct Z_Construct_UScriptStruct_FRandomRotationData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YawRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_YawRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeYaw_MetaData[];
#endif
		static void NewProp_bRandomizeYaw_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeYaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RollRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RollRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeRoll_MetaData[];
#endif
		static void NewProp_bRandomizeRoll_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeRoll;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PitchRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizePitch_MetaData[];
#endif
		static void NewProp_bRandomizePitch_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizePitch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomConeHalfAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RandomConeHalfAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizeRotationInACone_MetaData[];
#endif
		static void NewProp_bRandomizeRotationInACone_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizeRotationInACone;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRandomRotationData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_YawRange_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "EditCondition", "bRandomizeYaw" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of yaw angle (in degree)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_YawRange = { "YawRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomRotationData, YawRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_YawRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_YawRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw_SetBit(void* Obj)
	{
		((FRandomRotationData*)Obj)->bRandomizeYaw = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw = { "bRandomizeYaw", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomRotationData), &Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RollRange_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "EditCondition", "bRandomizeRoll" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of roll angle (in degree)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RollRange = { "RollRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomRotationData, RollRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RollRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RollRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll_SetBit(void* Obj)
	{
		((FRandomRotationData*)Obj)->bRandomizeRoll = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll = { "bRandomizeRoll", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomRotationData), &Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_PitchRange_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "EditCondition", "bRandomizePitch" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "The range of pitch angle (in degree)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_PitchRange = { "PitchRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomRotationData, PitchRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_PitchRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_PitchRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch_SetBit(void* Obj)
	{
		((FRandomRotationData*)Obj)->bRandomizePitch = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch = { "bRandomizePitch", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomRotationData), &Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RandomConeHalfAngle_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "EditCondition", "bRandomizeRotationInACone" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "ToolTip", "Half of the angle (in degree) of the cone where we want to generate the rotation inside" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RandomConeHalfAngle = { "RandomConeHalfAngle", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRandomRotationData, RandomConeHalfAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RandomConeHalfAngle_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RandomConeHalfAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone_MetaData[] = {
		{ "Category", "RandomRotationData" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/DRUtils.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "Editor properties\nIf true, generate random rotation inside a cone" },
	};
#endif
	void Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone_SetBit(void* Obj)
	{
		((FRandomRotationData*)Obj)->bRandomizeRotationInACone = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone = { "bRandomizeRotationInACone", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRandomRotationData), &Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRandomRotationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_YawRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeYaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RollRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRoll,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_PitchRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizePitch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_RandomConeHalfAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRandomRotationData_Statics::NewProp_bRandomizeRotationInACone,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRandomRotationData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"RandomRotationData",
		sizeof(FRandomRotationData),
		alignof(FRandomRotationData),
		Z_Construct_UScriptStruct_FRandomRotationData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRandomRotationData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FRandomRotationData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRandomRotationData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRandomRotationData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RandomRotationData"), sizeof(FRandomRotationData), Get_Z_Construct_UScriptStruct_FRandomRotationData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRandomRotationData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRandomRotationData_Hash() { return 1135667154U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
