// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomLightComponent_SpotLight.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomLightComponent_SpotLight() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLightComponent_SpotLight_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLightComponent_SpotLight();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLightComponent();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
// End Cross Module References
	void URandomLightComponent_SpotLight::StaticRegisterNativesURandomLightComponent_SpotLight()
	{
	}
	UClass* Z_Construct_UClass_URandomLightComponent_SpotLight_NoRegister()
	{
		return URandomLightComponent_SpotLight::StaticClass();
	}
	struct Z_Construct_UClass_URandomLightComponent_SpotLight_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterConeAngleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OuterConeAngleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldModifyOuterConeAngle_MetaData[];
#endif
		static void NewProp_bShouldModifyOuterConeAngle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldModifyOuterConeAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerConeAngleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InnerConeAngleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldModifyInnerConeAngle_MetaData[];
#endif
		static void NewProp_bShouldModifyInnerConeAngle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldModifyInnerConeAngle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomLightComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomLightComponent_SpotLight.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent_SpotLight.h" },
		{ "ToolTip", "Randomize the properties of the owner actor's light components\nNOTE: This is not a LightComponent, it modify other light components\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_OuterConeAngleRange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bShouldModifyOuterConeAngle" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent_SpotLight.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_OuterConeAngleRange = { "OuterConeAngleRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLightComponent_SpotLight, OuterConeAngleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_OuterConeAngleRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_OuterConeAngleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent_SpotLight.h" },
	};
#endif
	void Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle_SetBit(void* Obj)
	{
		((URandomLightComponent_SpotLight*)Obj)->bShouldModifyOuterConeAngle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle = { "bShouldModifyOuterConeAngle", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomLightComponent_SpotLight), &Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_InnerConeAngleRange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bShouldModifyInnerConeAngle" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent_SpotLight.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_InnerConeAngleRange = { "InnerConeAngleRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLightComponent_SpotLight, InnerConeAngleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_InnerConeAngleRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_InnerConeAngleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent_SpotLight.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle_SetBit(void* Obj)
	{
		((URandomLightComponent_SpotLight*)Obj)->bShouldModifyInnerConeAngle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle = { "bShouldModifyInnerConeAngle", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomLightComponent_SpotLight), &Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_OuterConeAngleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyOuterConeAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_InnerConeAngleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::NewProp_bShouldModifyInnerConeAngle,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomLightComponent_SpotLight>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::ClassParams = {
		&URandomLightComponent_SpotLight::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomLightComponent_SpotLight()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomLightComponent_SpotLight_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomLightComponent_SpotLight, 3332655923);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomLightComponent_SpotLight>()
	{
		return URandomLightComponent_SpotLight::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomLightComponent_SpotLight(Z_Construct_UClass_URandomLightComponent_SpotLight, &URandomLightComponent_SpotLight::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomLightComponent_SpotLight"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomLightComponent_SpotLight);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
