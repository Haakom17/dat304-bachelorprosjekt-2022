// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMaterialParam_ScalarComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMaterialParam_ScalarComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_ScalarComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_ScalarComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
// End Cross Module References
	void URandomMaterialParam_ScalarComponent::StaticRegisterNativesURandomMaterialParam_ScalarComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMaterialParam_ScalarComponent_NoRegister()
	{
		return URandomMaterialParam_ScalarComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ValueRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomMaterialParameterComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMaterialParam_ScalarComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_ScalarComponent.h" },
		{ "ToolTip", "RandomTextureComponent randomly change the color parameter of the materials in the owner's mesh\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::NewProp_ValueRange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_ScalarComponent.h" },
		{ "ToolTip", "Editor properties\nRange of the scalar value to randomize" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::NewProp_ValueRange = { "ValueRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_ScalarComponent, ValueRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::NewProp_ValueRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::NewProp_ValueRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::NewProp_ValueRange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMaterialParam_ScalarComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::ClassParams = {
		&URandomMaterialParam_ScalarComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMaterialParam_ScalarComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMaterialParam_ScalarComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMaterialParam_ScalarComponent, 1180653631);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMaterialParam_ScalarComponent>()
	{
		return URandomMaterialParam_ScalarComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMaterialParam_ScalarComponent(Z_Construct_UClass_URandomMaterialParam_ScalarComponent, &URandomMaterialParam_ScalarComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMaterialParam_ScalarComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMaterialParam_ScalarComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
