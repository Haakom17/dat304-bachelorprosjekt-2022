// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMaterialComponent_generated_h
#error "RandomMaterialComponent.generated.h already included, missing '#pragma once' in RandomMaterialComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMaterialComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMaterialComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_INCLASS \
private: \
	static void StaticRegisterNativesURandomMaterialComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMaterialComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMaterialComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialComponent(URandomMaterialComponent&&); \
	NO_API URandomMaterialComponent(const URandomMaterialComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialComponent(URandomMaterialComponent&&); \
	NO_API URandomMaterialComponent(const URandomMaterialComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMaterialComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaterialSelectionConfigData() { return STRUCT_OFFSET(URandomMaterialComponent, MaterialSelectionConfigData); } \
	FORCEINLINE static uint32 __PPO__bUseAllMaterialInDirectories() { return STRUCT_OFFSET(URandomMaterialComponent, bUseAllMaterialInDirectories); } \
	FORCEINLINE static uint32 __PPO__AffectedComponentType() { return STRUCT_OFFSET(URandomMaterialComponent, AffectedComponentType); } \
	FORCEINLINE static uint32 __PPO__MaterialDirectories() { return STRUCT_OFFSET(URandomMaterialComponent, MaterialDirectories); } \
	FORCEINLINE static uint32 __PPO__MaterialList() { return STRUCT_OFFSET(URandomMaterialComponent, MaterialList); } \
	FORCEINLINE static uint32 __PPO__OwnerMeshComponents() { return STRUCT_OFFSET(URandomMaterialComponent, OwnerMeshComponents); } \
	FORCEINLINE static uint32 __PPO__OwnerDecalComponents() { return STRUCT_OFFSET(URandomMaterialComponent, OwnerDecalComponents); } \
	FORCEINLINE static uint32 __PPO__MaterialStreamer() { return STRUCT_OFFSET(URandomMaterialComponent, MaterialStreamer); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_18_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMaterialComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
