// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomComponentBase_generated_h
#error "RandomComponentBase.generated.h already included, missing '#pragma once' in RandomComponentBase.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomComponentBase_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_RPC_WRAPPERS \
	virtual void OnRandomization_Implementation(); \
 \
	DECLARE_FUNCTION(execOnRandomization) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRandomization_Implementation(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRandomization) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnRandomization_Implementation(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_EVENT_PARMS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_CALLBACK_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomComponentBase(); \
	friend struct Z_Construct_UClass_URandomComponentBase_Statics; \
public: \
	DECLARE_CLASS(URandomComponentBase, UActorComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_INCLASS \
private: \
	static void StaticRegisterNativesURandomComponentBase(); \
	friend struct Z_Construct_UClass_URandomComponentBase_Statics; \
public: \
	DECLARE_CLASS(URandomComponentBase, UActorComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomComponentBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomComponentBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomComponentBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomComponentBase(URandomComponentBase&&); \
	NO_API URandomComponentBase(const URandomComponentBase&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomComponentBase(URandomComponentBase&&); \
	NO_API URandomComponentBase(const URandomComponentBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomComponentBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomComponentBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomComponentBase)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bShouldRandomize() { return STRUCT_OFFSET(URandomComponentBase, bShouldRandomize); } \
	FORCEINLINE static uint32 __PPO__RandomizationDurationRange() { return STRUCT_OFFSET(URandomComponentBase, RandomizationDurationRange); } \
	FORCEINLINE static uint32 __PPO__RandomizationDurationInterval() { return STRUCT_OFFSET(URandomComponentBase, RandomizationDurationInterval); } \
	FORCEINLINE static uint32 __PPO__bOnlyRandomizeOnce() { return STRUCT_OFFSET(URandomComponentBase, bOnlyRandomizeOnce); } \
	FORCEINLINE static uint32 __PPO__CountdownUntilNextRandomization() { return STRUCT_OFFSET(URandomComponentBase, CountdownUntilNextRandomization); } \
	FORCEINLINE static uint32 __PPO__bAlreadyRandomized() { return STRUCT_OFFSET(URandomComponentBase, bAlreadyRandomized); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_14_PROLOG \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_EVENT_PARMS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_CALLBACK_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_CALLBACK_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomComponentBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomComponentBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
