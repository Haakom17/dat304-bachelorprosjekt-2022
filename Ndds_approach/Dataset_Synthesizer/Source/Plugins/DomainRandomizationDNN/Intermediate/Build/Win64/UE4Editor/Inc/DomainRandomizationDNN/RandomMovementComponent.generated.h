// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMovementComponent_generated_h
#error "RandomMovementComponent.generated.h already included, missing '#pragma once' in RandomMovementComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMovementComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMovementComponent(); \
	friend struct Z_Construct_UClass_URandomMovementComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMovementComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMovementComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURandomMovementComponent(); \
	friend struct Z_Construct_UClass_URandomMovementComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMovementComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMovementComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMovementComponent(URandomMovementComponent&&); \
	NO_API URandomMovementComponent(const URandomMovementComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMovementComponent(URandomMovementComponent&&); \
	NO_API URandomMovementComponent(const URandomMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMovementComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMovementComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bRelatedToOriginLocation() { return STRUCT_OFFSET(URandomMovementComponent, bRelatedToOriginLocation); } \
	FORCEINLINE static uint32 __PPO__RandomLocationData() { return STRUCT_OFFSET(URandomMovementComponent, RandomLocationData); } \
	FORCEINLINE static uint32 __PPO__bUseObjectAxesInsteadOfWorldAxes() { return STRUCT_OFFSET(URandomMovementComponent, bUseObjectAxesInsteadOfWorldAxes); } \
	FORCEINLINE static uint32 __PPO__RandomLocationVolume() { return STRUCT_OFFSET(URandomMovementComponent, RandomLocationVolume); } \
	FORCEINLINE static uint32 __PPO__bShouldTeleport() { return STRUCT_OFFSET(URandomMovementComponent, bShouldTeleport); } \
	FORCEINLINE static uint32 __PPO__bCheckCollision() { return STRUCT_OFFSET(URandomMovementComponent, bCheckCollision); } \
	FORCEINLINE static uint32 __PPO__RandomSpeedRange() { return STRUCT_OFFSET(URandomMovementComponent, RandomSpeedRange); } \
	FORCEINLINE static uint32 __PPO__OriginalTransform() { return STRUCT_OFFSET(URandomMovementComponent, OriginalTransform); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_14_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
