// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/OrbitalMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOrbitalMovementComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_UOrbitalMovementComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_UOrbitalMovementComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomRotationData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void UOrbitalMovementComponent::StaticRegisterNativesUOrbitalMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_UOrbitalMovementComponent_NoRegister()
	{
		return UOrbitalMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOrbitalMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetYaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetYaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceChangeCountdown_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceChangeCountdown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceToTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceToTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentDistanceToTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentDistanceToTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationFromTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationFromTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WiggleRotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WiggleRotationData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldWiggle_MetaData[];
#endif
		static void NewProp_bShouldWiggle_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldWiggle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyChangeDistanceWhenPitchChanged_MetaData[];
#endif
		static void NewProp_bOnlyChangeDistanceWhenPitchChanged_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyChangeDistanceWhenPitchChanged;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetDistanceChangeDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetDistanceChangeDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceChangeSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DistanceChangeSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetDistanceRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetDistanceRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldChangeDistance_MetaData[];
#endif
		static void NewProp_bShouldChangeDistance_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldChangeDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRandomizePitchAfterEachYawRotation_MetaData[];
#endif
		static void NewProp_bRandomizePitchAfterEachYawRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRandomizePitchAfterEachYawRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRevertYawDirectionAfterEachRotation_MetaData[];
#endif
		static void NewProp_bRevertYawDirectionAfterEachRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRevertYawDirectionAfterEachRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchRotationRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PitchRotationRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_YawRotationRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_YawRotationRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchRotationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PitchRotationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalTargetActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FocalTargetActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckCollision_MetaData[];
#endif
		static void NewProp_bCheckCollision_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckCollision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTeleportRandomly_MetaData[];
#endif
		static void NewProp_bTeleportRandomly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTeleportRandomly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldMove_MetaData[];
#endif
		static void NewProp_bShouldMove_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldMove;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOrbitalMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "OrbitalMovementComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "RandomTextureComponent randomly change the color parameter of the materials in the owner's mesh\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetYaw_MetaData[] = {
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetYaw = { "TargetYaw", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, TargetYaw), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetYaw_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetYaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeCountdown_MetaData[] = {
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeCountdown = { "DistanceChangeCountdown", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, DistanceChangeCountdown), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeCountdown_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeCountdown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceToTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceToTarget = { "DistanceToTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, DistanceToTarget), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceToTarget_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceToTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_CurrentDistanceToTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_CurrentDistanceToTarget = { "CurrentDistanceToTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, CurrentDistanceToTarget), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_CurrentDistanceToTarget_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_CurrentDistanceToTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationFromTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationFromTarget = { "RotationFromTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, RotationFromTarget), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationFromTarget_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationFromTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_WiggleRotationData_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldWiggle" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_WiggleRotationData = { "WiggleRotationData", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, WiggleRotationData), Z_Construct_UScriptStruct_FRandomRotationData, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_WiggleRotationData_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_WiggleRotationData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "If true, the owner also randomly rotated when moving" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bShouldWiggle = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle = { "bShouldWiggle", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldChangeDistance" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bOnlyChangeDistanceWhenPitchChanged = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged = { "bOnlyChangeDistanceWhenPitchChanged", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceChangeDuration_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldChangeDistance" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "How long (seconds) to wait before the owner change its distance to the target" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceChangeDuration = { "TargetDistanceChangeDuration", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, TargetDistanceChangeDuration), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceChangeDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceChangeDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeSpeed_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldChangeDistance" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "How fast (unit per second) does the owner change its distance from the target\nNOTE: This speed is independent from the rotation speed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeSpeed = { "DistanceChangeSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, DistanceChangeSpeed), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceRange_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldChangeDistance" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "How far does the owner need to stay from the target" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceRange = { "TargetDistanceRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, TargetDistanceRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "If true, the owner will change its distance (selected randomly in TargetDistanceRange) to the target when orbiting" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bShouldChangeDistance = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance = { "bShouldChangeDistance", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bRandomizePitchAfterEachYawRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation = { "bRandomizePitchAfterEachYawRotation", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "If true, after each full yaw rotation, the yaw direction change from min to max to max to min and vice-versa" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bRevertYawDirectionAfterEachRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation = { "bRevertYawDirectionAfterEachRotation", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationRange_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationRange = { "PitchRotationRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, PitchRotationRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_YawRotationRange_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_YawRotationRange = { "YawRotationRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, YawRotationRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_YawRotationRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_YawRotationRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationSpeed_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "How fast (angle per second) should the owner change the pitch" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationSpeed = { "PitchRotationSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, PitchRotationSpeed), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationSpeed_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "How fast (angle per second) should the owner rotate" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationSpeed = { "RotationSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, RotationSpeed), METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_FocalTargetActor_MetaData[] = {
		{ "Category", "Movement" },
		{ "EditCondition", "bShouldMove" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "The actor which is the center of the orbit movement" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_FocalTargetActor = { "FocalTargetActor", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOrbitalMovementComponent, FocalTargetActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_FocalTargetActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_FocalTargetActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision_MetaData[] = {
		{ "Category", "OrbitalMovementComponent" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "If true, the actor will not be able to move if its way is blocked\nOtherwise the actor can go through other object or teleport overlap with the other" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bCheckCollision = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision = { "bCheckCollision", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "If true, the owner will teleport instead of moving between random locations on the constrained sphere" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bTeleportRandomly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly = { "bTeleportRandomly", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Public/OrbitalMovementComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove_SetBit(void* Obj)
	{
		((UOrbitalMovementComponent*)Obj)->bShouldMove = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove = { "bShouldMove", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOrbitalMovementComponent), &Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove_MetaData, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOrbitalMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetYaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeCountdown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceToTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_CurrentDistanceToTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationFromTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_WiggleRotationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldWiggle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bOnlyChangeDistanceWhenPitchChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceChangeDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_DistanceChangeSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_TargetDistanceRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldChangeDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRandomizePitchAfterEachYawRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bRevertYawDirectionAfterEachRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_YawRotationRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_PitchRotationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_RotationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_FocalTargetActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bCheckCollision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bTeleportRandomly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOrbitalMovementComponent_Statics::NewProp_bShouldMove,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOrbitalMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOrbitalMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOrbitalMovementComponent_Statics::ClassParams = {
		&UOrbitalMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOrbitalMovementComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOrbitalMovementComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UOrbitalMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOrbitalMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOrbitalMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOrbitalMovementComponent, 3150027396);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<UOrbitalMovementComponent>()
	{
		return UOrbitalMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOrbitalMovementComponent(Z_Construct_UClass_UOrbitalMovementComponent, &UOrbitalMovementComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("UOrbitalMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOrbitalMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
