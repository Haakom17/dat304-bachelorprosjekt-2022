// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/RandomizedActorManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomizedActorManager() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ARandomizedActorManager_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ARandomizedActorManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void ARandomizedActorManager::StaticRegisterNativesARandomizedActorManager()
	{
	}
	UClass* Z_Construct_UClass_ARandomizedActorManager_NoRegister()
	{
		return ARandomizedActorManager::StaticClass();
	}
	struct Z_Construct_UClass_ARandomizedActorManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomLocationVolume;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfActorsToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfActorsToSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClassesToSpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorClassesToSpawn;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorClassesToSpawn_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARandomizedActorManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActorManager_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "RandomizedActorManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActorManager.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_RandomLocationVolume_MetaData[] = {
		{ "Category", "RandomizedActorManager" },
		{ "ModuleRelativePath", "Public/RandomizedActorManager.h" },
		{ "ToolTip", "The volume to choose the location where those managed actor can be in" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_RandomLocationVolume = { "RandomLocationVolume", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActorManager, RandomLocationVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_RandomLocationVolume_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_RandomLocationVolume_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_NumberOfActorsToSpawn_MetaData[] = {
		{ "Category", "RandomizedActorManager" },
		{ "ModuleRelativePath", "Public/RandomizedActorManager.h" },
		{ "ToolTip", "Total number of actors to spawn in the level" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_NumberOfActorsToSpawn = { "NumberOfActorsToSpawn", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActorManager, NumberOfActorsToSpawn), METADATA_PARAMS(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_NumberOfActorsToSpawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_NumberOfActorsToSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData[] = {
		{ "Category", "RandomizedActorManager" },
		{ "ModuleRelativePath", "Public/RandomizedActorManager.h" },
		{ "ToolTip", "List of actor classes to spawn and manage" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn = { "ActorClassesToSpawn", nullptr, (EPropertyFlags)0x0024080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActorManager, ActorClassesToSpawn), METADATA_PARAMS(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn_Inner = { "ActorClassesToSpawn", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARandomizedActorManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_RandomLocationVolume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_NumberOfActorsToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActorManager_Statics::NewProp_ActorClassesToSpawn_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARandomizedActorManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARandomizedActorManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARandomizedActorManager_Statics::ClassParams = {
		&ARandomizedActorManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARandomizedActorManager_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ARandomizedActorManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ARandomizedActorManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARandomizedActorManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARandomizedActorManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARandomizedActorManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARandomizedActorManager, 2054205312);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ARandomizedActorManager>()
	{
		return ARandomizedActorManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARandomizedActorManager(Z_Construct_UClass_ARandomizedActorManager, &ARandomizedActorManager::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ARandomizedActorManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARandomizedActorManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
