// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/RandomComponentBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomComponentBase() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UFunction* Z_Construct_UFunction_URandomComponentBase_OnRandomization();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatRange();
// End Cross Module References
	static FName NAME_URandomComponentBase_OnRandomization = FName(TEXT("OnRandomization"));
	void URandomComponentBase::OnRandomization()
	{
		ProcessEvent(FindFunctionChecked(NAME_URandomComponentBase_OnRandomization),NULL);
	}
	void URandomComponentBase::StaticRegisterNativesURandomComponentBase()
	{
		UClass* Class = URandomComponentBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnRandomization", &URandomComponentBase::execOnRandomization },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics::Function_MetaDataParams[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URandomComponentBase, nullptr, "OnRandomization", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URandomComponentBase_OnRandomization()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URandomComponentBase_OnRandomization_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URandomComponentBase_NoRegister()
	{
		return URandomComponentBase::StaticClass();
	}
	struct Z_Construct_UClass_URandomComponentBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAlreadyRandomized_MetaData[];
#endif
		static void NewProp_bAlreadyRandomized_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAlreadyRandomized;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CountdownUntilNextRandomization_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CountdownUntilNextRandomization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyRandomizeOnce_MetaData[];
#endif
		static void NewProp_bOnlyRandomizeOnce_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyRandomizeOnce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomizationDurationInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomizationDurationInterval;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomizationDurationRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomizationDurationRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldRandomize_MetaData[];
#endif
		static void NewProp_bShouldRandomize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldRandomize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomComponentBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URandomComponentBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URandomComponentBase_OnRandomization, "OnRandomization" }, // 3903299212
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "RandomComponentBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized_MetaData[] = {
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
	};
#endif
	void Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized_SetBit(void* Obj)
	{
		((URandomComponentBase*)Obj)->bAlreadyRandomized = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized = { "bAlreadyRandomized", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomComponentBase), &Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_CountdownUntilNextRandomization_MetaData[] = {
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_CountdownUntilNextRandomization = { "CountdownUntilNextRandomization", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomComponentBase, CountdownUntilNextRandomization), METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_CountdownUntilNextRandomization_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_CountdownUntilNextRandomization_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
		{ "ToolTip", "If true, this component only randomize once in the begining" },
	};
#endif
	void Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce_SetBit(void* Obj)
	{
		((URandomComponentBase*)Obj)->bOnlyRandomizeOnce = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce = { "bOnlyRandomizeOnce", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomComponentBase), &Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationInterval_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bShouldRandomize" },
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
		{ "ToolTip", "How long (in seconds) does the component wait between randomizations\nNOTE: If the Max value of RandomizationDurationInterval is < 0 then this component doesnt update at all\nIf the Max value of RandomizationDurationInterval is = 0 then this component update every frame\nIf the Min value different from the Max value then the component will choose a random duration to wait inside that range in between randomization update" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationInterval = { "RandomizationDurationInterval", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomComponentBase, RandomizationDurationInterval), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationInterval_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationInterval_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationRange_MetaData[] = {
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
		{ "ToolTip", "How long (in seconds) does the component wait between randomizations\nNOTE: If the UpperBound of RandomizationDuration is <= 0 then this component doesnt update at all\nIf LowerBound different from UpperBound then the component will choose a random duration to wait inside that range in between randomization update\nDEPRECATED_FORGAME(4.16, \"Use RandomizationDurationInterval instead\")" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationRange = { "RandomizationDurationRange", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomComponentBase, RandomizationDurationRange), Z_Construct_UScriptStruct_FFloatRange, METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationRange_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/RandomComponentBase.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize_SetBit(void* Obj)
	{
		((URandomComponentBase*)Obj)->bShouldRandomize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize = { "bShouldRandomize", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomComponentBase), &Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize_MetaData, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomComponentBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bAlreadyRandomized,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_CountdownUntilNextRandomization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bOnlyRandomizeOnce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_RandomizationDurationRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomComponentBase_Statics::NewProp_bShouldRandomize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomComponentBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomComponentBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomComponentBase_Statics::ClassParams = {
		&URandomComponentBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URandomComponentBase_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::PropPointers),
		0,
		0x00B000A5u,
		METADATA_PARAMS(Z_Construct_UClass_URandomComponentBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_URandomComponentBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomComponentBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomComponentBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomComponentBase, 3265250135);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomComponentBase>()
	{
		return URandomComponentBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomComponentBase(Z_Construct_UClass_URandomComponentBase, &URandomComponentBase::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomComponentBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomComponentBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
