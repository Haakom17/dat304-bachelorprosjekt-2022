// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_DRUtils_generated_h
#error "DRUtils.generated.h already included, missing '#pragma once' in DRUtils.h"
#endif
#define DOMAINRANDOMIZATIONDNN_DRUtils_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_301_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomAssetStreamer_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__AssetDirectories() { return STRUCT_OFFSET(FRandomAssetStreamer, AssetDirectories); } \
	FORCEINLINE static uint32 __PPO__ManagedAssetClass() { return STRUCT_OFFSET(FRandomAssetStreamer, ManagedAssetClass); } \
	FORCEINLINE static uint32 __PPO__AllAssetReferences() { return STRUCT_OFFSET(FRandomAssetStreamer, AllAssetReferences); } \
	FORCEINLINE static uint32 __PPO__LoadedAssetReferences() { return STRUCT_OFFSET(FRandomAssetStreamer, LoadedAssetReferences); } \
	FORCEINLINE static uint32 __PPO__LoadingAssetReferences() { return STRUCT_OFFSET(FRandomAssetStreamer, LoadingAssetReferences); } \
	FORCEINLINE static uint32 __PPO__LastLoadedAssetIndex() { return STRUCT_OFFSET(FRandomAssetStreamer, LastLoadedAssetIndex); } \
	FORCEINLINE static uint32 __PPO__LastUsedAssetIndex() { return STRUCT_OFFSET(FRandomAssetStreamer, LastUsedAssetIndex); } \
	FORCEINLINE static uint32 __PPO__UnusedAssetCount() { return STRUCT_OFFSET(FRandomAssetStreamer, UnusedAssetCount); }


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomAssetStreamer>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_279_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandUtils_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandUtils>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_244_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomMaterialSelection_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomMaterialSelection>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_177_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomColorData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomColorData>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_114_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomScale3DData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomScale3DData>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_67_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomLocationData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomLocationData>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRandomRotationData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bRandomizeRotationInACone() { return STRUCT_OFFSET(FRandomRotationData, bRandomizeRotationInACone); } \
	FORCEINLINE static uint32 __PPO__RandomConeHalfAngle() { return STRUCT_OFFSET(FRandomRotationData, RandomConeHalfAngle); } \
	FORCEINLINE static uint32 __PPO__bRandomizePitch() { return STRUCT_OFFSET(FRandomRotationData, bRandomizePitch); } \
	FORCEINLINE static uint32 __PPO__PitchRange() { return STRUCT_OFFSET(FRandomRotationData, PitchRange); } \
	FORCEINLINE static uint32 __PPO__bRandomizeRoll() { return STRUCT_OFFSET(FRandomRotationData, bRandomizeRoll); } \
	FORCEINLINE static uint32 __PPO__RollRange() { return STRUCT_OFFSET(FRandomRotationData, RollRange); } \
	FORCEINLINE static uint32 __PPO__bRandomizeYaw() { return STRUCT_OFFSET(FRandomRotationData, bRandomizeYaw); } \
	FORCEINLINE static uint32 __PPO__YawRange() { return STRUCT_OFFSET(FRandomRotationData, YawRange); }


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FRandomRotationData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRUtils_h


#define FOREACH_ENUM_EAFFECTEDMATERIALOWNERCOMPONENTTYPE(op) \
	op(EAffectedMaterialOwnerComponentType::OnlyAffectMeshComponents) \
	op(EAffectedMaterialOwnerComponentType::OnlyAffectDecalComponents) \
	op(EAffectedMaterialOwnerComponentType::AffectBothMeshAndDecalComponents) \
	op(EAffectedMaterialOwnerComponentType::AffectedMaterialOwnerComponentType_MAX) 

enum class EAffectedMaterialOwnerComponentType : uint8;
template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<EAffectedMaterialOwnerComponentType>();

#define FOREACH_ENUM_EMATERIALSELECTIONTYPE(op) \
	op(EMaterialSelectionType::ModifyAllMaterials) \
	op(EMaterialSelectionType::ModifyMaterialsInIndexesList) \
	op(EMaterialSelectionType::ModifyMaterialInSlotNamesList) \
	op(EMaterialSelectionType::MaterialSelectionType_MAX) 

enum class EMaterialSelectionType : uint8;
template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<EMaterialSelectionType>();

#define FOREACH_ENUM_ERANDOMCOLORTYPE(op) \
	op(ERandomColorType::RandomizeAllColor) \
	op(ERandomColorType::RandomizeBetweenTwoColors) \
	op(ERandomColorType::RandomizeAroundAColor) \
	op(ERandomColorType::RandomColorType_MAX) 

enum class ERandomColorType : uint8;
template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ERandomColorType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
