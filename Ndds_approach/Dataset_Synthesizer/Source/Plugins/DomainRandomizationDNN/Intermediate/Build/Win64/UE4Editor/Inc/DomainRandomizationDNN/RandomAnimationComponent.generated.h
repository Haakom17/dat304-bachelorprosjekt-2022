// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomAnimationComponent_generated_h
#error "RandomAnimationComponent.generated.h already included, missing '#pragma once' in RandomAnimationComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomAnimationComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_50_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FNVHumanSkeletalBoneData>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<struct FNVHalfBodyBoneData>();

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomAnimationComponent(); \
	friend struct Z_Construct_UClass_URandomAnimationComponent_Statics; \
public: \
	DECLARE_CLASS(URandomAnimationComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomAnimationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_INCLASS \
private: \
	static void StaticRegisterNativesURandomAnimationComponent(); \
	friend struct Z_Construct_UClass_URandomAnimationComponent_Statics; \
public: \
	DECLARE_CLASS(URandomAnimationComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomAnimationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomAnimationComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomAnimationComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomAnimationComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomAnimationComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomAnimationComponent(URandomAnimationComponent&&); \
	NO_API URandomAnimationComponent(const URandomAnimationComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomAnimationComponent(URandomAnimationComponent&&); \
	NO_API URandomAnimationComponent(const URandomAnimationComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomAnimationComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomAnimationComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomAnimationComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CurrentAnimation() { return STRUCT_OFFSET(URandomAnimationComponent, CurrentAnimation); } \
	FORCEINLINE static uint32 __PPO__FolderAnimSequenceReferences() { return STRUCT_OFFSET(URandomAnimationComponent, FolderAnimSequenceReferences); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_64_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomAnimationComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomAnimationComponent_h


#define FOREACH_ENUM_ENVHALFBODYBONETYPE(op) \
	op(ENVHalfBodyBoneType::Heel) \
	op(ENVHalfBodyBoneType::Knee) \
	op(ENVHalfBodyBoneType::Pelvis) \
	op(ENVHalfBodyBoneType::Shoulder) \
	op(ENVHalfBodyBoneType::Elbow) \
	op(ENVHalfBodyBoneType::Wrist) \
	op(ENVHalfBodyBoneType::Ear) \
	op(ENVHalfBodyBoneType::Eye) \
	op(ENVHalfBodyBoneType::Nose) \
	op(ENVHalfBodyBoneType::NVHalfBodyBoneType_MAX) 

enum class ENVHalfBodyBoneType : uint8;
template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ENVHalfBodyBoneType>();

#define FOREACH_ENUM_ENVHALFBODYTYPE(op) \
	op(ENVHalfBodyType::LeftSide) \
	op(ENVHalfBodyType::RightSide) \
	op(ENVHalfBodyType::NVHalfBodyType_MAX) 

enum class ENVHalfBodyType : uint8;
template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ENVHalfBodyType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
