// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMaterialParam_ColorComponent_generated_h
#error "RandomMaterialParam_ColorComponent.generated.h already included, missing '#pragma once' in RandomMaterialParam_ColorComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMaterialParam_ColorComponent_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMaterialParam_ColorComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParam_ColorComponent, URandomMaterialParameterComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParam_ColorComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesURandomMaterialParam_ColorComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParam_ColorComponent, URandomMaterialParameterComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParam_ColorComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMaterialParam_ColorComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMaterialParam_ColorComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParam_ColorComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParam_ColorComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParam_ColorComponent(URandomMaterialParam_ColorComponent&&); \
	NO_API URandomMaterialParam_ColorComponent(const URandomMaterialParam_ColorComponent&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParam_ColorComponent(URandomMaterialParam_ColorComponent&&); \
	NO_API URandomMaterialParam_ColorComponent(const URandomMaterialParam_ColorComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParam_ColorComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParam_ColorComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMaterialParam_ColorComponent)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ColorParameterName() { return STRUCT_OFFSET(URandomMaterialParam_ColorComponent, ColorParameterName); } \
	FORCEINLINE static uint32 __PPO__ColorData() { return STRUCT_OFFSET(URandomMaterialParam_ColorComponent, ColorData); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_17_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMaterialParam_ColorComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_ColorComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
