// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomizedActor_generated_h
#error "RandomizedActor.generated.h already included, missing '#pragma once' in RandomizedActor.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomizedActor_generated_h

#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_RPC_WRAPPERS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARandomizedActor(); \
	friend struct Z_Construct_UClass_ARandomizedActor_Statics; \
public: \
	DECLARE_CLASS(ARandomizedActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ARandomizedActor)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARandomizedActor(); \
	friend struct Z_Construct_UClass_ARandomizedActor_Statics; \
public: \
	DECLARE_CLASS(ARandomizedActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ARandomizedActor)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARandomizedActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARandomizedActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomizedActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomizedActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomizedActor(ARandomizedActor&&); \
	NO_API ARandomizedActor(const ARandomizedActor&); \
public:


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomizedActor(ARandomizedActor&&); \
	NO_API ARandomizedActor(const ARandomizedActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomizedActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomizedActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARandomizedActor)


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMeshComp() { return STRUCT_OFFSET(ARandomizedActor, StaticMeshComp); } \
	FORCEINLINE static uint32 __PPO__RandomMeshComp() { return STRUCT_OFFSET(ARandomizedActor, RandomMeshComp); } \
	FORCEINLINE static uint32 __PPO__RandomMaterialComp() { return STRUCT_OFFSET(ARandomizedActor, RandomMaterialComp); } \
	FORCEINLINE static uint32 __PPO__RandomMaterialParam_ColorComp() { return STRUCT_OFFSET(ARandomizedActor, RandomMaterialParam_ColorComp); } \
	FORCEINLINE static uint32 __PPO__RandomVisibilityComp() { return STRUCT_OFFSET(ARandomizedActor, RandomVisibilityComp); } \
	FORCEINLINE static uint32 __PPO__RandomMovementComp() { return STRUCT_OFFSET(ARandomizedActor, RandomMovementComp); } \
	FORCEINLINE static uint32 __PPO__RandomRotationComp() { return STRUCT_OFFSET(ARandomizedActor, RandomRotationComp); } \
	FORCEINLINE static uint32 __PPO__RandomDataObjects() { return STRUCT_OFFSET(ARandomizedActor, RandomDataObjects); }


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_13_PROLOG
#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_RPC_WRAPPERS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_INCLASS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_INCLASS_NO_PURE_DECLS \
	Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ARandomizedActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
