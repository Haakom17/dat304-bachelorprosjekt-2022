// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneManager() {}
// Cross Module References
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneManager_NoRegister();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneManager_ResetState();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Class();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics
	{
		struct _Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms
		{
			ANVSceneManager* SceneManager;
			bool bIsSucceeded;
		};
		static void NewProp_bIsSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSucceeded;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneManager;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_bIsSucceeded_SetBit(void* Obj)
	{
		((_Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms*)Obj)->bIsSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_bIsSucceeded = { "bIsSucceeded", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms), &Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_bIsSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_SceneManager = { "SceneManager", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms, SceneManager), Z_Construct_UClass_ANVSceneManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_bIsSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::NewProp_SceneManager,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "Declaration for SetupCompleted multicast.\nThis event happens, when all instances are ready to capture." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer, nullptr, "NVSceneManger_SetupCompleted__DelegateSignature", sizeof(_Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* ENVSceneManagerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVSceneManagerState"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVSceneManagerState>()
	{
		return ENVSceneManagerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVSceneManagerState(ENVSceneManagerState_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVSceneManagerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState_Hash() { return 135155365U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVSceneManagerState"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVSceneManagerState::NotActive", (int64)ENVSceneManagerState::NotActive },
				{ "ENVSceneManagerState::Active", (int64)ENVSceneManagerState::Active },
				{ "ENVSceneManagerState::Ready", (int64)ENVSceneManagerState::Ready },
				{ "ENVSceneManagerState::Captured", (int64)ENVSceneManagerState::Captured },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Active.DisplayName", "This SceneManager is active." },
				{ "Active.ToolTip", "This Scene manager is active to capture the scene." },
				{ "BlueprintType", "true" },
				{ "Captured.DisplayName", "Capturing is done." },
				{ "Captured.ToolTip", "All scenes are captured." },
				{ "ModuleRelativePath", "Public/NVSceneManager.h" },
				{ "NotActive.DisplayName", "This SceneManager is not active." },
				{ "NotActive.ToolTip", "This Scene manager is not used." },
				{ "Ready.DisplayName", "Ready to capture." },
				{ "Ready.ToolTip", "This Scene manager is active and ready to capture or it is capturing now." },
				{ "ToolTip", "Capturing State." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVSceneManagerState",
				"ENVSceneManagerState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void ANVSceneManager::StaticRegisterNativesANVSceneManager()
	{
		UClass* Class = ANVSceneManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetANVSceneManagerPtr", &ANVSceneManager::execGetANVSceneManagerPtr },
			{ "OnCapturingCompleted", &ANVSceneManager::execOnCapturingCompleted },
			{ "ResetState", &ANVSceneManager::execResetState },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics
	{
		struct NVSceneManager_eventGetANVSceneManagerPtr_Parms
		{
			ANVSceneManager* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneManager_eventGetANVSceneManagerPtr_Parms, ReturnValue), Z_Construct_UClass_ANVSceneManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "Get singleton this instance.\nAfter lifecycle \"PostInitializeComponents\" is finished, this method is available." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneManager, nullptr, "GetANVSceneManagerPtr", sizeof(NVSceneManager_eventGetANVSceneManagerPtr_Parms), Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics
	{
		struct NVSceneManager_eventOnCapturingCompleted_Parms
		{
			ANVSceneCapturerActor* SceneCapturer;
			bool bIsSucceeded;
		};
		static void NewProp_bIsSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSucceeded;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapturer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_bIsSucceeded_SetBit(void* Obj)
	{
		((NVSceneManager_eventOnCapturingCompleted_Parms*)Obj)->bIsSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_bIsSucceeded = { "bIsSucceeded", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NVSceneManager_eventOnCapturingCompleted_Parms), &Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_bIsSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_SceneCapturer = { "SceneCapturer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneManager_eventOnCapturingCompleted_Parms, SceneCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_bIsSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::NewProp_SceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneManager, nullptr, "OnCapturingCompleted", sizeof(NVSceneManager_eventOnCapturingCompleted_Parms), Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneManager_ResetState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneManager_ResetState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "if state is CAPTURED, this change the state to READY." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneManager_ResetState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneManager, nullptr, "ResetState", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneManager_ResetState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneManager_ResetState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneManager_ResetState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneManager_ResetState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANVSceneManager_NoRegister()
	{
		return ANVSceneManager::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentMarkerIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CurrentMarkerIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneManagerState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SceneManagerState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SceneManagerState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentSceneMarker_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentSceneMarker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureExportDirNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneCaptureExportDirNames;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SceneCaptureExportDirNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCapturers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneCapturers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapturers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseMarkerNameAsPostfix_MetaData[];
#endif
		static void NewProp_bUseMarkerNameAsPostfix_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseMarkerNameAsPostfix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSetupCompleted_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSetupCompleted;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoExitAfterExportingComplete_MetaData[];
#endif
		static void NewProp_bAutoExitAfterExportingComplete_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoExitAfterExportingComplete;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureAtAllMarkers_MetaData[];
#endif
		static void NewProp_bCaptureAtAllMarkers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureAtAllMarkers;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMarkers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneMarkers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneMarkers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsActive_MetaData[];
#endif
		static void NewProp_bIsActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsActive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectInstanceSegmentation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectInstanceSegmentation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectClassSegmentation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectClassSegmentation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANVSceneManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANVSceneManager_GetANVSceneManagerPtr, "GetANVSceneManagerPtr" }, // 2309775963
		{ &Z_Construct_UFunction_ANVSceneManager_OnCapturingCompleted, "OnCapturingCompleted" }, // 3478818621
		{ &Z_Construct_UFunction_ANVSceneManager_ResetState, "ResetState" }, // 3366542199
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering" },
		{ "IncludePath", "NVSceneManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Actor representing the scene to be annotated and have its info captured and exported.\nAlthough multiple instances can be created, only one can be active for scene capturing at a time.\nAll other instances' 'bIsActive' property will be disabled when capturing starts.\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentMarkerIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentMarkerIndex = { "CurrentMarkerIndex", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, CurrentMarkerIndex), METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentMarkerIndex_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentMarkerIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState = { "SceneManagerState", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, SceneManagerState), Z_Construct_UEnum_NVSceneCapturer_ENVSceneManagerState, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentSceneMarker_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentSceneMarker = { "CurrentSceneMarker", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, CurrentSceneMarker), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentSceneMarker_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentSceneMarker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames = { "SceneCaptureExportDirNames", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, SceneCaptureExportDirNames), METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames_Inner = { "SceneCaptureExportDirNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "Transient properties\nKeep track of all the scene capturers in the map" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers = { "SceneCapturers", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, SceneCapturers), METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers_Inner = { "SceneCapturers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix_MetaData[] = {
		{ "Category", "NVSceneManager" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix_SetBit(void* Obj)
	{
		((ANVSceneManager*)Obj)->bUseMarkerNameAsPostfix = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix = { "bUseMarkerNameAsPostfix", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneManager), &Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_OnSetupCompleted_MetaData[] = {
		{ "Category", "Events" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_OnSetupCompleted = { "OnSetupCompleted", nullptr, (EPropertyFlags)0x0020080010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, OnSetupCompleted), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_OnSetupCompleted_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_OnSetupCompleted_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete_MetaData[] = {
		{ "Category", "NVSceneManager" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "If true, this exporter will automatically shutdown the game after it finish exporting" },
	};
#endif
	void Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete_SetBit(void* Obj)
	{
		((ANVSceneManager*)Obj)->bAutoExitAfterExportingComplete = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete = { "bAutoExitAfterExportingComplete", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneManager), &Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers_MetaData[] = {
		{ "Category", "NVSceneManager" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers_SetBit(void* Obj)
	{
		((ANVSceneManager*)Obj)->bCaptureAtAllMarkers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers = { "bCaptureAtAllMarkers", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneManager), &Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers_MetaData[] = {
		{ "Category", "NVSceneManager" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "List of anchors for point-of-interests" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers = { "SceneMarkers", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, SceneMarkers), METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers_Inner = { "SceneMarkers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive_MetaData[] = {
		{ "Category", "NVSceneManager" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
		{ "ToolTip", "Editor properties\nWhether this scene manager actively change the scene or not" },
	};
#endif
	void Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive_SetBit(void* Obj)
	{
		((ANVSceneManager*)Obj)->bIsActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive = { "bIsActive", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneManager), &Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectInstanceSegmentation_MetaData[] = {
		{ "Category", "CapturerScene" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectInstanceSegmentation = { "ObjectInstanceSegmentation", nullptr, (EPropertyFlags)0x0010008000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, ObjectInstanceSegmentation), Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectInstanceSegmentation_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectInstanceSegmentation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectClassSegmentation_MetaData[] = {
		{ "Category", "CapturerScene" },
		{ "ModuleRelativePath", "Public/NVSceneManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectClassSegmentation = { "ObjectClassSegmentation", nullptr, (EPropertyFlags)0x0010008000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneManager, ObjectClassSegmentation), Z_Construct_UScriptStruct_FNVObjectSegmentation_Class, METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectClassSegmentation_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectClassSegmentation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentMarkerIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneManagerState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_CurrentSceneMarker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCaptureExportDirNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneCapturers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bUseMarkerNameAsPostfix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_OnSetupCompleted,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bAutoExitAfterExportingComplete,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bCaptureAtAllMarkers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_SceneMarkers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_bIsActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectInstanceSegmentation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneManager_Statics::NewProp_ObjectClassSegmentation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneManager_Statics::ClassParams = {
		&ANVSceneManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANVSceneManager_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneManager_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVSceneManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneManager, 2904642664);
	template<> NVSCENECAPTURER_API UClass* StaticClass<ANVSceneManager>()
	{
		return ANVSceneManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneManager(Z_Construct_UClass_ANVSceneManager, &ANVSceneManager::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("ANVSceneManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
