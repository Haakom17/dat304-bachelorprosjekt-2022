// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneCapturerViewpointComponent_generated_h
#error "NVSceneCapturerViewpointComponent.generated.h already included, missing '#pragma once' in NVSceneCapturerViewpointComponent.h"
#endif
#define NVSCENECAPTURER_NVSceneCapturerViewpointComponent_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_22_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneCapturerViewpointSettings>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerViewpointComponent(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerViewpointComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerViewpointComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerViewpointComponent(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerViewpointComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerViewpointComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneCapturerViewpointComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerViewpointComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerViewpointComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerViewpointComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerViewpointComponent(UNVSceneCapturerViewpointComponent&&); \
	NO_API UNVSceneCapturerViewpointComponent(const UNVSceneCapturerViewpointComponent&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerViewpointComponent(UNVSceneCapturerViewpointComponent&&); \
	NO_API UNVSceneCapturerViewpointComponent(const UNVSceneCapturerViewpointComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerViewpointComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerViewpointComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerViewpointComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OwnerSceneCapturer() { return STRUCT_OFFSET(UNVSceneCapturerViewpointComponent, OwnerSceneCapturer); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_62_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h_66_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneCapturerViewpointComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerViewpointComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
