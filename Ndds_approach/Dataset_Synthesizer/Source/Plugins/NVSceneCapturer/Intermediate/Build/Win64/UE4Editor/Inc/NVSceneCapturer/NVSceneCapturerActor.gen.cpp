// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneCapturerActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerActor() {}
// Cross Module References
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature();
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVCameraSettingExportData();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVViewpointSettingExportData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVImageSize();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCameraIntrinsicSettings();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVCapturerSettingExportData();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerSettings();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVFrameCounter();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataHandler_NoRegister();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FTimerHandle();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVNamedImageSizePreset();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USphereComponent_NoRegister();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVFeatureExtractorSettings();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics
	{
		struct _Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms
		{
			ANVSceneCapturerActor* SceneCapturer;
			bool bIsSucceeded;
		};
		static void NewProp_bIsSucceeded_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSucceeded;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapturer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_bIsSucceeded_SetBit(void* Obj)
	{
		((_Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms*)Obj)->bIsSucceeded = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_bIsSucceeded = { "bIsSucceeded", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms), &Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_bIsSucceeded_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_SceneCapturer = { "SceneCapturer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms, SceneCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_bIsSucceeded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::NewProp_SceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer, nullptr, "NVSceneCapturer_Completed__DelegateSignature", sizeof(_Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics
	{
		struct _Script_NVSceneCapturer_eventNVSceneCapturer_Stopped_Parms
		{
			ANVSceneCapturerActor* SceneCapturer;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapturer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::NewProp_SceneCapturer = { "SceneCapturer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_NVSceneCapturer_eventNVSceneCapturer_Stopped_Parms, SceneCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::NewProp_SceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer, nullptr, "NVSceneCapturer_Stopped__DelegateSignature", sizeof(_Script_NVSceneCapturer_eventNVSceneCapturer_Stopped_Parms), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics
	{
		struct _Script_NVSceneCapturer_eventNVSceneCapturer_Started_Parms
		{
			ANVSceneCapturerActor* SceneCapturer;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapturer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::NewProp_SceneCapturer = { "SceneCapturer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_NVSceneCapturer_eventNVSceneCapturer_Started_Parms, SceneCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::NewProp_SceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer, nullptr, "NVSceneCapturer_Started__DelegateSignature", sizeof(_Script_NVSceneCapturer_eventNVSceneCapturer_Started_Parms), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FNVCameraSettingExportData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVCameraSettingExportData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVCameraSettingExportData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVCameraSettingExportData"), sizeof(FNVCameraSettingExportData), Get_Z_Construct_UScriptStruct_FNVCameraSettingExportData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVCameraSettingExportData>()
{
	return FNVCameraSettingExportData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVCameraSettingExportData(FNVCameraSettingExportData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVCameraSettingExportData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCameraSettingExportData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCameraSettingExportData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVCameraSettingExportData")),new UScriptStruct::TCppStructOps<FNVCameraSettingExportData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCameraSettingExportData;
	struct Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_camera_settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_camera_settings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_camera_settings_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVCameraSettingExportData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings = { "camera_settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCameraSettingExportData, camera_settings), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings_Inner = { "camera_settings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVViewpointSettingExportData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::NewProp_camera_settings_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVCameraSettingExportData",
		sizeof(FNVCameraSettingExportData),
		alignof(FNVCameraSettingExportData),
		Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVCameraSettingExportData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVCameraSettingExportData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVCameraSettingExportData"), sizeof(FNVCameraSettingExportData), Get_Z_Construct_UScriptStruct_FNVCameraSettingExportData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVCameraSettingExportData_Hash() { return 361979322U; }
class UScriptStruct* FNVViewpointSettingExportData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVViewpointSettingExportData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVViewpointSettingExportData"), sizeof(FNVViewpointSettingExportData), Get_Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVViewpointSettingExportData>()
{
	return FNVViewpointSettingExportData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVViewpointSettingExportData(FNVViewpointSettingExportData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVViewpointSettingExportData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVViewpointSettingExportData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVViewpointSettingExportData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVViewpointSettingExportData")),new UScriptStruct::TCppStructOps<FNVViewpointSettingExportData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVViewpointSettingExportData;
	struct Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_captured_image_size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_captured_image_size;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_intrinsic_settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_intrinsic_settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_horizontal_fov_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_horizontal_fov;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVViewpointSettingExportData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_CameraProjectionMatrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_CameraProjectionMatrix = { "CameraProjectionMatrix", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVViewpointSettingExportData, CameraProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_CameraProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_CameraProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_captured_image_size_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_captured_image_size = { "captured_image_size", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVViewpointSettingExportData, captured_image_size), Z_Construct_UScriptStruct_FNVImageSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_captured_image_size_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_captured_image_size_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_intrinsic_settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_intrinsic_settings = { "intrinsic_settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVViewpointSettingExportData, intrinsic_settings), Z_Construct_UScriptStruct_FCameraIntrinsicSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_intrinsic_settings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_intrinsic_settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_horizontal_fov_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Horizontal field-of-view" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_horizontal_fov = { "horizontal_fov", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVViewpointSettingExportData, horizontal_fov), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_horizontal_fov_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_horizontal_fov_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVViewpointSettingExportData, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_CameraProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_captured_image_size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_intrinsic_settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_horizontal_fov,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVViewpointSettingExportData",
		sizeof(FNVViewpointSettingExportData),
		alignof(FNVViewpointSettingExportData),
		Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVViewpointSettingExportData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVViewpointSettingExportData"), sizeof(FNVViewpointSettingExportData), Get_Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Hash() { return 4190431981U; }
class UScriptStruct* FNVCapturerSettingExportData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVCapturerSettingExportData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVCapturerSettingExportData"), sizeof(FNVCapturerSettingExportData), Get_Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVCapturerSettingExportData>()
{
	return FNVCapturerSettingExportData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVCapturerSettingExportData(FNVCapturerSettingExportData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVCapturerSettingExportData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCapturerSettingExportData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCapturerSettingExportData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVCapturerSettingExportData")),new UScriptStruct::TCppStructOps<FNVCapturerSettingExportData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCapturerSettingExportData;
	struct Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportedObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExportedObjects;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExportedObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportedObjectCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ExportedObjectCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturersSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturersSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVCapturerSettingExportData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects = { "ExportedObjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCapturerSettingExportData, ExportedObjects), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects_Inner = { "ExportedObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjectCount_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjectCount = { "ExportedObjectCount", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCapturerSettingExportData, ExportedObjectCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjectCount_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjectCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_CapturersSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_CapturersSettings = { "CapturersSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCapturerSettingExportData, CapturersSettings), Z_Construct_UScriptStruct_FNVSceneCapturerSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_CapturersSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_CapturersSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_ExportedObjectCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::NewProp_CapturersSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVCapturerSettingExportData",
		sizeof(FNVCapturerSettingExportData),
		alignof(FNVCapturerSettingExportData),
		Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVCapturerSettingExportData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVCapturerSettingExportData"), sizeof(FNVCapturerSettingExportData), Get_Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Hash() { return 2945444483U; }
class UScriptStruct* FNVSceneAnnotatedActorData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneAnnotatedActorData"), sizeof(FNVSceneAnnotatedActorData), Get_Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneAnnotatedActorData>()
{
	return FNVSceneAnnotatedActorData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneAnnotatedActorData(FNVSceneAnnotatedActorData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneAnnotatedActorData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneAnnotatedActorData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneAnnotatedActorData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSceneAnnotatedActorData")),new UScriptStruct::TCppStructOps<FNVSceneAnnotatedActorData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneAnnotatedActorData;
	struct Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_exported_objects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_exported_objects;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_exported_objects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_exported_object_classes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_exported_object_classes;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_exported_object_classes_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneAnnotatedActorData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects = { "exported_objects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneAnnotatedActorData, exported_objects), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects_Inner = { "exported_objects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes = { "exported_object_classes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneAnnotatedActorData, exported_object_classes), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes_Inner = { "exported_object_classes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_objects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::NewProp_exported_object_classes_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneAnnotatedActorData",
		sizeof(FNVSceneAnnotatedActorData),
		alignof(FNVSceneAnnotatedActorData),
		Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneAnnotatedActorData"), sizeof(FNVSceneAnnotatedActorData), Get_Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Hash() { return 1130759838U; }
class UScriptStruct* FNCapturerSettingExportedActorData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NCapturerSettingExportedActorData"), sizeof(FNCapturerSettingExportedActorData), Get_Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNCapturerSettingExportedActorData>()
{
	return FNCapturerSettingExportedActorData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNCapturerSettingExportedActorData(FNCapturerSettingExportedActorData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NCapturerSettingExportedActorData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNCapturerSettingExportedActorData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNCapturerSettingExportedActorData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NCapturerSettingExportedActorData")),new UScriptStruct::TCppStructOps<FNCapturerSettingExportedActorData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNCapturerSettingExportedActorData;
	struct Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorRef;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CuboidCenterLocal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CuboidCenterLocal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cuboid_dimensions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_cuboid_dimensions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fixed_model_transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_fixed_model_transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_segmentation_instance_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_segmentation_instance_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_segmentation_class_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_segmentation_class_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Class;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNCapturerSettingExportedActorData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_ActorRef_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_ActorRef = { "ActorRef", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, ActorRef), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_ActorRef_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_ActorRef_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_CuboidCenterLocal_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_CuboidCenterLocal = { "CuboidCenterLocal", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, CuboidCenterLocal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_CuboidCenterLocal_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_CuboidCenterLocal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_cuboid_dimensions_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_cuboid_dimensions = { "cuboid_dimensions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, cuboid_dimensions), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_cuboid_dimensions_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_cuboid_dimensions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_fixed_model_transform_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_fixed_model_transform = { "fixed_model_transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, fixed_model_transform), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_fixed_model_transform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_fixed_model_transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_instance_id_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_instance_id = { "segmentation_instance_id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, segmentation_instance_id), METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_instance_id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_instance_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_class_id_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_class_id = { "segmentation_class_id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, segmentation_class_id), METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_class_id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_class_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_Class_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNCapturerSettingExportedActorData, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_Class_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_Class_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_ActorRef,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_CuboidCenterLocal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_cuboid_dimensions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_fixed_model_transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_instance_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_segmentation_class_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::NewProp_Class,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NCapturerSettingExportedActorData",
		sizeof(FNCapturerSettingExportedActorData),
		alignof(FNCapturerSettingExportedActorData),
		Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NCapturerSettingExportedActorData"), sizeof(FNCapturerSettingExportedActorData), Get_Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Hash() { return 3486278622U; }
	void ANVSceneCapturerActor::StaticRegisterNativesANVSceneCapturerActor()
	{
		UClass* Class = ANVSceneCapturerActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCapturedDuration", &ANVSceneCapturerActor::execGetCapturedDuration },
			{ "GetCapturedFPS", &ANVSceneCapturerActor::execGetCapturedFPS },
			{ "GetCapturedFrameCounter", &ANVSceneCapturerActor::execGetCapturedFrameCounter },
			{ "GetCaptureProgressFraction", &ANVSceneCapturerActor::execGetCaptureProgressFraction },
			{ "GetCurrentState", &ANVSceneCapturerActor::execGetCurrentState },
			{ "GetEstimatedTimeUntilFinishCapturing", &ANVSceneCapturerActor::execGetEstimatedTimeUntilFinishCapturing },
			{ "GetExportedFrameCount", &ANVSceneCapturerActor::execGetExportedFrameCount },
			{ "GetNumberOfFramesLeftToCapture", &ANVSceneCapturerActor::execGetNumberOfFramesLeftToCapture },
			{ "GetNumberOfFramesToCapture", &ANVSceneCapturerActor::execGetNumberOfFramesToCapture },
			{ "GetSceneDataHandler", &ANVSceneCapturerActor::execGetSceneDataHandler },
			{ "GetViewpointList", &ANVSceneCapturerActor::execGetViewpointList },
			{ "PauseCapturing", &ANVSceneCapturerActor::execPauseCapturing },
			{ "ResumeCapturing", &ANVSceneCapturerActor::execResumeCapturing },
			{ "ReturnViewportToPlayerController", &ANVSceneCapturerActor::execReturnViewportToPlayerController },
			{ "SetNumberOfFramesToCapture", &ANVSceneCapturerActor::execSetNumberOfFramesToCapture },
			{ "StartCapturing", &ANVSceneCapturerActor::execStartCapturing },
			{ "StopCapturing", &ANVSceneCapturerActor::execStopCapturing },
			{ "TakeOverViewport", &ANVSceneCapturerActor::execTakeOverViewport },
			{ "ToggleTakeOverViewport", &ANVSceneCapturerActor::execToggleTakeOverViewport },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics
	{
		struct NVSceneCapturerActor_eventGetCapturedDuration_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetCapturedDuration_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetCapturedDuration", sizeof(NVSceneCapturerActor_eventGetCapturedDuration_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics
	{
		struct NVSceneCapturerActor_eventGetCapturedFPS_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetCapturedFPS_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Capturing information" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetCapturedFPS", sizeof(NVSceneCapturerActor_eventGetCapturedFPS_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics
	{
		struct NVSceneCapturerActor_eventGetCapturedFrameCounter_Parms
		{
			FNVFrameCounter ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetCapturedFrameCounter_Parms, ReturnValue), Z_Construct_UScriptStruct_FNVFrameCounter, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Frame counters" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetCapturedFrameCounter", sizeof(NVSceneCapturerActor_eventGetCapturedFrameCounter_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics
	{
		struct NVSceneCapturerActor_eventGetCaptureProgressFraction_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetCaptureProgressFraction_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetCaptureProgressFraction", sizeof(NVSceneCapturerActor_eventGetCaptureProgressFraction_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics
	{
		struct NVSceneCapturerActor_eventGetCurrentState_Parms
		{
			ENVSceneCapturerState ReturnValue;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetCurrentState_Parms, ReturnValue), Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::NewProp_ReturnValue_Underlying,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetCurrentState", sizeof(NVSceneCapturerActor_eventGetCurrentState_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics
	{
		struct NVSceneCapturerActor_eventGetEstimatedTimeUntilFinishCapturing_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetEstimatedTimeUntilFinishCapturing_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetEstimatedTimeUntilFinishCapturing", sizeof(NVSceneCapturerActor_eventGetEstimatedTimeUntilFinishCapturing_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics
	{
		struct NVSceneCapturerActor_eventGetExportedFrameCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetExportedFrameCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetExportedFrameCount", sizeof(NVSceneCapturerActor_eventGetExportedFrameCount_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics
	{
		struct NVSceneCapturerActor_eventGetNumberOfFramesLeftToCapture_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetNumberOfFramesLeftToCapture_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetNumberOfFramesLeftToCapture", sizeof(NVSceneCapturerActor_eventGetNumberOfFramesLeftToCapture_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics
	{
		struct NVSceneCapturerActor_eventGetNumberOfFramesToCapture_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetNumberOfFramesToCapture_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetNumberOfFramesToCapture", sizeof(NVSceneCapturerActor_eventGetNumberOfFramesToCapture_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics
	{
		struct NVSceneCapturerActor_eventGetSceneDataHandler_Parms
		{
			UNVSceneDataHandler* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetSceneDataHandler_Parms, ReturnValue), Z_Construct_UClass_UNVSceneDataHandler_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Control what to do with the captured scene data" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetSceneDataHandler", sizeof(NVSceneCapturerActor_eventGetSceneDataHandler_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics
	{
		struct NVSceneCapturerActor_eventGetViewpointList_Parms
		{
			TArray<UNVSceneCapturerViewpointComponent*> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventGetViewpointList_Parms, ReturnValue), METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::NewProp_ReturnValue_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "GetViewpointList", sizeof(NVSceneCapturerActor_eventGetViewpointList_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "PauseCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "ResumeCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "ReturnViewportToPlayerController", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics
	{
		struct NVSceneCapturerActor_eventSetNumberOfFramesToCapture_Parms
		{
			int32 NewSceneCount;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewSceneCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::NewProp_NewSceneCount = { "NewSceneCount", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerActor_eventSetNumberOfFramesToCapture_Parms, NewSceneCount), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::NewProp_NewSceneCount,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Setter and Getter for Number of scene to capture" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "SetNumberOfFramesToCapture", sizeof(NVSceneCapturerActor_eventSetNumberOfFramesToCapture_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Capture controls" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "StartCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "StopCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "TakeOverViewport", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics
	{
		struct NVSceneCapturerActor_eventToggleTakeOverViewport_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((NVSceneCapturerActor_eventToggleTakeOverViewport_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(NVSceneCapturerActor_eventToggleTakeOverViewport_Parms), &Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "return false means PlayerController view.\n       true means Viewport is taken over." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerActor, nullptr, "ToggleTakeOverViewport", sizeof(NVSceneCapturerActor_eventToggleTakeOverViewport_Parms), Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister()
	{
		return ANVSceneCapturerActor::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneCapturerActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageToCapturePerFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ImageToCapturePerFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewpointList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewpointList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ViewpointList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeHandle_StartCapturingDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TimeHandle_StartCapturingDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSkipFirstFrame_MetaData[];
#endif
		static void NewProp_bSkipFirstFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSkipFirstFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTakingOverViewport_MetaData[];
#endif
		static void NewProp_bTakingOverViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTakingOverViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bNeedToExportScene_MetaData[];
#endif
		static void NewProp_bNeedToExportScene_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNeedToExportScene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberOfFramesToCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberOfFramesToCapture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CurrentState;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CurrentState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPlayerControllerViewTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedPlayerControllerViewTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedFrameCounter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedFrameCounter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCaptureTimestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LastCaptureTimestamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartCapturingDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartCapturingDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CapturedDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartCapturingTimestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartCapturingTimestamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageSizePresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ImageSizePresets;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageSizePresets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPauseGameLogicWhenFlushing_MetaData[];
#endif
		static void NewProp_bPauseGameLogicWhenFlushing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPauseGameLogicWhenFlushing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDataVisualizer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDataVisualizer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDataHandler_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDataHandler;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bTakeOverGameViewport_MetaData[];
#endif
		static void NewProp_bTakeOverGameViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bTakeOverGameViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberOfFramesToCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumberOfFramesToCapture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeBetweenSceneCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeBetweenSceneCapture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoStartCapturing_MetaData[];
#endif
		static void NewProp_bAutoStartCapturing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoStartCapturing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureExtractorSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeatureExtractorSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeatureExtractorSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturerSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturerSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsActive_MetaData[];
#endif
		static void NewProp_bIsActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsActive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCompletedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCompletedEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStoppedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStoppedEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStartedEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStartedEvent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneCapturerActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANVSceneCapturerActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedDuration, "GetCapturedDuration" }, // 3393374890
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFPS, "GetCapturedFPS" }, // 2059002913
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetCapturedFrameCounter, "GetCapturedFrameCounter" }, // 655633794
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetCaptureProgressFraction, "GetCaptureProgressFraction" }, // 3694479634
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetCurrentState, "GetCurrentState" }, // 3420361198
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetEstimatedTimeUntilFinishCapturing, "GetEstimatedTimeUntilFinishCapturing" }, // 1795216228
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetExportedFrameCount, "GetExportedFrameCount" }, // 1282794043
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesLeftToCapture, "GetNumberOfFramesLeftToCapture" }, // 2738752973
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetNumberOfFramesToCapture, "GetNumberOfFramesToCapture" }, // 1571906441
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetSceneDataHandler, "GetSceneDataHandler" }, // 3303125176
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_GetViewpointList, "GetViewpointList" }, // 970040450
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_PauseCapturing, "PauseCapturing" }, // 2815902233
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_ResumeCapturing, "ResumeCapturing" }, // 2359596104
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_ReturnViewportToPlayerController, "ReturnViewportToPlayerController" }, // 2485012347
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_SetNumberOfFramesToCapture, "SetNumberOfFramesToCapture" }, // 3804340238
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_StartCapturing, "StartCapturing" }, // 4285899991
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_StopCapturing, "StopCapturing" }, // 49800160
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_TakeOverViewport, "TakeOverViewport" }, // 836066556
		{ &Z_Construct_UFunction_ANVSceneCapturerActor_ToggleTakeOverViewport, "ToggleTakeOverViewport" }, // 525409205
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering Collision Physics Navigation Shape Cooking HLOD Mobile" },
		{ "IncludePath", "NVSceneCapturerActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "The scene exporter actor.\n\n@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageToCapturePerFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageToCapturePerFrame = { "ImageToCapturePerFrame", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, ImageToCapturePerFrame), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageToCapturePerFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageToCapturePerFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList = { "ViewpointList", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, ViewpointList), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList_Inner = { "ViewpointList", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeHandle_StartCapturingDelay_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeHandle_StartCapturingDelay = { "TimeHandle_StartCapturingDelay", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, TimeHandle_StartCapturingDelay), Z_Construct_UScriptStruct_FTimerHandle, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeHandle_StartCapturingDelay_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeHandle_StartCapturingDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bSkipFirstFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame = { "bSkipFirstFrame", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bTakingOverViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport = { "bTakingOverViewport", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bNeedToExportScene = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene = { "bNeedToExportScene", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_NumberOfFramesToCapture_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_NumberOfFramesToCapture = { "NumberOfFramesToCapture", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, NumberOfFramesToCapture), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_NumberOfFramesToCapture_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_NumberOfFramesToCapture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState = { "CurrentState", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CurrentState), Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CachedPlayerControllerViewTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CachedPlayerControllerViewTarget = { "CachedPlayerControllerViewTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CachedPlayerControllerViewTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CachedPlayerControllerViewTarget_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CachedPlayerControllerViewTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedFrameCounter_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedFrameCounter = { "CapturedFrameCounter", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CapturedFrameCounter), Z_Construct_UScriptStruct_FNVFrameCounter, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedFrameCounter_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedFrameCounter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_LastCaptureTimestamp_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_LastCaptureTimestamp = { "LastCaptureTimestamp", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, LastCaptureTimestamp), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_LastCaptureTimestamp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_LastCaptureTimestamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingDuration_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingDuration = { "StartCapturingDuration", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, StartCapturingDuration), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedDuration_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedDuration = { "CapturedDuration", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CapturedDuration), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedDuration_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingTimestamp_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingTimestamp = { "StartCapturingTimestamp", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, StartCapturingTimestamp), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingTimestamp_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingTimestamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "List of available image size presets" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets = { "ImageSizePresets", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, ImageSizePresets), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets_Inner = { "ImageSizePresets", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVNamedImageSizePreset, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "If true, the capturer will pause the game logic when it's trying to flushing - handle the scene data from previous frame" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bPauseGameLogicWhenFlushing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing = { "bPauseGameLogicWhenFlushing", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataVisualizer_MetaData[] = {
		{ "Category", "Capture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Control what to do with the captured scene data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataVisualizer = { "SceneDataVisualizer", nullptr, (EPropertyFlags)0x002208000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, SceneDataVisualizer), Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataVisualizer_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataVisualizer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataHandler_MetaData[] = {
		{ "Category", "Capture" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Control what to do with the captured scene data" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataHandler = { "SceneDataHandler", nullptr, (EPropertyFlags)0x002208000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, SceneDataHandler), Z_Construct_UClass_UNVSceneDataHandler_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataHandler_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataHandler_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "If true, the player's camera will be tied to this exporter's location and rotation" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bTakeOverGameViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport = { "bTakeOverGameViewport", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_MaxNumberOfFramesToCapture_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Maximum number of scenes (>= 0) to export before stopping\nNOTE: If TotalNumberOfScenesToExport == 0 then the exporter will keep exporting without limit until told to stop" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_MaxNumberOfFramesToCapture = { "MaxNumberOfFramesToCapture", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, MaxNumberOfFramesToCapture), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_MaxNumberOfFramesToCapture_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_MaxNumberOfFramesToCapture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeBetweenSceneCapture_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "NOTE: TimeBetweenSceneExport <= 0 mean export every frame" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeBetweenSceneCapture = { "TimeBetweenSceneCapture", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, TimeBetweenSceneCapture), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeBetweenSceneCapture_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeBetweenSceneCapture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "If true, this capturer will automatically start capturing the scene right when the game start (every TimeBetweenSceneExport seconds)" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bAutoStartCapturing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing = { "bAutoStartCapturing", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CollisionComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "NVSceneCapturerActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Editor properties\nCollision of the capturer actor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CollisionComponent = { "CollisionComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CollisionComponent), Z_Construct_UClass_USphereComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CollisionComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CollisionComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings_MetaData[] = {
		{ "Category", "FeatureExtraction" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "List of the feature extractors this capturer support" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings = { "FeatureExtractorSettings", nullptr, (EPropertyFlags)0x0010028000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, FeatureExtractorSettings), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings_Inner = { "FeatureExtractorSettings", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVFeatureExtractorSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturerSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturerSettings = { "CapturerSettings", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, CapturerSettings), Z_Construct_UScriptStruct_FNVSceneCapturerSettings, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturerSettings_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturerSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Editor properties\nWhether this capturer actor is active and can start capturing or not" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive_SetBit(void* Obj)
	{
		((ANVSceneCapturerActor*)Obj)->bIsActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive = { "bIsActive", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerActor), &Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnCompletedEvent_MetaData[] = {
		{ "Category", "Events" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnCompletedEvent = { "OnCompletedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, OnCompletedEvent), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnCompletedEvent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnCompletedEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStoppedEvent_MetaData[] = {
		{ "Category", "Events" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStoppedEvent = { "OnStoppedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, OnStoppedEvent), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStoppedEvent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStoppedEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStartedEvent_MetaData[] = {
		{ "Category", "Events" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerActor.h" },
		{ "ToolTip", "Event properties" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStartedEvent = { "OnStartedEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::MulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerActor, OnStartedEvent), Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStartedEvent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStartedEvent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneCapturerActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageToCapturePerFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ViewpointList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeHandle_StartCapturingDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bSkipFirstFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakingOverViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bNeedToExportScene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_NumberOfFramesToCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CurrentState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CachedPlayerControllerViewTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedFrameCounter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_LastCaptureTimestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturedDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_StartCapturingTimestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_ImageSizePresets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bPauseGameLogicWhenFlushing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataVisualizer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_SceneDataHandler,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bTakeOverGameViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_MaxNumberOfFramesToCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_TimeBetweenSceneCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bAutoStartCapturing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CollisionComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_FeatureExtractorSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_CapturerSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_bIsActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnCompletedEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStoppedEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerActor_Statics::NewProp_OnStartedEvent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneCapturerActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneCapturerActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneCapturerActor_Statics::ClassParams = {
		&ANVSceneCapturerActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANVSceneCapturerActor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneCapturerActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneCapturerActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneCapturerActor, 4062693735);
	template<> NVSCENECAPTURER_API UClass* StaticClass<ANVSceneCapturerActor>()
	{
		return ANVSceneCapturerActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneCapturerActor(Z_Construct_UClass_ANVSceneCapturerActor, &ANVSceneCapturerActor::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("ANVSceneCapturerActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneCapturerActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
