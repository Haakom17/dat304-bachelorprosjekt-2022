// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVCoordinateComponent_generated_h
#error "NVCoordinateComponent.generated.h already included, missing '#pragma once' in NVCoordinateComponent.h"
#endif
#define NVSCENECAPTURER_NVCoordinateComponent_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVCoordinateComponent(); \
	friend struct Z_Construct_UClass_UNVCoordinateComponent_Statics; \
public: \
	DECLARE_CLASS(UNVCoordinateComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVCoordinateComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUNVCoordinateComponent(); \
	friend struct Z_Construct_UClass_UNVCoordinateComponent_Statics; \
public: \
	DECLARE_CLASS(UNVCoordinateComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVCoordinateComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVCoordinateComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVCoordinateComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVCoordinateComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVCoordinateComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVCoordinateComponent(UNVCoordinateComponent&&); \
	NO_API UNVCoordinateComponent(const UNVCoordinateComponent&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVCoordinateComponent(UNVCoordinateComponent&&); \
	NO_API UNVCoordinateComponent(const UNVCoordinateComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVCoordinateComponent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVCoordinateComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVCoordinateComponent)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_17_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVCoordinateComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCoordinateComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
