// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ANVSceneManager;
class ANVSceneCapturerActor;
#ifdef NVSCENECAPTURER_NVSceneManager_generated_h
#error "NVSceneManager.generated.h already included, missing '#pragma once' in NVSceneManager.h"
#endif
#define NVSCENECAPTURER_NVSceneManager_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_40_DELEGATE \
struct _Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms \
{ \
	ANVSceneManager* SceneManager; \
	bool bIsSucceeded; \
}; \
static inline void FNVSceneManger_SetupCompleted_DelegateWrapper(const FMulticastScriptDelegate& NVSceneManger_SetupCompleted, ANVSceneManager* SceneManager, bool bIsSucceeded) \
{ \
	_Script_NVSceneCapturer_eventNVSceneManger_SetupCompleted_Parms Parms; \
	Parms.SceneManager=SceneManager; \
	Parms.bIsSucceeded=bIsSucceeded ? true : false; \
	NVSceneManger_SetupCompleted.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCapturingCompleted) \
	{ \
		P_GET_OBJECT(ANVSceneCapturerActor,Z_Param_SceneCapturer); \
		P_GET_UBOOL(Z_Param_bIsSucceeded); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCapturingCompleted(Z_Param_SceneCapturer,Z_Param_bIsSucceeded); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetANVSceneManagerPtr) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ANVSceneManager**)Z_Param__Result=ANVSceneManager::GetANVSceneManagerPtr(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCapturingCompleted) \
	{ \
		P_GET_OBJECT(ANVSceneCapturerActor,Z_Param_SceneCapturer); \
		P_GET_UBOOL(Z_Param_bIsSucceeded); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCapturingCompleted(Z_Param_SceneCapturer,Z_Param_bIsSucceeded); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResetState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResetState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetANVSceneManagerPtr) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ANVSceneManager**)Z_Param__Result=ANVSceneManager::GetANVSceneManagerPtr(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneManager(); \
	friend struct Z_Construct_UClass_ANVSceneManager_Statics; \
public: \
	DECLARE_CLASS(ANVSceneManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneManager)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneManager(); \
	friend struct Z_Construct_UClass_ANVSceneManager_Statics; \
public: \
	DECLARE_CLASS(ANVSceneManager, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneManager)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneManager(ANVSceneManager&&); \
	NO_API ANVSceneManager(const ANVSceneManager&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneManager(ANVSceneManager&&); \
	NO_API ANVSceneManager(const ANVSceneManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneManager); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneManager)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsActive() { return STRUCT_OFFSET(ANVSceneManager, bIsActive); } \
	FORCEINLINE static uint32 __PPO__SceneMarkers() { return STRUCT_OFFSET(ANVSceneManager, SceneMarkers); } \
	FORCEINLINE static uint32 __PPO__bCaptureAtAllMarkers() { return STRUCT_OFFSET(ANVSceneManager, bCaptureAtAllMarkers); } \
	FORCEINLINE static uint32 __PPO__bAutoExitAfterExportingComplete() { return STRUCT_OFFSET(ANVSceneManager, bAutoExitAfterExportingComplete); } \
	FORCEINLINE static uint32 __PPO__OnSetupCompleted() { return STRUCT_OFFSET(ANVSceneManager, OnSetupCompleted); } \
	FORCEINLINE static uint32 __PPO__bUseMarkerNameAsPostfix() { return STRUCT_OFFSET(ANVSceneManager, bUseMarkerNameAsPostfix); } \
	FORCEINLINE static uint32 __PPO__SceneCapturers() { return STRUCT_OFFSET(ANVSceneManager, SceneCapturers); } \
	FORCEINLINE static uint32 __PPO__SceneCaptureExportDirNames() { return STRUCT_OFFSET(ANVSceneManager, SceneCaptureExportDirNames); } \
	FORCEINLINE static uint32 __PPO__CurrentSceneMarker() { return STRUCT_OFFSET(ANVSceneManager, CurrentSceneMarker); } \
	FORCEINLINE static uint32 __PPO__SceneManagerState() { return STRUCT_OFFSET(ANVSceneManager, SceneManagerState); } \
	FORCEINLINE static uint32 __PPO__CurrentMarkerIndex() { return STRUCT_OFFSET(ANVSceneManager, CurrentMarkerIndex); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_48_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h_52_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class ANVSceneManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneManager_h


#define FOREACH_ENUM_ENVSCENEMANAGERSTATE(op) \
	op(ENVSceneManagerState::NotActive) \
	op(ENVSceneManagerState::Active) \
	op(ENVSceneManagerState::Ready) \
	op(ENVSceneManagerState::Captured) 

enum class ENVSceneManagerState : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVSceneManagerState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
