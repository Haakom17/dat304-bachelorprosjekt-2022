// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneMarker.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneMarker() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneMarkerComponent();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneMarkerInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
class UScriptStruct* FNVSceneMarkerComponent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneMarkerComponent, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneMarkerComponent"), sizeof(FNVSceneMarkerComponent), Get_Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneMarkerComponent>()
{
	return FNVSceneMarkerComponent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneMarkerComponent(FNVSceneMarkerComponent::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneMarkerComponent"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneMarkerComponent
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneMarkerComponent()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSceneMarkerComponent")),new UScriptStruct::TCppStructOps<FNVSceneMarkerComponent>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneMarkerComponent;
	struct Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Observers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Observers;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Observers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsActive_MetaData[];
#endif
		static void NewProp_bIsActive_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsActive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
		{ "ToolTip", "============================================== FNVSceneMarkerComponent ==============================================\n\n Anchor for point of interest in map." },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneMarkerComponent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers = { "Observers", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneMarkerComponent, Observers), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers_Inner = { "Observers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "NVSceneMarkerComponent" },
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneMarkerComponent, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Description_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "NVSceneMarkerComponent" },
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneMarkerComponent, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_DisplayName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive_MetaData[] = {
		{ "Category", "NVSceneMarkerComponent" },
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
		{ "ToolTip", "Editor properties\nToDo: move to protected.\nScene markers can be manually deactivated via toggling the IsActive attribute to allow the user to control which subset of scene markers gets captured and exported." },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive_SetBit(void* Obj)
	{
		((FNVSceneMarkerComponent*)Obj)->bIsActive = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive = { "bIsActive", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneMarkerComponent), &Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Observers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::NewProp_bIsActive,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneMarkerComponent",
		sizeof(FNVSceneMarkerComponent),
		alignof(FNVSceneMarkerComponent),
		Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneMarkerComponent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneMarkerComponent"), sizeof(FNVSceneMarkerComponent), Get_Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Hash() { return 3590675878U; }
	void UNVSceneMarkerInterface::StaticRegisterNativesUNVSceneMarkerInterface()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister()
	{
		return UNVSceneMarkerInterface::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneMarkerInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneMarkerInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneMarkerInterface_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneMarker.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneMarkerInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<INVSceneMarkerInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneMarkerInterface_Statics::ClassParams = {
		&UNVSceneMarkerInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneMarkerInterface_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneMarkerInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneMarkerInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneMarkerInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneMarkerInterface, 18018530);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneMarkerInterface>()
	{
		return UNVSceneMarkerInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneMarkerInterface(Z_Construct_UClass_UNVSceneMarkerInterface, &UNVSceneMarkerInterface::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneMarkerInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneMarkerInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
