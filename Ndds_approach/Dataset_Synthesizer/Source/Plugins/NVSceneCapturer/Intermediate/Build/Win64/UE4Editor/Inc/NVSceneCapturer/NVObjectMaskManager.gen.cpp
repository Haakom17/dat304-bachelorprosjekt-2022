// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVObjectMaskManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVObjectMaskManager() {}
// Cross Module References
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Class();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger_Stencil_NoRegister();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger_Stencil();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVObjectMaskMananger_VertexColor();
// End Cross Module References
	static UEnum* ENVActorClassSegmentationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVActorClassSegmentationType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVActorClassSegmentationType>()
	{
		return ENVActorClassSegmentationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVActorClassSegmentationType(ENVActorClassSegmentationType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVActorClassSegmentationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType_Hash() { return 4082342907U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVActorClassSegmentationType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVActorClassSegmentationType::UseActorMeshName", (int64)ENVActorClassSegmentationType::UseActorMeshName },
				{ "ENVActorClassSegmentationType::UseActorTag", (int64)ENVActorClassSegmentationType::UseActorTag },
				{ "ENVActorClassSegmentationType::UseActorClassName", (int64)ENVActorClassSegmentationType::UseActorClassName },
				{ "ENVActorClassSegmentationType::ENVActorClassSegmentationType_MAX", (int64)ENVActorClassSegmentationType::ENVActorClassSegmentationType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ENVActorClassSegmentationType_MAX.Hidden", "" },
				{ "ENVActorClassSegmentationType_MAX.ToolTip", "@endcond DOXYGEN_SUPPRESSED_CODE" },
				{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
				{ "ToolTip", "This enum describe how to get the class type out of an actor to use for segmentation" },
				{ "UseActorClassName.ToolTip", "Use the actor's class (either C++ or blueprint) name for its class name. All the actor instances of the same class/blueprint will have the same mask" },
				{ "UseActorMeshName.ToolTip", "Use the actor's mesh name for its class type name, actor with no visible mesh will be ignored\nNOTE: Since each actor can have multiple mesh components, we only use the first valid mesh's name for the mask" },
				{ "UseActorTag.ToolTip", "Use the actor's Tags for its mask name, actor with no tags will be ignored\nNOTE: Since each actor can have multiple tags, we only use the first one in the Tags list for the mask" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVActorClassSegmentationType",
				"ENVActorClassSegmentationType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVIdAssignmentType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVIdAssignmentType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVIdAssignmentType>()
	{
		return ENVIdAssignmentType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVIdAssignmentType(ENVIdAssignmentType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVIdAssignmentType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType_Hash() { return 2789361745U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVIdAssignmentType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVIdAssignmentType::Sequential", (int64)ENVIdAssignmentType::Sequential },
				{ "ENVIdAssignmentType::SpreadEvenly", (int64)ENVIdAssignmentType::SpreadEvenly },
				{ "ENVIdAssignmentType::NVActorMaskIdType_MAX", (int64)ENVIdAssignmentType::NVActorMaskIdType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
				{ "NVActorMaskIdType_MAX.Hidden", "" },
				{ "NVActorMaskIdType_MAX.ToolTip", "@endcond DOXYGEN_SUPPRESSED_CODE" },
				{ "Sequential.ToolTip", "The id will be given sequentially to each masks" },
				{ "SpreadEvenly.ToolTip", "The id will be spread evenly between mask\nThe gap between id = MaxMaskValue / NumberOfMasks" },
				{ "ToolTip", "This enum describe how to assign an id for a mask\nThe reason for the \"spread evenly\" is for easy visualization.\nIDs are translated to color linearly (grayscale 8 bits for stencil mask,\nRGBA8 for vertex color mask).When the number of objects is small,\nthe mask images all look black and thus are hard to distinguish.\nIn this case \"spread evenly\" is useful to emphasize the color difference for\nvisibility.However, for all other scenarios, such as DL training,\nthe color doesn't matter and a sequentially allocating IDs may be easier to debug.\nIt also helps in cases where on is manually assigning additional IDs --\none need only increase the max ID." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVIdAssignmentType",
				"ENVIdAssignmentType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVActorMaskNameType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVActorMaskNameType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVActorMaskNameType>()
	{
		return ENVActorMaskNameType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVActorMaskNameType(ENVActorMaskNameType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVActorMaskNameType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType_Hash() { return 3283260221U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVActorMaskNameType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVActorMaskNameType::UseActorInstanceName", (int64)ENVActorMaskNameType::UseActorInstanceName },
				{ "ENVActorMaskNameType::UseActorMeshName", (int64)ENVActorMaskNameType::UseActorMeshName },
				{ "ENVActorMaskNameType::UseActorTag", (int64)ENVActorMaskNameType::UseActorTag },
				{ "ENVActorMaskNameType::UseActorClassName", (int64)ENVActorMaskNameType::UseActorClassName },
				{ "ENVActorMaskNameType::NVObjectClassMaskType_MAX", (int64)ENVActorMaskNameType::NVObjectClassMaskType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
				{ "NVObjectClassMaskType_MAX.Hidden", "" },
				{ "NVObjectClassMaskType_MAX.ToolTip", "@endcond DOXYGEN_SUPPRESSED_CODE" },
				{ "ToolTip", "This enum describe how to get the mask name out of an actor" },
				{ "UseActorClassName.ToolTip", "Use the actor's class (either C++ or blueprint) name for its mask name. All the actor instances of the same class/blueprint will have the same mask" },
				{ "UseActorInstanceName.ToolTip", "Use the actor's instance name for its mask name. Each actors in the scene will have a unique name\nso this option will cause all the actors to  have its own unique mask.\nNOTE: Should only use this option for VertexColor mask since the total number of masks can be huge" },
				{ "UseActorMeshName.ToolTip", "Use the actor's mesh name for its mask name, actor with no visible mesh will be ignored\nNOTE: Since each actor can have multiple mesh components, we only use the first valid mesh's name for the mask" },
				{ "UseActorTag.ToolTip", "Use the actor's Tags for its mask name, actor with no tags will be ignored\nNOTE: Since each actor can have multiple tags, we only use the first one in the Tags list for the mask" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVActorMaskNameType",
				"ENVActorMaskNameType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNVObjectSegmentation_Class::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVObjectSegmentation_Class"), sizeof(FNVObjectSegmentation_Class), Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVObjectSegmentation_Class>()
{
	return FNVObjectSegmentation_Class::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVObjectSegmentation_Class(FNVObjectSegmentation_Class::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVObjectSegmentation_Class"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Class
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Class()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVObjectSegmentation_Class")),new UScriptStruct::TCppStructOps<FNVObjectSegmentation_Class>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Class;
	struct Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StencilMaskManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StencilMaskManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentationIdAssignmentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SegmentationIdAssignmentType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SegmentationIdAssignmentType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClassSegmentationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ClassSegmentationType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ClassSegmentationType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVObjectSegmentation_Class>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_StencilMaskManager_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_StencilMaskManager = { "StencilMaskManager", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVObjectSegmentation_Class, StencilMaskManager), Z_Construct_UClass_UNVObjectMaskMananger_Stencil_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_StencilMaskManager_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_StencilMaskManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType_MetaData[] = {
		{ "Category", "Segmentation" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType = { "SegmentationIdAssignmentType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVObjectSegmentation_Class, SegmentationIdAssignmentType), Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType_MetaData[] = {
		{ "Category", "Segmentation" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Editor properties\nHow to get the class type out of actors in the scene to use for segmentation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType = { "ClassSegmentationType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVObjectSegmentation_Class, ClassSegmentationType), Z_Construct_UEnum_NVSceneCapturer_ENVActorClassSegmentationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_StencilMaskManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_SegmentationIdAssignmentType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::NewProp_ClassSegmentationType_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVObjectSegmentation_Class",
		sizeof(FNVObjectSegmentation_Class),
		alignof(FNVObjectSegmentation_Class),
		Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Class()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVObjectSegmentation_Class"), sizeof(FNVObjectSegmentation_Class), Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Hash() { return 3436134405U; }
class UScriptStruct* FNVObjectSegmentation_Instance::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVObjectSegmentation_Instance"), sizeof(FNVObjectSegmentation_Instance), Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVObjectSegmentation_Instance>()
{
	return FNVObjectSegmentation_Instance::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVObjectSegmentation_Instance(FNVObjectSegmentation_Instance::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVObjectSegmentation_Instance"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Instance
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Instance()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVObjectSegmentation_Instance")),new UScriptStruct::TCppStructOps<FNVObjectSegmentation_Instance>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVObjectSegmentation_Instance;
	struct Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexColorMaskManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VertexColorMaskManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentationIdAssignmentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SegmentationIdAssignmentType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SegmentationIdAssignmentType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVObjectSegmentation_Instance>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_VertexColorMaskManager_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_VertexColorMaskManager = { "VertexColorMaskManager", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVObjectSegmentation_Instance, VertexColorMaskManager), Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_VertexColorMaskManager_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_VertexColorMaskManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType_MetaData[] = {
		{ "Category", "Segmentation" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType = { "SegmentationIdAssignmentType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVObjectSegmentation_Instance, SegmentationIdAssignmentType), Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_VertexColorMaskManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::NewProp_SegmentationIdAssignmentType_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVObjectSegmentation_Instance",
		sizeof(FNVObjectSegmentation_Instance),
		alignof(FNVObjectSegmentation_Instance),
		Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVObjectSegmentation_Instance"), sizeof(FNVObjectSegmentation_Instance), Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Hash() { return 1776829184U; }
	void UNVObjectMaskMananger::StaticRegisterNativesUNVObjectMaskMananger()
	{
	}
	UClass* Z_Construct_UClass_UNVObjectMaskMananger_NoRegister()
	{
		return UNVObjectMaskMananger::StaticClass();
	}
	struct Z_Construct_UClass_UNVObjectMaskMananger_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllMaskActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllMaskActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AllMaskActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllMaskNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllMaskNames;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AllMaskNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDebug_MetaData[];
#endif
		static void NewProp_bDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentationIdAssignmentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SegmentationIdAssignmentType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SegmentationIdAssignmentType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorMaskNameType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ActorMaskNameType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ActorMaskNameType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVObjectMaskMananger_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVObjectMaskManager.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Mask base class: scan actors in the scene, assign them an ID based on mask type" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors = { "AllMaskActors", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger, AllMaskActors), METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors_Inner = { "AllMaskActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Transient" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames = { "AllMaskNames", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger, AllMaskNames), METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames_Inner = { "AllMaskNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug_MetaData[] = {
		{ "Category", "ActorMask" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Turn on this flag to print out debug information (e.g: list of mask name ...) when this object run" },
	};
#endif
	void Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug_SetBit(void* Obj)
	{
		((UNVObjectMaskMananger*)Obj)->bDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug = { "bDebug", nullptr, (EPropertyFlags)0x00200c0000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVObjectMaskMananger), &Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType_MetaData[] = {
		{ "Category", "ActorMask" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "How the segmentation id get generated for actors in the scene" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType = { "SegmentationIdAssignmentType", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger, SegmentationIdAssignmentType), Z_Construct_UEnum_NVSceneCapturer_ENVIdAssignmentType, METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType_MetaData[] = {
		{ "Category", "ActorMask" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType = { "ActorMaskNameType", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger, ActorMaskNameType), Z_Construct_UEnum_NVSceneCapturer_ENVActorMaskNameType, METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVObjectMaskMananger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_AllMaskNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_bDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_SegmentationIdAssignmentType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Statics::NewProp_ActorMaskNameType_Underlying,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVObjectMaskMananger_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVObjectMaskMananger>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVObjectMaskMananger_Statics::ClassParams = {
		&UNVObjectMaskMananger::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVObjectMaskMananger_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::PropPointers),
		0,
		0x003010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVObjectMaskMananger()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVObjectMaskMananger_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVObjectMaskMananger, 608183831);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVObjectMaskMananger>()
	{
		return UNVObjectMaskMananger::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVObjectMaskMananger(Z_Construct_UClass_UNVObjectMaskMananger, &UNVObjectMaskMananger::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVObjectMaskMananger"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVObjectMaskMananger);
	void UNVObjectMaskMananger_Stencil::StaticRegisterNativesUNVObjectMaskMananger_Stencil()
	{
	}
	UClass* Z_Construct_UClass_UNVObjectMaskMananger_Stencil_NoRegister()
	{
		return UNVObjectMaskMananger_Stencil::StaticClass();
	}
	struct Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskNameIdMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MaskNameIdMap;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MaskNameIdMap_Key_KeyProp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaskNameIdMap_ValueProp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVObjectMaskMananger,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVObjectMaskManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "UNVObjectMaskMananger_Stencil scan actors in the scene, assign them an ID using StencilMask\nNOTE: MaskId 0 mean the actor is ignored" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Transient" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap = { "MaskNameIdMap", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger_Stencil, MaskNameIdMap), METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_Key_KeyProp = { "MaskNameIdMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_ValueProp = { "MaskNameIdMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::NewProp_MaskNameIdMap_ValueProp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVObjectMaskMananger_Stencil>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::ClassParams = {
		&UNVObjectMaskMananger_Stencil::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVObjectMaskMananger_Stencil()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVObjectMaskMananger_Stencil, 2053716862);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVObjectMaskMananger_Stencil>()
	{
		return UNVObjectMaskMananger_Stencil::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVObjectMaskMananger_Stencil(Z_Construct_UClass_UNVObjectMaskMananger_Stencil, &UNVObjectMaskMananger_Stencil::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVObjectMaskMananger_Stencil"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVObjectMaskMananger_Stencil);
	void UNVObjectMaskMananger_VertexColor::StaticRegisterNativesUNVObjectMaskMananger_VertexColor()
	{
	}
	UClass* Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_NoRegister()
	{
		return UNVObjectMaskMananger_VertexColor::StaticClass();
	}
	struct Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskNameIdMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_MaskNameIdMap;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MaskNameIdMap_Key_KeyProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_MaskNameIdMap_ValueProp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVObjectMaskMananger,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVObjectMaskManager.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "UNVObjectMaskMananger_VertexColor scan actors in the scene, assign them an ID using VertexColor (32bits)\nNOTE: MaskId 0 mean the actor is ignored" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVObjectMaskManager.h" },
		{ "ToolTip", "Transient" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap = { "MaskNameIdMap", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVObjectMaskMananger_VertexColor, MaskNameIdMap), METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_Key_KeyProp = { "MaskNameIdMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_ValueProp = { "MaskNameIdMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::NewProp_MaskNameIdMap_ValueProp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVObjectMaskMananger_VertexColor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::ClassParams = {
		&UNVObjectMaskMananger_VertexColor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVObjectMaskMananger_VertexColor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVObjectMaskMananger_VertexColor, 129751696);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVObjectMaskMananger_VertexColor>()
	{
		return UNVObjectMaskMananger_VertexColor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVObjectMaskMananger_VertexColor(Z_Construct_UClass_UNVObjectMaskMananger_VertexColor, &UNVObjectMaskMananger_VertexColor::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVObjectMaskMananger_VertexColor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVObjectMaskMananger_VertexColor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
