// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerHUD_Overlay_generated_h
#error "NVSceneCapturerHUD_Overlay.generated.h already included, missing '#pragma once' in NVSceneCapturerHUD_Overlay.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerHUD_Overlay_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_Overlay(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_Overlay_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_Overlay, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_Overlay)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_Overlay(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_Overlay_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_Overlay, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_Overlay)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneCapturerHUD_Overlay(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_Overlay) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_Overlay); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_Overlay); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_Overlay(UNVSceneCapturerHUD_Overlay&&); \
	NO_API UNVSceneCapturerHUD_Overlay(const UNVSceneCapturerHUD_Overlay&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_Overlay(UNVSceneCapturerHUD_Overlay&&); \
	NO_API UNVSceneCapturerHUD_Overlay(const UNVSceneCapturerHUD_Overlay&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_Overlay); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_Overlay); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_Overlay)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_16_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class UNVSceneCapturerHUD_Overlay>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_Overlay_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
