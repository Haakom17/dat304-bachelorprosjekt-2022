// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneFeatureExtractor_DataExport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneFeatureExtractor_DataExport() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVDataExportSettings();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
// End Cross Module References
class UScriptStruct* FNVDataExportSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVDataExportSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVDataExportSettings, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVDataExportSettings"), sizeof(FNVDataExportSettings), Get_Z_Construct_UScriptStruct_FNVDataExportSettings_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVDataExportSettings>()
{
	return FNVDataExportSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVDataExportSettings(FNVDataExportSettings::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVDataExportSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVDataExportSettings
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVDataExportSettings()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVDataExportSettings")),new UScriptStruct::TCppStructOps<FNVDataExportSettings>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVDataExportSettings;
	struct Z_Construct_UScriptStruct_FNVDataExportSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportImageCoordinateInPixel_MetaData[];
#endif
		static void NewProp_bExportImageCoordinateInPixel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportImageCoordinateInPixel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceScaleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceScaleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOutputEvenIfNoObjectsAreInView_MetaData[];
#endif
		static void NewProp_bOutputEvenIfNoObjectsAreInView_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutputEvenIfNoObjectsAreInView;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundingBox2dType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundingBox2dType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundingBox2dType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundsType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundsType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundsType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreHiddenActor_MetaData[];
#endif
		static void NewProp_bIgnoreHiddenActor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreHiddenActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncludeObjectsType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IncludeObjectsType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IncludeObjectsType_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVDataExportSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ToolTip", "If true, all the image space coordinates are exported in absolute pixel\nOtherwise the coordinates are in  ratio between the position and the image size" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel_SetBit(void* Obj)
	{
		((FNVDataExportSettings*)Obj)->bExportImageCoordinateInPixel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel = { "bExportImageCoordinateInPixel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVDataExportSettings), &Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_DistanceScaleRange_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_DistanceScaleRange = { "DistanceScaleRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVDataExportSettings, DistanceScaleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_DistanceScaleRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_DistanceScaleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_SetBit(void* Obj)
	{
		((FNVDataExportSettings*)Obj)->bOutputEvenIfNoObjectsAreInView = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView = { "bOutputEvenIfNoObjectsAreInView", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVDataExportSettings), &Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ToolTip", "How to generate the 2d bounding box for each exported actor mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType = { "BoundingBox2dType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVDataExportSettings, BoundingBox2dType), Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ToolTip", "How to generate 3d bounding box for each exported actor mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType = { "BoundsType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVDataExportSettings, BoundsType), Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ToolTip", "If true, the exporter will ignore all the hidden actors in game" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor_SetBit(void* Obj)
	{
		((FNVDataExportSettings*)Obj)->bIgnoreHiddenActor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor = { "bIgnoreHiddenActor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVDataExportSettings), &Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType = { "IncludeObjectsType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVDataExportSettings, IncludeObjectsType), Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bExportImageCoordinateInPixel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_DistanceScaleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bOutputEvenIfNoObjectsAreInView,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundingBox2dType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_BoundsType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_bIgnoreHiddenActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::NewProp_IncludeObjectsType_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVDataExportSettings",
		sizeof(FNVDataExportSettings),
		alignof(FNVDataExportSettings),
		Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVDataExportSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVDataExportSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVDataExportSettings"), sizeof(FNVDataExportSettings), Get_Z_Construct_UScriptStruct_FNVDataExportSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVDataExportSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVDataExportSettings_Hash() { return 267907614U; }
	void UNVSceneFeatureExtractor_AnnotationData::StaticRegisterNativesUNVSceneFeatureExtractor_AnnotationData()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_NoRegister()
	{
		return UNVSceneFeatureExtractor_AnnotationData::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataExportSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DataExportSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NVSceneFeatureExtractor_DataExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all the feature extractors that export the scene data to json file" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ProjectionMatrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ProjectionMatrix = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, ProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ViewProjectionMatrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ViewProjectionMatrix = { "ViewProjectionMatrix", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, ViewProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ViewProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ViewProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_DataExportSettings_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_DataExport.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_DataExportSettings = { "DataExportSettings", nullptr, (EPropertyFlags)0x00200a0000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, DataExportSettings), Z_Construct_UScriptStruct_FNVDataExportSettings, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_DataExportSettings_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_DataExportSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_ViewProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::NewProp_DataExportSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_AnnotationData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_AnnotationData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_AnnotationData, 1288561543);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_AnnotationData>()
	{
		return UNVSceneFeatureExtractor_AnnotationData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_AnnotationData(Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData, &UNVSceneFeatureExtractor_AnnotationData::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_AnnotationData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_AnnotationData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
