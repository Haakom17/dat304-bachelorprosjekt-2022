// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/HUD/NVSceneCapturerHUD_PIPPanel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerHUD_PIPPanel() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected();
	SLATECORE_API UEnum* Z_Construct_UEnum_SlateCore_ESelectInfo();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UNVSceneCapturerHUD_PIPPanel::StaticRegisterNativesUNVSceneCapturerHUD_PIPPanel()
	{
		UClass* Class = UNVSceneCapturerHUD_PIPPanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnChannelSelected", &UNVSceneCapturerHUD_PIPPanel::execOnChannelSelected },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics
	{
		struct NVSceneCapturerHUD_PIPPanel_eventOnChannelSelected_Parms
		{
			FString SelectedValue;
			TEnumAsByte<ESelectInfo::Type> type;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_type;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SelectedValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::NewProp_type = { "type", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerHUD_PIPPanel_eventOnChannelSelected_Parms, type), Z_Construct_UEnum_SlateCore_ESelectInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::NewProp_SelectedValue = { "SelectedValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerHUD_PIPPanel_eventOnChannelSelected_Parms, SelectedValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::NewProp_type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::NewProp_SelectedValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel, nullptr, "OnChannelSelected", sizeof(NVSceneCapturerHUD_PIPPanel_eventOnChannelSelected_Parms), Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_NoRegister()
	{
		return UNVSceneCapturerHUD_PIPPanel::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VizMaterialDynamic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VizMaterialDynamic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDataVisualizer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDataVisualizer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveCapturerActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveCapturerActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisualizerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VisualizerMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseRenderTargetForVisualization_MetaData[];
#endif
		static void NewProp_bUseRenderTargetForVisualization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseRenderTargetForVisualization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_PIPPanel_OnChannelSelected, "OnChannelSelected" }, // 4264226104
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HUD/NVSceneCapturerHUD_PIPPanel.h" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VizMaterialDynamic_MetaData[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VizMaterialDynamic = { "VizMaterialDynamic", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, VizMaterialDynamic), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VizMaterialDynamic_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VizMaterialDynamic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_SceneDataVisualizer_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_SceneDataVisualizer = { "SceneDataVisualizer", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, SceneDataVisualizer), Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_SceneDataVisualizer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_SceneDataVisualizer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_ActiveCapturerActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_ActiveCapturerActor = { "ActiveCapturerActor", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, ActiveCapturerActor), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_ActiveCapturerActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_ActiveCapturerActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VisualizerMaterial_MetaData[] = {
		{ "Category", "SceneDataVisualizer" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
		{ "ToolTip", "The material used for visualizing the captured image buffer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VisualizerMaterial = { "VisualizerMaterial", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, VisualizerMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VisualizerMaterial_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VisualizerMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization_MetaData[] = {
		{ "Category", "PIP Panel" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_PIPPanel.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization_SetBit(void* Obj)
	{
		((UNVSceneCapturerHUD_PIPPanel*)Obj)->bUseRenderTargetForVisualization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization = { "bUseRenderTargetForVisualization", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneCapturerHUD_PIPPanel), &Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VizMaterialDynamic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_SceneDataVisualizer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_ActiveCapturerActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_VisualizerMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::NewProp_bUseRenderTargetForVisualization,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneCapturerHUD_PIPPanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::ClassParams = {
		&UNVSceneCapturerHUD_PIPPanel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneCapturerHUD_PIPPanel, 1509275833);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<UNVSceneCapturerHUD_PIPPanel>()
	{
		return UNVSceneCapturerHUD_PIPPanel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneCapturerHUD_PIPPanel(Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel, &UNVSceneCapturerHUD_PIPPanel::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("UNVSceneCapturerHUD_PIPPanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneCapturerHUD_PIPPanel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
