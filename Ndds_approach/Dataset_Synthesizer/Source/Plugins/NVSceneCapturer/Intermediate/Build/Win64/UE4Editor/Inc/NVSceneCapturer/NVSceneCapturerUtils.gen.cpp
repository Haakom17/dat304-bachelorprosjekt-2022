// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneCapturerUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerUtils() {}
// Cross Module References
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCuboidVertexType();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVFrameCounter();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCameraIntrinsicSettings();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVImageSize();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneExporterConfig();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCapturedFrameData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCapturedSceneData();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCapturedObjectData();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCapturedViewpointData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSocketData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVBox2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVCuboidData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBox();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVTexturePixelData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCapturableActorTag_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCapturableActorTag();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
// End Cross Module References
	static UEnum* ENVSceneCapturerState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVSceneCapturerState"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVSceneCapturerState>()
	{
		return ENVSceneCapturerState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVSceneCapturerState(ENVSceneCapturerState_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVSceneCapturerState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState_Hash() { return 518027485U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVSceneCapturerState"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVSceneCapturerState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVSceneCapturerState::NotActive", (int64)ENVSceneCapturerState::NotActive },
				{ "ENVSceneCapturerState::Active", (int64)ENVSceneCapturerState::Active },
				{ "ENVSceneCapturerState::Running", (int64)ENVSceneCapturerState::Running },
				{ "ENVSceneCapturerState::Paused", (int64)ENVSceneCapturerState::Paused },
				{ "ENVSceneCapturerState::Completed", (int64)ENVSceneCapturerState::Completed },
				{ "ENVSceneCapturerState::NVSceneCapturerState_MAX", (int64)ENVSceneCapturerState::NVSceneCapturerState_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Active.DisplayName", "Active.    The capturer is active. but not started." },
				{ "Completed.DisplayName", "Completed. The capturer finished exporting a batch." },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
				{ "NotActive.DisplayName", "NotActive. The capturer is not active." },
				{ "NVSceneCapturerState_MAX.Hidden", "" },
				{ "Paused.DisplayName", "Paused.    The capturer is paused, can be resumed." },
				{ "Running.DisplayName", "Running.   The capturer is running/exporting." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVSceneCapturerState",
				"ENVSceneCapturerState",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVBoundBox2dGenerationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVBoundBox2dGenerationType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVBoundBox2dGenerationType>()
	{
		return ENVBoundBox2dGenerationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVBoundBox2dGenerationType(ENVBoundBox2dGenerationType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVBoundBox2dGenerationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType_Hash() { return 2037392384U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVBoundBox2dGenerationType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVBoundBox2dGenerationType::From3dBoundingBox", (int64)ENVBoundBox2dGenerationType::From3dBoundingBox },
				{ "ENVBoundBox2dGenerationType::FromMeshBodyCollision", (int64)ENVBoundBox2dGenerationType::FromMeshBodyCollision },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "From3dBoundingBox.ToolTip", "Generate the 2d bounding box from the mesh's 3d bounding box vertexes" },
				{ "FromMeshBodyCollision.ToolTip", "Generate the 2d bounding box from the mesh's body collision" },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVBoundBox2dGenerationType",
				"ENVBoundBox2dGenerationType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVBoundsGenerationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVBoundsGenerationType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVBoundsGenerationType>()
	{
		return ENVBoundsGenerationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVBoundsGenerationType(ENVBoundsGenerationType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVBoundsGenerationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType_Hash() { return 3478935091U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVBoundsGenerationType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVBoundsGenerationType::VE_AABB", (int64)ENVBoundsGenerationType::VE_AABB },
				{ "ENVBoundsGenerationType::VE_OOBB", (int64)ENVBoundsGenerationType::VE_OOBB },
				{ "ENVBoundsGenerationType::VE_TightOOBB", (int64)ENVBoundsGenerationType::VE_TightOOBB },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
				{ "VE_AABB.DisplayName", "AABB" },
				{ "VE_AABB.ToolTip", "World space AABB" },
				{ "VE_OOBB.DisplayName", "OOBB" },
				{ "VE_OOBB.ToolTip", "Object Space AABB, scaled, rotated and translated" },
				{ "VE_TightOOBB.DisplayName", "Tight Arbitrary OOBB" },
				{ "VE_TightOOBB.ToolTip", "Arbitrary tight fitting bounds generated from mesh vertices.\nREMOVE: May not support this" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVBoundsGenerationType",
				"ENVBoundsGenerationType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVIncludeObjects_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVIncludeObjects"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVIncludeObjects>()
	{
		return ENVIncludeObjects_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVIncludeObjects(ENVIncludeObjects_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVIncludeObjects"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects_Hash() { return 3934028084U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVIncludeObjects"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVIncludeObjects::AllTaggedObjects", (int64)ENVIncludeObjects::AllTaggedObjects },
				{ "ENVIncludeObjects::MatchesTag", (int64)ENVIncludeObjects::MatchesTag },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVIncludeObjects",
				"ENVIncludeObjects",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVCuboidVertexType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVCuboidVertexType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVCuboidVertexType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCuboidVertexType>()
	{
		return ENVCuboidVertexType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVCuboidVertexType(ENVCuboidVertexType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVCuboidVertexType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVCuboidVertexType_Hash() { return 3859347992U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCuboidVertexType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVCuboidVertexType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVCuboidVertexType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVCuboidVertexType::FrontTopRight", (int64)ENVCuboidVertexType::FrontTopRight },
				{ "ENVCuboidVertexType::FrontTopLeft", (int64)ENVCuboidVertexType::FrontTopLeft },
				{ "ENVCuboidVertexType::FrontBottomLeft", (int64)ENVCuboidVertexType::FrontBottomLeft },
				{ "ENVCuboidVertexType::FrontBottomRight", (int64)ENVCuboidVertexType::FrontBottomRight },
				{ "ENVCuboidVertexType::RearTopRight", (int64)ENVCuboidVertexType::RearTopRight },
				{ "ENVCuboidVertexType::RearTopLeft", (int64)ENVCuboidVertexType::RearTopLeft },
				{ "ENVCuboidVertexType::RearBottomLeft", (int64)ENVCuboidVertexType::RearBottomLeft },
				{ "ENVCuboidVertexType::RearBottomRight", (int64)ENVCuboidVertexType::RearBottomRight },
				{ "ENVCuboidVertexType::CuboidVertexType_MAX", (int64)ENVCuboidVertexType::CuboidVertexType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CuboidVertexType_MAX.Hidden", "" },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
				{ "ToolTip", "This enum represent 8 corner vertexes of a rectangular cuboid\nNOTE: The order of the enums here is what the researcher want for the training data.\nIf they want to change the exported order of these vertexes then we must update this order too" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVCuboidVertexType",
				"ENVCuboidVertexType",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVCapturedPixelFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVCapturedPixelFormat"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCapturedPixelFormat>()
	{
		return ENVCapturedPixelFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVCapturedPixelFormat(ENVCapturedPixelFormat_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVCapturedPixelFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat_Hash() { return 1396466671U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVCapturedPixelFormat"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "R8", (int64)R8 },
				{ "RGBA8", (int64)RGBA8 },
				{ "R8G8", (int64)R8G8 },
				{ "R32f", (int64)R32f },
				{ "NVCapturedPixelFormat_MAX", (int64)NVCapturedPixelFormat_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
				{ "NVCapturedPixelFormat_MAX.Hidden", "" },
				{ "NVCapturedPixelFormat_MAX.ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
				{ "R32f.ToolTip", "R channel, 32 bit per channel floating point, range [-3.402823 x 10^38, 3.402823 x 10^38]\nNOTE: This format capture to 32 bits floating point value and can be exported to RGBA8 format" },
				{ "R8.ToolTip", "R channel, 8 bit per channel fixed point, range [0, 1]\nUse this format for the grayscale 8 bit image type" },
				{ "R8G8.ToolTip", "RG channel, 8 bit per channel, range [65535]\nUse this format for the grayscale 16 bits image type" },
				{ "RGBA8.ToolTip", "RGBA channels, 8 bit per channel fixed point, range [0, 1]\nUse this format for the normal full color image type" },
				{ "ToolTip", "The pixel format which can be captured" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVCapturedPixelFormat",
				"ENVCapturedPixelFormat",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVImageFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVImageFormat"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVImageFormat>()
	{
		return ENVImageFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVImageFormat(ENVImageFormat_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVImageFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat_Hash() { return 2847145931U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVImageFormat"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVImageFormat::PNG", (int64)ENVImageFormat::PNG },
				{ "ENVImageFormat::JPEG", (int64)ENVImageFormat::JPEG },
				{ "ENVImageFormat::GrayscaleJPEG", (int64)ENVImageFormat::GrayscaleJPEG },
				{ "ENVImageFormat::BMP", (int64)ENVImageFormat::BMP },
				{ "ENVImageFormat::NVImageFormat_MAX", (int64)ENVImageFormat::NVImageFormat_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "BMP.DisplayName", "BMP (Windows Bitmap" },
				{ "BMP.ToolTip", "Windows Bitmap." },
				{ "GrayscaleJPEG.DisplayName", "GrayscaleJPEG (Single channel jpeg" },
				{ "GrayscaleJPEG.ToolTip", "Single channel jpeg." },
				{ "JPEG.DisplayName", "JPEG (Joint Photographic Experts Group)." },
				{ "JPEG.ToolTip", "Joint Photographic Experts Group." },
				{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
				{ "NVImageFormat_MAX.Hidden", "" },
				{ "NVImageFormat_MAX.ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
				{ "PNG.DisplayName", "PNG (Portable Network Graphics)." },
				{ "PNG.ToolTip", "Portable Network Graphics." },
				{ "ToolTip", "NOTE: Should remove this enum when the EImageFormat in IImageWrapper marked as UENUM" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVImageFormat",
				"ENVImageFormat",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNVFrameCounter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVFrameCounter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVFrameCounter, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVFrameCounter"), sizeof(FNVFrameCounter), Get_Z_Construct_UScriptStruct_FNVFrameCounter_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVFrameCounter>()
{
	return FNVFrameCounter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVFrameCounter(FNVFrameCounter::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVFrameCounter"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFrameCounter
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFrameCounter()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVFrameCounter")),new UScriptStruct::TCppStructOps<FNVFrameCounter>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFrameCounter;
	struct Z_Construct_UScriptStruct_FNVFrameCounter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVFrameCounter_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVFrameCounter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVFrameCounter>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVFrameCounter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVFrameCounter",
		sizeof(FNVFrameCounter),
		alignof(FNVFrameCounter),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVFrameCounter_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVFrameCounter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVFrameCounter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVFrameCounter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVFrameCounter"), sizeof(FNVFrameCounter), Get_Z_Construct_UScriptStruct_FNVFrameCounter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVFrameCounter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVFrameCounter_Hash() { return 872412662U; }
class UScriptStruct* FNVSceneCapturerSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneCapturerSettings, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneCapturerSettings"), sizeof(FNVSceneCapturerSettings), Get_Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneCapturerSettings>()
{
	return FNVSceneCapturerSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneCapturerSettings(FNVSceneCapturerSettings::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneCapturerSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerSettings
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerSettings()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSceneCapturerSettings")),new UScriptStruct::TCppStructOps<FNVSceneCapturerSettings>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerSettings;
	struct Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraIntrinsicMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraIntrinsicMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraIntrinsicSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraIntrinsicSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseExplicitCameraIntrinsic_MetaData[];
#endif
		static void NewProp_bUseExplicitCameraIntrinsic_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseExplicitCameraIntrinsic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSaveImageAsyncTaskCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxSaveImageAsyncTaskCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedImageSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CapturedImageSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOVAngleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FOVAngleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FOVAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FOVAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportImageFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExportImageFormat;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExportImageFormat_Underlying;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneCapturerSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraProjectionMatrix_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraProjectionMatrix = { "CameraProjectionMatrix", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, CameraProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicMatrix_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicMatrix = { "CameraIntrinsicMatrix", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, CameraIntrinsicMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicSettings_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "EditCondition", "bUseExplicitCameraIntrinsic" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicSettings = { "CameraIntrinsicSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, CameraIntrinsicSettings), Z_Construct_UScriptStruct_FCameraIntrinsicSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic_SetBit(void* Obj)
	{
		((FNVSceneCapturerSettings*)Obj)->bUseExplicitCameraIntrinsic = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic = { "bUseExplicitCameraIntrinsic", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneCapturerSettings), &Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_MaxSaveImageAsyncTaskCount_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "NOTE: Only advance user who need to change this" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_MaxSaveImageAsyncTaskCount = { "MaxSaveImageAsyncTaskCount", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, MaxSaveImageAsyncTaskCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_MaxSaveImageAsyncTaskCount_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_MaxSaveImageAsyncTaskCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CapturedImageSize_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CapturedImageSize = { "CapturedImageSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, CapturedImageSize), Z_Construct_UScriptStruct_FNVImageSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CapturedImageSize_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CapturedImageSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngleRange_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ClampMax", "360.0" },
		{ "ClampMin", "0.001" },
		{ "DisplayName", "Field of View" },
		{ "EditCondition", "!bUseExplicitCameraIntrinsic" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "UIMax", "170" },
		{ "UIMin", "5.0" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngleRange = { "FOVAngleRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, FOVAngleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngleRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngle_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "EditCondition", "!bUseExplicitCameraIntrinsic" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngle = { "FOVAngle", nullptr, (EPropertyFlags)0x0010000000022001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, FOVAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngle_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat_MetaData[] = {
		{ "Category", "CapturerSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat = { "ExportImageFormat", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerSettings, ExportImageFormat), Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CameraIntrinsicSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_bUseExplicitCameraIntrinsic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_MaxSaveImageAsyncTaskCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_CapturedImageSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_FOVAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::NewProp_ExportImageFormat_Underlying,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneCapturerSettings",
		sizeof(FNVSceneCapturerSettings),
		alignof(FNVSceneCapturerSettings),
		Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneCapturerSettings"), sizeof(FNVSceneCapturerSettings), Get_Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Hash() { return 2482204255U; }
class UScriptStruct* FNVSceneExporterConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneExporterConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneExporterConfig, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneExporterConfig"), sizeof(FNVSceneExporterConfig), Get_Z_Construct_UScriptStruct_FNVSceneExporterConfig_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneExporterConfig>()
{
	return FNVSceneExporterConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneExporterConfig(FNVSceneExporterConfig::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneExporterConfig"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneExporterConfig
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneExporterConfig()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSceneExporterConfig")),new UScriptStruct::TCppStructOps<FNVSceneExporterConfig>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneExporterConfig;
	struct Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistanceScaleRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistanceScaleRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOutputEvenIfNoObjectsAreInView_MetaData[];
#endif
		static void NewProp_bOutputEvenIfNoObjectsAreInView_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOutputEvenIfNoObjectsAreInView;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundingBox2dType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundingBox2dType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundingBox2dType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundsType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BoundsType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BoundsType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreHiddenActor_MetaData[];
#endif
		static void NewProp_bIgnoreHiddenActor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreHiddenActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncludeObjectsType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IncludeObjectsType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IncludeObjectsType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportScreenShot_MetaData[];
#endif
		static void NewProp_bExportScreenShot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportScreenShot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportObjectData_MetaData[];
#endif
		static void NewProp_bExportObjectData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportObjectData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneExporterConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_DistanceScaleRange_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "TODO: Should move this to the data feature extractors" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_DistanceScaleRange = { "DistanceScaleRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneExporterConfig, DistanceScaleRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_DistanceScaleRange_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_DistanceScaleRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_SetBit(void* Obj)
	{
		((FNVSceneExporterConfig*)Obj)->bOutputEvenIfNoObjectsAreInView = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView = { "bOutputEvenIfNoObjectsAreInView", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneExporterConfig), &Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "How to generate the 2d bounding box for each exported actor mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType = { "BoundingBox2dType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneExporterConfig, BoundingBox2dType), Z_Construct_UEnum_NVSceneCapturer_ENVBoundBox2dGenerationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "How to generate 3d bounding box for each exported actor mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType = { "BoundsType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneExporterConfig, BoundsType), Z_Construct_UEnum_NVSceneCapturer_ENVBoundsGenerationType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "If true, the exporter will ignore all the hidden actors in game\nTODO (TT): Should remove ENVIncludeObjects::AllVisibleObjects and keep this flag option independently from the InludeObjectsType" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor_SetBit(void* Obj)
	{
		((FNVSceneExporterConfig*)Obj)->bIgnoreHiddenActor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor = { "bIgnoreHiddenActor", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneExporterConfig), &Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType = { "IncludeObjectsType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneExporterConfig, IncludeObjectsType), Z_Construct_UEnum_NVSceneCapturer_ENVIncludeObjects, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot_SetBit(void* Obj)
	{
		((FNVSceneExporterConfig*)Obj)->bExportScreenShot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot = { "bExportScreenShot", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneExporterConfig), &Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData_MetaData[] = {
		{ "Category", "Export" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData_SetBit(void* Obj)
	{
		((FNVSceneExporterConfig*)Obj)->bExportObjectData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData = { "bExportObjectData", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneExporterConfig), &Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_DistanceScaleRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bOutputEvenIfNoObjectsAreInView,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundingBox2dType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_BoundsType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bIgnoreHiddenActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_IncludeObjectsType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportScreenShot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::NewProp_bExportObjectData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneExporterConfig",
		sizeof(FNVSceneExporterConfig),
		alignof(FNVSceneExporterConfig),
		Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneExporterConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneExporterConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneExporterConfig"), sizeof(FNVSceneExporterConfig), Get_Z_Construct_UScriptStruct_FNVSceneExporterConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneExporterConfig_Hash() { return 1801503773U; }
class UScriptStruct* FCapturedFrameData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FCapturedFrameData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCapturedFrameData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("CapturedFrameData"), sizeof(FCapturedFrameData), Get_Z_Construct_UScriptStruct_FCapturedFrameData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FCapturedFrameData>()
{
	return FCapturedFrameData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCapturedFrameData(FCapturedFrameData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("CapturedFrameData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedFrameData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedFrameData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CapturedFrameData")),new UScriptStruct::TCppStructOps<FCapturedFrameData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedFrameData;
	struct Z_Construct_UScriptStruct_FCapturedFrameData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneBitmap_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneBitmap;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneBitmap_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedFrameData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCapturedFrameData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Pixels data of the frame" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap = { "SceneBitmap", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedFrameData, SceneBitmap), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap_Inner = { "SceneBitmap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_Data_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_Data = { "Data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedFrameData, Data), Z_Construct_UScriptStruct_FCapturedSceneData, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_Data_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_Data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCapturedFrameData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_SceneBitmap_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedFrameData_Statics::NewProp_Data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCapturedFrameData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"CapturedFrameData",
		sizeof(FCapturedFrameData),
		alignof(FCapturedFrameData),
		Z_Construct_UScriptStruct_FCapturedFrameData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedFrameData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCapturedFrameData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCapturedFrameData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CapturedFrameData"), sizeof(FCapturedFrameData), Get_Z_Construct_UScriptStruct_FCapturedFrameData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCapturedFrameData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCapturedFrameData_Hash() { return 3969707737U; }
class UScriptStruct* FCapturedSceneData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FCapturedSceneData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCapturedSceneData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("CapturedSceneData"), sizeof(FCapturedSceneData), Get_Z_Construct_UScriptStruct_FCapturedSceneData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FCapturedSceneData>()
{
	return FCapturedSceneData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCapturedSceneData(FCapturedSceneData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("CapturedSceneData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedSceneData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedSceneData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CapturedSceneData")),new UScriptStruct::TCppStructOps<FCapturedSceneData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedSceneData;
	struct Z_Construct_UScriptStruct_FCapturedSceneData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Objects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Objects;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Objects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_camera_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_camera_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedSceneData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCapturedSceneData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects = { "Objects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedSceneData, Objects), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects_Inner = { "Objects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCapturedObjectData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_camera_data_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_camera_data = { "camera_data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedSceneData, camera_data), Z_Construct_UScriptStruct_FCapturedViewpointData, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_camera_data_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_camera_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCapturedSceneData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_Objects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedSceneData_Statics::NewProp_camera_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCapturedSceneData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"CapturedSceneData",
		sizeof(FCapturedSceneData),
		alignof(FCapturedSceneData),
		Z_Construct_UScriptStruct_FCapturedSceneData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedSceneData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCapturedSceneData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCapturedSceneData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CapturedSceneData"), sizeof(FCapturedSceneData), Get_Z_Construct_UScriptStruct_FCapturedSceneData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCapturedSceneData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCapturedSceneData_Hash() { return 959699188U; }
class UScriptStruct* FCapturedViewpointData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FCapturedViewpointData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCapturedViewpointData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("CapturedViewpointData"), sizeof(FCapturedViewpointData), Get_Z_Construct_UScriptStruct_FCapturedViewpointData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FCapturedViewpointData>()
{
	return FCapturedViewpointData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCapturedViewpointData(FCapturedViewpointData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("CapturedViewpointData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedViewpointData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedViewpointData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CapturedViewpointData")),new UScriptStruct::TCppStructOps<FCapturedViewpointData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedViewpointData;
	struct Z_Construct_UScriptStruct_FCapturedViewpointData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_fov_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_fov;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_quaternion_xyzw_worldframe_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_quaternion_xyzw_worldframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_location_worldframe_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_location_worldframe;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCapturedViewpointData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_fov_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_fov = { "fov", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, fov), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_fov_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_fov_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_CameraSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_CameraSettings = { "CameraSettings", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, CameraSettings), Z_Construct_UScriptStruct_FCameraIntrinsicSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_CameraSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_CameraSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ViewProjectionMatrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ViewProjectionMatrix = { "ViewProjectionMatrix", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, ViewProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ViewProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ViewProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ProjectionMatrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ProjectionMatrix = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, ProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_quaternion_xyzw_worldframe_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_quaternion_xyzw_worldframe = { "quaternion_xyzw_worldframe", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, quaternion_xyzw_worldframe), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_quaternion_xyzw_worldframe_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_quaternion_xyzw_worldframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_location_worldframe_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_location_worldframe = { "location_worldframe", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedViewpointData, location_worldframe), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_location_worldframe_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_location_worldframe_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_fov,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_CameraSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ViewProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_ProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_quaternion_xyzw_worldframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::NewProp_location_worldframe,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"CapturedViewpointData",
		sizeof(FCapturedViewpointData),
		alignof(FCapturedViewpointData),
		Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCapturedViewpointData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCapturedViewpointData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CapturedViewpointData"), sizeof(FCapturedViewpointData), Get_Z_Construct_UScriptStruct_FCapturedViewpointData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCapturedViewpointData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCapturedViewpointData_Hash() { return 4201941128U; }
class UScriptStruct* FCapturedObjectData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FCapturedObjectData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCapturedObjectData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("CapturedObjectData"), sizeof(FCapturedObjectData), Get_Z_Construct_UScriptStruct_FCapturedObjectData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FCapturedObjectData>()
{
	return FCapturedObjectData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCapturedObjectData(FCapturedObjectData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("CapturedObjectData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedObjectData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedObjectData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CapturedObjectData")),new UScriptStruct::TCppStructOps<FCapturedObjectData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFCapturedObjectData;
	struct Z_Construct_UScriptStruct_FCapturedObjectData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_socket_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_socket_data;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_socket_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_projected_cuboid_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_projected_cuboid;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_projected_cuboid_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cuboid_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_cuboid;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_cuboid_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bounding_box_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bounding_box;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_distance_scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_distance_scale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_viewpoint_altitude_angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_viewpoint_altitude_angle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_viewpoint_azimuth_angle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_viewpoint_azimuth_angle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bounding_box_forward_direction_imagespace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bounding_box_forward_direction_imagespace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bounding_box_forward_direction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bounding_box_forward_direction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_projected_cuboid_centroid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_projected_cuboid_centroid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_cuboid_centroid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_cuboid_centroid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bounding_box_center_worldspace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bounding_box_center_worldspace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_pose_transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_pose_transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_actor_to_camera_matrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_actor_to_camera_matrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_actor_to_world_matrix_opencv_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_actor_to_world_matrix_opencv;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_actor_to_world_matrix_ue4_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_actor_to_world_matrix_ue4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_quaternion_xyzw_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_quaternion_xyzw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_quaternion_worldspace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_quaternion_worldspace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_rotation_worldspace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_rotation_worldspace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_location_worldspace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_location_worldspace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_dimensions_worldspace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_dimensions_worldspace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_visibility_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_visibility;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_occlusion_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_occlusion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_occluded_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_occluded;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_truncated_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_truncated;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_instance_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_instance_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Class_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Class;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCapturedObjectData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data = { "socket_data", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, socket_data), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data_Inner = { "socket_data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVSocketData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid = { "projected_cuboid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, projected_cuboid), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_Inner = { "projected_cuboid", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "TODO: Create a struct for the cuboid since it must have exactly 8 corner vertexes" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid = { "cuboid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, cuboid), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_Inner = { "cuboid", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box = { "bounding_box", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, bounding_box), Z_Construct_UScriptStruct_FNVBox2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_distance_scale_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_distance_scale = { "distance_scale", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, distance_scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_distance_scale_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_distance_scale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_altitude_angle_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_altitude_angle = { "viewpoint_altitude_angle", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, viewpoint_altitude_angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_altitude_angle_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_altitude_angle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_azimuth_angle_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_azimuth_angle = { "viewpoint_azimuth_angle", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, viewpoint_azimuth_angle), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_azimuth_angle_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_azimuth_angle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_imagespace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_imagespace = { "bounding_box_forward_direction_imagespace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, bounding_box_forward_direction_imagespace), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_imagespace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_imagespace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction = { "bounding_box_forward_direction", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, bounding_box_forward_direction), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_centroid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_centroid = { "projected_cuboid_centroid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, projected_cuboid_centroid), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_centroid_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_centroid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_centroid_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_centroid = { "cuboid_centroid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, cuboid_centroid), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_centroid_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_centroid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_center_worldspace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_center_worldspace = { "bounding_box_center_worldspace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, bounding_box_center_worldspace), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_center_worldspace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_center_worldspace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_pose_transform_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_pose_transform = { "pose_transform", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, pose_transform), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_pose_transform_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_pose_transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_camera_matrix_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_camera_matrix = { "actor_to_camera_matrix", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, actor_to_camera_matrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_camera_matrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_camera_matrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_opencv_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_opencv = { "actor_to_world_matrix_opencv", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, actor_to_world_matrix_opencv), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_opencv_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_opencv_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_ue4_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_ue4 = { "actor_to_world_matrix_ue4", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, actor_to_world_matrix_ue4), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_ue4_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_ue4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_xyzw_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_xyzw = { "quaternion_xyzw", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, quaternion_xyzw), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_xyzw_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_xyzw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation = { "rotation", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_worldspace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_worldspace = { "quaternion_worldspace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, quaternion_worldspace), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_worldspace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_worldspace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_worldspace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_worldspace = { "rotation_worldspace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, rotation_worldspace), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_worldspace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_worldspace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location = { "location", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_worldspace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_worldspace = { "location_worldspace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, location_worldspace), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_worldspace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_worldspace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_dimensions_worldspace_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_dimensions_worldspace = { "dimensions_worldspace", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, dimensions_worldspace), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_dimensions_worldspace_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_dimensions_worldspace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_visibility_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_visibility = { "visibility", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, visibility), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_visibility_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_visibility_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occlusion_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Fraction of how much the object's 2d bounding box get occluded" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occlusion = { "occlusion", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, occlusion), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occlusion_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occlusion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occluded_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occluded = { "occluded", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, occluded), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occluded_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occluded_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_truncated_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_truncated = { "truncated", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, truncated), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_truncated_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_truncated_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_instance_id_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "The Id of the object instance" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_instance_id = { "instance_id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, instance_id), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_instance_id_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_instance_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Class_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Name of the object's class" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Class = { "Class", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, Class), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Class_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Class_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Properties\nObject's name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCapturedObjectData, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCapturedObjectData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_socket_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_distance_scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_altitude_angle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_viewpoint_azimuth_angle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction_imagespace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_forward_direction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_projected_cuboid_centroid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_cuboid_centroid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_bounding_box_center_worldspace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_pose_transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_camera_matrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_opencv,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_actor_to_world_matrix_ue4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_xyzw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_quaternion_worldspace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_rotation_worldspace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_location_worldspace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_dimensions_worldspace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_visibility,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occlusion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_occluded,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_truncated,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_instance_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Class,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCapturedObjectData_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCapturedObjectData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"CapturedObjectData",
		sizeof(FCapturedObjectData),
		alignof(FCapturedObjectData),
		Z_Construct_UScriptStruct_FCapturedObjectData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCapturedObjectData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCapturedObjectData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCapturedObjectData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CapturedObjectData"), sizeof(FCapturedObjectData), Get_Z_Construct_UScriptStruct_FCapturedObjectData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCapturedObjectData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCapturedObjectData_Hash() { return 2222822395U; }
class UScriptStruct* FNVBox2D::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVBox2D_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVBox2D, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVBox2D"), sizeof(FNVBox2D), Get_Z_Construct_UScriptStruct_FNVBox2D_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVBox2D>()
{
	return FNVBox2D::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVBox2D(FNVBox2D::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVBox2D"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVBox2D
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVBox2D()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVBox2D")),new UScriptStruct::TCppStructOps<FNVBox2D>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVBox2D;
	struct Z_Construct_UScriptStruct_FNVBox2D_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bottom_right_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_bottom_right;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_top_left_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_top_left;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVBox2D_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVBox2D_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVBox2D>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_bottom_right_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_bottom_right = { "bottom_right", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVBox2D, bottom_right), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_bottom_right_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_bottom_right_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_top_left_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_top_left = { "top_left", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVBox2D, top_left), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_top_left_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_top_left_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVBox2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_bottom_right,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVBox2D_Statics::NewProp_top_left,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVBox2D_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVBox2D",
		sizeof(FNVBox2D),
		alignof(FNVBox2D),
		Z_Construct_UScriptStruct_FNVBox2D_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVBox2D_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVBox2D_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVBox2D_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVBox2D()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVBox2D_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVBox2D"), sizeof(FNVBox2D), Get_Z_Construct_UScriptStruct_FNVBox2D_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVBox2D_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVBox2D_Hash() { return 1380187144U; }
class UScriptStruct* FNVCuboidData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVCuboidData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVCuboidData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVCuboidData"), sizeof(FNVCuboidData), Get_Z_Construct_UScriptStruct_FNVCuboidData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVCuboidData>()
{
	return FNVCuboidData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVCuboidData(FNVCuboidData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVCuboidData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCuboidData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCuboidData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVCuboidData")),new UScriptStruct::TCppStructOps<FNVCuboidData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVCuboidData;
	struct Z_Construct_UScriptStruct_FNVCuboidData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalBox;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Center_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Center;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertexes_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertexes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCuboidData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVCuboidData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Rotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCuboidData, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Rotation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_LocalBox_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_LocalBox = { "LocalBox", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCuboidData, LocalBox), Z_Construct_UScriptStruct_FBox, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_LocalBox_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_LocalBox_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Center_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "The center position of the cuboid" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Center = { "Center", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVCuboidData, Center), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Center_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Center_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Vertexes_MetaData[] = {
		{ "ArraySizeEnum", "/Script/NVSceneCapturer.ENVCuboidVertexType" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "List of position for each vertexes in the cuboid" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Vertexes = { "Vertexes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Vertexes, FNVCuboidData), STRUCT_OFFSET(FNVCuboidData, Vertexes), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Vertexes_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Vertexes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVCuboidData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_LocalBox,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Center,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVCuboidData_Statics::NewProp_Vertexes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVCuboidData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVCuboidData",
		sizeof(FNVCuboidData),
		alignof(FNVCuboidData),
		Z_Construct_UScriptStruct_FNVCuboidData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVCuboidData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVCuboidData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVCuboidData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVCuboidData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVCuboidData"), sizeof(FNVCuboidData), Get_Z_Construct_UScriptStruct_FNVCuboidData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVCuboidData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVCuboidData_Hash() { return 3752380154U; }
class UScriptStruct* FNVSocketData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSocketData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSocketData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSocketData"), sizeof(FNVSocketData), Get_Z_Construct_UScriptStruct_FNVSocketData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSocketData>()
{
	return FNVSocketData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSocketData(FNVSocketData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSocketData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSocketData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSocketData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSocketData")),new UScriptStruct::TCppStructOps<FNVSocketData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSocketData;
	struct Z_Construct_UScriptStruct_FNVSocketData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocketLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SocketLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocketName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SocketName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSocketData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Data to be captured and exported for each socket" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSocketData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSocketData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketLocation = { "SocketLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSocketData, SocketLocation), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketLocation_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketName = { "SocketName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSocketData, SocketName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSocketData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSocketData_Statics::NewProp_SocketName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSocketData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSocketData",
		sizeof(FNVSocketData),
		alignof(FNVSocketData),
		Z_Construct_UScriptStruct_FNVSocketData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSocketData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSocketData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSocketData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSocketData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSocketData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSocketData"), sizeof(FNVSocketData), Get_Z_Construct_UScriptStruct_FNVSocketData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSocketData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSocketData_Hash() { return 2433288752U; }
class UScriptStruct* FNVTexturePixelData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVTexturePixelData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVTexturePixelData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVTexturePixelData"), sizeof(FNVTexturePixelData), Get_Z_Construct_UScriptStruct_FNVTexturePixelData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVTexturePixelData>()
{
	return FNVTexturePixelData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVTexturePixelData(FNVTexturePixelData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVTexturePixelData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTexturePixelData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTexturePixelData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVTexturePixelData")),new UScriptStruct::TCppStructOps<FNVTexturePixelData>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTexturePixelData;
	struct Z_Construct_UScriptStruct_FNVTexturePixelData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PixelSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RowStride_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_RowStride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PixelData;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PixelData_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVTexturePixelData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelSize_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelSize = { "PixelSize", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVTexturePixelData, PixelSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelSize_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_RowStride_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_RowStride = { "RowStride", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVTexturePixelData, RowStride), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_RowStride_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_RowStride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData = { "PixelData", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVTexturePixelData, PixelData), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData_Inner = { "PixelData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_RowStride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::NewProp_PixelData_Inner,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVTexturePixelData",
		sizeof(FNVTexturePixelData),
		alignof(FNVTexturePixelData),
		Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVTexturePixelData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVTexturePixelData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVTexturePixelData"), sizeof(FNVTexturePixelData), Get_Z_Construct_UScriptStruct_FNVTexturePixelData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVTexturePixelData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVTexturePixelData_Hash() { return 3827452172U; }
	void UNVCapturableActorTag::StaticRegisterNativesUNVCapturableActorTag()
	{
	}
	UClass* Z_Construct_UClass_UNVCapturableActorTag_NoRegister()
	{
		return UNVCapturableActorTag::StaticClass();
	}
	struct Z_Construct_UClass_UNVCapturableActorTag_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocketNameToExportList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SocketNameToExportList;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SocketNameToExportList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportAllMeshSocketInfo_MetaData[];
#endif
		static void NewProp_bExportAllMeshSocketInfo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportAllMeshSocketInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeMe_MetaData[];
#endif
		static void NewProp_bIncludeMe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeMe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Tag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVCapturableActorTag_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCapturableActorTag_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVSceneCapturerUtils.h" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList_MetaData[] = {
		{ "Category", "Config" },
		{ "editcondition", "!bExportAllMeshSocketInfo" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "List of the name of the sockets from the owner's mesh need to be exported" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList = { "SocketNameToExportList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVCapturableActorTag, SocketNameToExportList), METADATA_PARAMS(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList_Inner = { "SocketNameToExportList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "If true, find all the socket in the owner's meshes and export all of their name and transform data\nOtherwise only export socket in the list" },
	};
#endif
	void Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo_SetBit(void* Obj)
	{
		((UNVCapturableActorTag*)Obj)->bExportAllMeshSocketInfo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo = { "bExportAllMeshSocketInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVCapturableActorTag), &Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
	};
#endif
	void Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe_SetBit(void* Obj)
	{
		((UNVCapturableActorTag*)Obj)->bIncludeMe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe = { "bIncludeMe", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVCapturableActorTag), &Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_Tag_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerUtils.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_Tag = { "Tag", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVCapturableActorTag, Tag), METADATA_PARAMS(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_Tag_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_Tag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVCapturableActorTag_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_SocketNameToExportList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bExportAllMeshSocketInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_bIncludeMe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCapturableActorTag_Statics::NewProp_Tag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVCapturableActorTag_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVCapturableActorTag>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVCapturableActorTag_Statics::ClassParams = {
		&UNVCapturableActorTag::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVCapturableActorTag_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVCapturableActorTag_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVCapturableActorTag_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVCapturableActorTag()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVCapturableActorTag_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVCapturableActorTag, 1176029724);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVCapturableActorTag>()
	{
		return UNVCapturableActorTag::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVCapturableActorTag(Z_Construct_UClass_UNVCapturableActorTag, &UNVCapturableActorTag::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVCapturableActorTag"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVCapturableActorTag);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
