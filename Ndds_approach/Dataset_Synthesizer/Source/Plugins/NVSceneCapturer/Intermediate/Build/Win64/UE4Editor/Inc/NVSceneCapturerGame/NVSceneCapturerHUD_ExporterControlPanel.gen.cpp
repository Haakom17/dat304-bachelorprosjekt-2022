// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerHUD_ExporterControlPanel() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked();
	UMG_API UClass* Z_Construct_UClass_UPanelWidget_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataExporter_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
// End Cross Module References
	void UNVSceneCapturerHUD_ExporterControlPanel::StaticRegisterNativesUNVSceneCapturerHUD_ExporterControlPanel()
	{
		UClass* Class = UNVSceneCapturerHUD_ExporterControlPanel::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnCompleted_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnCompleted_Clicked },
			{ "OnOpenOutputDirectory_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnOpenOutputDirectory_Clicked },
			{ "OnPauseExporting_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnPauseExporting_Clicked },
			{ "OnResumeExporting_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnResumeExporting_Clicked },
			{ "OnStartExporting_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnStartExporting_Clicked },
			{ "OnStopExporting_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnStopExporting_Clicked },
			{ "OnToggleExporterViewport_Clicked", &UNVSceneCapturerHUD_ExporterControlPanel::execOnToggleExporterViewport_Clicked },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnCompleted_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnOpenOutputDirectory_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnPauseExporting_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnResumeExporting_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
		{ "ToolTip", "NOTE: All these function need to be marked with UFUNCTION so they can be used with the Slate widget event" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnStartExporting_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnStopExporting_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, nullptr, "OnToggleExporterViewport_Clicked", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_NoRegister()
	{
		return UNVSceneCapturerHUD_ExporterControlPanel::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StatePanelList_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StatePanelList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentActiveStatePanel_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentActiveStatePanel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneDataExporter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneDataExporter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveCapturerActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveCapturerActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TitleText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TitleText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnCompleted_Clicked, "OnCompleted_Clicked" }, // 850316891
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnOpenOutputDirectory_Clicked, "OnOpenOutputDirectory_Clicked" }, // 2321901859
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnPauseExporting_Clicked, "OnPauseExporting_Clicked" }, // 2017820002
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnResumeExporting_Clicked, "OnResumeExporting_Clicked" }, // 3808289359
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStartExporting_Clicked, "OnStartExporting_Clicked" }, // 210601067
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnStopExporting_Clicked, "OnStopExporting_Clicked" }, // 1869450942
		{ &Z_Construct_UFunction_UNVSceneCapturerHUD_ExporterControlPanel_OnToggleExporterViewport_Clicked, "OnToggleExporterViewport_Clicked" }, // 773394292
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_StatePanelList_MetaData[] = {
		{ "ArraySizeEnum", "/Script/NVSceneCapturer.ENVSceneCapturerState" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_StatePanelList = { "StatePanelList", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(StatePanelList, UNVSceneCapturerHUD_ExporterControlPanel), STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, StatePanelList), Z_Construct_UClass_UPanelWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_StatePanelList_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_StatePanelList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_CurrentActiveStatePanel_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_CurrentActiveStatePanel = { "CurrentActiveStatePanel", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, CurrentActiveStatePanel), Z_Construct_UClass_UPanelWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_CurrentActiveStatePanel_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_CurrentActiveStatePanel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_SceneDataExporter_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_SceneDataExporter = { "SceneDataExporter", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, SceneDataExporter), Z_Construct_UClass_UNVSceneDataExporter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_SceneDataExporter_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_SceneDataExporter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_ActiveCapturerActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_ActiveCapturerActor = { "ActiveCapturerActor", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, ActiveCapturerActor), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_ActiveCapturerActor_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_ActiveCapturerActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_TitleText_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD_ExporterControlPanel.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_TitleText = { "TitleText", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, TitleText), METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_TitleText_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_TitleText_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_StatePanelList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_CurrentActiveStatePanel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_SceneDataExporter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_ActiveCapturerActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::NewProp_TitleText,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneCapturerHUD_ExporterControlPanel>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::ClassParams = {
		&UNVSceneCapturerHUD_ExporterControlPanel::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneCapturerHUD_ExporterControlPanel, 199248464);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<UNVSceneCapturerHUD_ExporterControlPanel>()
	{
		return UNVSceneCapturerHUD_ExporterControlPanel::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneCapturerHUD_ExporterControlPanel(Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel, &UNVSceneCapturerHUD_ExporterControlPanel::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("UNVSceneCapturerHUD_ExporterControlPanel"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneCapturerHUD_ExporterControlPanel);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
