// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/NVSceneCapturerPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerPlayerController() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug();
	NVSCENECAPTURERGAME_API UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport();
// End Cross Module References
	void ANVSceneCapturerPlayerController::StaticRegisterNativesANVSceneCapturerPlayerController()
	{
		UClass* Class = ANVSceneCapturerPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleCursorMode", &ANVSceneCapturerPlayerController::execToggleCursorMode },
			{ "ToggleExporterManagement", &ANVSceneCapturerPlayerController::execToggleExporterManagement },
			{ "ToggleHUDOverlay", &ANVSceneCapturerPlayerController::execToggleHUDOverlay },
			{ "TogglePause", &ANVSceneCapturerPlayerController::execTogglePause },
			{ "ToggleShowExportActorDebug", &ANVSceneCapturerPlayerController::execToggleShowExportActorDebug },
			{ "ToggleTakeOverViewport", &ANVSceneCapturerPlayerController::execToggleTakeOverViewport },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleCursorMode", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleExporterManagement", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleHUDOverlay", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "TogglePause", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleShowExportActorDebug", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleTakeOverViewport", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController_NoRegister()
	{
		return ANVSceneCapturerPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode, "ToggleCursorMode" }, // 3285543687
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement, "ToggleExporterManagement" }, // 410887524
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay, "ToggleHUDOverlay" }, // 3001909900
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause, "TogglePause" }, // 1234924706
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug, "ToggleShowExportActorDebug" }, // 50699574
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport, "ToggleTakeOverViewport" }, // 3791614218
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "NVSceneCapturerPlayerController.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneCapturerPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::ClassParams = {
		&ANVSceneCapturerPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneCapturerPlayerController, 1865871426);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<ANVSceneCapturerPlayerController>()
	{
		return ANVSceneCapturerPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneCapturerPlayerController(Z_Construct_UClass_ANVSceneCapturerPlayerController, &ANVSceneCapturerPlayerController::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("ANVSceneCapturerPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneCapturerPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
