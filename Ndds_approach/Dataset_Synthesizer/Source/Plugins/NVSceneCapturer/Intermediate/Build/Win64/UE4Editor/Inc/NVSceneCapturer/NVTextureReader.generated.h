// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVTextureReader_generated_h
#error "NVTextureReader.generated.h already included, missing '#pragma once' in NVTextureReader.h"
#endif
#define NVSCENECAPTURER_NVTextureReader_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVTextureReader_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__SourceRenderTarget() { return STRUCT_OFFSET(FNVTextureRenderTargetReader, SourceRenderTarget); } \
	typedef FNVTextureReader Super;


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVTextureRenderTargetReader>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVTextureReader_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVTextureReader_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVTextureReader>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVTextureReader_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
