// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneCapturerViewpointComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerViewpointComponent() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerSettings();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVFeatureExtractorSettings();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister();
// End Cross Module References
class UScriptStruct* FNVSceneCapturerViewpointSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneCapturerViewpointSettings"), sizeof(FNVSceneCapturerViewpointSettings), Get_Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneCapturerViewpointSettings>()
{
	return FNVSceneCapturerViewpointSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneCapturerViewpointSettings(FNVSceneCapturerViewpointSettings::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneCapturerViewpointSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerViewpointSettings
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerViewpointSettings()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVSceneCapturerViewpointSettings")),new UScriptStruct::TCppStructOps<FNVSceneCapturerViewpointSettings>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCapturerViewpointSettings;
	struct Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CaptureSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideCaptureSettings_MetaData[];
#endif
		static void NewProp_bOverrideCaptureSettings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideCaptureSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureExtractorSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeatureExtractorSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeatureExtractorSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideFeatureExtractorSettings_MetaData[];
#endif
		static void NewProp_bOverrideFeatureExtractorSettings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideFeatureExtractorSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportFileNamePostfix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExportFileNamePostfix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneCapturerViewpointSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_CaptureSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "editcondition", "bOverrideCaptureSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_CaptureSettings = { "CaptureSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerViewpointSettings, CaptureSettings), Z_Construct_UScriptStruct_FNVSceneCapturerSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_CaptureSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_CaptureSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the viewpoint have its own capture settings and doesn't use the owner capturer's feature extractor settings" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings_SetBit(void* Obj)
	{
		((FNVSceneCapturerViewpointSettings*)Obj)->bOverrideCaptureSettings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings = { "bOverrideCaptureSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneCapturerViewpointSettings), &Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings_MetaData[] = {
		{ "Category", "FeatureExtraction" },
		{ "editcondition", "bOverrideFeatureExtractorSettings" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings = { "FeatureExtractorSettings", nullptr, (EPropertyFlags)0x0010008000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerViewpointSettings, FeatureExtractorSettings), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings_Inner = { "FeatureExtractorSettings", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVFeatureExtractorSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings_MetaData[] = {
		{ "Category", "FeatureExtraction" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, the viewpoint have its own feature extractors settings and doesn't use the owner capturer's feature extractor settings" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings_SetBit(void* Obj)
	{
		((FNVSceneCapturerViewpointSettings*)Obj)->bOverrideFeatureExtractorSettings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings = { "bOverrideFeatureExtractorSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneCapturerViewpointSettings), &Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_ExportFileNamePostfix_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ToolTip", "The string to add to the end of the exported file's name captured from this viewpoint. e.g: \"infrared\", \"left\", \"right\" ..." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_ExportFileNamePostfix = { "ExportFileNamePostfix", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerViewpointSettings, ExportFileNamePostfix), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_ExportFileNamePostfix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_ExportFileNamePostfix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ToolTip", "Name of the viewpoint to show" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCapturerViewpointSettings, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_DisplayName_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ToolTip", "Editor properties\nIf true, the viewpoint will capture otherwise it won't" },
	};
#endif
	void Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FNVSceneCapturerViewpointSettings*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FNVSceneCapturerViewpointSettings), &Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_CaptureSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideCaptureSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_FeatureExtractorSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bOverrideFeatureExtractorSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_ExportFileNamePostfix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::NewProp_bIsEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneCapturerViewpointSettings",
		sizeof(FNVSceneCapturerViewpointSettings),
		alignof(FNVSceneCapturerViewpointSettings),
		Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneCapturerViewpointSettings"), sizeof(FNVSceneCapturerViewpointSettings), Get_Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings_Hash() { return 1452901930U; }
	void UNVSceneCapturerViewpointComponent::StaticRegisterNativesUNVSceneCapturerViewpointComponent()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister()
	{
		return UNVSceneCapturerViewpointComponent::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraMesh;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerSceneCapturer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerSceneCapturer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureExtractorList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeatureExtractorList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FeatureExtractorList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Collision Physics PhysX Activation Sockets Rendering LOD Tags Trigger PhysicsVolume" },
		{ "IncludePath", "NVSceneCapturerViewpointComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "UNVSceneCapturerViewpointComponent: Represents each viewpoint from where the capturer captures data\n\n@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_CameraMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_CameraMesh = { "CameraMesh", nullptr, (EPropertyFlags)0x0020080800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerViewpointComponent, CameraMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_CameraMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_CameraMesh_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_OwnerSceneCapturer_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_OwnerSceneCapturer = { "OwnerSceneCapturer", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerViewpointComponent, OwnerSceneCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_OwnerSceneCapturer_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_OwnerSceneCapturer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList = { "FeatureExtractorList", nullptr, (EPropertyFlags)0x0010008000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerViewpointComponent, FeatureExtractorList), METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList_Inner = { "FeatureExtractorList", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerViewpointComponent.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010008000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCapturerViewpointComponent, Settings), Z_Construct_UScriptStruct_FNVSceneCapturerViewpointSettings, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_Settings_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_CameraMesh,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_OwnerSceneCapturer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_FeatureExtractorList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneCapturerViewpointComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::ClassParams = {
		&UNVSceneCapturerViewpointComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneCapturerViewpointComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneCapturerViewpointComponent, 3028587283);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneCapturerViewpointComponent>()
	{
		return UNVSceneCapturerViewpointComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneCapturerViewpointComponent(Z_Construct_UClass_UNVSceneCapturerViewpointComponent, &UNVSceneCapturerViewpointComponent::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneCapturerViewpointComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneCapturerViewpointComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
