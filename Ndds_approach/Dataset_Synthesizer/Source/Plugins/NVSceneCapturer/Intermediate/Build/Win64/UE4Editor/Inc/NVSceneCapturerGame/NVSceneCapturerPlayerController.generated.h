// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerPlayerController_generated_h
#error "NVSceneCapturerPlayerController.generated.h already included, missing '#pragma once' in NVSceneCapturerPlayerController.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerPlayerController_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTogglePause) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TogglePause(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleTakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleShowExportActorDebug) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleShowExportActorDebug(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleHUDOverlay) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleHUDOverlay(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleExporterManagement) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleExporterManagement(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleCursorMode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleCursorMode(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTogglePause) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TogglePause(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleTakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleShowExportActorDebug) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleShowExportActorDebug(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleHUDOverlay) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleHUDOverlay(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleExporterManagement) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleExporterManagement(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleCursorMode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ToggleCursorMode(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneCapturerPlayerController(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerPlayerController)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneCapturerPlayerController(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerPlayerController)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneCapturerPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerPlayerController(ANVSceneCapturerPlayerController&&); \
	NO_API ANVSceneCapturerPlayerController(const ANVSceneCapturerPlayerController&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerPlayerController(ANVSceneCapturerPlayerController&&); \
	NO_API ANVSceneCapturerPlayerController(const ANVSceneCapturerPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerPlayerController)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_16_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class ANVSceneCapturerPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
