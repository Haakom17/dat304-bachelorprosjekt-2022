// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVImageExporter_generated_h
#error "NVImageExporter.generated.h already included, missing '#pragma once' in NVImageExporter.h"
#endif
#define NVSCENECAPTURER_NVImageExporter_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVImageExporter_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVImageExporterData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVImageExporterData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVImageExporter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
