// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVCameraSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVCameraSettings() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FCameraIntrinsicSettings();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVNamedImageSizePreset();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVImageSize();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UCameraSettingsFactoryBase_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UCameraSettingsFactoryBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UCameraSettingFactory_HFOV_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UCameraSettingFactory_HFOV();
// End Cross Module References
class UScriptStruct* FCameraIntrinsicSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCameraIntrinsicSettings, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("CameraIntrinsicSettings"), sizeof(FCameraIntrinsicSettings), Get_Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FCameraIntrinsicSettings>()
{
	return FCameraIntrinsicSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCameraIntrinsicSettings(FCameraIntrinsicSettings::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("CameraIntrinsicSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCameraIntrinsicSettings
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFCameraIntrinsicSettings()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("CameraIntrinsicSettings")),new UScriptStruct::TCppStructOps<FCameraIntrinsicSettings>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFCameraIntrinsicSettings;
	struct Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntrinsicMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IntrinsicMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_S_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_S;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Cy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cx_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Cx;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Fy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Fx_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Fx;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ResY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ResX;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCameraIntrinsicSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ProjectionMatrix_MetaData[] = {
		{ "Category", "Matrix" },
		{ "DisplayName", "Projection matrix" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ProjectionMatrix = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, ProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ProjectionMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_IntrinsicMatrix_MetaData[] = {
		{ "Category", "Matrix" },
		{ "DisplayName", "Intrinsic matrix" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_IntrinsicMatrix = { "IntrinsicMatrix", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, IntrinsicMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_IntrinsicMatrix_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_IntrinsicMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_S_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Skew coefficient" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Skew coefficient" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_S = { "S", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, S), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_S_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_S_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cy_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Principal point - Y" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cy = { "Cy", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, Cy), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cy_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cx_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Principal point - X" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Principal point offset" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cx = { "Cx", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, Cx), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cx_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cx_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fy_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Focal length - Y" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Focal length along Y axis" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fy = { "Fy", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, Fy), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fy_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fx_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Focal length - X" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Focal length along X axis" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fx = { "Fx", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, Fx), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fx_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fx_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResY_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Resolution - Y" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Resolution's height" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResY = { "ResY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, ResY), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResY_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResX_MetaData[] = {
		{ "Category", "CameraIntrinsicSettings" },
		{ "DisplayName", "Resolution - X" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Editor properties\nResolution's width" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResX = { "ResX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCameraIntrinsicSettings, ResX), METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResX_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResX_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_IntrinsicMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_S,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Cx,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_Fx,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::NewProp_ResX,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"CameraIntrinsicSettings",
		sizeof(FCameraIntrinsicSettings),
		alignof(FCameraIntrinsicSettings),
		Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCameraIntrinsicSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CameraIntrinsicSettings"), sizeof(FCameraIntrinsicSettings), Get_Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Hash() { return 297314987U; }
class UScriptStruct* FNVNamedImageSizePreset::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVNamedImageSizePreset, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVNamedImageSizePreset"), sizeof(FNVNamedImageSizePreset), Get_Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVNamedImageSizePreset>()
{
	return FNVNamedImageSizePreset::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVNamedImageSizePreset(FNVNamedImageSizePreset::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVNamedImageSizePreset"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVNamedImageSizePreset
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVNamedImageSizePreset()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVNamedImageSizePreset")),new UScriptStruct::TCppStructOps<FNVNamedImageSizePreset>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVNamedImageSizePreset;
	struct Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVNamedImageSizePreset>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_ImageSize_MetaData[] = {
		{ "Category", "NVNamedImageSizePreset" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_ImageSize = { "ImageSize", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVNamedImageSizePreset, ImageSize), Z_Construct_UScriptStruct_FNVImageSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_ImageSize_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_ImageSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "NVNamedImageSizePreset" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVNamedImageSizePreset, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_Name_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_ImageSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVNamedImageSizePreset",
		sizeof(FNVNamedImageSizePreset),
		alignof(FNVNamedImageSizePreset),
		Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVNamedImageSizePreset()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVNamedImageSizePreset"), sizeof(FNVNamedImageSizePreset), Get_Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Hash() { return 1162731099U; }
class UScriptStruct* FNVImageSize::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVImageSize_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVImageSize, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVImageSize"), sizeof(FNVImageSize), Get_Z_Construct_UScriptStruct_FNVImageSize_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVImageSize>()
{
	return FNVImageSize::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVImageSize(FNVImageSize::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVImageSize"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageSize
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageSize()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("NVImageSize")),new UScriptStruct::TCppStructOps<FNVImageSize>);
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageSize;
	struct Z_Construct_UScriptStruct_FNVImageSize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Width;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageSize_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVImageSize_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVImageSize>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "NVImageSize" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "The image's height (in pixels)." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVImageSize, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Height_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "NVImageSize" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "The image's width (in pixels)." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVImageSize, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Width_MetaData, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Width_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVImageSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageSize_Statics::NewProp_Width,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVImageSize_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVImageSize",
		sizeof(FNVImageSize),
		alignof(FNVImageSize),
		Z_Construct_UScriptStruct_FNVImageSize_Statics::PropPointers,
		ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageSize_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageSize_Statics::Struct_MetaDataParams, ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageSize_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVImageSize()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVImageSize_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVImageSize"), sizeof(FNVImageSize), Get_Z_Construct_UScriptStruct_FNVImageSize_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVImageSize_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVImageSize_Hash() { return 2941919433U; }
	void UCameraSettingsFactoryBase::StaticRegisterNativesUCameraSettingsFactoryBase()
	{
	}
	UClass* Z_Construct_UClass_UCameraSettingsFactoryBase_NoRegister()
	{
		return UCameraSettingsFactoryBase::StaticClass();
	}
	struct Z_Construct_UClass_UCameraSettingsFactoryBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Resolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Resolution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVCameraSettings.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "============================= Camera settings factories =============================\n Base class for all the different ways to create a camera settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_CameraSettings_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "The camera settings which this factory present" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_CameraSettings = { "CameraSettings", nullptr, (EPropertyFlags)0x00200c0000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraSettingsFactoryBase, CameraSettings), Z_Construct_UScriptStruct_FCameraIntrinsicSettings, METADATA_PARAMS(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_CameraSettings_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_CameraSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_Resolution_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_Resolution = { "Resolution", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraSettingsFactoryBase, Resolution), Z_Construct_UScriptStruct_FNVImageSize, METADATA_PARAMS(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_Resolution_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_Resolution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_CameraSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::NewProp_Resolution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraSettingsFactoryBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::ClassParams = {
		&UCameraSettingsFactoryBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::PropPointers),
		0,
		0x003010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraSettingsFactoryBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraSettingsFactoryBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraSettingsFactoryBase, 3650635382);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UCameraSettingsFactoryBase>()
	{
		return UCameraSettingsFactoryBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraSettingsFactoryBase(Z_Construct_UClass_UCameraSettingsFactoryBase, &UCameraSettingsFactoryBase::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UCameraSettingsFactoryBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraSettingsFactoryBase);
	void UCameraSettingFactory_HFOV::StaticRegisterNativesUCameraSettingFactory_HFOV()
	{
	}
	UClass* Z_Construct_UClass_UCameraSettingFactory_HFOV_NoRegister()
	{
		return UCameraSettingFactory_HFOV::StaticClass();
	}
	struct Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HFOV_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HFOV;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraSettingsFactoryBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "IncludePath", "NVCameraSettings.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Create camera settings using horizontal field of view" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::NewProp_HFOV_MetaData[] = {
		{ "Category", "Settings" },
		{ "DisplayName", "Horizontal Field-Of-View" },
		{ "ModuleRelativePath", "Public/NVCameraSettings.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::NewProp_HFOV = { "HFOV", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCameraSettingFactory_HFOV, HFOV), METADATA_PARAMS(Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::NewProp_HFOV_MetaData, ARRAY_COUNT(Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::NewProp_HFOV_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::NewProp_HFOV,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraSettingFactory_HFOV>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::ClassParams = {
		&UCameraSettingFactory_HFOV::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraSettingFactory_HFOV()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraSettingFactory_HFOV, 3390406483);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UCameraSettingFactory_HFOV>()
	{
		return UCameraSettingFactory_HFOV::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraSettingFactory_HFOV(Z_Construct_UClass_UCameraSettingFactory_HFOV, &UCameraSettingFactory_HFOV::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UCameraSettingFactory_HFOV"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraSettingFactory_HFOV);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
