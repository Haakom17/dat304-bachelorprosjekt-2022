// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UNVSceneFeatureExtractor;
class UNVSceneCapturerViewpointComponent;
#ifdef NVSCENECAPTURER_NVSceneDataHandler_generated_h
#error "NVSceneDataHandler.generated.h already included, missing '#pragma once' in NVSceneDataHandler.h"
#endif
#define NVSCENECAPTURER_NVSceneDataHandler_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataHandler(); \
	friend struct Z_Construct_UClass_UNVSceneDataHandler_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataHandler, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataHandler)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataHandler(); \
	friend struct Z_Construct_UClass_UNVSceneDataHandler_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataHandler, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataHandler)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataHandler(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataHandler) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataHandler); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataHandler); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataHandler(UNVSceneDataHandler&&); \
	NO_API UNVSceneDataHandler(const UNVSceneDataHandler&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataHandler(UNVSceneDataHandler&&); \
	NO_API UNVSceneDataHandler(const UNVSceneDataHandler&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataHandler); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataHandler); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataHandler)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_18_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataHandler>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetExportFilePath) \
	{ \
		P_GET_OBJECT(UNVSceneFeatureExtractor,Z_Param_CapturedFeatureExtractor); \
		P_GET_OBJECT(UNVSceneCapturerViewpointComponent,Z_Param_CapturedViewpoint); \
		P_GET_PROPERTY(UIntProperty,Z_Param_FrameIndex); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileExtension); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetExportFilePath(Z_Param_CapturedFeatureExtractor,Z_Param_CapturedViewpoint,Z_Param_FrameIndex,Z_Param_FileExtension); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetSubFolderName) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_NewSubFolderName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetSubFolderName(Z_Param_NewSubFolderName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSubFolderName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetSubFolderName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFullOutputDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetFullOutputDirectoryPath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryPath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetExportFolderName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetExportFolderName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRootCaptureDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetRootCaptureDirectoryPath(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetExportFilePath) \
	{ \
		P_GET_OBJECT(UNVSceneFeatureExtractor,Z_Param_CapturedFeatureExtractor); \
		P_GET_OBJECT(UNVSceneCapturerViewpointComponent,Z_Param_CapturedViewpoint); \
		P_GET_PROPERTY(UIntProperty,Z_Param_FrameIndex); \
		P_GET_PROPERTY(UStrProperty,Z_Param_FileExtension); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetExportFilePath(Z_Param_CapturedFeatureExtractor,Z_Param_CapturedViewpoint,Z_Param_FrameIndex,Z_Param_FileExtension); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetSubFolderName) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_NewSubFolderName); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetSubFolderName(Z_Param_NewSubFolderName); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetSubFolderName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetSubFolderName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetFullOutputDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetFullOutputDirectoryPath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryPath(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetExportFolderName) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetExportFolderName(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetRootCaptureDirectoryPath) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=P_THIS->GetRootCaptureDirectoryPath(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataExporter(); \
	friend struct Z_Construct_UClass_UNVSceneDataExporter_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataExporter, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataExporter)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataExporter(); \
	friend struct Z_Construct_UClass_UNVSceneDataExporter_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataExporter, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataExporter)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataExporter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataExporter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataExporter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataExporter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataExporter(UNVSceneDataExporter&&); \
	NO_API UNVSceneDataExporter(const UNVSceneDataExporter&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataExporter(UNVSceneDataExporter&&); \
	NO_API UNVSceneDataExporter(const UNVSceneDataExporter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataExporter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataExporter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataExporter)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootCapturedDirectoryPath() { return STRUCT_OFFSET(UNVSceneDataExporter, RootCapturedDirectoryPath); } \
	FORCEINLINE static uint32 __PPO__DirectoryConflictHandleType() { return STRUCT_OFFSET(UNVSceneDataExporter, DirectoryConflictHandleType); } \
	FORCEINLINE static uint32 __PPO__bAutoOpenExportedDirectory() { return STRUCT_OFFSET(UNVSceneDataExporter, bAutoOpenExportedDirectory); } \
	FORCEINLINE static uint32 __PPO__MaxSaveImageAsyncCount() { return STRUCT_OFFSET(UNVSceneDataExporter, MaxSaveImageAsyncCount); } \
	FORCEINLINE static uint32 __PPO__SubFolderName() { return STRUCT_OFFSET(UNVSceneDataExporter, SubFolderName); } \
	FORCEINLINE static uint32 __PPO__FullOutputDirectoryPath() { return STRUCT_OFFSET(UNVSceneDataExporter, FullOutputDirectoryPath); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_76_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataExporter>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataVisualizer(); \
	friend struct Z_Construct_UClass_UNVSceneDataVisualizer_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataVisualizer, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataVisualizer)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataVisualizer(); \
	friend struct Z_Construct_UClass_UNVSceneDataVisualizer_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataVisualizer, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataVisualizer)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataVisualizer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataVisualizer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataVisualizer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataVisualizer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataVisualizer(UNVSceneDataVisualizer&&); \
	NO_API UNVSceneDataVisualizer(const UNVSceneDataVisualizer&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataVisualizer(UNVSceneDataVisualizer&&); \
	NO_API UNVSceneDataVisualizer(const UNVSceneDataVisualizer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataVisualizer); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataVisualizer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataVisualizer)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VizTextureMap() { return STRUCT_OFFSET(UNVSceneDataVisualizer, VizTextureMap); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_184_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataVisualizer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h


#define FOREACH_ENUM_ENVCAPTUREDIRECTORYCONFLICTHANDLETYPE(op) \
	op(ENVCaptureDirectoryConflictHandleType::OverwriteExistingFiles) \
	op(ENVCaptureDirectoryConflictHandleType::CleanDirectory) \
	op(ENVCaptureDirectoryConflictHandleType::CreateNewDirectoryWithTimestampPostfix) \
	op(ENVCaptureDirectoryConflictHandleType::CaptureDirectoryConflictHandleType_MAX) 

enum class ENVCaptureDirectoryConflictHandleType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCaptureDirectoryConflictHandleType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
