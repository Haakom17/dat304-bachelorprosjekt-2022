// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneFeatureExtractor_DataExport_generated_h
#error "NVSceneFeatureExtractor_DataExport.generated.h already included, missing '#pragma once' in NVSceneFeatureExtractor_DataExport.h"
#endif
#define NVSCENECAPTURER_NVSceneFeatureExtractor_DataExport_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVDataExportSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVDataExportSettings>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_AnnotationData(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_AnnotationData, UNVSceneFeatureExtractor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_AnnotationData)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_AnnotationData(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_AnnotationData_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_AnnotationData, UNVSceneFeatureExtractor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_AnnotationData)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_AnnotationData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_AnnotationData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_AnnotationData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_AnnotationData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_AnnotationData(UNVSceneFeatureExtractor_AnnotationData&&); \
	NO_API UNVSceneFeatureExtractor_AnnotationData(const UNVSceneFeatureExtractor_AnnotationData&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_AnnotationData(UNVSceneFeatureExtractor_AnnotationData&&); \
	NO_API UNVSceneFeatureExtractor_AnnotationData(const UNVSceneFeatureExtractor_AnnotationData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_AnnotationData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_AnnotationData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_AnnotationData)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DataExportSettings() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, DataExportSettings); } \
	FORCEINLINE static uint32 __PPO__ViewProjectionMatrix() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, ViewProjectionMatrix); } \
	FORCEINLINE static uint32 __PPO__ProjectionMatrix() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_AnnotationData, ProjectionMatrix); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_50_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h_53_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_AnnotationData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_DataExport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
