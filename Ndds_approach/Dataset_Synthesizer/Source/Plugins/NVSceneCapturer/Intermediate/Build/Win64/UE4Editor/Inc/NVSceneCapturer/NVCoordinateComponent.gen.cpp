// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVCoordinateComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVCoordinateComponent() {}
// Cross Module References
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCoordinateComponent_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCoordinateComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void UNVCoordinateComponent::StaticRegisterNativesUNVCoordinateComponent()
	{
	}
	UClass* Z_Construct_UClass_UNVCoordinateComponent_NoRegister()
	{
		return UNVCoordinateComponent::StaticClass();
	}
	struct Z_Construct_UClass_UNVCoordinateComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AxisSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AxisSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArrowThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ArrowThickness;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVCoordinateComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimitiveComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCoordinateComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input Mobility Trigger" },
		{ "IncludePath", "NVCoordinateComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVCoordinateComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "The new actor which get annotated and have its info captured and exported\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_AxisSize_MetaData[] = {
		{ "Category", "NVCoordinateComponent" },
		{ "ModuleRelativePath", "Public/NVCoordinateComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_AxisSize = { "AxisSize", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVCoordinateComponent, AxisSize), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_AxisSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_AxisSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_ArrowThickness_MetaData[] = {
		{ "Category", "NVCoordinateComponent" },
		{ "ModuleRelativePath", "Public/NVCoordinateComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_ArrowThickness = { "ArrowThickness", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVCoordinateComponent, ArrowThickness), METADATA_PARAMS(Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_ArrowThickness_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_ArrowThickness_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVCoordinateComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_AxisSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVCoordinateComponent_Statics::NewProp_ArrowThickness,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVCoordinateComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVCoordinateComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVCoordinateComponent_Statics::ClassParams = {
		&UNVCoordinateComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVCoordinateComponent_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_UNVCoordinateComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVCoordinateComponent_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVCoordinateComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVCoordinateComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVCoordinateComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVCoordinateComponent, 957200755);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVCoordinateComponent>()
	{
		return UNVCoordinateComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVCoordinateComponent(Z_Construct_UClass_UNVCoordinateComponent, &UNVCoordinateComponent::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVCoordinateComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVCoordinateComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
