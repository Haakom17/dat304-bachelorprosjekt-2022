// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneCaptureComponent2D_generated_h
#error "NVSceneCaptureComponent2D.generated.h already included, missing '#pragma once' in NVSceneCaptureComponent2D.h"
#endif
#define NVSCENECAPTURER_NVSceneCaptureComponent2D_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStopCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartCapturing(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStopCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartCapturing(); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneCaptureComponent2D(); \
	friend struct Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCaptureComponent2D, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCaptureComponent2D)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneCaptureComponent2D(); \
	friend struct Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCaptureComponent2D, USceneCaptureComponent2D, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCaptureComponent2D)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneCaptureComponent2D(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCaptureComponent2D) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCaptureComponent2D); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCaptureComponent2D); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCaptureComponent2D(UNVSceneCaptureComponent2D&&); \
	NO_API UNVSceneCaptureComponent2D(const UNVSceneCaptureComponent2D&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCaptureComponent2D(UNVSceneCaptureComponent2D&&); \
	NO_API UNVSceneCaptureComponent2D(const UNVSceneCaptureComponent2D&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCaptureComponent2D); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCaptureComponent2D); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCaptureComponent2D)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_PRIVATE_PROPERTY_OFFSET
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_22_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneCaptureComponent2D>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCaptureComponent2D_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
