// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVAnnotatedActor_generated_h
#error "NVAnnotatedActor.generated.h already included, missing '#pragma once' in NVAnnotatedActor.h"
#endif
#define NVSCENECAPTURER_NVAnnotatedActor_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_RPC_WRAPPERS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVAnnotatedActor(); \
	friend struct Z_Construct_UClass_ANVAnnotatedActor_Statics; \
public: \
	DECLARE_CLASS(ANVAnnotatedActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVAnnotatedActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_INCLASS \
private: \
	static void StaticRegisterNativesANVAnnotatedActor(); \
	friend struct Z_Construct_UClass_ANVAnnotatedActor_Statics; \
public: \
	DECLARE_CLASS(ANVAnnotatedActor, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVAnnotatedActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVAnnotatedActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVAnnotatedActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVAnnotatedActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVAnnotatedActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVAnnotatedActor(ANVAnnotatedActor&&); \
	NO_API ANVAnnotatedActor(const ANVAnnotatedActor&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVAnnotatedActor(ANVAnnotatedActor&&); \
	NO_API ANVAnnotatedActor(const ANVAnnotatedActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVAnnotatedActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVAnnotatedActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVAnnotatedActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(ANVAnnotatedActor, MeshComponent); } \
	FORCEINLINE static uint32 __PPO__BoxComponent() { return STRUCT_OFFSET(ANVAnnotatedActor, BoxComponent); } \
	FORCEINLINE static uint32 __PPO__CoordComponent() { return STRUCT_OFFSET(ANVAnnotatedActor, CoordComponent); } \
	FORCEINLINE static uint32 __PPO__bForceCenterOfBoundingBoxAtRoot() { return STRUCT_OFFSET(ANVAnnotatedActor, bForceCenterOfBoundingBoxAtRoot); } \
	FORCEINLINE static uint32 __PPO__bSetClassNameFromMesh() { return STRUCT_OFFSET(ANVAnnotatedActor, bSetClassNameFromMesh); } \
	FORCEINLINE static uint32 __PPO__MeshCuboid() { return STRUCT_OFFSET(ANVAnnotatedActor, MeshCuboid); } \
	FORCEINLINE static uint32 __PPO__CuboidDimension() { return STRUCT_OFFSET(ANVAnnotatedActor, CuboidDimension); } \
	FORCEINLINE static uint32 __PPO__CuboidCenterLocal() { return STRUCT_OFFSET(ANVAnnotatedActor, CuboidCenterLocal); } \
	FORCEINLINE static uint32 __PPO__PCACenter() { return STRUCT_OFFSET(ANVAnnotatedActor, PCACenter); } \
	FORCEINLINE static uint32 __PPO__PCARotation() { return STRUCT_OFFSET(ANVAnnotatedActor, PCARotation); } \
	FORCEINLINE static uint32 __PPO__PCADirection() { return STRUCT_OFFSET(ANVAnnotatedActor, PCADirection); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_17_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class ANVAnnotatedActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVAnnotatedActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
