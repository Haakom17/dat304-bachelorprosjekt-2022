// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ANVSceneCapturerActor;
class UNVSceneDataHandler;
class UNVSceneCapturerViewpointComponent;
struct FNVFrameCounter;
enum class ENVSceneCapturerState : uint8;
#ifdef NVSCENECAPTURER_NVSceneCapturerActor_generated_h
#error "NVSceneCapturerActor.generated.h already included, missing '#pragma once' in NVSceneCapturerActor.h"
#endif
#define NVSCENECAPTURER_NVSceneCapturerActor_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_101_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVCameraSettingExportData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVCameraSettingExportData>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_78_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVViewpointSettingExportData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVViewpointSettingExportData>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_62_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVCapturerSettingExportData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVCapturerSettingExportData>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneAnnotatedActorData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneAnnotatedActorData>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNCapturerSettingExportedActorData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNCapturerSettingExportedActorData>();

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_113_DELEGATE \
struct _Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms \
{ \
	ANVSceneCapturerActor* SceneCapturer; \
	bool bIsSucceeded; \
}; \
static inline void FNVSceneCapturer_Completed_DelegateWrapper(const FMulticastScriptDelegate& NVSceneCapturer_Completed, ANVSceneCapturerActor* SceneCapturer, bool bIsSucceeded) \
{ \
	_Script_NVSceneCapturer_eventNVSceneCapturer_Completed_Parms Parms; \
	Parms.SceneCapturer=SceneCapturer; \
	Parms.bIsSucceeded=bIsSucceeded ? true : false; \
	NVSceneCapturer_Completed.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_112_DELEGATE \
struct _Script_NVSceneCapturer_eventNVSceneCapturer_Stopped_Parms \
{ \
	ANVSceneCapturerActor* SceneCapturer; \
}; \
static inline void FNVSceneCapturer_Stopped_DelegateWrapper(const FMulticastScriptDelegate& NVSceneCapturer_Stopped, ANVSceneCapturerActor* SceneCapturer) \
{ \
	_Script_NVSceneCapturer_eventNVSceneCapturer_Stopped_Parms Parms; \
	Parms.SceneCapturer=SceneCapturer; \
	NVSceneCapturer_Stopped.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_111_DELEGATE \
struct _Script_NVSceneCapturer_eventNVSceneCapturer_Started_Parms \
{ \
	ANVSceneCapturerActor* SceneCapturer; \
}; \
static inline void FNVSceneCapturer_Started_DelegateWrapper(const FMulticastScriptDelegate& NVSceneCapturer_Started, ANVSceneCapturerActor* SceneCapturer) \
{ \
	_Script_NVSceneCapturer_eventNVSceneCapturer_Started_Parms Parms; \
	Parms.SceneCapturer=SceneCapturer; \
	NVSceneCapturer_Started.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSceneDataHandler) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UNVSceneDataHandler**)Z_Param__Result=P_THIS->GetSceneDataHandler(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetViewpointList) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<UNVSceneCapturerViewpointComponent*>*)Z_Param__Result=P_THIS->GetViewpointList(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetExportedFrameCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetExportedFrameCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedDuration) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCapturedDuration(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCaptureProgressFraction) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCaptureProgressFraction(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEstimatedTimeUntilFinishCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetEstimatedTimeUntilFinishCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberOfFramesLeftToCapture) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfFramesLeftToCapture(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedFPS) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCapturedFPS(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedFrameCounter) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FNVFrameCounter*)Z_Param__Result=P_THIS->GetCapturedFrameCounter(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ENVSceneCapturerState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReturnViewportToPlayerController) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReturnViewportToPlayerController(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ToggleTakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResumeCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResumeCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPauseCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PauseCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStopCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberOfFramesToCapture) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfFramesToCapture(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetNumberOfFramesToCapture) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_NewSceneCount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetNumberOfFramesToCapture(Z_Param_NewSceneCount); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSceneDataHandler) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(UNVSceneDataHandler**)Z_Param__Result=P_THIS->GetSceneDataHandler(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetViewpointList) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<UNVSceneCapturerViewpointComponent*>*)Z_Param__Result=P_THIS->GetViewpointList(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetExportedFrameCount) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetExportedFrameCount(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedDuration) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCapturedDuration(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCaptureProgressFraction) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCaptureProgressFraction(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetEstimatedTimeUntilFinishCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetEstimatedTimeUntilFinishCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberOfFramesLeftToCapture) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfFramesLeftToCapture(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedFPS) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=P_THIS->GetCapturedFPS(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCapturedFrameCounter) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FNVFrameCounter*)Z_Param__Result=P_THIS->GetCapturedFrameCounter(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ENVSceneCapturerState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReturnViewportToPlayerController) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ReturnViewportToPlayerController(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->TakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execToggleTakeOverViewport) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=P_THIS->ToggleTakeOverViewport(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execResumeCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->ResumeCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPauseCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->PauseCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStopCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StopCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execStartCapturing) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->StartCapturing(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execGetNumberOfFramesToCapture) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfFramesToCapture(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSetNumberOfFramesToCapture) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_NewSceneCount); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetNumberOfFramesToCapture(Z_Param_NewSceneCount); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneCapturerActor(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerActor_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneCapturerActor(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerActor_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneCapturerActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerActor(ANVSceneCapturerActor&&); \
	NO_API ANVSceneCapturerActor(const ANVSceneCapturerActor&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerActor(ANVSceneCapturerActor&&); \
	NO_API ANVSceneCapturerActor(const ANVSceneCapturerActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerActor)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComponent() { return STRUCT_OFFSET(ANVSceneCapturerActor, CollisionComponent); } \
	FORCEINLINE static uint32 __PPO__bAutoStartCapturing() { return STRUCT_OFFSET(ANVSceneCapturerActor, bAutoStartCapturing); } \
	FORCEINLINE static uint32 __PPO__TimeBetweenSceneCapture() { return STRUCT_OFFSET(ANVSceneCapturerActor, TimeBetweenSceneCapture); } \
	FORCEINLINE static uint32 __PPO__MaxNumberOfFramesToCapture() { return STRUCT_OFFSET(ANVSceneCapturerActor, MaxNumberOfFramesToCapture); } \
	FORCEINLINE static uint32 __PPO__bTakeOverGameViewport() { return STRUCT_OFFSET(ANVSceneCapturerActor, bTakeOverGameViewport); } \
	FORCEINLINE static uint32 __PPO__SceneDataHandler() { return STRUCT_OFFSET(ANVSceneCapturerActor, SceneDataHandler); } \
	FORCEINLINE static uint32 __PPO__SceneDataVisualizer() { return STRUCT_OFFSET(ANVSceneCapturerActor, SceneDataVisualizer); } \
	FORCEINLINE static uint32 __PPO__bPauseGameLogicWhenFlushing() { return STRUCT_OFFSET(ANVSceneCapturerActor, bPauseGameLogicWhenFlushing); } \
	FORCEINLINE static uint32 __PPO__ImageSizePresets() { return STRUCT_OFFSET(ANVSceneCapturerActor, ImageSizePresets); } \
	FORCEINLINE static uint32 __PPO__StartCapturingTimestamp() { return STRUCT_OFFSET(ANVSceneCapturerActor, StartCapturingTimestamp); } \
	FORCEINLINE static uint32 __PPO__CapturedDuration() { return STRUCT_OFFSET(ANVSceneCapturerActor, CapturedDuration); } \
	FORCEINLINE static uint32 __PPO__StartCapturingDuration() { return STRUCT_OFFSET(ANVSceneCapturerActor, StartCapturingDuration); } \
	FORCEINLINE static uint32 __PPO__LastCaptureTimestamp() { return STRUCT_OFFSET(ANVSceneCapturerActor, LastCaptureTimestamp); } \
	FORCEINLINE static uint32 __PPO__CapturedFrameCounter() { return STRUCT_OFFSET(ANVSceneCapturerActor, CapturedFrameCounter); } \
	FORCEINLINE static uint32 __PPO__CachedPlayerControllerViewTarget() { return STRUCT_OFFSET(ANVSceneCapturerActor, CachedPlayerControllerViewTarget); } \
	FORCEINLINE static uint32 __PPO__CurrentState() { return STRUCT_OFFSET(ANVSceneCapturerActor, CurrentState); } \
	FORCEINLINE static uint32 __PPO__NumberOfFramesToCapture() { return STRUCT_OFFSET(ANVSceneCapturerActor, NumberOfFramesToCapture); } \
	FORCEINLINE static uint32 __PPO__bNeedToExportScene() { return STRUCT_OFFSET(ANVSceneCapturerActor, bNeedToExportScene); } \
	FORCEINLINE static uint32 __PPO__bTakingOverViewport() { return STRUCT_OFFSET(ANVSceneCapturerActor, bTakingOverViewport); } \
	FORCEINLINE static uint32 __PPO__bSkipFirstFrame() { return STRUCT_OFFSET(ANVSceneCapturerActor, bSkipFirstFrame); } \
	FORCEINLINE static uint32 __PPO__TimeHandle_StartCapturingDelay() { return STRUCT_OFFSET(ANVSceneCapturerActor, TimeHandle_StartCapturingDelay); } \
	FORCEINLINE static uint32 __PPO__ViewpointList() { return STRUCT_OFFSET(ANVSceneCapturerActor, ViewpointList); } \
	FORCEINLINE static uint32 __PPO__ImageToCapturePerFrame() { return STRUCT_OFFSET(ANVSceneCapturerActor, ImageToCapturePerFrame); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_120_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h_124_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class ANVSceneCapturerActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
