// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneCaptureComponent2D.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCaptureComponent2D() {}
// Cross Module References
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing();
	NVSCENECAPTURER_API UFunction* Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EPixelFormat();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ETextureRenderTargetFormat();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVImageSize();
// End Cross Module References
	void UNVSceneCaptureComponent2D::StaticRegisterNativesUNVSceneCaptureComponent2D()
	{
		UClass* Class = UNVSceneCaptureComponent2D::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "StartCapturing", &UNVSceneCaptureComponent2D::execStartCapturing },
			{ "StopCapturing", &UNVSceneCaptureComponent2D::execStopCapturing },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCaptureComponent2D, nullptr, "StartCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneCaptureComponent2D, nullptr, "StopCapturing", 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister()
	{
		return UNVSceneCaptureComponent2D::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreReadbackAlpha_MetaData[];
#endif
		static void NewProp_bIgnoreReadbackAlpha_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreReadbackAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideTexturePixelFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OverrideTexturePixelFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureTargetFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TextureTargetFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureTargetSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureTargetSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneCaptureComponent2D,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNVSceneCaptureComponent2D_StartCapturing, "StartCapturing" }, // 242153028
		{ &Z_Construct_UFunction_UNVSceneCaptureComponent2D_StopCapturing, "StopCapturing" }, // 3853140787
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Collision PhysX Activation Sockets MobileTonemapper PostProcessVolume Projection Rendering Transform PlanarReflection LOD Collision Object Physics SceneComponent abstract Collision Object Physics SceneComponent Mobility Trigger PhysicsVolume" },
		{ "IncludePath", "NVSceneCaptureComponent2D.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha_MetaData[] = {
		{ "Category", "SceneCapture" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
		{ "ToolTip", "If true, don't read back the raw alpha value from the render target but set it to 1" },
	};
#endif
	void Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha_SetBit(void* Obj)
	{
		((UNVSceneCaptureComponent2D*)Obj)->bIgnoreReadbackAlpha = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha = { "bIgnoreReadbackAlpha", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneCaptureComponent2D), &Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_OverrideTexturePixelFormat_MetaData[] = {
		{ "Category", "SceneCapture" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
		{ "ToolTip", "Pixel format of the texture used to capture the scene\nNOTE: This can be different from the TextureTargetFormat" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_OverrideTexturePixelFormat = { "OverrideTexturePixelFormat", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCaptureComponent2D, OverrideTexturePixelFormat), Z_Construct_UEnum_CoreUObject_EPixelFormat, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_OverrideTexturePixelFormat_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_OverrideTexturePixelFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetFormat_MetaData[] = {
		{ "Category", "SceneCapture" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
		{ "ToolTip", "Pixel format of the captured TextureTarget\nNOTE: If a valid TextureTarget is specified then this property will be ignored" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetFormat = { "TextureTargetFormat", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCaptureComponent2D, TextureTargetFormat), Z_Construct_UEnum_Engine_ETextureRenderTargetFormat, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetFormat_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetSize_MetaData[] = {
		{ "Category", "SceneCapture" },
		{ "ModuleRelativePath", "Public/NVSceneCaptureComponent2D.h" },
		{ "ToolTip", "Editor properties\nThe size (width x height in pixels) of the captured TextureTarget\nNOTE: If a valid TextureTarget is specified then this property will be ignored" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetSize = { "TextureTargetSize", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneCaptureComponent2D, TextureTargetSize), Z_Construct_UScriptStruct_FNVImageSize, METADATA_PARAMS(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetSize_MetaData, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_bIgnoreReadbackAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_OverrideTexturePixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::NewProp_TextureTargetSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneCaptureComponent2D>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::ClassParams = {
		&UNVSceneCaptureComponent2D::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		ARRAY_COUNT(FuncInfo),
		ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneCaptureComponent2D_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneCaptureComponent2D, 2030818827);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneCaptureComponent2D>()
	{
		return UNVSceneCaptureComponent2D::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneCaptureComponent2D(Z_Construct_UClass_UNVSceneCaptureComponent2D, &UNVSceneCaptureComponent2D::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneCaptureComponent2D"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneCaptureComponent2D);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
