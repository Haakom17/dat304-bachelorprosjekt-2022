// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerHUD_PIPPanel_generated_h
#error "NVSceneCapturerHUD_PIPPanel.generated.h already included, missing '#pragma once' in NVSceneCapturerHUD_PIPPanel.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerHUD_PIPPanel_generated_h

#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnChannelSelected) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SelectedValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_type); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChannelSelected(Z_Param_SelectedValue,ESelectInfo::Type(Z_Param_type)); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnChannelSelected) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_SelectedValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_type); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnChannelSelected(Z_Param_SelectedValue,ESelectInfo::Type(Z_Param_type)); \
		P_NATIVE_END; \
	}


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_PIPPanel(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_PIPPanel, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_PIPPanel)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_PIPPanel(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_PIPPanel_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_PIPPanel, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_PIPPanel)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneCapturerHUD_PIPPanel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_PIPPanel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_PIPPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_PIPPanel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_PIPPanel(UNVSceneCapturerHUD_PIPPanel&&); \
	NO_API UNVSceneCapturerHUD_PIPPanel(const UNVSceneCapturerHUD_PIPPanel&); \
public:


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_PIPPanel(UNVSceneCapturerHUD_PIPPanel&&); \
	NO_API UNVSceneCapturerHUD_PIPPanel(const UNVSceneCapturerHUD_PIPPanel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_PIPPanel); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_PIPPanel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_PIPPanel)


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bUseRenderTargetForVisualization() { return STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, bUseRenderTargetForVisualization); } \
	FORCEINLINE static uint32 __PPO__VisualizerMaterial() { return STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, VisualizerMaterial); } \
	FORCEINLINE static uint32 __PPO__ActiveCapturerActor() { return STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, ActiveCapturerActor); } \
	FORCEINLINE static uint32 __PPO__SceneDataVisualizer() { return STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, SceneDataVisualizer); } \
	FORCEINLINE static uint32 __PPO__VizMaterialDynamic() { return STRUCT_OFFSET(UNVSceneCapturerHUD_PIPPanel, VizMaterialDynamic); }


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_16_PROLOG
#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_RPC_WRAPPERS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_INCLASS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_INCLASS_NO_PURE_DECLS \
	Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class UNVSceneCapturerHUD_PIPPanel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_PIPPanel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
