// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVAnnotatedActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVAnnotatedActor() {}
// Cross Module References
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVAnnotatedActor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVAnnotatedActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVCuboidData();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCoordinateComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVCapturableActorTag_NoRegister();
// End Cross Module References
	void ANVAnnotatedActor::StaticRegisterNativesANVAnnotatedActor()
	{
	}
	UClass* Z_Construct_UClass_ANVAnnotatedActor_NoRegister()
	{
		return ANVAnnotatedActor::StaticClass();
	}
	struct Z_Construct_UClass_ANVAnnotatedActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PCADirection_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PCADirection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PCARotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PCARotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PCACenter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PCACenter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CuboidCenterLocal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CuboidCenterLocal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CuboidDimension_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CuboidDimension;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshCuboid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshCuboid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSetClassNameFromMesh_MetaData[];
#endif
		static void NewProp_bSetClassNameFromMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSetClassNameFromMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceCenterOfBoundingBoxAtRoot_MetaData[];
#endif
		static void NewProp_bForceCenterOfBoundingBoxAtRoot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceCenterOfBoundingBoxAtRoot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CoordComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CoordComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoxComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BoxComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnnotationTag_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AnnotationTag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVAnnotatedActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "HideCategories", "Replication Tick Tags Input" },
		{ "IncludePath", "NVAnnotatedActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "The new actor which get annotated and have its info captured and exported\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCADirection_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCADirection = { "PCADirection", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, PCADirection), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCADirection_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCADirection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCARotation_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCARotation = { "PCARotation", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, PCARotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCARotation_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCARotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCACenter_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCACenter = { "PCACenter", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, PCACenter), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCACenter_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCACenter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidCenterLocal_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidCenterLocal = { "CuboidCenterLocal", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, CuboidCenterLocal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidCenterLocal_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidCenterLocal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidDimension_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidDimension = { "CuboidDimension", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, CuboidDimension), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidDimension_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidDimension_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshCuboid_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshCuboid = { "MeshCuboid", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, MeshCuboid), Z_Construct_UScriptStruct_FNVCuboidData, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshCuboid_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshCuboid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh_MetaData[] = {
		{ "Category", "AnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ToolTip", "If true, this actor's annotation tag will be the same as the mesh" },
	};
#endif
	void Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh_SetBit(void* Obj)
	{
		((ANVAnnotatedActor*)Obj)->bSetClassNameFromMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh = { "bSetClassNameFromMesh", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVAnnotatedActor), &Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot_MetaData[] = {
		{ "Category", "AnnotatedActor" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ToolTip", "If true, the mesh of this actor will be placed so its center of 3d bounding box is at the root of the actor\nOtherwise user can place the actor manually" },
	};
#endif
	void Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot_SetBit(void* Obj)
	{
		((ANVAnnotatedActor*)Obj)->bForceCenterOfBoundingBoxAtRoot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot = { "bForceCenterOfBoundingBoxAtRoot", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVAnnotatedActor), &Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CoordComponent_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CoordComponent = { "CoordComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, CoordComponent), Z_Construct_UClass_UNVCoordinateComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CoordComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CoordComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_BoxComponent_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_BoxComponent = { "BoxComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, BoxComponent), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_BoxComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_BoxComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ToolTip", "Editor properties\nTODO: Need to support both static mesh and skeletal mesh types" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_AnnotationTag_MetaData[] = {
		{ "Category", "NVAnnotatedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVAnnotatedActor.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_AnnotationTag = { "AnnotationTag", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVAnnotatedActor, AnnotationTag), Z_Construct_UClass_UNVCapturableActorTag_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_AnnotationTag_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_AnnotationTag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVAnnotatedActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCADirection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCARotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_PCACenter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidCenterLocal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CuboidDimension,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshCuboid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bSetClassNameFromMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_bForceCenterOfBoundingBoxAtRoot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_CoordComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_BoxComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_MeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVAnnotatedActor_Statics::NewProp_AnnotationTag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVAnnotatedActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVAnnotatedActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVAnnotatedActor_Statics::ClassParams = {
		&ANVAnnotatedActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVAnnotatedActor_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ANVAnnotatedActor_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVAnnotatedActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVAnnotatedActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVAnnotatedActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVAnnotatedActor, 2182386502);
	template<> NVSCENECAPTURER_API UClass* StaticClass<ANVAnnotatedActor>()
	{
		return ANVAnnotatedActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVAnnotatedActor(Z_Construct_UClass_ANVAnnotatedActor, &ANVAnnotatedActor::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("ANVAnnotatedActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVAnnotatedActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
