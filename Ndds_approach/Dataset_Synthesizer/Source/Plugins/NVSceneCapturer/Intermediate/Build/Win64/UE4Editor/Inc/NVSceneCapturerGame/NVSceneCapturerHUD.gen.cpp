// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/HUD/NVSceneCapturerHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerHUD() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerHUD_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_UNVSceneCapturerHUD_Overlay_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UClass* Z_Construct_UClass_UFont_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void ANVSceneCapturerHUD::StaticRegisterNativesANVSceneCapturerHUD()
	{
	}
	UClass* Z_Construct_UClass_ANVSceneCapturerHUD_NoRegister()
	{
		return ANVSceneCapturerHUD::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneCapturerHUD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastCapturerLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastCapturerLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HUDOverlay_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HUDOverlay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugCapturerDirectionLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugCapturerDirectionLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugCapturerPathLifeTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugCapturerPathLifeTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugCapturerPathColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugCapturerPathColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowDebugCapturerPath_MetaData[];
#endif
		static void NewProp_bShowDebugCapturerPath_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowDebugCapturerPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowExportActorDebug_MetaData[];
#endif
		static void NewProp_bShowExportActorDebug_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowExportActorDebug;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CuboidDirectionRotation_HACK_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CuboidDirectionRotation_HACK;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CuboidDirectionDebugLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CuboidDirectionDebugLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDebugCuboidIn3d_MetaData[];
#endif
		static void NewProp_bDrawDebugCuboidIn3d_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDebugCuboidIn3d;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugCuboidVertexColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugCuboidVertexColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugVertexRadius3d_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugVertexRadius3d;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugLineThickness3d_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugLineThickness3d;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugVertexRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugVertexRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugLineThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DebugLineThickness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugTextBackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugTextBackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugTextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugTextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugLineColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DebugLineColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugFont_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DebugFont;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HUDOverlayClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_HUDOverlayClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneCapturerHUD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AHUD,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Rendering Actor Input Replication" },
		{ "IncludePath", "HUD/NVSceneCapturerHUD.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_LastCapturerLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_LastCapturerLocation = { "LastCapturerLocation", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, LastCapturerLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_LastCapturerLocation_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_LastCapturerLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlay_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlay = { "HUDOverlay", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, HUDOverlay), Z_Construct_UClass_UNVSceneCapturerHUD_Overlay_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlay_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerDirectionLength_MetaData[] = {
		{ "Category", "Debug" },
		{ "EditCondition", "bShowDebugCapturerPath" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerDirectionLength = { "DebugCapturerDirectionLength", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerDirectionLength), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerDirectionLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerDirectionLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathLifeTime_MetaData[] = {
		{ "Category", "Debug" },
		{ "EditCondition", "bShowDebugCapturerPath" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathLifeTime = { "DebugCapturerPathLifeTime", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerPathLifeTime), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathLifeTime_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathLifeTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathColor_MetaData[] = {
		{ "Category", "Debug" },
		{ "EditCondition", "bShowDebugCapturerPath" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathColor = { "DebugCapturerPathColor", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerPathColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath_SetBit(void* Obj)
	{
		((ANVSceneCapturerHUD*)Obj)->bShowDebugCapturerPath = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath = { "bShowDebugCapturerPath", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerHUD), &Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug_SetBit(void* Obj)
	{
		((ANVSceneCapturerHUD*)Obj)->bShowExportActorDebug = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug = { "bShowExportActorDebug", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerHUD), &Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionRotation_HACK_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionRotation_HACK = { "CuboidDirectionRotation_HACK", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, CuboidDirectionRotation_HACK), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionRotation_HACK_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionRotation_HACK_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionDebugLength_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionDebugLength = { "CuboidDirectionDebugLength", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, CuboidDirectionDebugLength), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionDebugLength_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionDebugLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	void Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d_SetBit(void* Obj)
	{
		((ANVSceneCapturerHUD*)Obj)->bDrawDebugCuboidIn3d = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d = { "bDrawDebugCuboidIn3d", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneCapturerHUD), &Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCuboidVertexColor_MetaData[] = {
		{ "ArraySizeEnum", "/Script/NVSceneCapturer.ENVCuboidVertexType" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCuboidVertexColor = { "DebugCuboidVertexColor", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(DebugCuboidVertexColor, ANVSceneCapturerHUD), STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCuboidVertexColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCuboidVertexColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCuboidVertexColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius3d_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius3d = { "DebugVertexRadius3d", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugVertexRadius3d), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius3d_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius3d_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness3d_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness3d = { "DebugLineThickness3d", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineThickness3d), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness3d_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness3d_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius = { "DebugVertexRadius", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugVertexRadius), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness = { "DebugLineThickness", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineThickness), METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextBackgroundColor_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextBackgroundColor = { "DebugTextBackgroundColor", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugTextBackgroundColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextBackgroundColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextBackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextColor_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextColor = { "DebugTextColor", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugTextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineColor_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineColor = { "DebugLineColor", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineColor_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugFont_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugFont = { "DebugFont", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, DebugFont), Z_Construct_UClass_UFont_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugFont_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugFont_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlayClass_MetaData[] = {
		{ "Category", "HUD" },
		{ "ModuleRelativePath", "Public/HUD/NVSceneCapturerHUD.h" },
		{ "ToolTip", "Editor properties:\nThe HUD widget class we want to use for the HUD overlay" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlayClass = { "HUDOverlayClass", nullptr, (EPropertyFlags)0x0024080000010005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerHUD, HUDOverlayClass), Z_Construct_UClass_UNVSceneCapturerHUD_Overlay_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlayClass_MetaData, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlayClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneCapturerHUD_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_LastCapturerLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerDirectionLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathLifeTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCapturerPathColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowDebugCapturerPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bShowExportActorDebug,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionRotation_HACK,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_CuboidDirectionDebugLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_bDrawDebugCuboidIn3d,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugCuboidVertexColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius3d,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness3d,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugVertexRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineThickness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextBackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugTextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugLineColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_DebugFont,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerHUD_Statics::NewProp_HUDOverlayClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneCapturerHUD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneCapturerHUD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneCapturerHUD_Statics::ClassParams = {
		&ANVSceneCapturerHUD::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVSceneCapturerHUD_Statics::PropPointers,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerHUD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneCapturerHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneCapturerHUD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneCapturerHUD, 3690670760);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<ANVSceneCapturerHUD>()
	{
		return ANVSceneCapturerHUD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneCapturerHUD(Z_Construct_UClass_ANVSceneCapturerHUD, &ANVSceneCapturerHUD::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("ANVSceneCapturerHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneCapturerHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
