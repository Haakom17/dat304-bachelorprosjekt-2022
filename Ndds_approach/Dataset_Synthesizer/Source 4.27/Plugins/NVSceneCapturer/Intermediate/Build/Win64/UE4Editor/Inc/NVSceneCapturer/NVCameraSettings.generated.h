// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVCameraSettings_generated_h
#error "NVCameraSettings.generated.h already included, missing '#pragma once' in NVCameraSettings.h"
#endif
#define NVSCENECAPTURER_NVCameraSettings_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCameraIntrinsicSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FCameraIntrinsicSettings>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVNamedImageSizePreset_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVNamedImageSizePreset>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_14_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVImageSize_Statics; \
	NVSCENECAPTURER_API static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVImageSize>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraSettingsFactoryBase(); \
	friend struct Z_Construct_UClass_UCameraSettingsFactoryBase_Statics; \
public: \
	DECLARE_CLASS(UCameraSettingsFactoryBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UCameraSettingsFactoryBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_INCLASS \
private: \
	static void StaticRegisterNativesUCameraSettingsFactoryBase(); \
	friend struct Z_Construct_UClass_UCameraSettingsFactoryBase_Statics; \
public: \
	DECLARE_CLASS(UCameraSettingsFactoryBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UCameraSettingsFactoryBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraSettingsFactoryBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraSettingsFactoryBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraSettingsFactoryBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraSettingsFactoryBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraSettingsFactoryBase(UCameraSettingsFactoryBase&&); \
	NO_API UCameraSettingsFactoryBase(const UCameraSettingsFactoryBase&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraSettingsFactoryBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraSettingsFactoryBase(UCameraSettingsFactoryBase&&); \
	NO_API UCameraSettingsFactoryBase(const UCameraSettingsFactoryBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraSettingsFactoryBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraSettingsFactoryBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraSettingsFactoryBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Resolution() { return STRUCT_OFFSET(UCameraSettingsFactoryBase, Resolution); } \
	FORCEINLINE static uint32 __PPO__CameraSettings() { return STRUCT_OFFSET(UCameraSettingsFactoryBase, CameraSettings); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_93_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_96_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UCameraSettingsFactoryBase>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCameraSettingFactory_HFOV(); \
	friend struct Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics; \
public: \
	DECLARE_CLASS(UCameraSettingFactory_HFOV, UCameraSettingsFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UCameraSettingFactory_HFOV)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_INCLASS \
private: \
	static void StaticRegisterNativesUCameraSettingFactory_HFOV(); \
	friend struct Z_Construct_UClass_UCameraSettingFactory_HFOV_Statics; \
public: \
	DECLARE_CLASS(UCameraSettingFactory_HFOV, UCameraSettingsFactoryBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UCameraSettingFactory_HFOV)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCameraSettingFactory_HFOV(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCameraSettingFactory_HFOV) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraSettingFactory_HFOV); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraSettingFactory_HFOV); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraSettingFactory_HFOV(UCameraSettingFactory_HFOV&&); \
	NO_API UCameraSettingFactory_HFOV(const UCameraSettingFactory_HFOV&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCameraSettingFactory_HFOV(UCameraSettingFactory_HFOV&&); \
	NO_API UCameraSettingFactory_HFOV(const UCameraSettingFactory_HFOV&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCameraSettingFactory_HFOV); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCameraSettingFactory_HFOV); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCameraSettingFactory_HFOV)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HFOV() { return STRUCT_OFFSET(UCameraSettingFactory_HFOV, HFOV); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_121_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h_124_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UCameraSettingFactory_HFOV>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVCameraSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
