// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UNVSceneFeatureExtractor;
class UNVSceneCapturerViewpointComponent;
#ifdef NVSCENECAPTURER_NVSceneDataHandler_generated_h
#error "NVSceneDataHandler.generated.h already included, missing '#pragma once' in NVSceneDataHandler.h"
#endif
#define NVSCENECAPTURER_NVSceneDataHandler_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataHandler(); \
	friend struct Z_Construct_UClass_UNVSceneDataHandler_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataHandler, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataHandler)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataHandler(); \
	friend struct Z_Construct_UClass_UNVSceneDataHandler_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataHandler, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataHandler)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataHandler(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataHandler) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataHandler); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataHandler(UNVSceneDataHandler&&); \
	NO_API UNVSceneDataHandler(const UNVSceneDataHandler&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataHandler(UNVSceneDataHandler&&); \
	NO_API UNVSceneDataHandler(const UNVSceneDataHandler&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataHandler); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataHandler); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataHandler)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_18_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataHandler>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetExportFilePath); \
	DECLARE_FUNCTION(execSetSubFolderName); \
	DECLARE_FUNCTION(execGetSubFolderName); \
	DECLARE_FUNCTION(execGetFullOutputDirectoryPath); \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryName); \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryPath); \
	DECLARE_FUNCTION(execGetExportFolderName); \
	DECLARE_FUNCTION(execGetRootCaptureDirectoryPath);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetExportFilePath); \
	DECLARE_FUNCTION(execSetSubFolderName); \
	DECLARE_FUNCTION(execGetSubFolderName); \
	DECLARE_FUNCTION(execGetFullOutputDirectoryPath); \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryName); \
	DECLARE_FUNCTION(execGetConfiguredOutputDirectoryPath); \
	DECLARE_FUNCTION(execGetExportFolderName); \
	DECLARE_FUNCTION(execGetRootCaptureDirectoryPath);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataExporter(); \
	friend struct Z_Construct_UClass_UNVSceneDataExporter_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataExporter, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataExporter)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataExporter(); \
	friend struct Z_Construct_UClass_UNVSceneDataExporter_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataExporter, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataExporter)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataExporter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataExporter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataExporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataExporter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataExporter(UNVSceneDataExporter&&); \
	NO_API UNVSceneDataExporter(const UNVSceneDataExporter&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataExporter(UNVSceneDataExporter&&); \
	NO_API UNVSceneDataExporter(const UNVSceneDataExporter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataExporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataExporter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataExporter)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootCapturedDirectoryPath() { return STRUCT_OFFSET(UNVSceneDataExporter, RootCapturedDirectoryPath); } \
	FORCEINLINE static uint32 __PPO__DirectoryConflictHandleType() { return STRUCT_OFFSET(UNVSceneDataExporter, DirectoryConflictHandleType); } \
	FORCEINLINE static uint32 __PPO__bAutoOpenExportedDirectory() { return STRUCT_OFFSET(UNVSceneDataExporter, bAutoOpenExportedDirectory); } \
	FORCEINLINE static uint32 __PPO__MaxSaveImageAsyncCount() { return STRUCT_OFFSET(UNVSceneDataExporter, MaxSaveImageAsyncCount); } \
	FORCEINLINE static uint32 __PPO__SubFolderName() { return STRUCT_OFFSET(UNVSceneDataExporter, SubFolderName); } \
	FORCEINLINE static uint32 __PPO__FullOutputDirectoryPath() { return STRUCT_OFFSET(UNVSceneDataExporter, FullOutputDirectoryPath); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_76_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_79_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataExporter>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneDataVisualizer(); \
	friend struct Z_Construct_UClass_UNVSceneDataVisualizer_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataVisualizer, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataVisualizer)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneDataVisualizer(); \
	friend struct Z_Construct_UClass_UNVSceneDataVisualizer_Statics; \
public: \
	DECLARE_CLASS(UNVSceneDataVisualizer, UNVSceneDataHandler, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneDataVisualizer)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneDataVisualizer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneDataVisualizer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataVisualizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataVisualizer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataVisualizer(UNVSceneDataVisualizer&&); \
	NO_API UNVSceneDataVisualizer(const UNVSceneDataVisualizer&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneDataVisualizer(UNVSceneDataVisualizer&&); \
	NO_API UNVSceneDataVisualizer(const UNVSceneDataVisualizer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneDataVisualizer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneDataVisualizer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVSceneDataVisualizer)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VizTextureMap() { return STRUCT_OFFSET(UNVSceneDataVisualizer, VizTextureMap); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_184_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h_187_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneDataVisualizer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneDataHandler_h


#define FOREACH_ENUM_ENVCAPTUREDIRECTORYCONFLICTHANDLETYPE(op) \
	op(ENVCaptureDirectoryConflictHandleType::OverwriteExistingFiles) \
	op(ENVCaptureDirectoryConflictHandleType::CleanDirectory) \
	op(ENVCaptureDirectoryConflictHandleType::CreateNewDirectoryWithTimestampPostfix) \
	op(ENVCaptureDirectoryConflictHandleType::CaptureDirectoryConflictHandleType_MAX) 

enum class ENVCaptureDirectoryConflictHandleType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCaptureDirectoryConflictHandleType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
