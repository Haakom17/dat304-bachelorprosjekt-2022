// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ANVSceneCapturerActor;
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerGameModeBase_generated_h
#error "NVSceneCapturerGameModeBase.generated.h already included, missing '#pragma once' in NVSceneCapturerGameModeBase.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerGameModeBase_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetFirstActiveCapturer); \
	DECLARE_FUNCTION(execGetSceneCapturerList);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetFirstActiveCapturer); \
	DECLARE_FUNCTION(execGetSceneCapturerList);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneCapturerGameModeBase(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerGameModeBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneCapturerGameModeBase(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerGameModeBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneCapturerGameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerGameModeBase(ANVSceneCapturerGameModeBase&&); \
	NO_API ANVSceneCapturerGameModeBase(const ANVSceneCapturerGameModeBase&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerGameModeBase(ANVSceneCapturerGameModeBase&&); \
	NO_API ANVSceneCapturerGameModeBase(const ANVSceneCapturerGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerGameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ANVSceneCapturerGameModeBase)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultSceneManagerClass() { return STRUCT_OFFSET(ANVSceneCapturerGameModeBase, DefaultSceneManagerClass); } \
	FORCEINLINE static uint32 __PPO__SceneExporterList() { return STRUCT_OFFSET(ANVSceneCapturerGameModeBase, SceneExporterList); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_16_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class ANVSceneCapturerGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_NVSceneCapturerGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
