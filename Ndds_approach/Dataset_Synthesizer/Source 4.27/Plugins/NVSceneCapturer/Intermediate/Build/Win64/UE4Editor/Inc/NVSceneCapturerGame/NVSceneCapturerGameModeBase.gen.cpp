// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/NVSceneCapturerGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerGameModeBase() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerGameModeBase_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneManager_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ANVSceneCapturerGameModeBase::execGetFirstActiveCapturer)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ANVSceneCapturerActor**)Z_Param__Result=P_THIS->GetFirstActiveCapturer();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerGameModeBase::execGetSceneCapturerList)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<ANVSceneCapturerActor*>*)Z_Param__Result=P_THIS->GetSceneCapturerList();
		P_NATIVE_END;
	}
	void ANVSceneCapturerGameModeBase::StaticRegisterNativesANVSceneCapturerGameModeBase()
	{
		UClass* Class = ANVSceneCapturerGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetFirstActiveCapturer", &ANVSceneCapturerGameModeBase::execGetFirstActiveCapturer },
			{ "GetSceneCapturerList", &ANVSceneCapturerGameModeBase::execGetSceneCapturerList },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics
	{
		struct NVSceneCapturerGameModeBase_eventGetFirstActiveCapturer_Parms
		{
			ANVSceneCapturerActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerGameModeBase_eventGetFirstActiveCapturer_Parms, ReturnValue), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerGameModeBase, nullptr, "GetFirstActiveCapturer", nullptr, nullptr, sizeof(NVSceneCapturerGameModeBase_eventGetFirstActiveCapturer_Parms), Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics
	{
		struct NVSceneCapturerGameModeBase_eventGetSceneCapturerList_Parms
		{
			TArray<ANVSceneCapturerActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000008000582, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneCapturerGameModeBase_eventGetSceneCapturerList_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::Function_MetaDataParams[] = {
		{ "Category", "Capturer" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerGameModeBase, nullptr, "GetSceneCapturerList", nullptr, nullptr, sizeof(NVSceneCapturerGameModeBase_eventGetSceneCapturerList_Parms), Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANVSceneCapturerGameModeBase_NoRegister()
	{
		return ANVSceneCapturerGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSceneManagerClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultSceneManagerClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneExporterList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneExporterList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneExporterList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetFirstActiveCapturer, "GetFirstActiveCapturer" }, // 3684629159
		{ &Z_Construct_UFunction_ANVSceneCapturerGameModeBase_GetSceneCapturerList, "GetSceneCapturerList" }, // 1455090443
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "NVSceneCapturerGameModeBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_DefaultSceneManagerClass_MetaData[] = {
		{ "Category", "Capturer" },
		{ "Comment", "/// Default class of the SceneManager to create automatically if the scene doesn't have one\n" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerGameModeBase.h" },
		{ "ToolTip", "Default class of the SceneManager to create automatically if the scene doesn't have one" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_DefaultSceneManagerClass = { "DefaultSceneManagerClass", nullptr, (EPropertyFlags)0x0024080000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerGameModeBase, DefaultSceneManagerClass), Z_Construct_UClass_ANVSceneManager_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_DefaultSceneManagerClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_DefaultSceneManagerClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList_Inner = { "SceneExporterList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneCapturerGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList = { "SceneExporterList", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneCapturerGameModeBase, SceneExporterList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_DefaultSceneManagerClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::NewProp_SceneExporterList,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneCapturerGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::ClassParams = {
		&ANVSceneCapturerGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneCapturerGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneCapturerGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneCapturerGameModeBase, 2723237309);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<ANVSceneCapturerGameModeBase>()
	{
		return ANVSceneCapturerGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneCapturerGameModeBase(Z_Construct_UClass_ANVSceneCapturerGameModeBase, &ANVSceneCapturerGameModeBase::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("ANVSceneCapturerGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneCapturerGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
