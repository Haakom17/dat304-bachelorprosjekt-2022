// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneFeatureExtractor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneFeatureExtractor() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCaptureComponentData();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVFeatureExtractorSettings();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_ANVSceneCapturerActor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister();
// End Cross Module References
class UScriptStruct* FNVSceneCaptureComponentData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVSceneCaptureComponentData"), sizeof(FNVSceneCaptureComponentData), Get_Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVSceneCaptureComponentData>()
{
	return FNVSceneCaptureComponentData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVSceneCaptureComponentData(FNVSceneCaptureComponentData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVSceneCaptureComponentData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCaptureComponentData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCaptureComponentData()
	{
		UScriptStruct::DeferCppStructOps<FNVSceneCaptureComponentData>(FName(TEXT("NVSceneCaptureComponentData")));
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVSceneCaptureComponentData;
	struct Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComp2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCaptureComp2D;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ComponentName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVSceneCaptureComponentData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_SceneCaptureComp2D_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_SceneCaptureComp2D = { "SceneCaptureComp2D", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCaptureComponentData, SceneCaptureComp2D), Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_SceneCaptureComp2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_SceneCaptureComp2D_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_ComponentName_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_ComponentName = { "ComponentName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVSceneCaptureComponentData, ComponentName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_ComponentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_ComponentName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_SceneCaptureComp2D,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::NewProp_ComponentName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVSceneCaptureComponentData",
		sizeof(FNVSceneCaptureComponentData),
		alignof(FNVSceneCaptureComponentData),
		Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCaptureComponentData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVSceneCaptureComponentData"), sizeof(FNVSceneCaptureComponentData), Get_Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Hash() { return 4211865233U; }
class UScriptStruct* FNVFeatureExtractorSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVFeatureExtractorSettings"), sizeof(FNVFeatureExtractorSettings), Get_Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVFeatureExtractorSettings>()
{
	return FNVFeatureExtractorSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVFeatureExtractorSettings(FNVFeatureExtractorSettings::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVFeatureExtractorSettings"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFeatureExtractorSettings
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFeatureExtractorSettings()
	{
		UScriptStruct::DeferCppStructOps<FNVFeatureExtractorSettings>(FName(TEXT("NVFeatureExtractorSettings")));
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVFeatureExtractorSettings;
	struct Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeatureExtractorRef_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FeatureExtractorRef;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVFeatureExtractorSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewProp_FeatureExtractorRef_MetaData[] = {
		{ "Category", "FeatureExtraction" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewProp_FeatureExtractorRef = { "FeatureExtractorRef", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVFeatureExtractorSettings, FeatureExtractorRef), Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewProp_FeatureExtractorRef_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewProp_FeatureExtractorRef_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::NewProp_FeatureExtractorRef,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVFeatureExtractorSettings",
		sizeof(FNVFeatureExtractorSettings),
		alignof(FNVFeatureExtractorSettings),
		Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVFeatureExtractorSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVFeatureExtractorSettings"), sizeof(FNVFeatureExtractorSettings), Get_Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Hash() { return 3298631933U; }
	void UNVSceneFeatureExtractor::StaticRegisterNativesUNVSceneFeatureExtractor()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister()
	{
		return UNVSceneFeatureExtractor::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportFileNamePostfix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExportFileNamePostfix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerCapturer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerCapturer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerViewpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerViewpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCapturing_MetaData[];
#endif
		static void NewProp_bCapturing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCapturing;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "///\n///\n///\n" },
		{ "IncludePath", "NVSceneFeatureExtractor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// Editor properties\n/// If true, the feature extractor will capture otherwise it won't\n/// ToDo: Move to protected.\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ToolTip", "Editor properties\nIf true, the feature extractor will capture otherwise it won't\nToDo: Move to protected." },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor), &Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// Name of the feature extractor to show\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ToolTip", "Name of the feature extractor to show" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor, DisplayName), METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// \n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor, Description), METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_ExportFileNamePostfix_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// The string to add to the end of the exported file's name captured from this feature extractor. e.g: \"depth\", \"mask\" ...\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ToolTip", "The string to add to the end of the exported file's name captured from this feature extractor. e.g: \"depth\", \"mask\" ..." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_ExportFileNamePostfix = { "ExportFileNamePostfix", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor, ExportFileNamePostfix), METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_ExportFileNamePostfix_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_ExportFileNamePostfix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerCapturer_MetaData[] = {
		{ "Comment", "// Transient properties\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerCapturer = { "OwnerCapturer", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor, OwnerCapturer), Z_Construct_UClass_ANVSceneCapturerActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerCapturer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerCapturer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerViewpoint_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerViewpoint = { "OwnerViewpoint", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor, OwnerViewpoint), Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerViewpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerViewpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor.h" },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor*)Obj)->bCapturing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing = { "bCapturing", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor), &Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_ExportFileNamePostfix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerCapturer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_OwnerViewpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::NewProp_bCapturing,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::ClassParams = {
		&UNVSceneFeatureExtractor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor, 2608197429);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor>()
	{
		return UNVSceneFeatureExtractor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor(Z_Construct_UClass_UNVSceneFeatureExtractor, &UNVSceneFeatureExtractor::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
