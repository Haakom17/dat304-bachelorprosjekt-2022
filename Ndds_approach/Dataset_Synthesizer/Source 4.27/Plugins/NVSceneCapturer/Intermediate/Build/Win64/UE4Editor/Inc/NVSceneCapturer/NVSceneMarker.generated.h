// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneMarker_generated_h
#error "NVSceneMarker.generated.h already included, missing '#pragma once' in NVSceneMarker.h"
#endif
#define NVSCENECAPTURER_NVSceneMarker_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_55_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneMarkerComponent_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__DisplayName() { return STRUCT_OFFSET(FNVSceneMarkerComponent, DisplayName); } \
	FORCEINLINE static uint32 __PPO__Description() { return STRUCT_OFFSET(FNVSceneMarkerComponent, Description); } \
	FORCEINLINE static uint32 __PPO__Observers() { return STRUCT_OFFSET(FNVSceneMarkerComponent, Observers); }


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneMarkerComponent>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneMarkerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneMarkerInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneMarkerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneMarkerInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneMarkerInterface(UNVSceneMarkerInterface&&); \
	NO_API UNVSceneMarkerInterface(const UNVSceneMarkerInterface&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneMarkerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneMarkerInterface(UNVSceneMarkerInterface&&); \
	NO_API UNVSceneMarkerInterface(const UNVSceneMarkerInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneMarkerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneMarkerInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneMarkerInterface)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUNVSceneMarkerInterface(); \
	friend struct Z_Construct_UClass_UNVSceneMarkerInterface_Statics; \
public: \
	DECLARE_CLASS(UNVSceneMarkerInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneMarkerInterface)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_GENERATED_UINTERFACE_BODY() \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_GENERATED_UINTERFACE_BODY() \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~INVSceneMarkerInterface() {} \
public: \
	typedef UNVSceneMarkerInterface UClassType; \
	typedef INVSceneMarkerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_INCLASS_IINTERFACE \
protected: \
	virtual ~INVSceneMarkerInterface() {} \
public: \
	typedef UNVSceneMarkerInterface UClassType; \
	typedef INVSceneMarkerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_16_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h_19_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneMarkerInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneMarker_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
