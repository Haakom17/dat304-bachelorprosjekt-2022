// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneFeatureExtractor_ImageExport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneFeatureExtractor_ImageExport() {}
// Cross Module References
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEngineShowFlagsSetting();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EPixelFormat();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ESceneCaptureSource();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneCaptureComponentData();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask();
// End Cross Module References
	void UNVSceneFeatureExtractor_PixelData::StaticRegisterNativesUNVSceneFeatureExtractor_PixelData()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_NoRegister()
	{
		return UNVSceneFeatureExtractor_PixelData::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyShowTrainingActors_MetaData[];
#endif
		static void NewProp_bOnlyShowTrainingActors_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyShowTrainingActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IgnoreActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IgnoreActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IgnoreActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideShowFlagSettings_MetaData[];
#endif
		static void NewProp_bOverrideShowFlagSettings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideShowFlagSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OverrideShowFlagSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideShowFlagSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OverrideShowFlagSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessBlendWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PostProcessBlendWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PostProcessMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideExportImageType_MetaData[];
#endif
		static void NewProp_bOverrideExportImageType_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideExportImageType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExportImageFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportImageFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExportImageFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedPixelFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CapturedPixelFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideTexturePixelFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OverrideTexturePixelFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CaptureSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUpdateContinuously_MetaData[];
#endif
		static void NewProp_bUpdateContinuously_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdateContinuously;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneCaptureComp2DDataList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComp2DDataList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SceneCaptureComp2DDataList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PostProcessMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCaptureComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/// Base class for all the feature extractors that capture the scene view in pixel data format\n" },
		{ "IncludePath", "NVSceneFeatureExtractor_ImageExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all the feature extractors that capture the scene view in pixel data format" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// Editor properties\n/// If true, only show the training actors in the exported images\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ToolTip", "Editor properties\nIf true, only show the training actors in the exported images" },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor_PixelData*)Obj)->bOnlyShowTrainingActors = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors = { "bOnlyShowTrainingActors", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor_PixelData), &Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors_Inner = { "IgnoreActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// List of actors to ignore when capturing image\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ToolTip", "List of actors to ignore when capturing image" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors = { "IgnoreActors", nullptr, (EPropertyFlags)0x0020080000000801, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, IgnoreActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings_MetaData[] = {
		{ "Category", "Config" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "PinHiddenByDefault", "" },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor_PixelData*)Obj)->bOverrideShowFlagSettings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings = { "bOverrideShowFlagSettings", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor_PixelData), &Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings_Inner = { "OverrideShowFlagSettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEngineShowFlagsSetting, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// ShowFlags for the SceneCapture's ViewFamily, to control rendering settings for this view. Hidden but accessible through details customization \n" },
		{ "editcondition", "bOverrideShowFlagSettings" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ToolTip", "ShowFlags for the SceneCapture's ViewFamily, to control rendering settings for this view. Hidden but accessible through details customization" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings = { "OverrideShowFlagSettings", nullptr, (EPropertyFlags)0x0020080200010005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, OverrideShowFlagSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessBlendWeight_MetaData[] = {
		{ "Category", "NVSceneFeatureExtractor_PixelData" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessBlendWeight = { "PostProcessBlendWeight", nullptr, (EPropertyFlags)0x00200c0000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessBlendWeight), METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessBlendWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessBlendWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterial_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterial = { "PostProcessMaterial", nullptr, (EPropertyFlags)0x00200a0000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "/// If true, this feature extractors have its own exported image type and doesn't use the owner capturer's image type\n" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "PinHiddenByDefault", "" },
		{ "ToolTip", "If true, this feature extractors have its own exported image type and doesn't use the owner capturer's image type" },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor_PixelData*)Obj)->bOverrideExportImageType = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType = { "bOverrideExportImageType", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor_PixelData), &Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat_MetaData[] = {
		{ "Category", "Config" },
		{ "editcondition", "bOverrideExportImageType" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat = { "ExportImageFormat", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, ExportImageFormat), Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CapturedPixelFormat_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CapturedPixelFormat = { "CapturedPixelFormat", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, CapturedPixelFormat), Z_Construct_UEnum_NVSceneCapturer_ENVCapturedPixelFormat, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CapturedPixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CapturedPixelFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideTexturePixelFormat_MetaData[] = {
		{ "Category", "NVSceneFeatureExtractor_PixelData" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideTexturePixelFormat = { "OverrideTexturePixelFormat", nullptr, (EPropertyFlags)0x00200c0000010001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, OverrideTexturePixelFormat), Z_Construct_UEnum_CoreUObject_EPixelFormat, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideTexturePixelFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideTexturePixelFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CaptureSource_MetaData[] = {
		{ "Category", "NVSceneFeatureExtractor_PixelData" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CaptureSource = { "CaptureSource", nullptr, (EPropertyFlags)0x00200c0000010001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, CaptureSource), Z_Construct_UEnum_Engine_ESceneCaptureSource, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CaptureSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CaptureSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously_MetaData[] = {
		{ "Category", "NVSceneFeatureExtractor_PixelData" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	void Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously_SetBit(void* Obj)
	{
		((UNVSceneFeatureExtractor_PixelData*)Obj)->bUpdateContinuously = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously = { "bUpdateContinuously", nullptr, (EPropertyFlags)0x00200c0000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneFeatureExtractor_PixelData), &Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList_Inner = { "SceneCaptureComp2DDataList", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FNVSceneCaptureComponentData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList_MetaData[] = {
		{ "Comment", "// Transient properties\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList = { "SceneCaptureComp2DDataList", nullptr, (EPropertyFlags)0x0020088000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, SceneCaptureComp2DDataList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterialInstance = { "PostProcessMaterialInstance", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComponent = { "SceneCaptureComponent", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, SceneCaptureComponent), Z_Construct_UClass_UNVSceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOnlyShowTrainingActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_IgnoreActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideShowFlagSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideShowFlagSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessBlendWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bOverrideExportImageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_ExportImageFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CapturedPixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_OverrideTexturePixelFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_CaptureSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_bUpdateContinuously,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComp2DDataList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_PostProcessMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::NewProp_SceneCaptureComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_PixelData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_PixelData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_PixelData, 119047310);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_PixelData>()
	{
		return UNVSceneFeatureExtractor_PixelData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_PixelData(Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData, &UNVSceneFeatureExtractor_PixelData::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_PixelData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_PixelData);
	void UNVSceneFeatureExtractor_SceneDepth::StaticRegisterNativesUNVSceneFeatureExtractor_SceneDepth()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_NoRegister()
	{
		return UNVSceneFeatureExtractor_SceneDepth::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDepthDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDepthDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/// Base class for all the feature extractors that export the scene's depth buffer\n" },
		{ "IncludePath", "NVSceneFeatureExtractor_ImageExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all the feature extractors that export the scene's depth buffer" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::NewProp_MaxDepthDistance_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// Editor properties\n/// The furthest distance to quantize when capturing the scene's depth\n" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ToolTip", "Editor properties\nThe furthest distance to quantize when capturing the scene's depth" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::NewProp_MaxDepthDistance = { "MaxDepthDistance", nullptr, (EPropertyFlags)0x0010020000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneFeatureExtractor_SceneDepth, MaxDepthDistance), METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::NewProp_MaxDepthDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::NewProp_MaxDepthDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::NewProp_MaxDepthDistance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_SceneDepth>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_SceneDepth::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::PropPointers),
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_SceneDepth, 3863161100);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_SceneDepth>()
	{
		return UNVSceneFeatureExtractor_SceneDepth::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_SceneDepth(Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth, &UNVSceneFeatureExtractor_SceneDepth::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_SceneDepth"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_SceneDepth);
	void UNVSceneFeatureExtractor_ScenePixelVelocity::StaticRegisterNativesUNVSceneFeatureExtractor_ScenePixelVelocity()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_NoRegister()
	{
		return UNVSceneFeatureExtractor_ScenePixelVelocity::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NVSceneFeatureExtractor_ImageExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_ScenePixelVelocity>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_ScenePixelVelocity::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_ScenePixelVelocity, 3428920925);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_ScenePixelVelocity>()
	{
		return UNVSceneFeatureExtractor_ScenePixelVelocity::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity(Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity, &UNVSceneFeatureExtractor_ScenePixelVelocity::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_ScenePixelVelocity"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_ScenePixelVelocity);
	void UNVSceneFeatureExtractor_StencilMask::StaticRegisterNativesUNVSceneFeatureExtractor_StencilMask()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_NoRegister()
	{
		return UNVSceneFeatureExtractor_StencilMask::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/// Base class for all the feature extractors that export the scene's stencil mask buffer\n" },
		{ "IncludePath", "NVSceneFeatureExtractor_ImageExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all the feature extractors that export the scene's stencil mask buffer" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_StencilMask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_StencilMask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_StencilMask, 179600219);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_StencilMask>()
	{
		return UNVSceneFeatureExtractor_StencilMask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_StencilMask(Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask, &UNVSceneFeatureExtractor_StencilMask::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_StencilMask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_StencilMask);
	void UNVSceneFeatureExtractor_VertexColorMask::StaticRegisterNativesUNVSceneFeatureExtractor_VertexColorMask()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_NoRegister()
	{
		return UNVSceneFeatureExtractor_VertexColorMask::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/// Base class for all the feature extractors that export the scene's vertex color buffer\n" },
		{ "IncludePath", "NVSceneFeatureExtractor_ImageExport.h" },
		{ "ModuleRelativePath", "Public/NVSceneFeatureExtractor_ImageExport.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for all the feature extractors that export the scene's vertex color buffer" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneFeatureExtractor_VertexColorMask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::ClassParams = {
		&UNVSceneFeatureExtractor_VertexColorMask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneFeatureExtractor_VertexColorMask, 138611012);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneFeatureExtractor_VertexColorMask>()
	{
		return UNVSceneFeatureExtractor_VertexColorMask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneFeatureExtractor_VertexColorMask(Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask, &UNVSceneFeatureExtractor_VertexColorMask::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneFeatureExtractor_VertexColorMask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneFeatureExtractor_VertexColorMask);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
