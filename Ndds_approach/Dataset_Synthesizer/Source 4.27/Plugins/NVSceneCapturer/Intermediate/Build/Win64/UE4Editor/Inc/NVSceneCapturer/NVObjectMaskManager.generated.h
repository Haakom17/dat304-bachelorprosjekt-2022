// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVObjectMaskManager_generated_h
#error "NVObjectMaskManager.generated.h already included, missing '#pragma once' in NVObjectMaskManager.h"
#endif
#define NVSCENECAPTURER_NVObjectMaskManager_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_197_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVObjectSegmentation_Class_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__ClassSegmentationType() { return STRUCT_OFFSET(FNVObjectSegmentation_Class, ClassSegmentationType); } \
	FORCEINLINE static uint32 __PPO__SegmentationIdAssignmentType() { return STRUCT_OFFSET(FNVObjectSegmentation_Class, SegmentationIdAssignmentType); } \
	FORCEINLINE static uint32 __PPO__StencilMaskManager() { return STRUCT_OFFSET(FNVObjectSegmentation_Class, StencilMaskManager); }


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVObjectSegmentation_Class>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_153_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVObjectSegmentation_Instance_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__SegmentationIdAssignmentType() { return STRUCT_OFFSET(FNVObjectSegmentation_Instance, SegmentationIdAssignmentType); } \
	FORCEINLINE static uint32 __PPO__VertexColorMaskManager() { return STRUCT_OFFSET(FNVObjectSegmentation_Instance, VertexColorMaskManager); }


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVObjectSegmentation_Instance>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVObjectMaskMananger(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVObjectMaskMananger) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger(UNVObjectMaskMananger&&); \
	NO_API UNVObjectMaskMananger(const UNVObjectMaskMananger&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger(UNVObjectMaskMananger&&); \
	NO_API UNVObjectMaskMananger(const UNVObjectMaskMananger&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UNVObjectMaskMananger)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActorMaskNameType() { return STRUCT_OFFSET(UNVObjectMaskMananger, ActorMaskNameType); } \
	FORCEINLINE static uint32 __PPO__SegmentationIdAssignmentType() { return STRUCT_OFFSET(UNVObjectMaskMananger, SegmentationIdAssignmentType); } \
	FORCEINLINE static uint32 __PPO__bDebug() { return STRUCT_OFFSET(UNVObjectMaskMananger, bDebug); } \
	FORCEINLINE static uint32 __PPO__AllMaskNames() { return STRUCT_OFFSET(UNVObjectMaskMananger, AllMaskNames); } \
	FORCEINLINE static uint32 __PPO__AllMaskActors() { return STRUCT_OFFSET(UNVObjectMaskMananger, AllMaskActors); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_67_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVObjectMaskMananger>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger_Stencil(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger_Stencil, UNVObjectMaskMananger, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger_Stencil)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_INCLASS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger_Stencil(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_Stencil_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger_Stencil, UNVObjectMaskMananger, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger_Stencil)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVObjectMaskMananger_Stencil(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVObjectMaskMananger_Stencil) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger_Stencil); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger_Stencil); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger_Stencil(UNVObjectMaskMananger_Stencil&&); \
	NO_API UNVObjectMaskMananger_Stencil(const UNVObjectMaskMananger_Stencil&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger_Stencil(UNVObjectMaskMananger_Stencil&&); \
	NO_API UNVObjectMaskMananger_Stencil(const UNVObjectMaskMananger_Stencil&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger_Stencil); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger_Stencil); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVObjectMaskMananger_Stencil)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaskNameIdMap() { return STRUCT_OFFSET(UNVObjectMaskMananger_Stencil, MaskNameIdMap); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_109_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_112_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVObjectMaskMananger_Stencil>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger_VertexColor(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger_VertexColor, UNVObjectMaskMananger, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger_VertexColor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_INCLASS \
private: \
	static void StaticRegisterNativesUNVObjectMaskMananger_VertexColor(); \
	friend struct Z_Construct_UClass_UNVObjectMaskMananger_VertexColor_Statics; \
public: \
	DECLARE_CLASS(UNVObjectMaskMananger_VertexColor, UNVObjectMaskMananger, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVObjectMaskMananger_VertexColor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVObjectMaskMananger_VertexColor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVObjectMaskMananger_VertexColor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger_VertexColor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger_VertexColor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger_VertexColor(UNVObjectMaskMananger_VertexColor&&); \
	NO_API UNVObjectMaskMananger_VertexColor(const UNVObjectMaskMananger_VertexColor&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVObjectMaskMananger_VertexColor(UNVObjectMaskMananger_VertexColor&&); \
	NO_API UNVObjectMaskMananger_VertexColor(const UNVObjectMaskMananger_VertexColor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVObjectMaskMananger_VertexColor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVObjectMaskMananger_VertexColor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVObjectMaskMananger_VertexColor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaskNameIdMap() { return STRUCT_OFFSET(UNVObjectMaskMananger_VertexColor, MaskNameIdMap); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_130_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h_133_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVObjectMaskMananger_VertexColor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVObjectMaskManager_h


#define FOREACH_ENUM_ENVACTORCLASSSEGMENTATIONTYPE(op) \
	op(ENVActorClassSegmentationType::UseActorMeshName) \
	op(ENVActorClassSegmentationType::UseActorTag) \
	op(ENVActorClassSegmentationType::UseActorClassName) 

enum class ENVActorClassSegmentationType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVActorClassSegmentationType>();

#define FOREACH_ENUM_ENVIDASSIGNMENTTYPE(op) \
	op(ENVIdAssignmentType::Sequential) \
	op(ENVIdAssignmentType::SpreadEvenly) \
	op(ENVIdAssignmentType::NVActorMaskIdType_MAX) 

enum class ENVIdAssignmentType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVIdAssignmentType>();

#define FOREACH_ENUM_ENVACTORMASKNAMETYPE(op) \
	op(ENVActorMaskNameType::UseActorInstanceName) \
	op(ENVActorMaskNameType::UseActorMeshName) \
	op(ENVActorMaskNameType::UseActorTag) \
	op(ENVActorMaskNameType::UseActorClassName) \
	op(ENVActorMaskNameType::NVObjectClassMaskType_MAX) 

enum class ENVActorMaskNameType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVActorMaskNameType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
