// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVTextureReader.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVTextureReader() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVTextureRenderTargetReader();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVTextureReader();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FNVTextureRenderTargetReader>() == std::is_polymorphic<FNVTextureReader>(), "USTRUCT FNVTextureRenderTargetReader cannot be polymorphic unless super FNVTextureReader is polymorphic");

class UScriptStruct* FNVTextureRenderTargetReader::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVTextureRenderTargetReader"), sizeof(FNVTextureRenderTargetReader), Get_Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVTextureRenderTargetReader>()
{
	return FNVTextureRenderTargetReader::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVTextureRenderTargetReader(FNVTextureRenderTargetReader::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVTextureRenderTargetReader"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureRenderTargetReader
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureRenderTargetReader()
	{
		UScriptStruct::DeferCppStructOps<FNVTextureRenderTargetReader>(FName(TEXT("NVTextureRenderTargetReader")));
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureRenderTargetReader;
	struct Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceRenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVTextureReader.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVTextureRenderTargetReader>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewProp_SourceRenderTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVTextureReader.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewProp_SourceRenderTarget = { "SourceRenderTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVTextureRenderTargetReader, SourceRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewProp_SourceRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewProp_SourceRenderTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::NewProp_SourceRenderTarget,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		Z_Construct_UScriptStruct_FNVTextureReader,
		&NewStructOps,
		"NVTextureRenderTargetReader",
		sizeof(FNVTextureRenderTargetReader),
		alignof(FNVTextureRenderTargetReader),
		Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVTextureRenderTargetReader()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVTextureRenderTargetReader"), sizeof(FNVTextureRenderTargetReader), Get_Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVTextureRenderTargetReader_Hash() { return 3633977267U; }
class UScriptStruct* FNVTextureReader::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVTextureReader_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVTextureReader, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVTextureReader"), sizeof(FNVTextureReader), Get_Z_Construct_UScriptStruct_FNVTextureReader_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVTextureReader>()
{
	return FNVTextureReader::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVTextureReader(FNVTextureReader::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVTextureReader"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureReader
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureReader()
	{
		UScriptStruct::DeferCppStructOps<FNVTextureReader>(FName(TEXT("NVTextureReader")));
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVTextureReader;
	struct Z_Construct_UScriptStruct_FNVTextureReader_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVTextureReader_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// This class read the pixels data from a texture target\n" },
		{ "ModuleRelativePath", "Public/NVTextureReader.h" },
		{ "ToolTip", "This class read the pixels data from a texture target" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVTextureReader_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVTextureReader>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVTextureReader_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVTextureReader",
		sizeof(FNVTextureReader),
		alignof(FNVTextureReader),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVTextureReader_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVTextureReader_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVTextureReader()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVTextureReader_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVTextureReader"), sizeof(FNVTextureReader), Get_Z_Construct_UScriptStruct_FNVTextureReader_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVTextureReader_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVTextureReader_Hash() { return 2528295844U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
