// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVImageExporter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVImageExporter() {}
// Cross Module References
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVImageExporterData();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVTexturePixelData();
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat();
// End Cross Module References
class UScriptStruct* FNVImageExporterData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern NVSCENECAPTURER_API uint32 Get_Z_Construct_UScriptStruct_FNVImageExporterData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVImageExporterData, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("NVImageExporterData"), sizeof(FNVImageExporterData), Get_Z_Construct_UScriptStruct_FNVImageExporterData_Hash());
	}
	return Singleton;
}
template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<FNVImageExporterData>()
{
	return FNVImageExporterData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVImageExporterData(FNVImageExporterData::StaticStruct, TEXT("/Script/NVSceneCapturer"), TEXT("NVImageExporterData"), false, nullptr, nullptr);
static struct FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageExporterData
{
	FScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageExporterData()
	{
		UScriptStruct::DeferCppStructOps<FNVImageExporterData>(FName(TEXT("NVImageExporterData")));
	}
} ScriptStruct_NVSceneCapturer_StaticRegisterNativesFNVImageExporterData;
	struct Z_Construct_UScriptStruct_FNVImageExporterData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PixelDataToBeExported_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PixelDataToBeExported;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExportFilePath;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ExportImageFormat_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportImageFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ExportImageFormat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageExporterData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVImageExporter.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVImageExporterData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_PixelDataToBeExported_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVImageExporter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_PixelDataToBeExported = { "PixelDataToBeExported", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVImageExporterData, PixelDataToBeExported), Z_Construct_UScriptStruct_FNVTexturePixelData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_PixelDataToBeExported_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_PixelDataToBeExported_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportFilePath_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVImageExporter.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportFilePath = { "ExportFilePath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVImageExporterData, ExportFilePath), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportFilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportFilePath_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVImageExporter.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat = { "ExportImageFormat", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNVImageExporterData, ExportImageFormat), Z_Construct_UEnum_NVSceneCapturer_ENVImageFormat, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVImageExporterData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_PixelDataToBeExported,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportFilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVImageExporterData_Statics::NewProp_ExportImageFormat,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVImageExporterData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
		nullptr,
		&NewStructOps,
		"NVImageExporterData",
		sizeof(FNVImageExporterData),
		alignof(FNVImageExporterData),
		Z_Construct_UScriptStruct_FNVImageExporterData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVImageExporterData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVImageExporterData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVImageExporterData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVImageExporterData"), sizeof(FNVImageExporterData), Get_Z_Construct_UScriptStruct_FNVImageExporterData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVImageExporterData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVImageExporterData_Hash() { return 193259058U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
