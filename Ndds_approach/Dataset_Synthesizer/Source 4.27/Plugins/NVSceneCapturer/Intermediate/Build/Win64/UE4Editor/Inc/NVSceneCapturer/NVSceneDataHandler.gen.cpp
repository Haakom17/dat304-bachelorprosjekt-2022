// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturer/Public/NVSceneDataHandler.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneDataHandler() {}
// Cross Module References
	NVSCENECAPTURER_API UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataHandler_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataHandler();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataExporter_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataExporter();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneDataVisualizer();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	static UEnum* ENVCaptureDirectoryConflictHandleType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType, Z_Construct_UPackage__Script_NVSceneCapturer(), TEXT("ENVCaptureDirectoryConflictHandleType"));
		}
		return Singleton;
	}
	template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCaptureDirectoryConflictHandleType>()
	{
		return ENVCaptureDirectoryConflictHandleType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVCaptureDirectoryConflictHandleType(ENVCaptureDirectoryConflictHandleType_StaticEnum, TEXT("/Script/NVSceneCapturer"), TEXT("ENVCaptureDirectoryConflictHandleType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType_Hash() { return 3530511890U; }
	UEnum* Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_NVSceneCapturer();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVCaptureDirectoryConflictHandleType"), 0, Get_Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVCaptureDirectoryConflictHandleType::OverwriteExistingFiles", (int64)ENVCaptureDirectoryConflictHandleType::OverwriteExistingFiles },
				{ "ENVCaptureDirectoryConflictHandleType::CleanDirectory", (int64)ENVCaptureDirectoryConflictHandleType::CleanDirectory },
				{ "ENVCaptureDirectoryConflictHandleType::CreateNewDirectoryWithTimestampPostfix", (int64)ENVCaptureDirectoryConflictHandleType::CreateNewDirectoryWithTimestampPostfix },
				{ "ENVCaptureDirectoryConflictHandleType::CaptureDirectoryConflictHandleType_MAX", (int64)ENVCaptureDirectoryConflictHandleType::CaptureDirectoryConflictHandleType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CaptureDirectoryConflictHandleType_MAX.Hidden", "" },
				{ "CaptureDirectoryConflictHandleType_MAX.Name", "ENVCaptureDirectoryConflictHandleType::CaptureDirectoryConflictHandleType_MAX" },
				{ "CleanDirectory.Comment", "/// Clean the directory by removing all files in it\n" },
				{ "CleanDirectory.DisplayName", "Remove existing files" },
				{ "CleanDirectory.Name", "ENVCaptureDirectoryConflictHandleType::CleanDirectory" },
				{ "CleanDirectory.ToolTip", "Clean the directory by removing all files in it" },
				{ "Comment", "//=================================== UNVSceneDataExporter ===================================\n" },
				{ "CreateNewDirectoryWithTimestampPostfix.Comment", "/// Create new directory with timestamp as postfix in name\n" },
				{ "CreateNewDirectoryWithTimestampPostfix.DisplayName", "Create new directory with timestamp as postfix in name" },
				{ "CreateNewDirectoryWithTimestampPostfix.Name", "ENVCaptureDirectoryConflictHandleType::CreateNewDirectoryWithTimestampPostfix" },
				{ "CreateNewDirectoryWithTimestampPostfix.ToolTip", "Create new directory with timestamp as postfix in name" },
				{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
				{ "OverwriteExistingFiles.Comment", "/// Just overwrite existing files in the same directory\n" },
				{ "OverwriteExistingFiles.DisplayName", "Overwrite existing files" },
				{ "OverwriteExistingFiles.Name", "ENVCaptureDirectoryConflictHandleType::OverwriteExistingFiles" },
				{ "OverwriteExistingFiles.ToolTip", "Just overwrite existing files in the same directory" },
				{ "ToolTip", "=================================== UNVSceneDataExporter ===================================" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_NVSceneCapturer,
				nullptr,
				"ENVCaptureDirectoryConflictHandleType",
				"ENVCaptureDirectoryConflictHandleType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UNVSceneDataHandler::StaticRegisterNativesUNVSceneDataHandler()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneDataHandler_NoRegister()
	{
		return UNVSceneDataHandler::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneDataHandler_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneDataHandler_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataHandler_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "///\n/// Base interface for serializing/visualizing captured pixel and annotation data.\n///\n" },
		{ "IncludePath", "NVSceneDataHandler.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "Base interface for serializing/visualizing captured pixel and annotation data." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneDataHandler_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneDataHandler>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneDataHandler_Statics::ClassParams = {
		&UNVSceneDataHandler::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x003010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataHandler_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataHandler_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneDataHandler()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneDataHandler_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneDataHandler, 1981379943);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneDataHandler>()
	{
		return UNVSceneDataHandler::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneDataHandler(Z_Construct_UClass_UNVSceneDataHandler, &UNVSceneDataHandler::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneDataHandler"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneDataHandler);
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetExportFilePath)
	{
		P_GET_OBJECT(UNVSceneFeatureExtractor,Z_Param_CapturedFeatureExtractor);
		P_GET_OBJECT(UNVSceneCapturerViewpointComponent,Z_Param_CapturedViewpoint);
		P_GET_PROPERTY(FIntProperty,Z_Param_FrameIndex);
		P_GET_PROPERTY(FStrProperty,Z_Param_FileExtension);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetExportFilePath(Z_Param_CapturedFeatureExtractor,Z_Param_CapturedViewpoint,Z_Param_FrameIndex,Z_Param_FileExtension);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execSetSubFolderName)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_NewSubFolderName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSubFolderName(Z_Param_NewSubFolderName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetSubFolderName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetSubFolderName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetFullOutputDirectoryPath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetFullOutputDirectoryPath();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetConfiguredOutputDirectoryName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetConfiguredOutputDirectoryPath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetConfiguredOutputDirectoryPath();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetExportFolderName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetExportFolderName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UNVSceneDataExporter::execGetRootCaptureDirectoryPath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetRootCaptureDirectoryPath();
		P_NATIVE_END;
	}
	void UNVSceneDataExporter::StaticRegisterNativesUNVSceneDataExporter()
	{
		UClass* Class = UNVSceneDataExporter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetConfiguredOutputDirectoryName", &UNVSceneDataExporter::execGetConfiguredOutputDirectoryName },
			{ "GetConfiguredOutputDirectoryPath", &UNVSceneDataExporter::execGetConfiguredOutputDirectoryPath },
			{ "GetExportFilePath", &UNVSceneDataExporter::execGetExportFilePath },
			{ "GetExportFolderName", &UNVSceneDataExporter::execGetExportFolderName },
			{ "GetFullOutputDirectoryPath", &UNVSceneDataExporter::execGetFullOutputDirectoryPath },
			{ "GetRootCaptureDirectoryPath", &UNVSceneDataExporter::execGetRootCaptureDirectoryPath },
			{ "GetSubFolderName", &UNVSceneDataExporter::execGetSubFolderName },
			{ "SetSubFolderName", &UNVSceneDataExporter::execSetSubFolderName },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics
	{
		struct NVSceneDataExporter_eventGetConfiguredOutputDirectoryName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetConfiguredOutputDirectoryName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetConfiguredOutputDirectoryName", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetConfiguredOutputDirectoryName_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics
	{
		struct NVSceneDataExporter_eventGetConfiguredOutputDirectoryPath_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetConfiguredOutputDirectoryPath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetConfiguredOutputDirectoryPath", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetConfiguredOutputDirectoryPath_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics
	{
		struct NVSceneDataExporter_eventGetExportFilePath_Parms
		{
			UNVSceneFeatureExtractor* CapturedFeatureExtractor;
			UNVSceneCapturerViewpointComponent* CapturedViewpoint;
			int32 FrameIndex;
			FString FileExtension;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedFeatureExtractor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturedFeatureExtractor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CapturedViewpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CapturedViewpoint;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileExtension_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileExtension;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedFeatureExtractor_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedFeatureExtractor = { "CapturedFeatureExtractor", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFilePath_Parms, CapturedFeatureExtractor), Z_Construct_UClass_UNVSceneFeatureExtractor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedFeatureExtractor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedFeatureExtractor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedViewpoint_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedViewpoint = { "CapturedViewpoint", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFilePath_Parms, CapturedViewpoint), Z_Construct_UClass_UNVSceneCapturerViewpointComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedViewpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedViewpoint_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FrameIndex = { "FrameIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFilePath_Parms, FrameIndex), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FileExtension_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FileExtension = { "FileExtension", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFilePath_Parms, FileExtension), METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FileExtension_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FileExtension_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFilePath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedFeatureExtractor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_CapturedViewpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FrameIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_FileExtension,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetExportFilePath", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetExportFilePath_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics
	{
		struct NVSceneDataExporter_eventGetExportFolderName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetExportFolderName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetExportFolderName", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetExportFolderName_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics
	{
		struct NVSceneDataExporter_eventGetFullOutputDirectoryPath_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetFullOutputDirectoryPath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetFullOutputDirectoryPath", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetFullOutputDirectoryPath_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics
	{
		struct NVSceneDataExporter_eventGetRootCaptureDirectoryPath_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetRootCaptureDirectoryPath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetRootCaptureDirectoryPath", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetRootCaptureDirectoryPath_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics
	{
		struct NVSceneDataExporter_eventGetSubFolderName_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventGetSubFolderName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "GetSubFolderName", nullptr, nullptr, sizeof(NVSceneDataExporter_eventGetSubFolderName_Parms), Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics
	{
		struct NVSceneDataExporter_eventSetSubFolderName_Parms
		{
			FString NewSubFolderName;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewSubFolderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewSubFolderName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::NewProp_NewSubFolderName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::NewProp_NewSubFolderName = { "NewSubFolderName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NVSceneDataExporter_eventSetSubFolderName_Parms, NewSubFolderName), METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::NewProp_NewSubFolderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::NewProp_NewSubFolderName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::NewProp_NewSubFolderName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Exporter" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNVSceneDataExporter, nullptr, "SetSubFolderName", nullptr, nullptr, sizeof(NVSceneDataExporter_eventSetSubFolderName_Parms), Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNVSceneDataExporter_NoRegister()
	{
		return UNVSceneDataExporter::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneDataExporter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseMapNameForCapturedDirectory_MetaData[];
#endif
		static void NewProp_bUseMapNameForCapturedDirectory_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseMapNameForCapturedDirectory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomDirectoryName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomDirectoryName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootCapturedDirectoryPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootCapturedDirectoryPath;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DirectoryConflictHandleType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectoryConflictHandleType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DirectoryConflictHandleType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoOpenExportedDirectory_MetaData[];
#endif
		static void NewProp_bAutoOpenExportedDirectory_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoOpenExportedDirectory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxSaveImageAsyncCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_MaxSaveImageAsyncCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubFolderName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubFolderName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullOutputDirectoryPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FullOutputDirectoryPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneDataExporter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneDataHandler,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNVSceneDataExporter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryName, "GetConfiguredOutputDirectoryName" }, // 3989957721
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetConfiguredOutputDirectoryPath, "GetConfiguredOutputDirectoryPath" }, // 4058897054
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetExportFilePath, "GetExportFilePath" }, // 468608384
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetExportFolderName, "GetExportFolderName" }, // 2211830597
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetFullOutputDirectoryPath, "GetFullOutputDirectoryPath" }, // 2051313573
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetRootCaptureDirectoryPath, "GetRootCaptureDirectoryPath" }, // 3694007055
		{ &Z_Construct_UFunction_UNVSceneDataExporter_GetSubFolderName, "GetSubFolderName" }, // 2114513823
		{ &Z_Construct_UFunction_UNVSceneDataExporter_SetSubFolderName, "SetSubFolderName" }, // 1463445760
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "///\n/// NVSceneDataExporter - export all the captured data (image buffer and object annotation info) to files on disk\n///\n" },
		{ "IncludePath", "NVSceneDataHandler.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "NVSceneDataExporter - export all the captured data (image buffer and object annotation info) to files on disk" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory_MetaData[] = {
		{ "Category", "Save Path" },
		{ "Comment", "// Editor properties\n// ToDo: move to protected.\n/// If true, the exporter will use the current map's name for the export folder, otherwise it will use the ExportFolderName\n/// NOTE: If ExportFolderName is empty, it will fallback to use the map's name\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "Editor properties\nToDo: move to protected.\nIf true, the exporter will use the current map's name for the export folder, otherwise it will use the ExportFolderName\nNOTE: If ExportFolderName is empty, it will fallback to use the map's name" },
	};
#endif
	void Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory_SetBit(void* Obj)
	{
		((UNVSceneDataExporter*)Obj)->bUseMapNameForCapturedDirectory = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory = { "bUseMapNameForCapturedDirectory", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneDataExporter), &Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_CustomDirectoryName_MetaData[] = {
		{ "Category", "Save Path" },
		{ "EditCondition", "!bUseMapNameForCapturedDirectory" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_CustomDirectoryName = { "CustomDirectoryName", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, CustomDirectoryName), METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_CustomDirectoryName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_CustomDirectoryName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_RootCapturedDirectoryPath_MetaData[] = {
		{ "Category", "Save Path" },
		{ "Comment", "// Editor properties\n/// Path to the directory where to save captured data\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "Editor properties\nPath to the directory where to save captured data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_RootCapturedDirectoryPath = { "RootCapturedDirectoryPath", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, RootCapturedDirectoryPath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_RootCapturedDirectoryPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_RootCapturedDirectoryPath_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType_MetaData[] = {
		{ "Category", "Save Path" },
		{ "Comment", "/// How to handle conflict files\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "How to handle conflict files" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType = { "DirectoryConflictHandleType", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, DirectoryConflictHandleType), Z_Construct_UEnum_NVSceneCapturer_ENVCaptureDirectoryConflictHandleType, METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory_MetaData[] = {
		{ "Category", "Capture" },
		{ "Comment", "/// If true, this exporter will automatically open the exported directory after it finish exporting\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "If true, this exporter will automatically open the exported directory after it finish exporting" },
	};
#endif
	void Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory_SetBit(void* Obj)
	{
		((UNVSceneDataExporter*)Obj)->bAutoOpenExportedDirectory = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory = { "bAutoOpenExportedDirectory", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNVSceneDataExporter), &Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_MaxSaveImageAsyncCount_MetaData[] = {
		{ "Category", "Capture" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_MaxSaveImageAsyncCount = { "MaxSaveImageAsyncCount", nullptr, (EPropertyFlags)0x00200c0000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, MaxSaveImageAsyncCount), METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_MaxSaveImageAsyncCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_MaxSaveImageAsyncCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_SubFolderName_MetaData[] = {
		{ "Comment", "// Transient\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "Transient" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_SubFolderName = { "SubFolderName", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, SubFolderName), METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_SubFolderName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_SubFolderName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_FullOutputDirectoryPath_MetaData[] = {
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_FullOutputDirectoryPath = { "FullOutputDirectoryPath", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataExporter, FullOutputDirectoryPath), METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_FullOutputDirectoryPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_FullOutputDirectoryPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneDataExporter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bUseMapNameForCapturedDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_CustomDirectoryName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_RootCapturedDirectoryPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_DirectoryConflictHandleType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_bAutoOpenExportedDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_MaxSaveImageAsyncCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_SubFolderName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataExporter_Statics::NewProp_FullOutputDirectoryPath,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneDataExporter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneDataExporter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneDataExporter_Statics::ClassParams = {
		&UNVSceneDataExporter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UNVSceneDataExporter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataExporter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataExporter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneDataExporter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneDataExporter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneDataExporter, 2905253104);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneDataExporter>()
	{
		return UNVSceneDataExporter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneDataExporter(Z_Construct_UClass_UNVSceneDataExporter, &UNVSceneDataExporter::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneDataExporter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneDataExporter);
	void UNVSceneDataVisualizer::StaticRegisterNativesUNVSceneDataVisualizer()
	{
	}
	UClass* Z_Construct_UClass_UNVSceneDataVisualizer_NoRegister()
	{
		return UNVSceneDataVisualizer::StaticClass();
	}
	struct Z_Construct_UClass_UNVSceneDataVisualizer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VizTextureMap_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VizTextureMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VizTextureMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VizTextureMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVSceneDataVisualizer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNVSceneDataHandler,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataVisualizer_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "//=================================== UNVSceneDataVisualizer ===================================\n///\n/// NVSceneDataVisualizer - visualize all the captured data (image buffer and object annotation info) using material, UI\n///\n" },
		{ "IncludePath", "NVSceneDataHandler.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "=================================== UNVSceneDataVisualizer ===================================\n\n NVSceneDataVisualizer - visualize all the captured data (image buffer and object annotation info) using material, UI" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_ValueProp = { "VizTextureMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_Key_KeyProp = { "VizTextureMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_MetaData[] = {
		{ "Comment", "// Transient\n/// Map between a feature extractor name and its visualized texture\n" },
		{ "ModuleRelativePath", "Public/NVSceneDataHandler.h" },
		{ "ToolTip", "Transient\nMap between a feature extractor name and its visualized texture" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap = { "VizTextureMap", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVSceneDataVisualizer, VizTextureMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVSceneDataVisualizer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVSceneDataVisualizer_Statics::NewProp_VizTextureMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVSceneDataVisualizer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVSceneDataVisualizer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVSceneDataVisualizer_Statics::ClassParams = {
		&UNVSceneDataVisualizer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVSceneDataVisualizer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataVisualizer_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVSceneDataVisualizer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVSceneDataVisualizer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVSceneDataVisualizer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVSceneDataVisualizer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVSceneDataVisualizer, 3124317311);
	template<> NVSCENECAPTURER_API UClass* StaticClass<UNVSceneDataVisualizer>()
	{
		return UNVSceneDataVisualizer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVSceneDataVisualizer(Z_Construct_UClass_UNVSceneDataVisualizer, &UNVSceneDataVisualizer::StaticClass, TEXT("/Script/NVSceneCapturer"), TEXT("UNVSceneDataVisualizer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVSceneDataVisualizer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
