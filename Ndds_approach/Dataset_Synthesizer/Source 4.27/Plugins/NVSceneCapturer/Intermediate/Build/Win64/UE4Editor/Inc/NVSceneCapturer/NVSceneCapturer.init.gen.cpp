// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturer_init() {}
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature();
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature();
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature();
	NVSCENECAPTURER_API UFunction* Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturer()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Started__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Stopped__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneCapturer_Completed__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_NVSceneCapturer_NVSceneManger_SetupCompleted__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/NVSceneCapturer",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xC45113CA,
				0xD3542464,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
