// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneFeatureExtractor_ImageExport_generated_h
#error "NVSceneFeatureExtractor_ImageExport.generated.h already included, missing '#pragma once' in NVSceneFeatureExtractor_ImageExport.h"
#endif
#define NVSCENECAPTURER_NVSceneFeatureExtractor_ImageExport_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_PixelData(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_PixelData, UNVSceneFeatureExtractor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_PixelData)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_PixelData(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_PixelData_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_PixelData, UNVSceneFeatureExtractor, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_PixelData)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_PixelData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_PixelData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_PixelData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_PixelData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_PixelData(UNVSceneFeatureExtractor_PixelData&&); \
	NO_API UNVSceneFeatureExtractor_PixelData(const UNVSceneFeatureExtractor_PixelData&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_PixelData(UNVSceneFeatureExtractor_PixelData&&); \
	NO_API UNVSceneFeatureExtractor_PixelData(const UNVSceneFeatureExtractor_PixelData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_PixelData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_PixelData); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_PixelData)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bOnlyShowTrainingActors() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, bOnlyShowTrainingActors); } \
	FORCEINLINE static uint32 __PPO__IgnoreActors() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, IgnoreActors); } \
	FORCEINLINE static uint32 __PPO__bOverrideShowFlagSettings() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, bOverrideShowFlagSettings); } \
	FORCEINLINE static uint32 __PPO__OverrideShowFlagSettings() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, OverrideShowFlagSettings); } \
	FORCEINLINE static uint32 __PPO__PostProcessBlendWeight() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessBlendWeight); } \
	FORCEINLINE static uint32 __PPO__PostProcessMaterial() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessMaterial); } \
	FORCEINLINE static uint32 __PPO__bOverrideExportImageType() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, bOverrideExportImageType); } \
	FORCEINLINE static uint32 __PPO__ExportImageFormat() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, ExportImageFormat); } \
	FORCEINLINE static uint32 __PPO__CapturedPixelFormat() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, CapturedPixelFormat); } \
	FORCEINLINE static uint32 __PPO__OverrideTexturePixelFormat() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, OverrideTexturePixelFormat); } \
	FORCEINLINE static uint32 __PPO__CaptureSource() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, CaptureSource); } \
	FORCEINLINE static uint32 __PPO__bUpdateContinuously() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, bUpdateContinuously); } \
	FORCEINLINE static uint32 __PPO__SceneCaptureComp2DDataList() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, SceneCaptureComp2DDataList); } \
	FORCEINLINE static uint32 __PPO__PostProcessMaterialInstance() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, PostProcessMaterialInstance); } \
	FORCEINLINE static uint32 __PPO__SceneCaptureComponent() { return STRUCT_OFFSET(UNVSceneFeatureExtractor_PixelData, SceneCaptureComponent); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_23_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_PixelData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_SceneDepth(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_SceneDepth, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_SceneDepth)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_SceneDepth(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_SceneDepth_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_SceneDepth, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_SceneDepth)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_SceneDepth(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_SceneDepth) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_SceneDepth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_SceneDepth); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_SceneDepth(UNVSceneFeatureExtractor_SceneDepth&&); \
	NO_API UNVSceneFeatureExtractor_SceneDepth(const UNVSceneFeatureExtractor_SceneDepth&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_SceneDepth(UNVSceneFeatureExtractor_SceneDepth&&); \
	NO_API UNVSceneFeatureExtractor_SceneDepth(const UNVSceneFeatureExtractor_SceneDepth&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_SceneDepth); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_SceneDepth); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_SceneDepth)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_103_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_106_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_SceneDepth>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_ScenePixelVelocity(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_ScenePixelVelocity, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_ScenePixelVelocity)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_ScenePixelVelocity(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_ScenePixelVelocity_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_ScenePixelVelocity, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_ScenePixelVelocity)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_ScenePixelVelocity(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_ScenePixelVelocity) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_ScenePixelVelocity); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_ScenePixelVelocity); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_ScenePixelVelocity(UNVSceneFeatureExtractor_ScenePixelVelocity&&); \
	NO_API UNVSceneFeatureExtractor_ScenePixelVelocity(const UNVSceneFeatureExtractor_ScenePixelVelocity&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_ScenePixelVelocity(UNVSceneFeatureExtractor_ScenePixelVelocity&&); \
	NO_API UNVSceneFeatureExtractor_ScenePixelVelocity(const UNVSceneFeatureExtractor_ScenePixelVelocity&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_ScenePixelVelocity); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_ScenePixelVelocity); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_ScenePixelVelocity)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_120_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_123_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_ScenePixelVelocity>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_StencilMask(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_StencilMask, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_StencilMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_StencilMask(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_StencilMask_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_StencilMask, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_StencilMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_StencilMask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_StencilMask) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_StencilMask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_StencilMask); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_StencilMask(UNVSceneFeatureExtractor_StencilMask&&); \
	NO_API UNVSceneFeatureExtractor_StencilMask(const UNVSceneFeatureExtractor_StencilMask&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_StencilMask(UNVSceneFeatureExtractor_StencilMask&&); \
	NO_API UNVSceneFeatureExtractor_StencilMask(const UNVSceneFeatureExtractor_StencilMask&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_StencilMask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_StencilMask); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_StencilMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_133_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_136_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_StencilMask>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_VertexColorMask(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_VertexColorMask, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_VertexColorMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor_VertexColorMask(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_VertexColorMask_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor_VertexColorMask, UNVSceneFeatureExtractor_PixelData, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor_VertexColorMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor_VertexColorMask(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_VertexColorMask) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_VertexColorMask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_VertexColorMask); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_VertexColorMask(UNVSceneFeatureExtractor_VertexColorMask&&); \
	NO_API UNVSceneFeatureExtractor_VertexColorMask(const UNVSceneFeatureExtractor_VertexColorMask&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor_VertexColorMask(UNVSceneFeatureExtractor_VertexColorMask&&); \
	NO_API UNVSceneFeatureExtractor_VertexColorMask(const UNVSceneFeatureExtractor_VertexColorMask&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor_VertexColorMask); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor_VertexColorMask); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor_VertexColorMask)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_147_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h_150_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor_VertexColorMask>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_ImageExport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
