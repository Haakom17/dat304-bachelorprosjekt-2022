// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVSceneCapturerGame/Public/NVSceneCapturerPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneCapturerPlayerController() {}
// Cross Module References
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController_NoRegister();
	NVSCENECAPTURERGAME_API UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_NVSceneCapturerGame();
// End Cross Module References
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execTogglePause)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TogglePause();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execToggleTakeOverViewport)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleTakeOverViewport();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execToggleShowExportActorDebug)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleShowExportActorDebug();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execToggleHUDOverlay)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleHUDOverlay();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execToggleExporterManagement)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleExporterManagement();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ANVSceneCapturerPlayerController::execToggleCursorMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ToggleCursorMode();
		P_NATIVE_END;
	}
	void ANVSceneCapturerPlayerController::StaticRegisterNativesANVSceneCapturerPlayerController()
	{
		UClass* Class = ANVSceneCapturerPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ToggleCursorMode", &ANVSceneCapturerPlayerController::execToggleCursorMode },
			{ "ToggleExporterManagement", &ANVSceneCapturerPlayerController::execToggleExporterManagement },
			{ "ToggleHUDOverlay", &ANVSceneCapturerPlayerController::execToggleHUDOverlay },
			{ "TogglePause", &ANVSceneCapturerPlayerController::execTogglePause },
			{ "ToggleShowExportActorDebug", &ANVSceneCapturerPlayerController::execToggleShowExportActorDebug },
			{ "ToggleTakeOverViewport", &ANVSceneCapturerPlayerController::execToggleTakeOverViewport },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleCursorMode", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleExporterManagement", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleHUDOverlay", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "TogglePause", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleShowExportActorDebug", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams[] = {
		{ "Category", "NVSceneCapturerPlayerController" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ANVSceneCapturerPlayerController, nullptr, "ToggleTakeOverViewport", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020601, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController_NoRegister()
	{
		return ANVSceneCapturerPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_NVSceneCapturerGame,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleCursorMode, "ToggleCursorMode" }, // 833236682
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleExporterManagement, "ToggleExporterManagement" }, // 2943131251
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleHUDOverlay, "ToggleHUDOverlay" }, // 2310147546
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_TogglePause, "TogglePause" }, // 1584181471
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleShowExportActorDebug, "ToggleShowExportActorDebug" }, // 265396897
		{ &Z_Construct_UFunction_ANVSceneCapturerPlayerController_ToggleTakeOverViewport, "ToggleTakeOverViewport" }, // 1952441252
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "NVSceneCapturerPlayerController.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVSceneCapturerPlayerController.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneCapturerPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::ClassParams = {
		&ANVSceneCapturerPlayerController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneCapturerPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneCapturerPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneCapturerPlayerController, 1948949028);
	template<> NVSCENECAPTURERGAME_API UClass* StaticClass<ANVSceneCapturerPlayerController>()
	{
		return ANVSceneCapturerPlayerController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneCapturerPlayerController(Z_Construct_UClass_ANVSceneCapturerPlayerController, &ANVSceneCapturerPlayerController::StaticClass, TEXT("/Script/NVSceneCapturerGame"), TEXT("ANVSceneCapturerPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneCapturerPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
