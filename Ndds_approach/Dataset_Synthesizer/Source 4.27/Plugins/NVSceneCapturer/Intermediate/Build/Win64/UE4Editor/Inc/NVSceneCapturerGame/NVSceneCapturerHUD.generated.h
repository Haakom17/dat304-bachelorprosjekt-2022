// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerHUD_generated_h
#error "NVSceneCapturerHUD.generated.h already included, missing '#pragma once' in NVSceneCapturerHUD.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerHUD_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneCapturerHUD(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerHUD_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerHUD)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneCapturerHUD(); \
	friend struct Z_Construct_UClass_ANVSceneCapturerHUD_Statics; \
public: \
	DECLARE_CLASS(ANVSceneCapturerHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneCapturerHUD)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneCapturerHUD(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerHUD(ANVSceneCapturerHUD&&); \
	NO_API ANVSceneCapturerHUD(const ANVSceneCapturerHUD&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneCapturerHUD(ANVSceneCapturerHUD&&); \
	NO_API ANVSceneCapturerHUD(const ANVSceneCapturerHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneCapturerHUD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneCapturerHUD); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneCapturerHUD)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HUDOverlayClass() { return STRUCT_OFFSET(ANVSceneCapturerHUD, HUDOverlayClass); } \
	FORCEINLINE static uint32 __PPO__DebugFont() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugFont); } \
	FORCEINLINE static uint32 __PPO__DebugLineColor() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineColor); } \
	FORCEINLINE static uint32 __PPO__DebugTextColor() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugTextColor); } \
	FORCEINLINE static uint32 __PPO__DebugTextBackgroundColor() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugTextBackgroundColor); } \
	FORCEINLINE static uint32 __PPO__DebugLineThickness() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineThickness); } \
	FORCEINLINE static uint32 __PPO__DebugVertexRadius() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugVertexRadius); } \
	FORCEINLINE static uint32 __PPO__DebugLineThickness3d() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugLineThickness3d); } \
	FORCEINLINE static uint32 __PPO__DebugVertexRadius3d() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugVertexRadius3d); } \
	FORCEINLINE static uint32 __PPO__DebugCuboidVertexColor() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCuboidVertexColor); } \
	FORCEINLINE static uint32 __PPO__bDrawDebugCuboidIn3d() { return STRUCT_OFFSET(ANVSceneCapturerHUD, bDrawDebugCuboidIn3d); } \
	FORCEINLINE static uint32 __PPO__CuboidDirectionDebugLength() { return STRUCT_OFFSET(ANVSceneCapturerHUD, CuboidDirectionDebugLength); } \
	FORCEINLINE static uint32 __PPO__CuboidDirectionRotation_HACK() { return STRUCT_OFFSET(ANVSceneCapturerHUD, CuboidDirectionRotation_HACK); } \
	FORCEINLINE static uint32 __PPO__bShowExportActorDebug() { return STRUCT_OFFSET(ANVSceneCapturerHUD, bShowExportActorDebug); } \
	FORCEINLINE static uint32 __PPO__bShowDebugCapturerPath() { return STRUCT_OFFSET(ANVSceneCapturerHUD, bShowDebugCapturerPath); } \
	FORCEINLINE static uint32 __PPO__DebugCapturerPathColor() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerPathColor); } \
	FORCEINLINE static uint32 __PPO__DebugCapturerPathLifeTime() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerPathLifeTime); } \
	FORCEINLINE static uint32 __PPO__DebugCapturerDirectionLength() { return STRUCT_OFFSET(ANVSceneCapturerHUD, DebugCapturerDirectionLength); } \
	FORCEINLINE static uint32 __PPO__HUDOverlay() { return STRUCT_OFFSET(ANVSceneCapturerHUD, HUDOverlay); } \
	FORCEINLINE static uint32 __PPO__LastCapturerLocation() { return STRUCT_OFFSET(ANVSceneCapturerHUD, LastCapturerLocation); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_21_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class ANVSceneCapturerHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
