// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneCapturerUtils_generated_h
#error "NVSceneCapturerUtils.generated.h already included, missing '#pragma once' in NVSceneCapturerUtils.h"
#endif
#define NVSCENECAPTURER_NVSceneCapturerUtils_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_531_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVFrameCounter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVFrameCounter>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_462_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneCapturerSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneCapturerSettings>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_423_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneExporterConfig_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__bExportObjectData() { return STRUCT_OFFSET(FNVSceneExporterConfig, bExportObjectData); } \
	FORCEINLINE static uint32 __PPO__bExportScreenShot() { return STRUCT_OFFSET(FNVSceneExporterConfig, bExportScreenShot); } \
	FORCEINLINE static uint32 __PPO__IncludeObjectsType() { return STRUCT_OFFSET(FNVSceneExporterConfig, IncludeObjectsType); } \
	FORCEINLINE static uint32 __PPO__bIgnoreHiddenActor() { return STRUCT_OFFSET(FNVSceneExporterConfig, bIgnoreHiddenActor); } \
	FORCEINLINE static uint32 __PPO__BoundsType() { return STRUCT_OFFSET(FNVSceneExporterConfig, BoundsType); } \
	FORCEINLINE static uint32 __PPO__BoundingBox2dType() { return STRUCT_OFFSET(FNVSceneExporterConfig, BoundingBox2dType); } \
	FORCEINLINE static uint32 __PPO__bOutputEvenIfNoObjectsAreInView() { return STRUCT_OFFSET(FNVSceneExporterConfig, bOutputEvenIfNoObjectsAreInView); } \
	FORCEINLINE static uint32 __PPO__DistanceScaleRange() { return STRUCT_OFFSET(FNVSceneExporterConfig, DistanceScaleRange); }


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneExporterConfig>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_353_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCapturedFrameData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FCapturedFrameData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_340_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCapturedSceneData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FCapturedSceneData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_312_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCapturedViewpointData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FCapturedViewpointData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_208_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCapturedObjectData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FCapturedObjectData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_189_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVBox2D_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVBox2D>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_128_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVCuboidData_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Center() { return STRUCT_OFFSET(FNVCuboidData, Center); } \
	FORCEINLINE static uint32 __PPO__LocalBox() { return STRUCT_OFFSET(FNVCuboidData, LocalBox); } \
	FORCEINLINE static uint32 __PPO__Rotation() { return STRUCT_OFFSET(FNVCuboidData, Rotation); }


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVCuboidData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_97_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSocketData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSocketData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_80_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVTexturePixelData_Statics; \
	NVSCENECAPTURER_API static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVTexturePixelData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVCapturableActorTag(); \
	friend struct Z_Construct_UClass_UNVCapturableActorTag_Statics; \
public: \
	DECLARE_CLASS(UNVCapturableActorTag, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVCapturableActorTag)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_INCLASS \
private: \
	static void StaticRegisterNativesUNVCapturableActorTag(); \
	friend struct Z_Construct_UClass_UNVCapturableActorTag_Statics; \
public: \
	DECLARE_CLASS(UNVCapturableActorTag, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVCapturableActorTag)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVCapturableActorTag(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVCapturableActorTag) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVCapturableActorTag); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVCapturableActorTag); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVCapturableActorTag(UNVCapturableActorTag&&); \
	NO_API UNVCapturableActorTag(const UNVCapturableActorTag&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVCapturableActorTag(UNVCapturableActorTag&&); \
	NO_API UNVCapturableActorTag(const UNVCapturableActorTag&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVCapturableActorTag); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVCapturableActorTag); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UNVCapturableActorTag)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_365_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h_368_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVCapturableActorTag>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneCapturerUtils_h


#define FOREACH_ENUM_ENVSCENECAPTURERSTATE(op) \
	op(ENVSceneCapturerState::NotActive) \
	op(ENVSceneCapturerState::Active) \
	op(ENVSceneCapturerState::Running) \
	op(ENVSceneCapturerState::Paused) \
	op(ENVSceneCapturerState::Completed) \
	op(ENVSceneCapturerState::NVSceneCapturerState_MAX) 

enum class ENVSceneCapturerState : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVSceneCapturerState>();

#define FOREACH_ENUM_ENVBOUNDBOX2DGENERATIONTYPE(op) \
	op(ENVBoundBox2dGenerationType::From3dBoundingBox) \
	op(ENVBoundBox2dGenerationType::FromMeshBodyCollision) 

enum class ENVBoundBox2dGenerationType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVBoundBox2dGenerationType>();

#define FOREACH_ENUM_ENVBOUNDSGENERATIONTYPE(op) \
	op(ENVBoundsGenerationType::VE_AABB) \
	op(ENVBoundsGenerationType::VE_OOBB) \
	op(ENVBoundsGenerationType::VE_TightOOBB) 

enum class ENVBoundsGenerationType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVBoundsGenerationType>();

#define FOREACH_ENUM_ENVINCLUDEOBJECTS(op) \
	op(ENVIncludeObjects::AllTaggedObjects) \
	op(ENVIncludeObjects::MatchesTag) 

enum class ENVIncludeObjects : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVIncludeObjects>();

#define FOREACH_ENUM_ENVCUBOIDVERTEXTYPE(op) \
	op(ENVCuboidVertexType::FrontTopRight) \
	op(ENVCuboidVertexType::FrontTopLeft) \
	op(ENVCuboidVertexType::FrontBottomLeft) \
	op(ENVCuboidVertexType::FrontBottomRight) \
	op(ENVCuboidVertexType::RearTopRight) \
	op(ENVCuboidVertexType::RearTopLeft) \
	op(ENVCuboidVertexType::RearBottomLeft) \
	op(ENVCuboidVertexType::RearBottomRight) \
	op(ENVCuboidVertexType::CuboidVertexType_MAX) 

enum class ENVCuboidVertexType : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVCuboidVertexType>();

#define FOREACH_ENUM_ENVCAPTUREDPIXELFORMAT(op) \
	op(R8) \
	op(RGBA8) \
	op(R8G8) \
	op(R32f) \
	op(NVCapturedPixelFormat_MAX) 
#define FOREACH_ENUM_ENVIMAGEFORMAT(op) \
	op(ENVImageFormat::PNG) \
	op(ENVImageFormat::JPEG) \
	op(ENVImageFormat::GrayscaleJPEG) \
	op(ENVImageFormat::BMP) \
	op(ENVImageFormat::NVImageFormat_MAX) 

enum class ENVImageFormat : uint8;
template<> NVSCENECAPTURER_API UEnum* StaticEnum<ENVImageFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
