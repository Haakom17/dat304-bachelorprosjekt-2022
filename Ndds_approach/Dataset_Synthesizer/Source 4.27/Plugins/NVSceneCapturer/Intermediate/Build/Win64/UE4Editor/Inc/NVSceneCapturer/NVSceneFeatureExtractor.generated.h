// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURER_NVSceneFeatureExtractor_generated_h
#error "NVSceneFeatureExtractor.generated.h already included, missing '#pragma once' in NVSceneFeatureExtractor.h"
#endif
#define NVSCENECAPTURER_NVSceneFeatureExtractor_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_83_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVSceneCaptureComponentData_Statics; \
	NVSCENECAPTURER_API static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVSceneCaptureComponentData>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FNVFeatureExtractorSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> NVSCENECAPTURER_API UScriptStruct* StaticStruct<struct FNVFeatureExtractorSettings>();

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_RPC_WRAPPERS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneFeatureExtractor(); \
	friend struct Z_Construct_UClass_UNVSceneFeatureExtractor_Statics; \
public: \
	DECLARE_CLASS(UNVSceneFeatureExtractor, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/NVSceneCapturer"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneFeatureExtractor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneFeatureExtractor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor(UNVSceneFeatureExtractor&&); \
	NO_API UNVSceneFeatureExtractor(const UNVSceneFeatureExtractor&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneFeatureExtractor(UNVSceneFeatureExtractor&&); \
	NO_API UNVSceneFeatureExtractor(const UNVSceneFeatureExtractor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneFeatureExtractor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneFeatureExtractor); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneFeatureExtractor)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OwnerCapturer() { return STRUCT_OFFSET(UNVSceneFeatureExtractor, OwnerCapturer); } \
	FORCEINLINE static uint32 __PPO__OwnerViewpoint() { return STRUCT_OFFSET(UNVSceneFeatureExtractor, OwnerViewpoint); } \
	FORCEINLINE static uint32 __PPO__bCapturing() { return STRUCT_OFFSET(UNVSceneFeatureExtractor, bCapturing); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_34_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURER_API UClass* StaticClass<class UNVSceneFeatureExtractor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturer_Public_NVSceneFeatureExtractor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
