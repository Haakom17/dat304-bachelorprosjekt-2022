// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVSCENECAPTURERGAME_NVSceneCapturerHUD_ExporterControlPanel_generated_h
#error "NVSceneCapturerHUD_ExporterControlPanel.generated.h already included, missing '#pragma once' in NVSceneCapturerHUD_ExporterControlPanel.h"
#endif
#define NVSCENECAPTURERGAME_NVSceneCapturerHUD_ExporterControlPanel_generated_h

#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_SPARSE_DATA
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOpenOutputDirectory_Clicked); \
	DECLARE_FUNCTION(execOnToggleExporterViewport_Clicked); \
	DECLARE_FUNCTION(execOnCompleted_Clicked); \
	DECLARE_FUNCTION(execOnResumeExporting_Clicked); \
	DECLARE_FUNCTION(execOnPauseExporting_Clicked); \
	DECLARE_FUNCTION(execOnStopExporting_Clicked); \
	DECLARE_FUNCTION(execOnStartExporting_Clicked);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOpenOutputDirectory_Clicked); \
	DECLARE_FUNCTION(execOnToggleExporterViewport_Clicked); \
	DECLARE_FUNCTION(execOnCompleted_Clicked); \
	DECLARE_FUNCTION(execOnResumeExporting_Clicked); \
	DECLARE_FUNCTION(execOnPauseExporting_Clicked); \
	DECLARE_FUNCTION(execOnStopExporting_Clicked); \
	DECLARE_FUNCTION(execOnStartExporting_Clicked);


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_ExporterControlPanel(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_ExporterControlPanel, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_ExporterControlPanel)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUNVSceneCapturerHUD_ExporterControlPanel(); \
	friend struct Z_Construct_UClass_UNVSceneCapturerHUD_ExporterControlPanel_Statics; \
public: \
	DECLARE_CLASS(UNVSceneCapturerHUD_ExporterControlPanel, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVSceneCapturerGame"), NO_API) \
	DECLARE_SERIALIZER(UNVSceneCapturerHUD_ExporterControlPanel)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVSceneCapturerHUD_ExporterControlPanel(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_ExporterControlPanel) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_ExporterControlPanel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_ExporterControlPanel); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_ExporterControlPanel(UNVSceneCapturerHUD_ExporterControlPanel&&); \
	NO_API UNVSceneCapturerHUD_ExporterControlPanel(const UNVSceneCapturerHUD_ExporterControlPanel&); \
public:


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVSceneCapturerHUD_ExporterControlPanel(UNVSceneCapturerHUD_ExporterControlPanel&&); \
	NO_API UNVSceneCapturerHUD_ExporterControlPanel(const UNVSceneCapturerHUD_ExporterControlPanel&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVSceneCapturerHUD_ExporterControlPanel); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVSceneCapturerHUD_ExporterControlPanel); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVSceneCapturerHUD_ExporterControlPanel)


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TitleText() { return STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, TitleText); } \
	FORCEINLINE static uint32 __PPO__ActiveCapturerActor() { return STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, ActiveCapturerActor); } \
	FORCEINLINE static uint32 __PPO__SceneDataExporter() { return STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, SceneDataExporter); } \
	FORCEINLINE static uint32 __PPO__CurrentActiveStatePanel() { return STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, CurrentActiveStatePanel); } \
	FORCEINLINE static uint32 __PPO__StatePanelList() { return STRUCT_OFFSET(UNVSceneCapturerHUD_ExporterControlPanel, StatePanelList); }


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_20_PROLOG
#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_RPC_WRAPPERS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_INCLASS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_SPARSE_DATA \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVSCENECAPTURERGAME_API UClass* StaticClass<class UNVSceneCapturerHUD_ExporterControlPanel>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVSceneCapturer_Source_NVSceneCapturerGame_Public_HUD_NVSceneCapturerHUD_ExporterControlPanel_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
