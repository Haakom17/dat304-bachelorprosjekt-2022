// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVUtilities/Public/Movement/NVWaypoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVWaypoint() {}
// Cross Module References
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVWaypoint_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVWaypoint();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_NVUtilities();
// End Cross Module References
	void ANVWaypoint::StaticRegisterNativesANVWaypoint()
	{
	}
	UClass* Z_Construct_UClass_ANVWaypoint_NoRegister()
	{
		return ANVWaypoint::StaticClass();
	}
	struct Z_Construct_UClass_ANVWaypoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVWaypoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVWaypoint_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "Movement/NVWaypoint.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Movement/NVWaypoint.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVWaypoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVWaypoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVWaypoint_Statics::ClassParams = {
		&ANVWaypoint::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVWaypoint_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVWaypoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVWaypoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVWaypoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVWaypoint, 2154088571);
	template<> NVUTILITIES_API UClass* StaticClass<ANVWaypoint>()
	{
		return ANVWaypoint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVWaypoint(Z_Construct_UClass_ANVWaypoint, &ANVWaypoint::StaticClass, TEXT("/Script/NVUtilities"), TEXT("ANVWaypoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVWaypoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
