// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVUTILITIES_NVZone_generated_h
#error "NVZone.generated.h already included, missing '#pragma once' in NVZone.h"
#endif
#define NVUTILITIES_NVZone_generated_h

#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_SPARSE_DATA
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_RPC_WRAPPERS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVZone(); \
	friend struct Z_Construct_UClass_ANVZone_Statics; \
public: \
	DECLARE_CLASS(ANVZone, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_INCLASS \
private: \
	static void StaticRegisterNativesANVZone(); \
	friend struct Z_Construct_UClass_ANVZone_Statics; \
public: \
	DECLARE_CLASS(ANVZone, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVZone) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVZone); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVZone(ANVZone&&); \
	NO_API ANVZone(const ANVZone&); \
public:


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVZone(ANVZone&&); \
	NO_API ANVZone(const ANVZone&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVZone); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RadiusRange() { return STRUCT_OFFSET(ANVZone, RadiusRange); } \
	FORCEINLINE static uint32 __PPO__HeightRange() { return STRUCT_OFFSET(ANVZone, HeightRange); }


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_15_PROLOG
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_RPC_WRAPPERS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_INCLASS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class ANVZone>();

#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_SPARSE_DATA
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_RPC_WRAPPERS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVNavigationZone(); \
	friend struct Z_Construct_UClass_ANVNavigationZone_Statics; \
public: \
	DECLARE_CLASS(ANVNavigationZone, ANVZone, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVNavigationZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_INCLASS \
private: \
	static void StaticRegisterNativesANVNavigationZone(); \
	friend struct Z_Construct_UClass_ANVNavigationZone_Statics; \
public: \
	DECLARE_CLASS(ANVNavigationZone, ANVZone, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVNavigationZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVNavigationZone(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVNavigationZone) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVNavigationZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVNavigationZone); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVNavigationZone(ANVNavigationZone&&); \
	NO_API ANVNavigationZone(const ANVNavigationZone&); \
public:


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVNavigationZone(ANVNavigationZone&&); \
	NO_API ANVNavigationZone(const ANVNavigationZone&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVNavigationZone); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVNavigationZone); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVNavigationZone)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bLookAtCenter() { return STRUCT_OFFSET(ANVNavigationZone, bLookAtCenter); }


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_38_PROLOG
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_RPC_WRAPPERS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_INCLASS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class ANVNavigationZone>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_NVZone_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
