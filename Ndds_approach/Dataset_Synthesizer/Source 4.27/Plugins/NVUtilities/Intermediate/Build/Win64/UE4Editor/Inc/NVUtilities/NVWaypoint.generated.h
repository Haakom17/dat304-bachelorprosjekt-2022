// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVUTILITIES_NVWaypoint_generated_h
#error "NVWaypoint.generated.h already included, missing '#pragma once' in NVWaypoint.h"
#endif
#define NVUTILITIES_NVWaypoint_generated_h

#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_SPARSE_DATA
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_RPC_WRAPPERS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVWaypoint(); \
	friend struct Z_Construct_UClass_ANVWaypoint_Statics; \
public: \
	DECLARE_CLASS(ANVWaypoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVWaypoint)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_INCLASS \
private: \
	static void StaticRegisterNativesANVWaypoint(); \
	friend struct Z_Construct_UClass_ANVWaypoint_Statics; \
public: \
	DECLARE_CLASS(ANVWaypoint, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/NVUtilities"), NO_API) \
	DECLARE_SERIALIZER(ANVWaypoint)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVWaypoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVWaypoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVWaypoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVWaypoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVWaypoint(ANVWaypoint&&); \
	NO_API ANVWaypoint(const ANVWaypoint&); \
public:


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVWaypoint(ANVWaypoint&&); \
	NO_API ANVWaypoint(const ANVWaypoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVWaypoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVWaypoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVWaypoint)


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_15_PROLOG
#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_RPC_WRAPPERS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_INCLASS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_SPARSE_DATA \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVUTILITIES_API UClass* StaticClass<class ANVWaypoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVUtilities_Source_NVUtilities_Public_Movement_NVWaypoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
