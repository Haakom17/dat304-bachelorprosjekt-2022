// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVUtilities/Public/NVZone.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVZone() {}
// Cross Module References
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVZone_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVZone();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_NVUtilities();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVNavigationZone_NoRegister();
	NVUTILITIES_API UClass* Z_Construct_UClass_ANVNavigationZone();
// End Cross Module References
	void ANVZone::StaticRegisterNativesANVZone()
	{
	}
	UClass* Z_Construct_UClass_ANVZone_NoRegister()
	{
		return ANVZone::StaticClass();
	}
	struct Z_Construct_UClass_ANVZone_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadiusRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RadiusRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeightRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVZone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVZone_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "NVZone.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVZone.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVZone_Statics::NewProp_RadiusRange_MetaData[] = {
		{ "Category", "Zone" },
		{ "Comment", "// Editor properties\n// How wide is the zone (in the XY plane)\n// NOTE: If the min value > 0 then the zone have an empty area inside\n" },
		{ "ModuleRelativePath", "Public/NVZone.h" },
		{ "ToolTip", "Editor properties\nHow wide is the zone (in the XY plane)\nNOTE: If the min value > 0 then the zone have an empty area inside" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVZone_Statics::NewProp_RadiusRange = { "RadiusRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVZone, RadiusRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_ANVZone_Statics::NewProp_RadiusRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVZone_Statics::NewProp_RadiusRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVZone_Statics::NewProp_HeightRange_MetaData[] = {
		{ "Category", "Zone" },
		{ "Comment", "// How tall is the zone (along Z axis)\n// NOTE: Min value is how far does the zone extends below the zone's center location (negative Z axis)\n// Max value is how far the zone extends above the zone's center location (positive Z axis)\n" },
		{ "ModuleRelativePath", "Public/NVZone.h" },
		{ "ToolTip", "How tall is the zone (along Z axis)\nNOTE: Min value is how far does the zone extends below the zone's center location (negative Z axis)\nMax value is how far the zone extends above the zone's center location (positive Z axis)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVZone_Statics::NewProp_HeightRange = { "HeightRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVZone, HeightRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_ANVZone_Statics::NewProp_HeightRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVZone_Statics::NewProp_HeightRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVZone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVZone_Statics::NewProp_RadiusRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVZone_Statics::NewProp_HeightRange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVZone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVZone>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVZone_Statics::ClassParams = {
		&ANVZone::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVZone_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ANVZone_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVZone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVZone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVZone()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVZone_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVZone, 2651278823);
	template<> NVUTILITIES_API UClass* StaticClass<ANVZone>()
	{
		return ANVZone::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVZone(Z_Construct_UClass_ANVZone, &ANVZone::StaticClass, TEXT("/Script/NVUtilities"), TEXT("ANVZone"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVZone);
	void ANVNavigationZone::StaticRegisterNativesANVNavigationZone()
	{
	}
	UClass* Z_Construct_UClass_ANVNavigationZone_NoRegister()
	{
		return ANVNavigationZone::StaticClass();
	}
	struct Z_Construct_UClass_ANVNavigationZone_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLookAtCenter_MetaData[];
#endif
		static void NewProp_bLookAtCenter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLookAtCenter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVNavigationZone_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ANVZone,
		(UObject* (*)())Z_Construct_UPackage__Script_NVUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVNavigationZone_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "NVZone.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/NVZone.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter_MetaData[] = {
		{ "Category", "Zone" },
		{ "Comment", "// Editor properties\n// If true, the navigating object need to look at the zone's center location\n" },
		{ "ModuleRelativePath", "Public/NVZone.h" },
		{ "ToolTip", "Editor properties\nIf true, the navigating object need to look at the zone's center location" },
	};
#endif
	void Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter_SetBit(void* Obj)
	{
		((ANVNavigationZone*)Obj)->bLookAtCenter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter = { "bLookAtCenter", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVNavigationZone), &Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVNavigationZone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVNavigationZone_Statics::NewProp_bLookAtCenter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVNavigationZone_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVNavigationZone>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVNavigationZone_Statics::ClassParams = {
		&ANVNavigationZone::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVNavigationZone_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ANVNavigationZone_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVNavigationZone_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVNavigationZone_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVNavigationZone()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVNavigationZone_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVNavigationZone, 1347617230);
	template<> NVUTILITIES_API UClass* StaticClass<ANVNavigationZone>()
	{
		return ANVNavigationZone::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVNavigationZone(Z_Construct_UClass_ANVNavigationZone, &ANVNavigationZone::StaticClass, TEXT("/Script/NVUtilities"), TEXT("ANVNavigationZone"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVNavigationZone);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
