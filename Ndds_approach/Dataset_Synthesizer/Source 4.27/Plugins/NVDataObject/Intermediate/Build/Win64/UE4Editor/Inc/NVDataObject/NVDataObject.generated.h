// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVDATAOBJECT_NVDataObject_generated_h
#error "NVDataObject.generated.h already included, missing '#pragma once' in NVDataObject.h"
#endif
#define NVDATAOBJECT_NVDataObject_generated_h

#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_SPARSE_DATA
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_RPC_WRAPPERS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UNVDataObjectAsset, NO_API)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVDataObjectAsset(); \
	friend struct Z_Construct_UClass_UNVDataObjectAsset_Statics; \
public: \
	DECLARE_CLASS(UNVDataObjectAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObject"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObjectAsset) \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_ARCHIVESERIALIZER


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUNVDataObjectAsset(); \
	friend struct Z_Construct_UClass_UNVDataObjectAsset_Statics; \
public: \
	DECLARE_CLASS(UNVDataObjectAsset, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObject"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObjectAsset) \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_ARCHIVESERIALIZER


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVDataObjectAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectAsset(UNVDataObjectAsset&&); \
	NO_API UNVDataObjectAsset(const UNVDataObjectAsset&); \
public:


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectAsset(UNVDataObjectAsset&&); \
	NO_API UNVDataObjectAsset(const UNVDataObjectAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectAsset)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_17_PROLOG
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_RPC_WRAPPERS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_INCLASS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVDATAOBJECT_API UClass* StaticClass<class UNVDataObjectAsset>();

#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_SPARSE_DATA
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_RPC_WRAPPERS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UNVDataObject, NO_API)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVDataObject(); \
	friend struct Z_Construct_UClass_UNVDataObject_Statics; \
public: \
	DECLARE_CLASS(UNVDataObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObject"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObject) \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_ARCHIVESERIALIZER


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_INCLASS \
private: \
	static void StaticRegisterNativesUNVDataObject(); \
	friend struct Z_Construct_UClass_UNVDataObject_Statics; \
public: \
	DECLARE_CLASS(UNVDataObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObject"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObject) \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_ARCHIVESERIALIZER


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVDataObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObject(UNVDataObject&&); \
	NO_API UNVDataObject(const UNVDataObject&); \
public:


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObject(UNVDataObject&&); \
	NO_API UNVDataObject(const UNVDataObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObject)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LogicHandlerClassType() { return STRUCT_OFFSET(UNVDataObject, LogicHandlerClassType); }


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_53_PROLOG
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_RPC_WRAPPERS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_INCLASS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_56_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVDATAOBJECT_API UClass* StaticClass<class UNVDataObject>();

#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_SPARSE_DATA
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_RPC_WRAPPERS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVDataObjectOwner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectOwner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectOwner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectOwner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectOwner(UNVDataObjectOwner&&); \
	NO_API UNVDataObjectOwner(const UNVDataObjectOwner&); \
public:


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVDataObjectOwner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectOwner(UNVDataObjectOwner&&); \
	NO_API UNVDataObjectOwner(const UNVDataObjectOwner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectOwner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectOwner); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectOwner)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUNVDataObjectOwner(); \
	friend struct Z_Construct_UClass_UNVDataObjectOwner_Statics; \
public: \
	DECLARE_CLASS(UNVDataObjectOwner, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/NVDataObject"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObjectOwner)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_GENERATED_UINTERFACE_BODY() \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_GENERATED_UINTERFACE_BODY() \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~INVDataObjectOwner() {} \
public: \
	typedef UNVDataObjectOwner UClassType; \
	typedef INVDataObjectOwner ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_INCLASS_IINTERFACE \
protected: \
	virtual ~INVDataObjectOwner() {} \
public: \
	typedef UNVDataObjectOwner UClassType; \
	typedef INVDataObjectOwner ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_74_PROLOG
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_82_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_RPC_WRAPPERS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_82_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h_77_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVDATAOBJECT_API UClass* StaticClass<class UNVDataObjectOwner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVDataObject_Source_NVDataObject_Public_NVDataObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
