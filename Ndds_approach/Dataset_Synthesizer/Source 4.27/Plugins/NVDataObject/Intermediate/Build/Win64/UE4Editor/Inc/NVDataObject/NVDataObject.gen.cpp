// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "NVDataObject/Public/NVDataObject.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVDataObject() {}
// Cross Module References
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObjectAsset_NoRegister();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObjectAsset();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_NVDataObject();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData_NoRegister();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObjectOwner_NoRegister();
	NVDATAOBJECT_API UClass* Z_Construct_UClass_UNVDataObjectOwner();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
	void UNVDataObjectAsset::StaticRegisterNativesUNVDataObjectAsset()
	{
	}
	UClass* Z_Construct_UClass_UNVDataObjectAsset_NoRegister()
	{
		return UNVDataObjectAsset::StaticClass();
	}
	struct Z_Construct_UClass_UNVDataObjectAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataObject;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetImportData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetImportData;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVDataObjectAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVDataObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectAsset_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// UNVDataObject contain data which define how we want to create an object in the game\n" },
		{ "IncludePath", "NVDataObject.h" },
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "UNVDataObject contain data which define how we want to create an object in the game" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_DataObject_MetaData[] = {
		{ "Category", "NVDataObjectAsset" },
		{ "Comment", "// Editor properties\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_DataObject = { "DataObject", nullptr, (EPropertyFlags)0x0010020000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVDataObjectAsset, DataObject), Z_Construct_UClass_UNVDataObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_DataObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_DataObject_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_AssetImportData_MetaData[] = {
		{ "Category", "Config" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_AssetImportData = { "AssetImportData", nullptr, (EPropertyFlags)0x00120408000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVDataObjectAsset, AssetImportData), Z_Construct_UClass_UAssetImportData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_AssetImportData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_AssetImportData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVDataObjectAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_DataObject,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVDataObjectAsset_Statics::NewProp_AssetImportData,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVDataObjectAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVDataObjectAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVDataObjectAsset_Statics::ClassParams = {
		&UNVDataObjectAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVDataObjectAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectAsset_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVDataObjectAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVDataObjectAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVDataObjectAsset, 1734628905);
	template<> NVDATAOBJECT_API UClass* StaticClass<UNVDataObjectAsset>()
	{
		return UNVDataObjectAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVDataObjectAsset(Z_Construct_UClass_UNVDataObjectAsset, &UNVDataObjectAsset::StaticClass, TEXT("/Script/NVDataObject"), TEXT("UNVDataObjectAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVDataObjectAsset);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UNVDataObjectAsset)
	void UNVDataObject::StaticRegisterNativesUNVDataObject()
	{
	}
	UClass* Z_Construct_UClass_UNVDataObject_NoRegister()
	{
		return UNVDataObject::StaticClass();
	}
	struct Z_Construct_UClass_UNVDataObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LogicHandlerClassType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_LogicHandlerClassType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVDataObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_NVDataObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NVDataObject.h" },
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObject_Statics::NewProp_LogicHandlerClassType_MetaData[] = {
		{ "Category", "Config" },
		{ "Comment", "// Editor properties\n// The class of the logic object\n" },
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
		{ "ToolTip", "Editor properties\nThe class of the logic object" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UNVDataObject_Statics::NewProp_LogicHandlerClassType = { "LogicHandlerClassType", nullptr, (EPropertyFlags)0x00240c0000020001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNVDataObject, LogicHandlerClassType), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UNVDataObject_Statics::NewProp_LogicHandlerClassType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObject_Statics::NewProp_LogicHandlerClassType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNVDataObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNVDataObject_Statics::NewProp_LogicHandlerClassType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVDataObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNVDataObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVDataObject_Statics::ClassParams = {
		&UNVDataObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNVDataObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObject_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNVDataObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVDataObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVDataObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVDataObject, 2855733858);
	template<> NVDATAOBJECT_API UClass* StaticClass<UNVDataObject>()
	{
		return UNVDataObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVDataObject(Z_Construct_UClass_UNVDataObject, &UNVDataObject::StaticClass, TEXT("/Script/NVDataObject"), TEXT("UNVDataObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVDataObject);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UNVDataObject)
	void UNVDataObjectOwner::StaticRegisterNativesUNVDataObjectOwner()
	{
	}
	UClass* Z_Construct_UClass_UNVDataObjectOwner_NoRegister()
	{
		return UNVDataObjectOwner::StaticClass();
	}
	struct Z_Construct_UClass_UNVDataObjectOwner_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNVDataObjectOwner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_NVDataObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNVDataObjectOwner_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/NVDataObject.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNVDataObjectOwner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<INVDataObjectOwner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNVDataObjectOwner_Statics::ClassParams = {
		&UNVDataObjectOwner::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UNVDataObjectOwner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNVDataObjectOwner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNVDataObjectOwner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNVDataObjectOwner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNVDataObjectOwner, 3721084465);
	template<> NVDATAOBJECT_API UClass* StaticClass<UNVDataObjectOwner>()
	{
		return UNVDataObjectOwner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNVDataObjectOwner(Z_Construct_UClass_UNVDataObjectOwner, &UNVDataObjectOwner::StaticClass, TEXT("/Script/NVDataObject"), TEXT("UNVDataObjectOwner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNVDataObjectOwner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
