// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef NVDATAOBJECTEDITOR_NVDataObjectFactory_generated_h
#error "NVDataObjectFactory.generated.h already included, missing '#pragma once' in NVDataObjectFactory.h"
#endif
#define NVDATAOBJECTEDITOR_NVDataObjectFactory_generated_h

#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_SPARSE_DATA
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_RPC_WRAPPERS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUNVDataObjectFactory(); \
	friend struct Z_Construct_UClass_UNVDataObjectFactory_Statics; \
public: \
	DECLARE_CLASS(UNVDataObjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObjectEditor"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObjectFactory)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUNVDataObjectFactory(); \
	friend struct Z_Construct_UClass_UNVDataObjectFactory_Statics; \
public: \
	DECLARE_CLASS(UNVDataObjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/NVDataObjectEditor"), NO_API) \
	DECLARE_SERIALIZER(UNVDataObjectFactory)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UNVDataObjectFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectFactory(UNVDataObjectFactory&&); \
	NO_API UNVDataObjectFactory(const UNVDataObjectFactory&); \
public:


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UNVDataObjectFactory(UNVDataObjectFactory&&); \
	NO_API UNVDataObjectFactory(const UNVDataObjectFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UNVDataObjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UNVDataObjectFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UNVDataObjectFactory)


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_19_PROLOG
#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_RPC_WRAPPERS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_INCLASS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_SPARSE_DATA \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> NVDATAOBJECTEDITOR_API UClass* StaticClass<class UNVDataObjectFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_NVDataObject_Source_NVDataObjectEditor_Public_NVDataObjectFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
