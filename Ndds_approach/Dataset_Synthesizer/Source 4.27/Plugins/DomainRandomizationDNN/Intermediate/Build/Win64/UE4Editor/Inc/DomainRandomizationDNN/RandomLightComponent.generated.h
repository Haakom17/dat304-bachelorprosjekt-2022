// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomLightComponent_generated_h
#error "RandomLightComponent.generated.h already included, missing '#pragma once' in RandomLightComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomLightComponent_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomLightComponent(); \
	friend struct Z_Construct_UClass_URandomLightComponent_Statics; \
public: \
	DECLARE_CLASS(URandomLightComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLightComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesURandomLightComponent(); \
	friend struct Z_Construct_UClass_URandomLightComponent_Statics; \
public: \
	DECLARE_CLASS(URandomLightComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomLightComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomLightComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomLightComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLightComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLightComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLightComponent(URandomLightComponent&&); \
	NO_API URandomLightComponent(const URandomLightComponent&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomLightComponent(URandomLightComponent&&); \
	NO_API URandomLightComponent(const URandomLightComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomLightComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomLightComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomLightComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bShouldModifyIntensity() { return STRUCT_OFFSET(URandomLightComponent, bShouldModifyIntensity); } \
	FORCEINLINE static uint32 __PPO__IntensityRange() { return STRUCT_OFFSET(URandomLightComponent, IntensityRange); } \
	FORCEINLINE static uint32 __PPO__bShouldModifyColor() { return STRUCT_OFFSET(URandomLightComponent, bShouldModifyColor); } \
	FORCEINLINE static uint32 __PPO__ColorData() { return STRUCT_OFFSET(URandomLightComponent, ColorData); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_19_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomLightComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomLightComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
