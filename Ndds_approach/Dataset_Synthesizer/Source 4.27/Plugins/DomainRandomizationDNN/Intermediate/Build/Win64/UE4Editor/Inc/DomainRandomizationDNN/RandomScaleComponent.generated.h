// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomScaleComponent_generated_h
#error "RandomScaleComponent.generated.h already included, missing '#pragma once' in RandomScaleComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomScaleComponent_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomScaleComponent(); \
	friend struct Z_Construct_UClass_URandomScaleComponent_Statics; \
public: \
	DECLARE_CLASS(URandomScaleComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomScaleComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesURandomScaleComponent(); \
	friend struct Z_Construct_UClass_URandomScaleComponent_Statics; \
public: \
	DECLARE_CLASS(URandomScaleComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomScaleComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomScaleComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomScaleComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomScaleComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomScaleComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomScaleComponent(URandomScaleComponent&&); \
	NO_API URandomScaleComponent(const URandomScaleComponent&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomScaleComponent(URandomScaleComponent&&); \
	NO_API URandomScaleComponent(const URandomScaleComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomScaleComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomScaleComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomScaleComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RandomScaleData() { return STRUCT_OFFSET(URandomScaleComponent, RandomScaleData); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_14_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomScaleComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomScaleComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
