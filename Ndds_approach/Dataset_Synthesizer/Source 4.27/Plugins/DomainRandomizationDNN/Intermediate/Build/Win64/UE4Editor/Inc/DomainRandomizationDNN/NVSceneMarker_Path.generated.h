// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_NVSceneMarker_Path_generated_h
#error "NVSceneMarker_Path.generated.h already included, missing '#pragma once' in NVSceneMarker_Path.h"
#endif
#define DOMAINRANDOMIZATIONDNN_NVSceneMarker_Path_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneMarker_Path(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_Path_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_Path, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_Path) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_Path*>(this); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneMarker_Path(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_Path_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_Path, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_Path) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_Path*>(this); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneMarker_Path(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_Path) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_Path); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_Path); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_Path(ANVSceneMarker_Path&&); \
	NO_API ANVSceneMarker_Path(const ANVSceneMarker_Path&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_Path(ANVSceneMarker_Path&&); \
	NO_API ANVSceneMarker_Path(const ANVSceneMarker_Path&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_Path); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_Path); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_Path)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneMarker() { return STRUCT_OFFSET(ANVSceneMarker_Path, SceneMarker); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_17_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ANVSceneMarker_Path>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Path_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
