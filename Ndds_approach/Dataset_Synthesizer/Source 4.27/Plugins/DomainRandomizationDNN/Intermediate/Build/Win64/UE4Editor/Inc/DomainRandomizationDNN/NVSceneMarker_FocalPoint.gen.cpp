// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SceneMarker/NVSceneMarker_FocalPoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneMarker_FocalPoint() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_FocalPoint_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_FocalPoint();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneMarkerComponent();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister();
// End Cross Module References
	void ANVSceneMarker_FocalPoint::StaticRegisterNativesANVSceneMarker_FocalPoint()
	{
	}
	UClass* Z_Construct_UClass_ANVSceneMarker_FocalPoint_NoRegister()
	{
		return ANVSceneMarker_FocalPoint::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BillboardComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BillboardComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpringArm_ViewPointComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpringArm_ViewPointComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMarker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneMarker;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseOrbitalMovement_MetaData[];
#endif
		static void NewProp_bUseOrbitalMovement_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseOrbitalMovement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n * ANVSceneMarker_FocalPoint - The scene marker actors are placed in the map and control how the capturer move around\n *//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering" },
		{ "IncludePath", "SceneMarker/NVSceneMarker_FocalPoint.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_FocalPoint.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "ANVSceneMarker_FocalPoint - The scene marker actors are placed in the map and control how the capturer move around\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_BillboardComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "NVSceneMarker_FocalPoint" },
		{ "Comment", "// Editor properties\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_FocalPoint.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_BillboardComponent = { "BillboardComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneMarker_FocalPoint, BillboardComponent), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_BillboardComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_BillboardComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SpringArm_ViewPointComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "NVSceneMarker_FocalPoint" },
		{ "Comment", "// The spring arm component control where to attach the viewpoint\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_FocalPoint.h" },
		{ "ToolTip", "The spring arm component control where to attach the viewpoint" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SpringArm_ViewPointComponent = { "SpringArm_ViewPointComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneMarker_FocalPoint, SpringArm_ViewPointComponent), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SpringArm_ViewPointComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SpringArm_ViewPointComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SceneMarker_MetaData[] = {
		{ "Category", "NVSceneMarker_FocalPoint" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_FocalPoint.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SceneMarker = { "SceneMarker", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneMarker_FocalPoint, SceneMarker), Z_Construct_UScriptStruct_FNVSceneMarkerComponent, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SceneMarker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SceneMarker_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement_MetaData[] = {
		{ "Category", "NVSceneMarker_FocalPoint" },
		{ "Comment", "// If true, the observer's orbital movement will be activated and it will focus on this focal point\n// If false, the observer's position will be attached to a socket on SpringArm_ViewPointComponent with no random movement\n" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_FocalPoint.h" },
		{ "ToolTip", "If true, the observer's orbital movement will be activated and it will focus on this focal point\nIf false, the observer's position will be attached to a socket on SpringArm_ViewPointComponent with no random movement" },
	};
#endif
	void Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement_SetBit(void* Obj)
	{
		((ANVSceneMarker_FocalPoint*)Obj)->bUseOrbitalMovement = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement = { "bUseOrbitalMovement", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ANVSceneMarker_FocalPoint), &Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement_SetBit, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_BillboardComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SpringArm_ViewPointComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_SceneMarker,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::NewProp_bUseOrbitalMovement,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister, (int32)VTABLE_OFFSET(ANVSceneMarker_FocalPoint, INVSceneMarkerInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneMarker_FocalPoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::ClassParams = {
		&ANVSceneMarker_FocalPoint::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneMarker_FocalPoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneMarker_FocalPoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneMarker_FocalPoint, 3273210405);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ANVSceneMarker_FocalPoint>()
	{
		return ANVSceneMarker_FocalPoint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneMarker_FocalPoint(Z_Construct_UClass_ANVSceneMarker_FocalPoint, &ANVSceneMarker_FocalPoint::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ANVSceneMarker_FocalPoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneMarker_FocalPoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
