// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomAnimationComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomAnimationComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyBoneType();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyType();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FNVHalfBodyBoneData();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomAnimationComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomAnimationComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	ENGINE_API UClass* Z_Construct_UClass_UAnimSequence_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
// End Cross Module References
	static UEnum* ENVHalfBodyBoneType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyBoneType, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("ENVHalfBodyBoneType"));
		}
		return Singleton;
	}
	template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ENVHalfBodyBoneType>()
	{
		return ENVHalfBodyBoneType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVHalfBodyBoneType(ENVHalfBodyBoneType_StaticEnum, TEXT("/Script/DomainRandomizationDNN"), TEXT("ENVHalfBodyBoneType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyBoneType_Hash() { return 3606045829U; }
	UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyBoneType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVHalfBodyBoneType"), 0, Get_Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyBoneType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVHalfBodyBoneType::Heel", (int64)ENVHalfBodyBoneType::Heel },
				{ "ENVHalfBodyBoneType::Knee", (int64)ENVHalfBodyBoneType::Knee },
				{ "ENVHalfBodyBoneType::Pelvis", (int64)ENVHalfBodyBoneType::Pelvis },
				{ "ENVHalfBodyBoneType::Shoulder", (int64)ENVHalfBodyBoneType::Shoulder },
				{ "ENVHalfBodyBoneType::Elbow", (int64)ENVHalfBodyBoneType::Elbow },
				{ "ENVHalfBodyBoneType::Wrist", (int64)ENVHalfBodyBoneType::Wrist },
				{ "ENVHalfBodyBoneType::Ear", (int64)ENVHalfBodyBoneType::Ear },
				{ "ENVHalfBodyBoneType::Eye", (int64)ENVHalfBodyBoneType::Eye },
				{ "ENVHalfBodyBoneType::Nose", (int64)ENVHalfBodyBoneType::Nose },
				{ "ENVHalfBodyBoneType::NVHalfBodyBoneType_MAX", (int64)ENVHalfBodyBoneType::NVHalfBodyBoneType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Ear.Name", "ENVHalfBodyBoneType::Ear" },
				{ "Elbow.Name", "ENVHalfBodyBoneType::Elbow" },
				{ "Eye.Name", "ENVHalfBodyBoneType::Eye" },
				{ "Heel.Name", "ENVHalfBodyBoneType::Heel" },
				{ "Knee.Name", "ENVHalfBodyBoneType::Knee" },
				{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
				{ "Nose.Name", "ENVHalfBodyBoneType::Nose" },
				{ "NVHalfBodyBoneType_MAX.Hidden", "" },
				{ "NVHalfBodyBoneType_MAX.Name", "ENVHalfBodyBoneType::NVHalfBodyBoneType_MAX" },
				{ "Pelvis.Name", "ENVHalfBodyBoneType::Pelvis" },
				{ "Shoulder.Name", "ENVHalfBodyBoneType::Shoulder" },
				{ "Wrist.Name", "ENVHalfBodyBoneType::Wrist" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
				nullptr,
				"ENVHalfBodyBoneType",
				"ENVHalfBodyBoneType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENVHalfBodyType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyType, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("ENVHalfBodyType"));
		}
		return Singleton;
	}
	template<> DOMAINRANDOMIZATIONDNN_API UEnum* StaticEnum<ENVHalfBodyType>()
	{
		return ENVHalfBodyType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENVHalfBodyType(ENVHalfBodyType_StaticEnum, TEXT("/Script/DomainRandomizationDNN"), TEXT("ENVHalfBodyType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyType_Hash() { return 3974171314U; }
	UEnum* Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENVHalfBodyType"), 0, Get_Z_Construct_UEnum_DomainRandomizationDNN_ENVHalfBodyType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENVHalfBodyType::LeftSide", (int64)ENVHalfBodyType::LeftSide },
				{ "ENVHalfBodyType::RightSide", (int64)ENVHalfBodyType::RightSide },
				{ "ENVHalfBodyType::NVHalfBodyType_MAX", (int64)ENVHalfBodyType::NVHalfBodyType_MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "LeftSide.Name", "ENVHalfBodyType::LeftSide" },
				{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
				{ "NVHalfBodyType_MAX.Hidden", "" },
				{ "NVHalfBodyType_MAX.Name", "ENVHalfBodyType::NVHalfBodyType_MAX" },
				{ "RightSide.Name", "ENVHalfBodyType::RightSide" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
				nullptr,
				"ENVHalfBodyType",
				"ENVHalfBodyType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FNVHumanSkeletalBoneData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("NVHumanSkeletalBoneData"), sizeof(FNVHumanSkeletalBoneData), Get_Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FNVHumanSkeletalBoneData>()
{
	return FNVHumanSkeletalBoneData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVHumanSkeletalBoneData(FNVHumanSkeletalBoneData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("NVHumanSkeletalBoneData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHumanSkeletalBoneData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHumanSkeletalBoneData()
	{
		UScriptStruct::DeferCppStructOps<FNVHumanSkeletalBoneData>(FName(TEXT("NVHumanSkeletalBoneData")));
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHumanSkeletalBoneData;
	struct Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SideBoneData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SideBoneData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVHumanSkeletalBoneData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewProp_SideBoneData_MetaData[] = {
		{ "ArraySizeEnum", "/Script/DomainRandomizationDNN.ENVHalfBodyType" },
		{ "Category", "NVHumanSkeletalBoneData" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewProp_SideBoneData = { "SideBoneData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(SideBoneData, FNVHumanSkeletalBoneData), STRUCT_OFFSET(FNVHumanSkeletalBoneData, SideBoneData), Z_Construct_UScriptStruct_FNVHalfBodyBoneData, METADATA_PARAMS(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewProp_SideBoneData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewProp_SideBoneData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::NewProp_SideBoneData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"NVHumanSkeletalBoneData",
		sizeof(FNVHumanSkeletalBoneData),
		alignof(FNVHumanSkeletalBoneData),
		Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVHumanSkeletalBoneData"), sizeof(FNVHumanSkeletalBoneData), Get_Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData_Hash() { return 3441690765U; }
class UScriptStruct* FNVHalfBodyBoneData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DOMAINRANDOMIZATIONDNN_API uint32 Get_Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNVHalfBodyBoneData, Z_Construct_UPackage__Script_DomainRandomizationDNN(), TEXT("NVHalfBodyBoneData"), sizeof(FNVHalfBodyBoneData), Get_Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Hash());
	}
	return Singleton;
}
template<> DOMAINRANDOMIZATIONDNN_API UScriptStruct* StaticStruct<FNVHalfBodyBoneData>()
{
	return FNVHalfBodyBoneData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNVHalfBodyBoneData(FNVHalfBodyBoneData::StaticStruct, TEXT("/Script/DomainRandomizationDNN"), TEXT("NVHalfBodyBoneData"), false, nullptr, nullptr);
static struct FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHalfBodyBoneData
{
	FScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHalfBodyBoneData()
	{
		UScriptStruct::DeferCppStructOps<FNVHalfBodyBoneData>(FName(TEXT("NVHalfBodyBoneData")));
	}
} ScriptStruct_DomainRandomizationDNN_StaticRegisterNativesFNVHalfBodyBoneData;
	struct Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoneNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BoneNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNVHalfBodyBoneData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewProp_BoneNames_MetaData[] = {
		{ "ArraySizeEnum", "/Script/DomainRandomizationDNN.ENVHalfBodyBoneType" },
		{ "Category", "NVHalfBodyBoneData" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewProp_BoneNames = { "BoneNames", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(BoneNames, FNVHalfBodyBoneData), STRUCT_OFFSET(FNVHalfBodyBoneData, BoneNames), METADATA_PARAMS(Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewProp_BoneNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewProp_BoneNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::NewProp_BoneNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
		nullptr,
		&NewStructOps,
		"NVHalfBodyBoneData",
		sizeof(FNVHalfBodyBoneData),
		alignof(FNVHalfBodyBoneData),
		Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNVHalfBodyBoneData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DomainRandomizationDNN();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NVHalfBodyBoneData"), sizeof(FNVHalfBodyBoneData), Get_Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNVHalfBodyBoneData_Hash() { return 4210461478U; }
	void URandomAnimationComponent::StaticRegisterNativesURandomAnimationComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomAnimationComponent_NoRegister()
	{
		return URandomAnimationComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomAnimationComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseAllAnimationsInAFolder_MetaData[];
#endif
		static void NewProp_bUseAllAnimationsInAFolder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAllAnimationsInAFolder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AnimationFolderPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AnimationFolderPath;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomAnimList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomAnimList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RandomAnimList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeWaitAfterEachAnimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeWaitAfterEachAnimation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HumanSkeletalBoneData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HumanSkeletalBoneData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentAnimation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentAnimation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FolderAnimSequenceReferences_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FolderAnimSequenceReferences_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FolderAnimSequenceReferences;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomAnimationComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomAnimationComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder_SetBit(void* Obj)
	{
		((URandomAnimationComponent*)Obj)->bUseAllAnimationsInAFolder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder = { "bUseAllAnimationsInAFolder", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomAnimationComponent), &Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_AnimationFolderPath_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// The path to the folder where to find the animation sequence\n" },
		{ "EditCondition", "bUseAllTextureInAFolder" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
		{ "ToolTip", "The path to the folder where to find the animation sequence" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_AnimationFolderPath = { "AnimationFolderPath", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, AnimationFolderPath), METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_AnimationFolderPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_AnimationFolderPath_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList_Inner = { "RandomAnimList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UAnimSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "!bUseAllTextureInAFolder" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList = { "RandomAnimList", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, RandomAnimList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_TimeWaitAfterEachAnimation_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_TimeWaitAfterEachAnimation = { "TimeWaitAfterEachAnimation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, TimeWaitAfterEachAnimation), METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_TimeWaitAfterEachAnimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_TimeWaitAfterEachAnimation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_HumanSkeletalBoneData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_HumanSkeletalBoneData = { "HumanSkeletalBoneData", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, HumanSkeletalBoneData), Z_Construct_UScriptStruct_FNVHumanSkeletalBoneData, METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_HumanSkeletalBoneData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_HumanSkeletalBoneData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_CurrentAnimation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_CurrentAnimation = { "CurrentAnimation", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, CurrentAnimation), Z_Construct_UClass_UAnimSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_CurrentAnimation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_CurrentAnimation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences_Inner = { "FolderAnimSequenceReferences", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomAnimationComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences = { "FolderAnimSequenceReferences", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomAnimationComponent, FolderAnimSequenceReferences), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomAnimationComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_bUseAllAnimationsInAFolder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_AnimationFolderPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_RandomAnimList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_TimeWaitAfterEachAnimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_HumanSkeletalBoneData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_CurrentAnimation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomAnimationComponent_Statics::NewProp_FolderAnimSequenceReferences,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomAnimationComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomAnimationComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomAnimationComponent_Statics::ClassParams = {
		&URandomAnimationComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomAnimationComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomAnimationComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomAnimationComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomAnimationComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomAnimationComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomAnimationComponent, 1503651908);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomAnimationComponent>()
	{
		return URandomAnimationComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomAnimationComponent(Z_Construct_UClass_URandomAnimationComponent, &URandomAnimationComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomAnimationComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomAnimationComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
