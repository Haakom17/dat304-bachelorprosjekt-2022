// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMaterialParameterComponentBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMaterialParameterComponentBase() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomMaterialSelection();
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDecalComponent_NoRegister();
// End Cross Module References
	void URandomMaterialParameterComponentBase::StaticRegisterNativesURandomMaterialParameterComponentBase()
	{
	}
	UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase_NoRegister()
	{
		return URandomMaterialParameterComponentBase::StaticClass();
	}
	struct Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MaterialParameterNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialParameterNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MaterialParameterNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSelectionConfigData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialSelectionConfigData;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AffectedComponentType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AffectedComponentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AffectedComponentType;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerMeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerMeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerMeshComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerDecalComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerDecalComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerDecalComponents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n* URandomMaterialParameterComponentBase randomly change the value of some parameters of the materials in the owner's mesh\n*//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMaterialParameterComponentBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
		{ "ToolTip", "URandomMaterialParameterComponentBase randomly change the value of some parameters of the materials in the owner's mesh\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames_Inner = { "MaterialParameterNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n// List of the parameters in the material that we want to modify\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
		{ "ToolTip", "Editor properties\nList of the parameters in the material that we want to modify" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames = { "MaterialParameterNames", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParameterComponentBase, MaterialParameterNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialSelectionConfigData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialSelectionConfigData = { "MaterialSelectionConfigData", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParameterComponentBase, MaterialSelectionConfigData), Z_Construct_UScriptStruct_FRandomMaterialSelection, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialSelectionConfigData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialSelectionConfigData_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType = { "AffectedComponentType", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParameterComponentBase, AffectedComponentType), Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents_Inner = { "OwnerMeshComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents_MetaData[] = {
		{ "Comment", "// List of the mesh components from the owner actor that we need to change their materials\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
		{ "ToolTip", "List of the mesh components from the owner actor that we need to change their materials" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents = { "OwnerMeshComponents", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParameterComponentBase, OwnerMeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents_Inner = { "OwnerDecalComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDecalComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParameterComponentBase.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents = { "OwnerDecalComponents", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParameterComponentBase, OwnerDecalComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialParameterNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_MaterialSelectionConfigData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_AffectedComponentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerMeshComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::NewProp_OwnerDecalComponents,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMaterialParameterComponentBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::ClassParams = {
		&URandomMaterialParameterComponentBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::PropPointers),
		0,
		0x00B000A5u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMaterialParameterComponentBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMaterialParameterComponentBase, 704856493);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMaterialParameterComponentBase>()
	{
		return URandomMaterialParameterComponentBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMaterialParameterComponentBase(Z_Construct_UClass_URandomMaterialParameterComponentBase, &URandomMaterialParameterComponentBase::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMaterialParameterComponentBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMaterialParameterComponentBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
