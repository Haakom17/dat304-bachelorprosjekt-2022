// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SceneMarker/NVSceneMarker_Volume.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNVSceneMarker_Volume() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_Volume_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ANVSceneMarker_Volume();
	ENGINE_API UClass* Z_Construct_UClass_AVolume();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	NVSCENECAPTURER_API UScriptStruct* Z_Construct_UScriptStruct_FNVSceneMarkerComponent();
	NVSCENECAPTURER_API UClass* Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister();
// End Cross Module References
	void ANVSceneMarker_Volume::StaticRegisterNativesANVSceneMarker_Volume()
	{
	}
	UClass* Z_Construct_UClass_ANVSceneMarker_Volume_NoRegister()
	{
		return ANVSceneMarker_Volume::StaticClass();
	}
	struct Z_Construct_UClass_ANVSceneMarker_Volume_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneMarker_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SceneMarker;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ANVSceneMarker_Volume_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVolume,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_Volume_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n * ANVSceneMarker_Volume - The scene marker actors are placed in the map and control how the capturer move around\n *//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication Tick Tags Input Actor Rendering Brush Physics Object Blueprint Display Rendering Physics Input" },
		{ "IncludePath", "SceneMarker/NVSceneMarker_Volume.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_Volume.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "ANVSceneMarker_Volume - The scene marker actors are placed in the map and control how the capturer move around\n /// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ANVSceneMarker_Volume_Statics::NewProp_SceneMarker_MetaData[] = {
		{ "Category", "NVSceneMarker_Volume" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/SceneMarker/NVSceneMarker_Volume.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ANVSceneMarker_Volume_Statics::NewProp_SceneMarker = { "SceneMarker", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ANVSceneMarker_Volume, SceneMarker), Z_Construct_UScriptStruct_FNVSceneMarkerComponent, METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_Volume_Statics::NewProp_SceneMarker_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Volume_Statics::NewProp_SceneMarker_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ANVSceneMarker_Volume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ANVSceneMarker_Volume_Statics::NewProp_SceneMarker,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ANVSceneMarker_Volume_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UNVSceneMarkerInterface_NoRegister, (int32)VTABLE_OFFSET(ANVSceneMarker_Volume, INVSceneMarkerInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ANVSceneMarker_Volume_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ANVSceneMarker_Volume>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ANVSceneMarker_Volume_Statics::ClassParams = {
		&ANVSceneMarker_Volume::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ANVSceneMarker_Volume_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Volume_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ANVSceneMarker_Volume_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ANVSceneMarker_Volume_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ANVSceneMarker_Volume()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ANVSceneMarker_Volume_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ANVSceneMarker_Volume, 2464427564);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ANVSceneMarker_Volume>()
	{
		return ANVSceneMarker_Volume::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ANVSceneMarker_Volume(Z_Construct_UClass_ANVSceneMarker_Volume, &ANVSceneMarker_Volume::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ANVSceneMarker_Volume"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ANVSceneMarker_Volume);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
