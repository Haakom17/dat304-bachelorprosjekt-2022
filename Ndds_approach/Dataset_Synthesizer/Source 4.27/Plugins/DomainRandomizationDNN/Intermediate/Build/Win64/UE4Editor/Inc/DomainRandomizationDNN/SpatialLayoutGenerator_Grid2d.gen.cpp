// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpatialLayoutGenerator_Grid2d() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_Grid2d();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	void USpatialLayoutGenerator_Grid2d::StaticRegisterNativesUSpatialLayoutGenerator_Grid2d()
	{
	}
	UClass* Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_NoRegister()
	{
		return USpatialLayoutGenerator_Grid2d::StaticClass();
	}
	struct Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridRowCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_GridRowCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridColumnCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_GridColumnCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GridCellSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GridCellSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USpatialLayoutGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridRowCount_MetaData[] = {
		{ "Category", "GridLayout" },
		{ "Comment", "/** The number of row in the grid */" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h" },
		{ "ToolTip", "The number of row in the grid" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridRowCount = { "GridRowCount", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridRowCount), METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridRowCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridRowCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridColumnCount_MetaData[] = {
		{ "Category", "GridLayout" },
		{ "Comment", "/** The number of column in the grid */" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h" },
		{ "ToolTip", "The number of column in the grid" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridColumnCount = { "GridColumnCount", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridColumnCount), METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridColumnCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridColumnCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridCellSize_MetaData[] = {
		{ "Category", "GridLayout" },
		{ "Comment", "/** The size of each cell in the grid */" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Grid2d.h" },
		{ "ToolTip", "The size of each cell in the grid" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridCellSize = { "GridCellSize", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridCellSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridCellSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridCellSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridRowCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridColumnCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::NewProp_GridCellSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpatialLayoutGenerator_Grid2d>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::ClassParams = {
		&USpatialLayoutGenerator_Grid2d::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpatialLayoutGenerator_Grid2d()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpatialLayoutGenerator_Grid2d, 2930275190);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<USpatialLayoutGenerator_Grid2d>()
	{
		return USpatialLayoutGenerator_Grid2d::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpatialLayoutGenerator_Grid2d(Z_Construct_UClass_USpatialLayoutGenerator_Grid2d, &USpatialLayoutGenerator_Grid2d::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("USpatialLayoutGenerator_Grid2d"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpatialLayoutGenerator_Grid2d);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
