// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_NVSceneMarker_Volume_generated_h
#error "NVSceneMarker_Volume.generated.h already included, missing '#pragma once' in NVSceneMarker_Volume.h"
#endif
#define DOMAINRANDOMIZATIONDNN_NVSceneMarker_Volume_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesANVSceneMarker_Volume(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_Volume_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_Volume, AVolume, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_Volume) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_Volume*>(this); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_INCLASS \
private: \
	static void StaticRegisterNativesANVSceneMarker_Volume(); \
	friend struct Z_Construct_UClass_ANVSceneMarker_Volume_Statics; \
public: \
	DECLARE_CLASS(ANVSceneMarker_Volume, AVolume, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ANVSceneMarker_Volume) \
	virtual UObject* _getUObject() const override { return const_cast<ANVSceneMarker_Volume*>(this); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ANVSceneMarker_Volume(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_Volume) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_Volume); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_Volume); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_Volume(ANVSceneMarker_Volume&&); \
	NO_API ANVSceneMarker_Volume(const ANVSceneMarker_Volume&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ANVSceneMarker_Volume(ANVSceneMarker_Volume&&); \
	NO_API ANVSceneMarker_Volume(const ANVSceneMarker_Volume&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ANVSceneMarker_Volume); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ANVSceneMarker_Volume); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ANVSceneMarker_Volume)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SceneMarker() { return STRUCT_OFFSET(ANVSceneMarker_Volume, SceneMarker); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_18_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ANVSceneMarker_Volume>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SceneMarker_NVSceneMarker_Volume_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
