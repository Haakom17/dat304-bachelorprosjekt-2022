// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMovementComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMovementComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMovementComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomLocationData();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
// End Cross Module References
	void URandomMovementComponent::StaticRegisterNativesURandomMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMovementComponent_NoRegister()
	{
		return URandomMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRelatedToOriginLocation_MetaData[];
#endif
		static void NewProp_bRelatedToOriginLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRelatedToOriginLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomLocationData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData[];
#endif
		static void NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseObjectAxesInsteadOfWorldAxes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomLocationVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomLocationVolume;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldTeleport_MetaData[];
#endif
		static void NewProp_bShouldTeleport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldTeleport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCheckCollision_MetaData[];
#endif
		static void NewProp_bCheckCollision_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCheckCollision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomSpeedRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RandomSpeedRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginalTransform;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMovementComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "Comment", "// If true, the owner will be moving around its original location\n// Otherwise the random location will be selected in the RandomLocationVolume\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "If true, the owner will be moving around its original location\nOtherwise the random location will be selected in the RandomLocationVolume" },
	};
#endif
	void Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation_SetBit(void* Obj)
	{
		((URandomMovementComponent*)Obj)->bRelatedToOriginLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation = { "bRelatedToOriginLocation", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementComponent), &Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationData_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "EditCondition", "bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationData = { "RandomLocationData", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementComponent, RandomLocationData), Z_Construct_UScriptStruct_FRandomLocationData, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "Comment", "// If true, the location will be picked along the object's axes instead of the world axes\n" },
		{ "EditCondition", "bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "If true, the location will be picked along the object's axes instead of the world axes" },
	};
#endif
	void Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit(void* Obj)
	{
		((URandomMovementComponent*)Obj)->bUseObjectAxesInsteadOfWorldAxes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes = { "bUseObjectAxesInsteadOfWorldAxes", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementComponent), &Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationVolume_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "Comment", "// The volume to choose a random location from\n" },
		{ "EditCondition", "!bRelatedToOriginLocation" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "The volume to choose a random location from" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationVolume = { "RandomLocationVolume", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementComponent, RandomLocationVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationVolume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationVolume_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "Comment", "// If true, the actor will instantly teleport whenever location changed\n// otherwise the object will move toward the new location with speed in the RandomSpeedRange\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "If true, the actor will instantly teleport whenever location changed\notherwise the object will move toward the new location with speed in the RandomSpeedRange" },
	};
#endif
	void Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport_SetBit(void* Obj)
	{
		((URandomMovementComponent*)Obj)->bShouldTeleport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport = { "bShouldTeleport", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementComponent), &Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "Comment", "// If true, the actor will not be able to move if its way is blocked\n// Otherwise the actor can go through other object or teleport overlap with the other\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
		{ "ToolTip", "If true, the actor will not be able to move if its way is blocked\nOtherwise the actor can go through other object or teleport overlap with the other" },
	};
#endif
	void Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision_SetBit(void* Obj)
	{
		((URandomMovementComponent*)Obj)->bCheckCollision = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision = { "bCheckCollision", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMovementComponent), &Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomSpeedRange_MetaData[] = {
		{ "Category", "RandomMovementComponent" },
		{ "EditCondition", "!bShouldTeleport" },
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomSpeedRange = { "RandomSpeedRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementComponent, RandomSpeedRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomSpeedRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomSpeedRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_OriginalTransform_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_OriginalTransform = { "OriginalTransform", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMovementComponent, OriginalTransform), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_OriginalTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_OriginalTransform_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bRelatedToOriginLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bUseObjectAxesInsteadOfWorldAxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomLocationVolume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bShouldTeleport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_bCheckCollision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_RandomSpeedRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMovementComponent_Statics::NewProp_OriginalTransform,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMovementComponent_Statics::ClassParams = {
		&URandomMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMovementComponent, 468721032);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMovementComponent>()
	{
		return URandomMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMovementComponent(Z_Construct_UClass_URandomMovementComponent, &URandomMovementComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
