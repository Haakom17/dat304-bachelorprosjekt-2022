// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_OrbitalMovementComponent_generated_h
#error "OrbitalMovementComponent.generated.h already included, missing '#pragma once' in OrbitalMovementComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_OrbitalMovementComponent_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOrbitalMovementComponent(); \
	friend struct Z_Construct_UClass_UOrbitalMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UOrbitalMovementComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(UOrbitalMovementComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUOrbitalMovementComponent(); \
	friend struct Z_Construct_UClass_UOrbitalMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UOrbitalMovementComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(UOrbitalMovementComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOrbitalMovementComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOrbitalMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOrbitalMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOrbitalMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOrbitalMovementComponent(UOrbitalMovementComponent&&); \
	NO_API UOrbitalMovementComponent(const UOrbitalMovementComponent&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOrbitalMovementComponent(UOrbitalMovementComponent&&); \
	NO_API UOrbitalMovementComponent(const UOrbitalMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOrbitalMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOrbitalMovementComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOrbitalMovementComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bShouldMove() { return STRUCT_OFFSET(UOrbitalMovementComponent, bShouldMove); } \
	FORCEINLINE static uint32 __PPO__bTeleportRandomly() { return STRUCT_OFFSET(UOrbitalMovementComponent, bTeleportRandomly); } \
	FORCEINLINE static uint32 __PPO__bCheckCollision() { return STRUCT_OFFSET(UOrbitalMovementComponent, bCheckCollision); } \
	FORCEINLINE static uint32 __PPO__FocalTargetActor() { return STRUCT_OFFSET(UOrbitalMovementComponent, FocalTargetActor); } \
	FORCEINLINE static uint32 __PPO__RotationSpeed() { return STRUCT_OFFSET(UOrbitalMovementComponent, RotationSpeed); } \
	FORCEINLINE static uint32 __PPO__PitchRotationSpeed() { return STRUCT_OFFSET(UOrbitalMovementComponent, PitchRotationSpeed); } \
	FORCEINLINE static uint32 __PPO__YawRotationRange() { return STRUCT_OFFSET(UOrbitalMovementComponent, YawRotationRange); } \
	FORCEINLINE static uint32 __PPO__PitchRotationRange() { return STRUCT_OFFSET(UOrbitalMovementComponent, PitchRotationRange); } \
	FORCEINLINE static uint32 __PPO__bRevertYawDirectionAfterEachRotation() { return STRUCT_OFFSET(UOrbitalMovementComponent, bRevertYawDirectionAfterEachRotation); } \
	FORCEINLINE static uint32 __PPO__bRandomizePitchAfterEachYawRotation() { return STRUCT_OFFSET(UOrbitalMovementComponent, bRandomizePitchAfterEachYawRotation); } \
	FORCEINLINE static uint32 __PPO__bShouldChangeDistance() { return STRUCT_OFFSET(UOrbitalMovementComponent, bShouldChangeDistance); } \
	FORCEINLINE static uint32 __PPO__TargetDistanceRange() { return STRUCT_OFFSET(UOrbitalMovementComponent, TargetDistanceRange); } \
	FORCEINLINE static uint32 __PPO__DistanceChangeSpeed() { return STRUCT_OFFSET(UOrbitalMovementComponent, DistanceChangeSpeed); } \
	FORCEINLINE static uint32 __PPO__TargetDistanceChangeDuration() { return STRUCT_OFFSET(UOrbitalMovementComponent, TargetDistanceChangeDuration); } \
	FORCEINLINE static uint32 __PPO__bOnlyChangeDistanceWhenPitchChanged() { return STRUCT_OFFSET(UOrbitalMovementComponent, bOnlyChangeDistanceWhenPitchChanged); } \
	FORCEINLINE static uint32 __PPO__bShouldWiggle() { return STRUCT_OFFSET(UOrbitalMovementComponent, bShouldWiggle); } \
	FORCEINLINE static uint32 __PPO__WiggleRotationData() { return STRUCT_OFFSET(UOrbitalMovementComponent, WiggleRotationData); } \
	FORCEINLINE static uint32 __PPO__RotationFromTarget() { return STRUCT_OFFSET(UOrbitalMovementComponent, RotationFromTarget); } \
	FORCEINLINE static uint32 __PPO__CurrentDistanceToTarget() { return STRUCT_OFFSET(UOrbitalMovementComponent, CurrentDistanceToTarget); } \
	FORCEINLINE static uint32 __PPO__DistanceToTarget() { return STRUCT_OFFSET(UOrbitalMovementComponent, DistanceToTarget); } \
	FORCEINLINE static uint32 __PPO__DistanceChangeCountdown() { return STRUCT_OFFSET(UOrbitalMovementComponent, DistanceChangeCountdown); } \
	FORCEINLINE static uint32 __PPO__TargetYaw() { return STRUCT_OFFSET(UOrbitalMovementComponent, TargetYaw); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_17_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class UOrbitalMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_OrbitalMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
