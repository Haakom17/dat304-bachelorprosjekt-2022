// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SpatialLayoutGenerator/SpatialLayoutGenerator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpatialLayoutGenerator() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
// End Cross Module References
	void USpatialLayoutGenerator::StaticRegisterNativesUSpatialLayoutGenerator()
	{
	}
	UClass* Z_Construct_UClass_USpatialLayoutGenerator_NoRegister()
	{
		return USpatialLayoutGenerator::StaticClass();
	}
	struct Z_Construct_UClass_USpatialLayoutGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpatialLayoutGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SpatialLayoutGenerator/SpatialLayoutGenerator.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpatialLayoutGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpatialLayoutGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpatialLayoutGenerator_Statics::ClassParams = {
		&USpatialLayoutGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpatialLayoutGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpatialLayoutGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpatialLayoutGenerator, 1356320007);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<USpatialLayoutGenerator>()
	{
		return USpatialLayoutGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpatialLayoutGenerator(Z_Construct_UClass_USpatialLayoutGenerator, &USpatialLayoutGenerator::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("USpatialLayoutGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpatialLayoutGenerator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
