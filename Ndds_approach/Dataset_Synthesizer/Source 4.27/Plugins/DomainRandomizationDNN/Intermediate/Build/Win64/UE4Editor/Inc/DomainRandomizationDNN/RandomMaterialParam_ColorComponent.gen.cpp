// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMaterialParam_ColorComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMaterialParam_ColorComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_ColorComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_ColorComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomColorData();
// End Cross Module References
	void URandomMaterialParam_ColorComponent::StaticRegisterNativesURandomMaterialParam_ColorComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMaterialParam_ColorComponent_NoRegister()
	{
		return URandomMaterialParam_ColorComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ColorParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomMaterialParameterComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n* RandomTextureComponent randomly change the color parameter of the materials in the owner's mesh\n*//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMaterialParam_ColorComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_ColorComponent.h" },
		{ "ToolTip", "RandomTextureComponent randomly change the color parameter of the materials in the owner's mesh\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorParameterName_MetaData[] = {
		{ "Comment", "// Editor properties\n// The name of the parameter which control the color change on the owner's material\n// DEPRECATED_FORGAME(4.16, \"Use MaterialParameterNames instead\")\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_ColorComponent.h" },
		{ "ToolTip", "Editor properties\nThe name of the parameter which control the color change on the owner's material\nDEPRECATED_FORGAME(4.16, \"Use MaterialParameterNames instead\")" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorParameterName = { "ColorParameterName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_ColorComponent, ColorParameterName), METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_ColorComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorData = { "ColorData", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_ColorComponent, ColorData), Z_Construct_UScriptStruct_FRandomColorData, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::NewProp_ColorData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMaterialParam_ColorComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::ClassParams = {
		&URandomMaterialParam_ColorComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMaterialParam_ColorComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMaterialParam_ColorComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMaterialParam_ColorComponent, 4013805410);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMaterialParam_ColorComponent>()
	{
		return URandomMaterialParam_ColorComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMaterialParam_ColorComponent(Z_Construct_UClass_URandomMaterialParam_ColorComponent, &URandomMaterialParam_ColorComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMaterialParam_ColorComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMaterialParam_ColorComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
