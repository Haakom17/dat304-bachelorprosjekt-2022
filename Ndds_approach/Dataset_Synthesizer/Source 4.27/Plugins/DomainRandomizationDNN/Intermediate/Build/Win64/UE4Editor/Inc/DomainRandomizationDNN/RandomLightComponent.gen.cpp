// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomLightComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomLightComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLightComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLightComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFloatInterval();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomColorData();
// End Cross Module References
	void URandomLightComponent::StaticRegisterNativesURandomLightComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomLightComponent_NoRegister()
	{
		return URandomLightComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomLightComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldModifyIntensity_MetaData[];
#endif
		static void NewProp_bShouldModifyIntensity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldModifyIntensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntensityRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IntensityRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldModifyColor_MetaData[];
#endif
		static void NewProp_bShouldModifyColor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldModifyColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomLightComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n* Randomize the properties of the owner actor's light components\n* NOTE: This is not a LightComponent, it modify other light components\n*//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomLightComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent.h" },
		{ "ToolTip", "Randomize the properties of the owner actor's light components\nNOTE: This is not a LightComponent, it modify other light components\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity_SetBit(void* Obj)
	{
		((URandomLightComponent*)Obj)->bShouldModifyIntensity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity = { "bShouldModifyIntensity", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomLightComponent), &Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_Statics::NewProp_IntensityRange_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bShouldModifyIntensity" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomLightComponent_Statics::NewProp_IntensityRange = { "IntensityRange", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLightComponent, IntensityRange), Z_Construct_UScriptStruct_FFloatInterval, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_IntensityRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_IntensityRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor_MetaData[] = {
		{ "Category", "Randomization" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent.h" },
	};
#endif
	void Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor_SetBit(void* Obj)
	{
		((URandomLightComponent*)Obj)->bShouldModifyColor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor = { "bShouldModifyColor", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomLightComponent), &Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLightComponent_Statics::NewProp_ColorData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditCondition", "bShouldModifyColor" },
		{ "ModuleRelativePath", "Public/Components/RandomLightComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomLightComponent_Statics::NewProp_ColorData = { "ColorData", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLightComponent, ColorData), Z_Construct_UScriptStruct_FRandomColorData, METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_ColorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::NewProp_ColorData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomLightComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyIntensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_Statics::NewProp_IntensityRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_Statics::NewProp_bShouldModifyColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLightComponent_Statics::NewProp_ColorData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomLightComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomLightComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomLightComponent_Statics::ClassParams = {
		&URandomLightComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomLightComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomLightComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLightComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomLightComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomLightComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomLightComponent, 3573028113);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomLightComponent>()
	{
		return URandomLightComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomLightComponent(Z_Construct_UClass_URandomLightComponent, &URandomLightComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomLightComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomLightComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
