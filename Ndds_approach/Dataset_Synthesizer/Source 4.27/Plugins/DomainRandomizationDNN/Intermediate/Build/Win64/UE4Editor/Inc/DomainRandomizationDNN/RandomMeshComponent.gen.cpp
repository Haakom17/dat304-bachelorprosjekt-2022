// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMeshComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMeshComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMeshComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomAssetStreamer();
// End Cross Module References
	void URandomMeshComponent::StaticRegisterNativesURandomMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMeshComponent_NoRegister()
	{
		return URandomMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseAllMeshInDirectories_MetaData[];
#endif
		static void NewProp_bUseAllMeshInDirectories_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAllMeshInDirectories;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MeshDirectories;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StaticMeshList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshStreamer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MeshStreamer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMeshComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMeshComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMeshComponent.h" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	void Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories_SetBit(void* Obj)
	{
		((URandomMeshComponent*)Obj)->bUseAllMeshInDirectories = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories = { "bUseAllMeshInDirectories", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMeshComponent), &Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories_Inner = { "MeshDirectories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of directories where we want to use the static meshes from\n" },
		{ "EditCondition", "bUseAllMeshInDirectories" },
		{ "ModuleRelativePath", "Public/Components/RandomMeshComponent.h" },
		{ "RelativeToGameContentDir", "" },
		{ "ToolTip", "List of directories where we want to use the static meshes from" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories = { "MeshDirectories", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMeshComponent, MeshDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList_Inner = { "StaticMeshList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of the StaticMesh that the actor will change through\n" },
		{ "EditCondition", "!bUseAllMeshInDirectories" },
		{ "ModuleRelativePath", "Public/Components/RandomMeshComponent.h" },
		{ "ToolTip", "List of the StaticMesh that the actor will change through" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList = { "StaticMeshList", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMeshComponent, StaticMeshList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshStreamer_MetaData[] = {
		{ "Comment", "// Transient properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMeshComponent.h" },
		{ "ToolTip", "Transient properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshStreamer = { "MeshStreamer", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMeshComponent, MeshStreamer), Z_Construct_UScriptStruct_FRandomAssetStreamer, METADATA_PARAMS(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshStreamer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshStreamer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMeshComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_bUseAllMeshInDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_StaticMeshList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMeshComponent_Statics::NewProp_MeshStreamer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMeshComponent_Statics::ClassParams = {
		&URandomMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMeshComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMeshComponent, 1596164902);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMeshComponent>()
	{
		return URandomMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMeshComponent(Z_Construct_UClass_URandomMeshComponent, &URandomMeshComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
