// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/RandomizedActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomizedActor() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ARandomizedActor_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_ARandomizedActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMeshComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_ColorComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomVisibilityComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMovementComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomRotationComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomDataObject_NoRegister();
// End Cross Module References
	void ARandomizedActor::StaticRegisterNativesARandomizedActor()
	{
	}
	UClass* Z_Construct_UClass_ARandomizedActor_NoRegister()
	{
		return ARandomizedActor::StaticClass();
	}
	struct Z_Construct_UClass_ARandomizedActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomMeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomMeshComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomMaterialComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomMaterialComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomMaterialParam_ColorComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomMaterialParam_ColorComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomVisibilityComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomVisibilityComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomMovementComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomMovementComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomRotationComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomRotationComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomDataObjects_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RandomDataObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomDataObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RandomDataObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARandomizedActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "IncludePath", "RandomizedActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_StaticMeshComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_StaticMeshComp = { "StaticMeshComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, StaticMeshComp), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_StaticMeshComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_StaticMeshComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMeshComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMeshComp = { "RandomMeshComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomMeshComp), Z_Construct_UClass_URandomMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMeshComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMeshComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialComp = { "RandomMaterialComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomMaterialComp), Z_Construct_UClass_URandomMaterialComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialParam_ColorComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialParam_ColorComp = { "RandomMaterialParam_ColorComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomMaterialParam_ColorComp), Z_Construct_UClass_URandomMaterialParam_ColorComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialParam_ColorComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialParam_ColorComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomVisibilityComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomVisibilityComp = { "RandomVisibilityComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomVisibilityComp), Z_Construct_UClass_URandomVisibilityComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomVisibilityComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomVisibilityComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMovementComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMovementComp = { "RandomMovementComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomMovementComp), Z_Construct_UClass_URandomMovementComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMovementComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMovementComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomRotationComp_MetaData[] = {
		{ "Category", "RandomizedActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomRotationComp = { "RandomRotationComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomRotationComp), Z_Construct_UClass_URandomRotationComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomRotationComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomRotationComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_Inner_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_Inner = { "RandomDataObjects", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_URandomDataObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_MetaData[] = {
		{ "Category", "Randomization" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/RandomizedActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects = { "RandomDataObjects", nullptr, (EPropertyFlags)0x0020088000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ARandomizedActor, RandomDataObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARandomizedActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_StaticMeshComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMeshComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMaterialParam_ColorComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomVisibilityComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomMovementComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomRotationComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARandomizedActor_Statics::NewProp_RandomDataObjects,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARandomizedActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARandomizedActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARandomizedActor_Statics::ClassParams = {
		&ARandomizedActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ARandomizedActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ARandomizedActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ARandomizedActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARandomizedActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARandomizedActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARandomizedActor, 3910635928);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<ARandomizedActor>()
	{
		return ARandomizedActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARandomizedActor(Z_Construct_UClass_ARandomizedActor, &ARandomizedActor::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("ARandomizedActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARandomizedActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
