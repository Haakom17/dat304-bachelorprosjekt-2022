// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_DRSceneManager_generated_h
#error "DRSceneManager.generated.h already included, missing '#pragma once' in DRSceneManager.h"
#endif
#define DOMAINRANDOMIZATIONDNN_DRSceneManager_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADRSceneManager(); \
	friend struct Z_Construct_UClass_ADRSceneManager_Statics; \
public: \
	DECLARE_CLASS(ADRSceneManager, ANVSceneManager, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ADRSceneManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_INCLASS \
private: \
	static void StaticRegisterNativesADRSceneManager(); \
	friend struct Z_Construct_UClass_ADRSceneManager_Statics; \
public: \
	DECLARE_CLASS(ADRSceneManager, ANVSceneManager, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ADRSceneManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADRSceneManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADRSceneManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADRSceneManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADRSceneManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADRSceneManager(ADRSceneManager&&); \
	NO_API ADRSceneManager(const ADRSceneManager&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADRSceneManager(ADRSceneManager&&); \
	NO_API ADRSceneManager(const ADRSceneManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADRSceneManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADRSceneManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADRSceneManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GroupActorManager() { return STRUCT_OFFSET(ADRSceneManager, GroupActorManager); } \
	FORCEINLINE static uint32 __PPO__NoiseActorManager() { return STRUCT_OFFSET(ADRSceneManager, NoiseActorManager); } \
	FORCEINLINE static uint32 __PPO__bIsReady() { return STRUCT_OFFSET(ADRSceneManager, bIsReady); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_18_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ADRSceneManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_DRSceneManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
