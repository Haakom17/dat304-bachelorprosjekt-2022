// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_SpatialLayoutGenerator_Grid2d_generated_h
#error "SpatialLayoutGenerator_Grid2d.generated.h already included, missing '#pragma once' in SpatialLayoutGenerator_Grid2d.h"
#endif
#define DOMAINRANDOMIZATIONDNN_SpatialLayoutGenerator_Grid2d_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpatialLayoutGenerator_Grid2d(); \
	friend struct Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics; \
public: \
	DECLARE_CLASS(USpatialLayoutGenerator_Grid2d, USpatialLayoutGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(USpatialLayoutGenerator_Grid2d)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSpatialLayoutGenerator_Grid2d(); \
	friend struct Z_Construct_UClass_USpatialLayoutGenerator_Grid2d_Statics; \
public: \
	DECLARE_CLASS(USpatialLayoutGenerator_Grid2d, USpatialLayoutGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(USpatialLayoutGenerator_Grid2d)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpatialLayoutGenerator_Grid2d(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpatialLayoutGenerator_Grid2d) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialLayoutGenerator_Grid2d); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialLayoutGenerator_Grid2d); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialLayoutGenerator_Grid2d(USpatialLayoutGenerator_Grid2d&&); \
	NO_API USpatialLayoutGenerator_Grid2d(const USpatialLayoutGenerator_Grid2d&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialLayoutGenerator_Grid2d(USpatialLayoutGenerator_Grid2d&&); \
	NO_API USpatialLayoutGenerator_Grid2d(const USpatialLayoutGenerator_Grid2d&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialLayoutGenerator_Grid2d); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialLayoutGenerator_Grid2d); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpatialLayoutGenerator_Grid2d)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GridRowCount() { return STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridRowCount); } \
	FORCEINLINE static uint32 __PPO__GridColumnCount() { return STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridColumnCount); } \
	FORCEINLINE static uint32 __PPO__GridCellSize() { return STRUCT_OFFSET(USpatialLayoutGenerator_Grid2d, GridCellSize); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_13_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class USpatialLayoutGenerator_Grid2d>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_Grid2d_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
