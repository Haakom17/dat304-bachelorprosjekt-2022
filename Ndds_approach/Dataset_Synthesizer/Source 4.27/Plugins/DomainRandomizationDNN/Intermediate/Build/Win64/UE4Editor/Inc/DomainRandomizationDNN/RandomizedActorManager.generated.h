// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomizedActorManager_generated_h
#error "RandomizedActorManager.generated.h already included, missing '#pragma once' in RandomizedActorManager.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomizedActorManager_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARandomizedActorManager(); \
	friend struct Z_Construct_UClass_ARandomizedActorManager_Statics; \
public: \
	DECLARE_CLASS(ARandomizedActorManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ARandomizedActorManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_INCLASS \
private: \
	static void StaticRegisterNativesARandomizedActorManager(); \
	friend struct Z_Construct_UClass_ARandomizedActorManager_Statics; \
public: \
	DECLARE_CLASS(ARandomizedActorManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(ARandomizedActorManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARandomizedActorManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARandomizedActorManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomizedActorManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomizedActorManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomizedActorManager(ARandomizedActorManager&&); \
	NO_API ARandomizedActorManager(const ARandomizedActorManager&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARandomizedActorManager(ARandomizedActorManager&&); \
	NO_API ARandomizedActorManager(const ARandomizedActorManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARandomizedActorManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARandomizedActorManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARandomizedActorManager)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActorClassesToSpawn() { return STRUCT_OFFSET(ARandomizedActorManager, ActorClassesToSpawn); } \
	FORCEINLINE static uint32 __PPO__NumberOfActorsToSpawn() { return STRUCT_OFFSET(ARandomizedActorManager, NumberOfActorsToSpawn); } \
	FORCEINLINE static uint32 __PPO__RandomLocationVolume() { return STRUCT_OFFSET(ARandomizedActorManager, RandomLocationVolume); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_13_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class ARandomizedActorManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomizedActorManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
