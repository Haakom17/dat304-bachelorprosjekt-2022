// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMeshComponent_generated_h
#error "RandomMeshComponent.generated.h already included, missing '#pragma once' in RandomMeshComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMeshComponent_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMeshComponent(); \
	friend struct Z_Construct_UClass_URandomMeshComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMeshComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMeshComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesURandomMeshComponent(); \
	friend struct Z_Construct_UClass_URandomMeshComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMeshComponent, URandomComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMeshComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMeshComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMeshComponent(URandomMeshComponent&&); \
	NO_API URandomMeshComponent(const URandomMeshComponent&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMeshComponent(URandomMeshComponent&&); \
	NO_API URandomMeshComponent(const URandomMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMeshComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMeshComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bUseAllMeshInDirectories() { return STRUCT_OFFSET(URandomMeshComponent, bUseAllMeshInDirectories); } \
	FORCEINLINE static uint32 __PPO__MeshDirectories() { return STRUCT_OFFSET(URandomMeshComponent, MeshDirectories); } \
	FORCEINLINE static uint32 __PPO__StaticMeshList() { return STRUCT_OFFSET(URandomMeshComponent, StaticMeshList); } \
	FORCEINLINE static uint32 __PPO__MeshStreamer() { return STRUCT_OFFSET(URandomMeshComponent, MeshStreamer); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_15_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
