// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Circle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpatialLayoutGenerator_Circle() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_Circle_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator_Circle();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_USpatialLayoutGenerator();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
// End Cross Module References
	void USpatialLayoutGenerator_Circle::StaticRegisterNativesUSpatialLayoutGenerator_Circle()
	{
	}
	UClass* Z_Construct_UClass_USpatialLayoutGenerator_Circle_NoRegister()
	{
		return USpatialLayoutGenerator_Circle::StaticClass();
	}
	struct Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CircleRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CircleRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bActorFacingCenter_MetaData[];
#endif
		static void NewProp_bActorFacingCenter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActorFacingCenter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USpatialLayoutGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SpatialLayoutGenerator/SpatialLayoutGenerator_Circle.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Circle.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_CircleRadius_MetaData[] = {
		{ "Category", "CircleLayout" },
		{ "Comment", "// Editor properties\n// Radius of the circle\n" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Circle.h" },
		{ "ToolTip", "Editor properties\nRadius of the circle" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_CircleRadius = { "CircleRadius", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USpatialLayoutGenerator_Circle, CircleRadius), METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_CircleRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_CircleRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter_MetaData[] = {
		{ "Category", "CircleLayout" },
		{ "Comment", "// If true, all actors should face the center of the circle\n" },
		{ "ModuleRelativePath", "Public/SpatialLayoutGenerator/SpatialLayoutGenerator_Circle.h" },
		{ "ToolTip", "If true, all actors should face the center of the circle" },
		{ "UIMin", "1" },
	};
#endif
	void Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter_SetBit(void* Obj)
	{
		((USpatialLayoutGenerator_Circle*)Obj)->bActorFacingCenter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter = { "bActorFacingCenter", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USpatialLayoutGenerator_Circle), &Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter_SetBit, METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_CircleRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::NewProp_bActorFacingCenter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USpatialLayoutGenerator_Circle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::ClassParams = {
		&USpatialLayoutGenerator_Circle::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::PropPointers),
		0,
		0x003010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USpatialLayoutGenerator_Circle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USpatialLayoutGenerator_Circle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USpatialLayoutGenerator_Circle, 3281938556);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<USpatialLayoutGenerator_Circle>()
	{
		return USpatialLayoutGenerator_Circle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USpatialLayoutGenerator_Circle(Z_Construct_UClass_USpatialLayoutGenerator_Circle, &USpatialLayoutGenerator_Circle::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("USpatialLayoutGenerator_Circle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USpatialLayoutGenerator_Circle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
