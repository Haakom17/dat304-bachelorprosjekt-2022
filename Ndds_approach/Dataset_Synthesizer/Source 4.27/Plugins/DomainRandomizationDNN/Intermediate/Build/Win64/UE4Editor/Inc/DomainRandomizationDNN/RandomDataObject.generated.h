// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomDataObject_generated_h
#error "RandomDataObject.generated.h already included, missing '#pragma once' in RandomDataObject.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomDataObject_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomDataObject(); \
	friend struct Z_Construct_UClass_URandomDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomDataObject, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_INCLASS \
private: \
	static void StaticRegisterNativesURandomDataObject(); \
	friend struct Z_Construct_UClass_URandomDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomDataObject, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomDataObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomDataObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomDataObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomDataObject(URandomDataObject&&); \
	NO_API URandomDataObject(const URandomDataObject&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomDataObject(URandomDataObject&&); \
	NO_API URandomDataObject(const URandomDataObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomDataObject); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(URandomDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bShouldRandomize() { return STRUCT_OFFSET(URandomDataObject, bShouldRandomize); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_13_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomDataObject>();

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMovementDataObject(); \
	friend struct Z_Construct_UClass_URandomMovementDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomMovementDataObject, URandomDataObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMovementDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_INCLASS \
private: \
	static void StaticRegisterNativesURandomMovementDataObject(); \
	friend struct Z_Construct_UClass_URandomMovementDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomMovementDataObject, URandomDataObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMovementDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMovementDataObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMovementDataObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMovementDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMovementDataObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMovementDataObject(URandomMovementDataObject&&); \
	NO_API URandomMovementDataObject(const URandomMovementDataObject&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMovementDataObject(URandomMovementDataObject&&); \
	NO_API URandomMovementDataObject(const URandomMovementDataObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMovementDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMovementDataObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMovementDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bRelatedToOriginLocation() { return STRUCT_OFFSET(URandomMovementDataObject, bRelatedToOriginLocation); } \
	FORCEINLINE static uint32 __PPO__RandomLocationData() { return STRUCT_OFFSET(URandomMovementDataObject, RandomLocationData); } \
	FORCEINLINE static uint32 __PPO__bUseObjectAxesInsteadOfWorldAxes() { return STRUCT_OFFSET(URandomMovementDataObject, bUseObjectAxesInsteadOfWorldAxes); } \
	FORCEINLINE static uint32 __PPO__RandomLocationVolume() { return STRUCT_OFFSET(URandomMovementDataObject, RandomLocationVolume); } \
	FORCEINLINE static uint32 __PPO__bShouldTeleport() { return STRUCT_OFFSET(URandomMovementDataObject, bShouldTeleport); } \
	FORCEINLINE static uint32 __PPO__RandomSpeedRange() { return STRUCT_OFFSET(URandomMovementDataObject, RandomSpeedRange); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_29_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMovementDataObject>();

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomRotationDataObject(); \
	friend struct Z_Construct_UClass_URandomRotationDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomRotationDataObject, URandomDataObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomRotationDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_INCLASS \
private: \
	static void StaticRegisterNativesURandomRotationDataObject(); \
	friend struct Z_Construct_UClass_URandomRotationDataObject_Statics; \
public: \
	DECLARE_CLASS(URandomRotationDataObject, URandomDataObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomRotationDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomRotationDataObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomRotationDataObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomRotationDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomRotationDataObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomRotationDataObject(URandomRotationDataObject&&); \
	NO_API URandomRotationDataObject(const URandomRotationDataObject&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomRotationDataObject(URandomRotationDataObject&&); \
	NO_API URandomRotationDataObject(const URandomRotationDataObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomRotationDataObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomRotationDataObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomRotationDataObject)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RandomRotationData() { return STRUCT_OFFSET(URandomRotationDataObject, RandomRotationData); } \
	FORCEINLINE static uint32 __PPO__bRelatedToOriginRotation() { return STRUCT_OFFSET(URandomRotationDataObject, bRelatedToOriginRotation); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_65_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h_68_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomRotationDataObject>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_RandomDataObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
