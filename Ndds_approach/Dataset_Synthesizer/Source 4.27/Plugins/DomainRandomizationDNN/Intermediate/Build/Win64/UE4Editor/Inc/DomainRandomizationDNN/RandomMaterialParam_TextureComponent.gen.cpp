// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMaterialParam_TextureComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMaterialParam_TextureComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_TextureComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParam_TextureComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialParameterComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomAssetStreamer();
// End Cross Module References
	void URandomMaterialParam_TextureComponent::StaticRegisterNativesURandomMaterialParam_TextureComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMaterialParam_TextureComponent_NoRegister()
	{
		return URandomMaterialParam_TextureComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureParameterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TextureParameterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseAllTextureInAFolder_MetaData[];
#endif
		static void NewProp_bUseAllTextureInAFolder_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAllTextureInAFolder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureDirectory;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TextureDirectories;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextureList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TextureList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureStreamer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureStreamer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomMaterialParameterComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMaterialParam_TextureComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureParameterName_MetaData[] = {
		{ "Comment", "// Editor properties\n// DEPRECATED_FORGAME(4.16, \"Use MaterialParameterNames instead\")\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
		{ "ToolTip", "Editor properties\nDEPRECATED_FORGAME(4.16, \"Use MaterialParameterNames instead\")" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureParameterName = { "TextureParameterName", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureParameterName), METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureParameterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureParameterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
	};
#endif
	void Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder_SetBit(void* Obj)
	{
		((URandomMaterialParam_TextureComponent*)Obj)->bUseAllTextureInAFolder = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder = { "bUseAllTextureInAFolder", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMaterialParam_TextureComponent), &Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectory_MetaData[] = {
		{ "Comment", "// Path to the directory where we want to use textures from\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
		{ "ToolTip", "Path to the directory where we want to use textures from" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectory = { "TextureDirectory", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureDirectory), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectory_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories_Inner = { "TextureDirectories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of directories where we want to use the static meshes from\n" },
		{ "EditCondition", "bUseAllTextureInAFolder" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
		{ "RelativeToGameContentDir", "" },
		{ "ToolTip", "List of directories where we want to use the static meshes from" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories = { "TextureDirectories", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList_Inner = { "TextureList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of the texture that the owner mesh's material will change through\n" },
		{ "EditCondition", "!bUseAllTextureInAFolder" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
		{ "ToolTip", "List of the texture that the owner mesh's material will change through" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList = { "TextureList", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureStreamer_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomMaterialParam_TextureComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureStreamer = { "TextureStreamer", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureStreamer), Z_Construct_UScriptStruct_FRandomAssetStreamer, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureStreamer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureStreamer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureParameterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_bUseAllTextureInAFolder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::NewProp_TextureStreamer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMaterialParam_TextureComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::ClassParams = {
		&URandomMaterialParam_TextureComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMaterialParam_TextureComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMaterialParam_TextureComponent, 292462750);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMaterialParam_TextureComponent>()
	{
		return URandomMaterialParam_TextureComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMaterialParam_TextureComponent(Z_Construct_UClass_URandomMaterialParam_TextureComponent, &URandomMaterialParam_TextureComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMaterialParam_TextureComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMaterialParam_TextureComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
