// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomLookAtComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomLookAtComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLookAtComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomLookAtComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void URandomLookAtComponent::StaticRegisterNativesURandomLookAtComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomLookAtComponent_NoRegister()
	{
		return URandomLookAtComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomLookAtComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FocalTargetActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalTargetActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocalTargetActors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentFocalTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentFocalTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomLookAtComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLookAtComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomLookAtComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomLookAtComponent.h" },
		{ "ToolTip", "@cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors_Inner = { "FocalTargetActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n// List of the focal actor to choose from\n" },
		{ "ModuleRelativePath", "Public/Components/RandomLookAtComponent.h" },
		{ "ToolTip", "Editor properties\nList of the focal actor to choose from" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors = { "FocalTargetActors", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLookAtComponent, FocalTargetActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_RotationSpeed_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// How fast (degree per seconds) the actor rotate when it need to change direction\n" },
		{ "ModuleRelativePath", "Public/Components/RandomLookAtComponent.h" },
		{ "ToolTip", "How fast (degree per seconds) the actor rotate when it need to change direction" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_RotationSpeed = { "RotationSpeed", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLookAtComponent, RotationSpeed), METADATA_PARAMS(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_RotationSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_RotationSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_CurrentFocalTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomLookAtComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_CurrentFocalTarget = { "CurrentFocalTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomLookAtComponent, CurrentFocalTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_CurrentFocalTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_CurrentFocalTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomLookAtComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_FocalTargetActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_RotationSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomLookAtComponent_Statics::NewProp_CurrentFocalTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomLookAtComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomLookAtComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomLookAtComponent_Statics::ClassParams = {
		&URandomLookAtComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomLookAtComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomLookAtComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomLookAtComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomLookAtComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomLookAtComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomLookAtComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomLookAtComponent, 1016435506);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomLookAtComponent>()
	{
		return URandomLookAtComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomLookAtComponent(Z_Construct_UClass_URandomLookAtComponent, &URandomLookAtComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomLookAtComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomLookAtComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
