// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_RandomMaterialParam_TextureComponent_generated_h
#error "RandomMaterialParam_TextureComponent.generated.h already included, missing '#pragma once' in RandomMaterialParam_TextureComponent.h"
#endif
#define DOMAINRANDOMIZATIONDNN_RandomMaterialParam_TextureComponent_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURandomMaterialParam_TextureComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParam_TextureComponent, URandomMaterialParameterComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParam_TextureComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_INCLASS \
private: \
	static void StaticRegisterNativesURandomMaterialParam_TextureComponent(); \
	friend struct Z_Construct_UClass_URandomMaterialParam_TextureComponent_Statics; \
public: \
	DECLARE_CLASS(URandomMaterialParam_TextureComponent, URandomMaterialParameterComponentBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(URandomMaterialParam_TextureComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URandomMaterialParam_TextureComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URandomMaterialParam_TextureComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParam_TextureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParam_TextureComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParam_TextureComponent(URandomMaterialParam_TextureComponent&&); \
	NO_API URandomMaterialParam_TextureComponent(const URandomMaterialParam_TextureComponent&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URandomMaterialParam_TextureComponent(URandomMaterialParam_TextureComponent&&); \
	NO_API URandomMaterialParam_TextureComponent(const URandomMaterialParam_TextureComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URandomMaterialParam_TextureComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URandomMaterialParam_TextureComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URandomMaterialParam_TextureComponent)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TextureParameterName() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureParameterName); } \
	FORCEINLINE static uint32 __PPO__bUseAllTextureInAFolder() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, bUseAllTextureInAFolder); } \
	FORCEINLINE static uint32 __PPO__TextureDirectory() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureDirectory); } \
	FORCEINLINE static uint32 __PPO__TextureDirectories() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureDirectories); } \
	FORCEINLINE static uint32 __PPO__TextureList() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureList); } \
	FORCEINLINE static uint32 __PPO__TextureStreamer() { return STRUCT_OFFSET(URandomMaterialParam_TextureComponent, TextureStreamer); }


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_15_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class URandomMaterialParam_TextureComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_Components_RandomMaterialParam_TextureComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
