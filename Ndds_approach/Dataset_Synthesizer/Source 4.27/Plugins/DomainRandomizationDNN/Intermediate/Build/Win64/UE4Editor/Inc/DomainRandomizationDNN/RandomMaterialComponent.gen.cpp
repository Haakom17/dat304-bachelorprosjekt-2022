// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DomainRandomizationDNN/Public/Components/RandomMaterialComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRandomMaterialComponent() {}
// Cross Module References
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomMaterialComponent();
	DOMAINRANDOMIZATIONDNN_API UClass* Z_Construct_UClass_URandomComponentBase();
	UPackage* Z_Construct_UPackage__Script_DomainRandomizationDNN();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomMaterialSelection();
	DOMAINRANDOMIZATIONDNN_API UEnum* Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDecalComponent_NoRegister();
	DOMAINRANDOMIZATIONDNN_API UScriptStruct* Z_Construct_UScriptStruct_FRandomAssetStreamer();
// End Cross Module References
	void URandomMaterialComponent::StaticRegisterNativesURandomMaterialComponent()
	{
	}
	UClass* Z_Construct_UClass_URandomMaterialComponent_NoRegister()
	{
		return URandomMaterialComponent::StaticClass();
	}
	struct Z_Construct_UClass_URandomMaterialComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSelectionConfigData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialSelectionConfigData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseAllMaterialInDirectories_MetaData[];
#endif
		static void NewProp_bUseAllMaterialInDirectories_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseAllMaterialInDirectories;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AffectedComponentType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AffectedComponentType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AffectedComponentType;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MaterialDirectories;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MaterialList;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerMeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerMeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerMeshComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerDecalComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerDecalComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OwnerDecalComponents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialStreamer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialStreamer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URandomMaterialComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_URandomComponentBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DomainRandomizationDNN,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "NVIDIA" },
		{ "Comment", "/**\n* RandomMaterialComponent randomly change the materials of the owner's mesh in specified material slot\n*//// @cond DOXYGEN_SUPPRESSED_CODE\n" },
		{ "HideCategories", "Replication ComponentReplication Cooking Events ComponentTick Actor Input Rendering Collision PhysX Activation Sockets Tags" },
		{ "IncludePath", "Components/RandomMaterialComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
		{ "ToolTip", "RandomMaterialComponent randomly change the materials of the owner's mesh in specified material slot\n/// @cond DOXYGEN_SUPPRESSED_CODE" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialSelectionConfigData_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// Editor properties\n" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Editor properties" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialSelectionConfigData = { "MaterialSelectionConfigData", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, MaterialSelectionConfigData), Z_Construct_UScriptStruct_FRandomMaterialSelection, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialSelectionConfigData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialSelectionConfigData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
	};
#endif
	void Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories_SetBit(void* Obj)
	{
		((URandomMaterialComponent*)Obj)->bUseAllMaterialInDirectories = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories = { "bUseAllMaterialInDirectories", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URandomMaterialComponent), &Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories_SetBit, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType_MetaData[] = {
		{ "Category", "Randomization" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType = { "AffectedComponentType", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, AffectedComponentType), Z_Construct_UEnum_DomainRandomizationDNN_EAffectedMaterialOwnerComponentType, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories_Inner = { "MaterialDirectories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of directories where we want to use the static meshes from\n" },
		{ "EditCondition", "bUseAllMaterialInDirectories" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
		{ "RelativeToGameContentDir", "" },
		{ "ToolTip", "List of directories where we want to use the static meshes from" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories = { "MaterialDirectories", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, MaterialDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList_Inner = { "MaterialList", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList_MetaData[] = {
		{ "Category", "Randomization" },
		{ "Comment", "// List of the material that the owner actor will switch through\n" },
		{ "EditCondition", "!bUseAllMaterialInDirectories" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
		{ "ToolTip", "List of the material that the owner actor will switch through" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList = { "MaterialList", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, MaterialList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents_Inner = { "OwnerMeshComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents_MetaData[] = {
		{ "Comment", "// List of the mesh components from the owner actor that we need to change their materials\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
		{ "ToolTip", "List of the mesh components from the owner actor that we need to change their materials" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents = { "OwnerMeshComponents", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, OwnerMeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents_Inner = { "OwnerDecalComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDecalComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents = { "OwnerDecalComponents", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, OwnerDecalComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialStreamer_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/RandomMaterialComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialStreamer = { "MaterialStreamer", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URandomMaterialComponent, MaterialStreamer), Z_Construct_UScriptStruct_FRandomAssetStreamer, METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialStreamer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialStreamer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URandomMaterialComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialSelectionConfigData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_bUseAllMaterialInDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_AffectedComponentType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialDirectories,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerMeshComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_OwnerDecalComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URandomMaterialComponent_Statics::NewProp_MaterialStreamer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URandomMaterialComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URandomMaterialComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URandomMaterialComponent_Statics::ClassParams = {
		&URandomMaterialComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URandomMaterialComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_URandomMaterialComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URandomMaterialComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URandomMaterialComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URandomMaterialComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URandomMaterialComponent, 364022107);
	template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<URandomMaterialComponent>()
	{
		return URandomMaterialComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URandomMaterialComponent(Z_Construct_UClass_URandomMaterialComponent, &URandomMaterialComponent::StaticClass, TEXT("/Script/DomainRandomizationDNN"), TEXT("URandomMaterialComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URandomMaterialComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
