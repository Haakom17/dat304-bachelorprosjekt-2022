// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DOMAINRANDOMIZATIONDNN_SpatialLayoutGenerator_generated_h
#error "SpatialLayoutGenerator.generated.h already included, missing '#pragma once' in SpatialLayoutGenerator.h"
#endif
#define DOMAINRANDOMIZATIONDNN_SpatialLayoutGenerator_generated_h

#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_SPARSE_DATA
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_RPC_WRAPPERS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpatialLayoutGenerator(); \
	friend struct Z_Construct_UClass_USpatialLayoutGenerator_Statics; \
public: \
	DECLARE_CLASS(USpatialLayoutGenerator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(USpatialLayoutGenerator)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUSpatialLayoutGenerator(); \
	friend struct Z_Construct_UClass_USpatialLayoutGenerator_Statics; \
public: \
	DECLARE_CLASS(USpatialLayoutGenerator, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DomainRandomizationDNN"), NO_API) \
	DECLARE_SERIALIZER(USpatialLayoutGenerator)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpatialLayoutGenerator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpatialLayoutGenerator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialLayoutGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialLayoutGenerator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialLayoutGenerator(USpatialLayoutGenerator&&); \
	NO_API USpatialLayoutGenerator(const USpatialLayoutGenerator&); \
public:


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpatialLayoutGenerator(USpatialLayoutGenerator&&); \
	NO_API USpatialLayoutGenerator(const USpatialLayoutGenerator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpatialLayoutGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpatialLayoutGenerator); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpatialLayoutGenerator)


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_PRIVATE_PROPERTY_OFFSET
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_13_PROLOG
#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_RPC_WRAPPERS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_INCLASS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_PRIVATE_PROPERTY_OFFSET \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_SPARSE_DATA \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_INCLASS_NO_PURE_DECLS \
	Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DOMAINRANDOMIZATIONDNN_API UClass* StaticClass<class USpatialLayoutGenerator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Source_4_27_Plugins_DomainRandomizationDNN_Source_DomainRandomizationDNN_Public_SpatialLayoutGenerator_SpatialLayoutGenerator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
