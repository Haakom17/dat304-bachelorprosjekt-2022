Tilstede: Alle

Møtestart: 16:17

Siden sist:

- Andreas:
    - Learning unreal
    - 3h

- Håkon:
    - Plan for 3d space
    - 2,5h

- Amund:
    - Tensor flow
    - unreal cv
    - 3 - 3,5h

Neste møte: 16:15, tirsdag

Til neste gang:

- Andreas:
    - Assets in unreal store
    - 3-4h

- Håkon:
    - ferdig 3d space plan
    - 6h

- Amund:
    - object detection
    - 5h

Møtet hevet: 16:26: 9m
