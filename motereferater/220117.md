Tilstede: Alle

Møtestart: 17:14

Siden sist:

- Andreas:
    - Tutorials, ukentlig rapport for time tracking
    - 6h

- Håkon:
    - Jobbet med components & collision
    - components hierarchy
    - 3h 35m

- Amund:
    - Yolo dokumentasjon
    - tensor flow unreal engine
    - zox.com computer vision tutorial
    - player input and pawns
    - 3h 45m

Saker:
Jobbe hos Håkon: Onsdag, ca 12:00, Håkon sender melding

Prate med andre gruppe om oppgave?
 - Høre med veileder og redrock folk

Mest sansynlig møte med veileder på Onsdag

Neste møte: 17:15

Til neste gang:

- Andreas:
    - Camera controls i unreal
    - 5 - 6h

- Håkon:
    - Components and colision
    - Components hirarki
    - Se etter andre ressurser
    - 3h

- Amund:
    - Se videre på collision and components, player input
    - 3h

Møtet hevet: 17:25
