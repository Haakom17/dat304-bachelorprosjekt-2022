Tilstede: Alle

Møtestart: 1815

Siden sist:

- Andreas:
    - Tutorials
    - 4h

- Håkon:
    - 3h
    - Landscape tutorials

- Amund:
    - "Pakke arbeid sålangt"
    - 3h

Saker:
Amund eksamen mistanke
- Ser ut som vi mister Amund

Møte med veileder: 12:00

Jobbe hos Håkon: Etter møte med veileder

Neste møte: på slutten av arbeidsøkt

Til neste gang:

- Andreas:
    - 6h
    - camera

- Håkon:
    - landscape
    - 6h

- Amund:
    - Pakke sammen arbeid

Møtet hevet: 18:30
