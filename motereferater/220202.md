Tilstede: Alle

Møtestart:  16:14

Siden sist:

- Andreas:
    - ikke så mye mer enn det vi gjorde i går
    - 4h

- Håkon:
    - 3d env i går
    - 6h

- Amund:
    - yolo exempler
    - 4h

Tar veiledningsmøter etterbehov

Neste møte: 1515

Til neste gang:

- Andreas:
    - Camera stuff
    - 3 - 4h

- Håkon:
    - Notere resten på 3d plan, laste opp
    - neste 3d env
    - 4h

- Amund:
    - object detection
    - 3h

Møtet hevet: 16:23
