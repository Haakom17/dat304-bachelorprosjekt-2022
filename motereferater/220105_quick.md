tilstede: Alle
Møtestart: 17:16

Siden sist
- Andreas:
    -   Lest documentasjon på unreal
    -   installert og kjøret gjennom prog guide
    -   rundt 3h

- Amund:
    -   mellom 3h og 4h
    -   lastet ned visual studio
    -   gått gjennom 5 første punkter på quickstart
    -   legger inn spend

- Håkon:
    -   Installert unreal på laptop
    -   om vi trenger tilgang på source code så puller vi fra github
    -   lest gjennom prosjekt beskrivelse
    -   NDDS plugin
    -   gått gjennom bachelor template + internett tjenester
    -   litt under 4h

Neste møte:
14:15 


Til neste gang:
- Andreas:
    - Se mer på time track
    - Mål om ca 3h
- Amund:
    - Se på ndds
    - Mål om ca 3h
- Håkon:
    - Starte med unreal
    - Mål om ca 4h


Møtet hevet:
17:36
