# This is a sample Python script.
import argparse
import os
import json
import cv2
import re
import numpy as np
from pathlib import Path



def argumentparser():
    parser = argparse.ArgumentParser(description='Takes in directory')
    parser.add_argument('folder', help='source directory')
    parser.add_argument('target_folder', help='targetdirectory')
    return parser.parse_args().folder, parser.parse_args().target_folder

def folderconvertor(directory):
    directorytoscan = directory[0]
    filenames = os.scandir(directorytoscan)
    ConvertedBoundingBox = []
    regex = "[0-9]*.json"
    path = directory[1]
    isExist = os.path.exists(path)

    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(path)
    isExist = os.path.exists(f"{path}/labels")
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(f"{path}/labels")
    isExist = os.path.exists(f"{path}/images")
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(f"{path}/images")
    for filename in os.scandir(directory[0]):
        if re.match(regex, filename.name):
            try:
                f = open(filename.path)
                labelAsJson = json.load(f)
                pngfile = os.path.splitext(filename.path)[0] + ".png"
                img = cv2.imread(pngfile)
                for object in range(len(labelAsJson["objects"])):
                    (h, w) = img.shape[:2]
                    x =  labelAsJson["objects"][object]["bounding_box"]["top_left"]
                    y = labelAsJson["objects"][object]["bounding_box"]["bottom_right"]
                    c = "0"
                    if labelAsJson["objects"][object]["class"] == "Chair":
                        c = "0"
                    elif labelAsJson["objects"][object]["class"] == "Cup":
                        c = "1"
                    window_name = 'Image'
                    x.reverse()
                    y.reverse()

                    x = np.array(x)
                    y = np.array(y)

                    mid = x + (y-x)/2

                    midx= mid[0]/w
                    midy = mid[1] / h
                    widthOutofone = (y[0] - x[0])/w
                    heightOutofone = (y[1] - x[1])/h
                    # Start coordinate, here (0, 0)
                    # represents the top left corner of image
                    nameOfTargetFile = Path(filename.name).stem
                    pathToWrite = f"{path}/labels/{nameOfTargetFile}.txt"
                    targetFile = open(pathToWrite, "a")
                    targetFile.write(f"{c} {midx} {midy} {widthOutofone} {heightOutofone}\n")
                f.close
                targetFile.close
                newimagepath = f'{path}/images/{nameOfTargetFile}.png'
                cv2.imwrite(newimagepath, img)


                pass

            except:
                pass
    return

def convertFileconvertor(old_format):
    new_format= old_format
    return new_format

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    directory = argumentparser()
    folderconvertor(directory)
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
