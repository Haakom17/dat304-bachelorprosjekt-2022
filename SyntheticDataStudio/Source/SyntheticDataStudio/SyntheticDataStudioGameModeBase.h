// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SyntheticDataStudioGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SYNTHETICDATASTUDIO_API ASyntheticDataStudioGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
