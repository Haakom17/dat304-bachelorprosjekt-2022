// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "LabelCreator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SYNTHETICDATASTUDIO_API ULabelCreator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULabelCreator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	AActor* Owner;
	FVector2D ResultVector;
	int32 Number;
	TArray<FVector2D> TwoDVectors;
	FBoxSphereBounds BoundingBox;

	FVector extent;
	FVector origin;

	FVector originPlussX;
	FVector originPlussY;
	FVector originPlussZ;
	FVector originPlussXY;
	FVector originPlussXZ;
	FVector originPlussYZ;
	FVector originPlussXYZ;

	float Xmax;
	float Xmin;
	float Ymax;
	float Ymin;

	AHUD* hud;
};
