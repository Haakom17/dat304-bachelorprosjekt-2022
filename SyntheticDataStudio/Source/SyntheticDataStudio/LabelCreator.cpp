// Fill out your copyright notice in the Description page of Project Settings.

#include "Kismet/GameplayStatics.h"
#include "GameFramework/HUD.h"
#include "DebugHUD.h"
#include "LabelCreator.h"

// Sets default values for this component's properties
ULabelCreator::ULabelCreator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	Owner = GetOwner();
	

	// ...
}


// Called when the game starts
void ULabelCreator::BeginPlay()
{
	Super::BeginPlay();
	Owner = GetOwner();
	//Owner->GetComponents<UStaticMeshComponent>(Components);
	//for (int32 i = 0; i < Components.Num(); i++)
	//{
	//	UStaticMeshComponent* MeshToBox = Components[i];
	//	BoundingBox = MeshToBox->Bounds;
	//}
	Number = 0;

	hud = UGameplayStatics::GetPlayerController(this, 0)->GetHUD();

	// ...
	
}


// Called every frame
void ULabelCreator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	Owner->GetActorBounds(false, origin, extent);
	originPlussX = origin;
	originPlussY = origin;
	originPlussZ = origin;
	originPlussXY = origin;
	originPlussXZ = origin;
	originPlussYZ = origin;
	originPlussXYZ = origin;

	originPlussX.X += extent.X;
	originPlussY.Y += extent.Y;
	originPlussZ.Z += extent.Z;
	originPlussXY.X += extent.X;
	originPlussXY.Y += extent.Y;
	originPlussXZ.X += extent.X;
	originPlussXZ.Z += extent.Z;
	originPlussYZ.Y += extent.Y;
	originPlussYZ.Z += extent.Z;
	originPlussXYZ += extent;

	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), origin, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussX, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussY, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussZ, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussXY, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussXZ, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussYZ, ResultVector);
	TwoDVectors.Add(ResultVector);
	UGameplayStatics::ProjectWorldToScreen(UGameplayStatics::GetPlayerController(GetWorld(), 0), originPlussXYZ, ResultVector);
	TwoDVectors.Add(ResultVector);
	
	Xmax = TwoDVectors[0].X;
	Ymax = TwoDVectors[0].Y;
	Xmin = TwoDVectors[0].X;
	Ymin = TwoDVectors[0].Y;

	for (int32 i = 0; i < TwoDVectors.Num(); i++)
	{
		if (Xmax < TwoDVectors[i].X) {
			Xmax = TwoDVectors[i].X;
		}
		if (Ymax < TwoDVectors[i].Y) {
			Ymax = TwoDVectors[i].Y;
		}
		if (Xmin > TwoDVectors[i].X) {
			Xmin = TwoDVectors[i].X;
		}
		if (Ymin > TwoDVectors[i].Y) {
			Ymin = TwoDVectors[i].Y;
		}
	}


	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(Xmin));

	TwoDVectors.Empty();
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(Ymin));
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(Xmax));
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(Ymax));
	//hud->Draw2DLine(Xmin, Ymin, Xmax, Ymax, FColor::Yellow);
}

