// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraActor.h"
#include "OrbitingCameraActor.generated.h"

/**
 * 
 */
UCLASS()
class SYNTHETICDATASTUDIO_API AOrbitingCameraActor : public ACameraActor
{
	GENERATED_BODY()

public:
	AOrbitingCameraActor();

protected:
	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	void Orbit();

	UPROPERTY(EditAnywhere)
		float speed;

	UPROPERTY(EditAnywhere)
		float range;
	
};
