// Fill out your copyright notice in the Description page of Project Settings.


#include "OrbitingCameraActor.h"

AOrbitingCameraActor::AOrbitingCameraActor()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AOrbitingCameraActor::BeginPlay()
{
	Super::BeginPlay();

	speed = 1.5;
}

void AOrbitingCameraActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Orbit();
}

void AOrbitingCameraActor::Orbit()
{
	SetActorRelativeLocation(GetActorLocation() + (speed * GetActorRightVector() * range));
	SetActorRelativeRotation(GetActorRotation() + FRotator(0, -speed, 0));
}