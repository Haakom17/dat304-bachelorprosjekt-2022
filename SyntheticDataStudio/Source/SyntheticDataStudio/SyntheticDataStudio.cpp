// Copyright Epic Games, Inc. All Rights Reserved.

#include "SyntheticDataStudio.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SyntheticDataStudio, "SyntheticDataStudio" );
