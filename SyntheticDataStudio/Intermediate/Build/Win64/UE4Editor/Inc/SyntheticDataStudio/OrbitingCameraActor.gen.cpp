// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SyntheticDataStudio/OrbitingCameraActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOrbitingCameraActor() {}
// Cross Module References
	SYNTHETICDATASTUDIO_API UClass* Z_Construct_UClass_AOrbitingCameraActor_NoRegister();
	SYNTHETICDATASTUDIO_API UClass* Z_Construct_UClass_AOrbitingCameraActor();
	ENGINE_API UClass* Z_Construct_UClass_ACameraActor();
	UPackage* Z_Construct_UPackage__Script_SyntheticDataStudio();
// End Cross Module References
	void AOrbitingCameraActor::StaticRegisterNativesAOrbitingCameraActor()
	{
	}
	UClass* Z_Construct_UClass_AOrbitingCameraActor_NoRegister()
	{
		return AOrbitingCameraActor::StaticClass();
	}
	struct Z_Construct_UClass_AOrbitingCameraActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_speed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_speed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_range_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_range;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AOrbitingCameraActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACameraActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SyntheticDataStudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOrbitingCameraActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Input Rendering" },
		{ "IncludePath", "OrbitingCameraActor.h" },
		{ "ModuleRelativePath", "OrbitingCameraActor.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_speed_MetaData[] = {
		{ "Category", "OrbitingCameraActor" },
		{ "ModuleRelativePath", "OrbitingCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_speed = { "speed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AOrbitingCameraActor, speed), METADATA_PARAMS(Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_speed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_speed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_range_MetaData[] = {
		{ "Category", "OrbitingCameraActor" },
		{ "ModuleRelativePath", "OrbitingCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_range = { "range", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AOrbitingCameraActor, range), METADATA_PARAMS(Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_range_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_range_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AOrbitingCameraActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_speed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AOrbitingCameraActor_Statics::NewProp_range,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AOrbitingCameraActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AOrbitingCameraActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AOrbitingCameraActor_Statics::ClassParams = {
		&AOrbitingCameraActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AOrbitingCameraActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AOrbitingCameraActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AOrbitingCameraActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AOrbitingCameraActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AOrbitingCameraActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AOrbitingCameraActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AOrbitingCameraActor, 3834521398);
	template<> SYNTHETICDATASTUDIO_API UClass* StaticClass<AOrbitingCameraActor>()
	{
		return AOrbitingCameraActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AOrbitingCameraActor(Z_Construct_UClass_AOrbitingCameraActor, &AOrbitingCameraActor::StaticClass, TEXT("/Script/SyntheticDataStudio"), TEXT("AOrbitingCameraActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AOrbitingCameraActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
