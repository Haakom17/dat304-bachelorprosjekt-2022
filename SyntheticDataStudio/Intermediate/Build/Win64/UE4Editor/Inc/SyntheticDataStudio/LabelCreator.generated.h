// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SYNTHETICDATASTUDIO_LabelCreator_generated_h
#error "LabelCreator.generated.h already included, missing '#pragma once' in LabelCreator.h"
#endif
#define SYNTHETICDATASTUDIO_LabelCreator_generated_h

#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_SPARSE_DATA
#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_RPC_WRAPPERS
#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULabelCreator(); \
	friend struct Z_Construct_UClass_ULabelCreator_Statics; \
public: \
	DECLARE_CLASS(ULabelCreator, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SyntheticDataStudio"), NO_API) \
	DECLARE_SERIALIZER(ULabelCreator)


#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_INCLASS \
private: \
	static void StaticRegisterNativesULabelCreator(); \
	friend struct Z_Construct_UClass_ULabelCreator_Statics; \
public: \
	DECLARE_CLASS(ULabelCreator, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SyntheticDataStudio"), NO_API) \
	DECLARE_SERIALIZER(ULabelCreator)


#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULabelCreator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULabelCreator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULabelCreator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULabelCreator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULabelCreator(ULabelCreator&&); \
	NO_API ULabelCreator(const ULabelCreator&); \
public:


#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULabelCreator(ULabelCreator&&); \
	NO_API ULabelCreator(const ULabelCreator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULabelCreator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULabelCreator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULabelCreator)


#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_PRIVATE_PROPERTY_OFFSET
#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_10_PROLOG
#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_PRIVATE_PROPERTY_OFFSET \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_SPARSE_DATA \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_RPC_WRAPPERS \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_INCLASS \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_PRIVATE_PROPERTY_OFFSET \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_SPARSE_DATA \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_INCLASS_NO_PURE_DECLS \
	SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SYNTHETICDATASTUDIO_API UClass* StaticClass<class ULabelCreator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SyntheticDataStudio_Source_SyntheticDataStudio_LabelCreator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
