// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SYNTHETICDATASTUDIO_OrbitingCameraActor_generated_h
#error "OrbitingCameraActor.generated.h already included, missing '#pragma once' in OrbitingCameraActor.h"
#endif
#define SYNTHETICDATASTUDIO_OrbitingCameraActor_generated_h

#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_SPARSE_DATA
#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_RPC_WRAPPERS
#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAOrbitingCameraActor(); \
	friend struct Z_Construct_UClass_AOrbitingCameraActor_Statics; \
public: \
	DECLARE_CLASS(AOrbitingCameraActor, ACameraActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SyntheticDataStudio"), NO_API) \
	DECLARE_SERIALIZER(AOrbitingCameraActor)


#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAOrbitingCameraActor(); \
	friend struct Z_Construct_UClass_AOrbitingCameraActor_Statics; \
public: \
	DECLARE_CLASS(AOrbitingCameraActor, ACameraActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SyntheticDataStudio"), NO_API) \
	DECLARE_SERIALIZER(AOrbitingCameraActor)


#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AOrbitingCameraActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AOrbitingCameraActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOrbitingCameraActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOrbitingCameraActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOrbitingCameraActor(AOrbitingCameraActor&&); \
	NO_API AOrbitingCameraActor(const AOrbitingCameraActor&); \
public:


#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AOrbitingCameraActor(AOrbitingCameraActor&&); \
	NO_API AOrbitingCameraActor(const AOrbitingCameraActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AOrbitingCameraActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AOrbitingCameraActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AOrbitingCameraActor)


#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_PRIVATE_PROPERTY_OFFSET
#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_12_PROLOG
#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_PRIVATE_PROPERTY_OFFSET \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_SPARSE_DATA \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_RPC_WRAPPERS \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_INCLASS \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_PRIVATE_PROPERTY_OFFSET \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_SPARSE_DATA \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_INCLASS_NO_PURE_DECLS \
	SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SYNTHETICDATASTUDIO_API UClass* StaticClass<class AOrbitingCameraActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SyntheticDataStudio_Source_SyntheticDataStudio_OrbitingCameraActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
