// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SyntheticDataStudio/SyntheticDataStudioGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSyntheticDataStudioGameModeBase() {}
// Cross Module References
	SYNTHETICDATASTUDIO_API UClass* Z_Construct_UClass_ASyntheticDataStudioGameModeBase_NoRegister();
	SYNTHETICDATASTUDIO_API UClass* Z_Construct_UClass_ASyntheticDataStudioGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_SyntheticDataStudio();
// End Cross Module References
	void ASyntheticDataStudioGameModeBase::StaticRegisterNativesASyntheticDataStudioGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_ASyntheticDataStudioGameModeBase_NoRegister()
	{
		return ASyntheticDataStudioGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SyntheticDataStudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "SyntheticDataStudioGameModeBase.h" },
		{ "ModuleRelativePath", "SyntheticDataStudioGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASyntheticDataStudioGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::ClassParams = {
		&ASyntheticDataStudioGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASyntheticDataStudioGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASyntheticDataStudioGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASyntheticDataStudioGameModeBase, 53384259);
	template<> SYNTHETICDATASTUDIO_API UClass* StaticClass<ASyntheticDataStudioGameModeBase>()
	{
		return ASyntheticDataStudioGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASyntheticDataStudioGameModeBase(Z_Construct_UClass_ASyntheticDataStudioGameModeBase, &ASyntheticDataStudioGameModeBase::StaticClass, TEXT("/Script/SyntheticDataStudio"), TEXT("ASyntheticDataStudioGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASyntheticDataStudioGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
