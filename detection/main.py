# This is a sample Python script.

import yolov5
import cv2
import torch

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def runCamera():
    model = torch.hub.load('C:/Users/Andre/dat304bach/dat304-bachelorprosjekt-2022/yolov5/yolov5', 'custom', path='C:/Users/Andre/dat304bach/dat304-bachelorprosjekt-2022/yolov5/yolov5/runs/train/exp6/weights/best.pt', source='local')

    cam = cv2.VideoCapture(0)

    while True:
        check, frame = cam.read()
        results = model(frame)
        for box in results.xyxy[0]:
            if box[5] == 0:
                xB = int(box[2])
                xA = int(box[0])
                yB = int(box[3])
                yA = int(box[1])

                cv2.rectangle(frame, (xA, yA), (xB, yB), (0, 255, 0), 2)
        cv2.imshow('video', frame)

        key = cv2.waitKey(1)
        if key == 27:
            break

    cam.release()
    cv2.destroyAllWindows()


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    runCamera()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
