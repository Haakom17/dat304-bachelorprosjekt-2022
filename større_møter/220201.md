Tilstede: Alle

Møtestart: 15:07

Siden sist: 

- Andreas:
    - Ikke så mye
    - 0h - 1h

- Håkon:
    - Plan for 3d space
    - 5,5h

- Amund:
    - 3d assets
    - 1h.

Saker:

Status rapport:

Camera movement: 
-  Andreas tror skal klare å implementere så fort vi har 3d space på plass

Plan for 3d space:
- liste over ting vi trenger å se på
- skrevet ned relevant objekter og labels
- starte med superbasic
- Håkon tenker å lage ny level med mer i
- etterhvert: lighting shadows, etc
- Håkon scanner og pusher plan

Yolo/object detection
- Amund ser videre på og prøver å implementere object detection

Plan videre:
Andreas:
- Assets store + camera implementation

Håkon
- sette opp 3d space

Amund:
- yolo/object detection

Neste møte: 16:15

Til neste gang:

- Andreas:
    - asset store
    - 4h 

- Håkon:
    - Sette opp base 3d environment
    - laste opp plan
    - 6h

- Amund:
    - object detection
    - 6h

Møtet hevet: 15:51
