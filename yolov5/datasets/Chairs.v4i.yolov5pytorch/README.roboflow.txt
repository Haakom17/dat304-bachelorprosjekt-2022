
Chairs - v4 2022-05-05 5:29pm
==============================

This dataset was exported via roboflow.ai on May 5, 2022 at 3:30 PM GMT

It includes 96 images.
Chairs are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 512x512 (Stretch)

No image augmentation techniques were applied.


