
Chairs2 - v1 2022-05-01 4:27pm
==============================

This dataset was exported via roboflow.ai on May 1, 2022 at 2:27 PM GMT

It includes 198 images.
Chairs are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 512x512 (Stretch)

No image augmentation techniques were applied.


