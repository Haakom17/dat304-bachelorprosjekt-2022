
Chairs - v3 2022-04-22 7:53pm fulltrain
==============================

This dataset was exported via roboflow.ai on April 22, 2022 at 5:54 PM GMT

It includes 96 images.
Chairs are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 512x512 (Stretch)

No image augmentation techniques were applied.


