
Cups and Plates - v2 cupsandchairs9:15pm
==============================

This dataset was exported via roboflow.ai on May 21, 2022 at 7:16 PM GMT

It includes 220 images.
Plates-and-Cups are annotated in YOLO v5 PyTorch format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 512x512 (Stretch)

No image augmentation techniques were applied.


